export default class Bet {
	constructor() {
		this.price = 0;
		this.stake = 0;
		this.outcomeName = '';
		this.eventName = '';
		this.betId = null;
		this.payout = 0;
		this.receipt = '';
	}
}