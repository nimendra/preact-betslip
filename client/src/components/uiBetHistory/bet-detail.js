import { h, Component } from 'preact';
import Card from 'preact-material-components/Card';
import Button from 'preact-material-components/Button';

import 'preact-material-components/Card/style.css';
import 'preact-material-components/Button/style.css';

import Bet from '../../models/Bet.js';
import UIBet from '../../components/uiBet';
import styles from './style.css';

import fetchApi from '../../utils/fetchApi';
import { setTimeout } from 'timers';

export default class UIBetHistoryBetDetail extends Component {
	constructor() {
		super();
		this.state = {};
	}
	
	render(){
		return (<div>Bet Detail</div>);
	}
}