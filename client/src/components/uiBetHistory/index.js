import { h, Component } from 'preact';
import Card from 'preact-material-components/Card';
import Button from 'preact-material-components/Button';

import 'preact-material-components/Card/style.css';
import 'preact-material-components/Button/style.css';

import Bet from '../../models/Bet.js';
import UIBetHistoryBetDetail from './bet-detail.js';
import styles from './style.css';

import fetchApi from '../../utils/fetchApi';
import { setTimeout } from 'timers';

export default class UIBetHistory extends Component {
	constructor() {
		super();

		this.state = {
			bets: []
		};

	}
    
	render() {
		let self = this;
		return (
			<div>Bet History
				<UIBetHistoryBetDetail />

				<div class="c-bets-panel__content  js-stop-scroll-prop">
					<div class="js-panels  c-bets-panel__panels" for="bets-panel">
			
						<div class="js-panel" role="tabpanel" aria-hidden="false">
							<div class="c-bets-panel__bet-slip">
								<bet-slip />
							</div>
						</div>
			
						<div class="js-panel c-bets-panel__tabpane is-active" role="tabpanel" aria-hidden="true">
				
							<div class="c-bet-subheader">
					Today
							</div>

							<div class="mdc-layout-grid  mdc-layout-grid--flush">
								<div class="mdc-layout-grid__inner">
						
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Golden State">Golden State</div>
														<div>@</div>
														<div class="c-bet-card__odds">4.50</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Golden State v Cleveland">Golden State v Cleveland</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   ">Thu 1 April 24:00</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€5.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€22.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded mdc-ripple-upgraded--background-active-fill mdc-ripple-upgraded--foreground-activation" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:284.063px; --mdc-ripple-surface-height:35.9896px; --mdc-ripple-fg-size:170.438px; --mdc-ripple-fg-scale:1.73866; --mdc-ripple-fg-translate-start:78.7187px, -78.9063px; --mdc-ripple-fg-translate-end:56.8125px, -67.224px;"
												>
		Cashout €17.50
												</button>
											</section>
	
										</div>
									</div>
						
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Tottenham">Tottenham</div>
														<div>@</div>
														<div class="c-bet-card__odds">4.75</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Hull v Tottenham">Hull v Tottenham</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   ">24:50</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€10.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€47.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:284.063px; --mdc-ripple-surface-height:35.9896px; --mdc-ripple-fg-size:170.438px; --mdc-ripple-fg-scale:1.73866;"
												>
		Cashout €21.00
												</button>
											</section>
	
										</div>
									</div>
						
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Cristiano Ronaldo">Cristiano Ronaldo</div>
														<div>@</div>
														<div class="c-bet-card__odds">12.10</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="First Goal Scorer">First Goal Scorer</div>
				
														<div class="c-bet-card__event" title="Man Utd v Crystal Palace">Man Utd v Crystal Palace</div>
														<div class="c-bet-card__flex">
					
															<span class="c-bet-card__time   is-live  ">LIVE</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€15.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€181.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:284.063px; --mdc-ripple-surface-height:35.9896px; --mdc-ripple-fg-size:170.438px; --mdc-ripple-fg-scale:1.73866;"
												>
		Cashout €57.25
												</button>
											</section>
	
										</div>
									</div>
						
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="West Ham">West Ham</div>
														<div>@</div>
														<div class="c-bet-card__odds">2.45</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Burnley v West Ham">Burnley v West Ham</div>
														<div class="c-bet-card__flex">
					
															<span class="c-bet-card__time    is-countdown ">47m</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€20.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€49.00</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:284.063px; --mdc-ripple-surface-height:35.9896px; --mdc-ripple-fg-size:170.438px; --mdc-ripple-fg-scale:1.73866;"
												>
		Cashout €28.00
												</button>
											</section>
	
										</div>
									</div>
						
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Leicester">Leicester</div>
														<div>@</div>
														<div class="c-bet-card__odds">2.00</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Leicester v Bournemouth">Leicester v Bournemouth</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   is-live  ">LIVE</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€15.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€30.00</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:284.063px; --mdc-ripple-surface-height:35.9896px; --mdc-ripple-fg-size:170.438px; --mdc-ripple-fg-scale:1.73866;"
												>
		Cashout €11.65
												</button>
											</section>
	
										</div>
									</div>
						
						
								</div>
							</div>
				
							<div class="c-bet-subheader">
					Yesterday
							</div>

							<div class="mdc-layout-grid  mdc-layout-grid--flush">
								<div class="mdc-layout-grid__inner">
						
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Cristiano Ronaldo">Cristiano Ronaldo</div>
														<div>@</div>
														<div class="c-bet-card__odds">12.10</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="First Goal Scorer">First Goal Scorer</div>
				
														<div class="c-bet-card__event" title="Man Utd v Crystal Palace">Man Utd v Crystal Palace</div>
														<div class="c-bet-card__flex">
					
															<span class="c-bet-card__time   is-live  ">LIVE</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€15.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€181.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:284.063px; --mdc-ripple-surface-height:35.9896px; --mdc-ripple-fg-size:170.438px; --mdc-ripple-fg-scale:1.73866;"
												>
		Cashout €57.25
												</button>
											</section>
	
										</div>
									</div>
						
						
								</div>
							</div>
				
							<div class="c-bet-subheader">
					This Month
							</div>

							<div class="mdc-layout-grid  mdc-layout-grid--flush">
								<div class="mdc-layout-grid__inner">
						
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Tottenham">Tottenham</div>
														<div>@</div>
														<div class="c-bet-card__odds">4.75</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Hull v Tottenham">Hull v Tottenham</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   ">24:50</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€10.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€47.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:284.063px; --mdc-ripple-surface-height:35.9896px; --mdc-ripple-fg-size:170.438px; --mdc-ripple-fg-scale:1.73866;"
												>
		Cashout €21.00
												</button>
											</section>
	
										</div>
									</div>
						
						
								</div>
							</div>
				
							<div class="c-bet-subheader">
					January
							</div>

							<div class="mdc-layout-grid  mdc-layout-grid--flush">
								<div class="mdc-layout-grid__inner">
						
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Multi (Triple)">Multi (Triple)</div>
														<div>@</div>
														<div class="c-bet-card__odds">13.00</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__event" title="Man Utd @ 1.40, Virgil van Dijk @ 7.50">Man Utd @ 1.40, Virgil van Dijk @ 7.50</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   is-live  ">LIVE</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€5.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€155.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:284.063px; --mdc-ripple-surface-height:35.9896px; --mdc-ripple-fg-size:170.438px; --mdc-ripple-fg-scale:1.73866;"
												>
		Cashout €75.00
												</button>
											</section>
	
										</div>
									</div>
						
						
								</div>
							</div>
				
						</div>

						<div class="js-panel  c-bets-panel__tabpane" role="tabpanel" aria-hidden="true">
				
							<div class="c-bet-subheader">
					Today
							</div>
							<div class="mdc-layout-grid  mdc-layout-grid--flush">
								<div class="mdc-layout-grid__inner">
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Arsenal">Arsenal</div>
														<div>@</div>
														<div class="c-bet-card__odds">1.45</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Arsenal v Everton">Arsenal v Everton</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time    is-countdown ">58m</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€7.50</div>
															<div class="c-bet-card__bet-details-return-amount  ">€10.87</div>
														</div>
													</div>
												</div>
											</section>
	
										</div>
									</div>
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Golden State">Golden State</div>
														<div>@</div>
														<div class="c-bet-card__odds">4.50</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Golden State v Cleveland">Golden State v Cleveland</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   ">Thu 1 April 24:00</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€5.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€22.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:0px; --mdc-ripple-surface-height:0px; --mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;"
												>
		Cashout €17.50
												</button>
											</section>
	
										</div>
									</div>
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Chelsea">Chelsea</div>
														<div>@</div>
														<div class="c-bet-card__odds">2.00</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="First Half Winner">First Half Winner</div>
				
														<div class="c-bet-card__event" title="Sunderland v Chelsea">Sunderland v Chelsea</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   ">17:30</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€10.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€20.00</div>
														</div>
													</div>
												</div>
											</section>
	
										</div>
									</div>
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Tottenham">Tottenham</div>
														<div>@</div>
														<div class="c-bet-card__odds">4.75</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Hull v Tottenham">Hull v Tottenham</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   ">24:50</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€10.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€47.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:0px; --mdc-ripple-surface-height:0px; --mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;"
												>
		Cashout €21.00
												</button>
											</section>
	
										</div>
									</div>
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Cristiano Ronaldo">Cristiano Ronaldo</div>
														<div>@</div>
														<div class="c-bet-card__odds">12.10</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="First Goal Scorer">First Goal Scorer</div>
				
														<div class="c-bet-card__event" title="Man Utd v Crystal Palace">Man Utd v Crystal Palace</div>
														<div class="c-bet-card__flex">
					
															<span class="c-bet-card__time   is-live  ">LIVE</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€15.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€181.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:0px; --mdc-ripple-surface-height:0px; --mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;"
												>
		Cashout €57.25
												</button>
											</section>
	
										</div>
									</div>
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="West Ham">West Ham</div>
														<div>@</div>
														<div class="c-bet-card__odds">2.45</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Burnley v West Ham">Burnley v West Ham</div>
														<div class="c-bet-card__flex">
					
															<span class="c-bet-card__time    is-countdown ">47m</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€20.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€49.00</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:0px; --mdc-ripple-surface-height:0px; --mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;"
												>
		Cashout €28.00
												</button>
											</section>
	
										</div>
									</div>
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Leicester">Leicester</div>
														<div>@</div>
														<div class="c-bet-card__odds">2.00</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Leicester v Bournemouth">Leicester v Bournemouth</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   is-live  ">LIVE</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€15.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€30.00</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:0px; --mdc-ripple-surface-height:0px; --mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;"
												>
		Cashout €11.65
												</button>
											</section>
	
										</div>
									</div>
						
								</div>
							</div>
				
							<div class="c-bet-subheader">
					Yesterday
							</div>
							<div class="mdc-layout-grid  mdc-layout-grid--flush">
								<div class="mdc-layout-grid__inner">
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Cristiano Ronaldo">Cristiano Ronaldo</div>
														<div>@</div>
														<div class="c-bet-card__odds">12.10</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="First Goal Scorer">First Goal Scorer</div>
				
														<div class="c-bet-card__event" title="Man Utd v Crystal Palace">Man Utd v Crystal Palace</div>
														<div class="c-bet-card__flex">
					
															<span class="c-bet-card__time   is-live  ">LIVE</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€15.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€181.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:0px; --mdc-ripple-surface-height:0px; --mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;"
												>
		Cashout €57.25
												</button>
											</section>
	
										</div>
									</div>
						
								</div>
							</div>
				
							<div class="c-bet-subheader">
					This Month
							</div>
							<div class="mdc-layout-grid  mdc-layout-grid--flush">
								<div class="mdc-layout-grid__inner">
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Liverpool">Liverpool</div>
														<div>@</div>
														<div class="c-bet-card__odds">3.70</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Middlesbrough v Liverpool">Middlesbrough v Liverpool</div>
														<div class="c-bet-card__flex">
					
															<span class="c-bet-card__time   ">24:00</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€10.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€37.00</div>
														</div>
													</div>
												</div>
											</section>
	
										</div>
									</div>
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Tottenham">Tottenham</div>
														<div>@</div>
														<div class="c-bet-card__odds">4.75</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__market" title="Match Result">Match Result</div>
				
														<div class="c-bet-card__event" title="Hull v Tottenham">Hull v Tottenham</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   ">24:50</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€10.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€47.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:0px; --mdc-ripple-surface-height:0px; --mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;"
												>
		Cashout €21.00
												</button>
											</section>
	
										</div>
									</div>
						
								</div>
							</div>
				
							<div class="c-bet-subheader">
					January
							</div>
							<div class="mdc-layout-grid  mdc-layout-grid--flush">
								<div class="mdc-layout-grid__inner">
						
									<div class="mdc-layout-grid__cell  mdc-layout-grid__cell--span-12">
										<div class="c-bet-card  mdc-card    ">
											<section class="mdc-card__primary  c-bet-card__primary">
												<div class="c-bet-card__flex">
													<div class="c-bet-card__selection-odds">
														<div class="c-bet-card__selection" title="Multi (Triple)">Multi (Triple)</div>
														<div>@</div>
														<div class="c-bet-card__odds">13.00</div>
													</div>
			
												</div>
												<div class="c-bet-card__details-stake">
													<div class="c-bet-card__details">
				
														<div class="c-bet-card__event" title="Man Utd @ 1.40, Virgil van Dijk @ 7.50">Man Utd @ 1.40, Virgil van Dijk @ 7.50</div>
														<div class="c-bet-card__flex">
					
					
															<span class="c-bet-card__time   is-live  ">LIVE</span>
														</div>
													</div>
													<div class="c-bet-card__bet-details">
														<div class="c-bet-card__bet-details-labels">
															<div class="c-bet-card__bet-details-labels-stake">Stake:</div>
					
															<div class="c-bet-card__bet-details-labels-return">Return:</div>
					
														</div>
														<div class="c-bet-card__bet-details-amount">
															<div class="c-bet-card__bet-details-stake-amount">€5.00</div>
															<div class="c-bet-card__bet-details-return-amount  ">€155.50</div>
														</div>
													</div>
												</div>
											</section>
	
											<section class="mdc-card__actions">
												<button data-snackbar-message="Bet Cashed Out. Receipt #123-456" data-snackbar-action="View" data-snackbar-timeout="5000" data-snackbar-handler="document.location.href = '/my-account/';" class="mdc-button mdc-button--raised c-bet-card__cashout-btn js-open-snackbar js-cashout-button mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"
													style="--mdc-ripple-surface-width:0px; --mdc-ripple-surface-height:0px; --mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;"
												>
		Cashout €75.00
												</button>
											</section>
	
										</div>
									</div>
						
								</div>
							</div>
				
						</div>

					</div>
				</div>


			</div>);
        
	}
}