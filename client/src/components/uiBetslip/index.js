import { h, Component } from 'preact';
import Card from 'preact-material-components/Card';
import Button from 'preact-material-components/Button';
import Snackbar from 'preact-material-components/Snackbar';

import 'preact-material-components/Card/style.css';
import 'preact-material-components/Button/style.css';
import 'preact-material-components/Snackbar/style.css';

import Bet from '../../models/Bet.js';
import UIBet from '../../components/uiBet';
import styles from './style.css';

import fetchApi from '../../utils/fetchApi';
import { setTimeout } from 'timers';

export default class UIBetslip extends Component {
	constructor() {
		super();

		this.state = {
			bets: []
		};

		this.wireup();
		this.doSubmit = this.doSubmit.bind(this);
	}

	doSubmit () {
		const url = `//localhost:8080/`;
		let self = this;
		setTimeout(() => {
			let a = self.state.bets.slice();
			a.map(b => {
				b.receipt = 'ok';
			});
			//remove all successful bets
			self.setState({
				bets: []
			});
			self.bar.MDComponent.show({
	  message: `${a.length} bets placed.`
			});
			localStorage.setItem('bets', JSON.stringify(self.state.bets));
		},100);

		// fetchApi(url, { }, (data) => {
		// 	  //callback(data);
		// 	  let a = this.state.bets.slice();
		// 	  a.map(b => {
		// 			  b.receipt = 'ok';
		// 	  });
		// 	  //remove all successful bets
		// 	  this.setState({
		// 		  bets: []
		// 	  });
		// 	  this.bar.MDComponent.show({
		// 		message: `${a.length} bets placed.`
		// 	  });
		// 	  localStorage.setItem('bets', JSON.stringify(this.state.bets));
		//   });
	  }

	wireup(){
		this.totalStake = () => {
			let total = 0;
			if (!(this.state.bets && this.state.bets.length > 0))
				return total;
			if (this.state.bets.length > 1)
				total = this.state.bets.reduce((a, b) =>  a.stake + b.stake);
			else
				total = this.state.bets[0].stake;
			return total;
		};

		this.betCount = () => {
			let total = 0;
			if (this.state.bets && this.state.bets.length > 0)
				total = this.state.bets.length;
			return total;
		};

		this.totalPayout = () => {
			let total = 0;
			if (!(this.state.bets && this.state.bets.length > 0))
				return total;
			if (this.state.bets.length > 1)
				total = this.state.bets.reduce((a, b) =>  a.payout + b.payout);
			else
				total = this.state.bets[0].payout;
			return total;
		};

		this.totalTax = () => {
			let total = 0;
			return total;
		};
	}

	componentWillMount() {
		//check if Preact is in SSR or CSR
		if (typeof window !== 'undefined') {
			let storedBets = localStorage.getItem('bets');
			if (storedBets !== null)
				this.setState({
					bets: JSON.parse(storedBets)
				});
		}
	}

	componentDidMount(){
		window.addEventListener('add-bet', (e) => {
			let b = new Bet();
			b.eventName = e.detail.eventName;
			b.outcomeName = e.detail.outcomeName;
			b.price = e.detail.price;
			let existingBets = this.state.bets.slice();
			let maxId = existingBets.length ? Math.max.apply(null, existingBets.map(b => b.betId)) : 0;
			b.betId = maxId + 1;
			b.stake = null;
			existingBets.push(b);
			this.setState({ bets: existingBets });
			localStorage.setItem('bets', JSON.stringify(this.state.bets));
		});
	}

	onBetStakeChanged(newState) {
		let a = this.state.bets.slice();
		a.map(b => {
			if (b.betId === newState.betId) {
				b.stake = parseFloat(newState.stake);
				b.payout = b.price * b.stake;
			}
		});
		this.setState({
			bets: a
		});
		localStorage.setItem('bets', JSON.stringify(this.state.bets));
	}

	onBetDeleted(newState) {
		let a = this.state.bets.slice();
		a = a.filter(e => e.betId !== newState.betId);
		this.setState({
			bets: a
		});
		localStorage.setItem('bets', JSON.stringify(this.state.bets));
	}

	render() {
		let self = this;
		return (
			<div className={styles.betslip}>
				<Card className={styles.bottombar}>
					<Card.Primary>
						<Card.Title><h2>Betslip ({this.betCount()})</h2></Card.Title>
					</Card.Primary>
				</Card>
				{this.state.bets.map((d, idx) => (
					<UIBet
						key={idx}
						initialStake={d.stake}
						betId={d.betId}
						outcomeName={d.outcomeName}
						eventName={d.eventName}
						price={d.price}
						receipt={d.receipt}
						callbackOnStakeChange={newState => self.onBetStakeChanged(newState)}
						callbackOnDelete={newState => self.onBetDeleted(newState)}
						className={styles.cardcontent}
					/>
					
				))}
				<hr />
				
				<div class="c-bets-panel__bet-slip">
					
      
							<div class="multiple-card" elevation="1" animated-shadow="false" hidden="" animated="" aria-label="">
								<div class="card-content">
									<div class="multiple-card__primary">
										<div class="multiple-card__body">
											<div class="multiple-card__title">Double @ 10.50</div>
											<div class="multiple-card__combo">1X</div>
										</div>
										<div class="multiple-card__stake">
											<paper-input class="multiple-card__input" no-label-float="" placeholder="0.00" type="tel" tabindex="0" aria-disabled="false">
												<div slot="prefix" class="multiple-card__currency">€</div>
											</paper-input>
											<div class="multiple-card__caption-text  multiple-card__return">
												<div>Return:</div>
												<div class="multiple-card__return-value" />€0.00
											</div>
											<div class="multiple-card__caption-text" hidden="">Tax: €10.00</div>
										</div>
									</div>
									<div class="multiple-card__error" hidden="">Error message here</div>
								</div>
							</div>

							
				</div>


				<div className={styles.bottombar}>
					<div className={styles.bottombar__details}>
						<div className={styles.bottombar__labels}>
							<div>Stake: €{this.totalStake()}</div>
							<div>Tax: €{this.totalTax()}</div>
							<div>Return: €{this.totalPayout()}</div>
						</div>
					
					</div>
					<Button ripple raised onClick={() => this.doSubmit()}>
					Bet €{this.totalStake()}
					</Button>
				</div>
				<Snackbar ref={bar => {this.bar=bar;}} />
			</div>
		);
	}
}
