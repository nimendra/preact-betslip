import { h, render, Component } from 'preact';
import Card from 'preact-material-components/Card';
import 'preact-material-components/Card/style.css';
import Textfield from 'preact-material-components/Textfield';
import 'preact-material-components/Textfield/style.css';

export default class UIBet extends Component {
	constructor({ initialStake }) {
		super();

		this.onTextChanged = this.onTextChanged.bind(this);
		this.onDelete = this.onDelete.bind(this);
	}

	onTextChanged(event) {
		if (
			isNaN(event.target.value) ||
      !event.target.value ||
      event.target.value === ''
		) {
			this.props.callbackOnStakeChange({
				betId: this.props.betId,
				stake: 0
			});
			return;
		}
		this.props.callbackOnStakeChange({
			betId: this.props.betId,
			stake: event.target.value
		});
	}

	onDelete(event) {
		this.props.callbackOnDelete({
			betId: this.props.betId
		});
	}

	render({}, {}) {
		return (
			<Card>
				<Card.HorizontalBlock>
					<Card.Primary>
						<Card.Title large>{this.props.outcomeName}</Card.Title>
						<Card.Subtitle>
							{this.props.eventName} @{this.props.price}
						</Card.Subtitle>
						<Card.HorizontalBlock>
							{' '}
							<Textfield
								type="number"
								step="0.01"
								label="Enter Stake"
								value={this.props.initialStake}
								onChange={this.onTextChanged}
								onKeyUp={this.onTextChanged}
							/>
						</Card.HorizontalBlock>
					</Card.Primary>
				</Card.HorizontalBlock>
				<Card.Actions>
					<Card.Action onClick={this.onDelete}>REMOVE</Card.Action>
					<Card.Title><h2>{this.props.receipt}</h2></Card.Title>
				</Card.Actions>
			</Card>
		);
	}
}
