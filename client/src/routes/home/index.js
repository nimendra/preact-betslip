import { h, Component } from 'preact';
import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';

import UIBetslip from '../../components/uiBetslip';

export default class Home extends Component {
	sendBet(eventName, outcomeName,price) {
		let ev = new CustomEvent('add-bet', { detail: {
			eventName, outcomeName, price
		} });
		window.dispatchEvent(ev);
	}

	render() {
		return (
			<div>
				<div >
					<hr />
					<Button ripple raised onClick={() => this.sendBet('MU vs CH','MU',1.5)}>
            Man United - 1.5
					</Button>
					<hr />
					<Button ripple raised onClick={() => this.sendBet('MU vs CH','CH',3.0)}>
            Chelsea - 3.0
					</Button>
					<hr />
				</div>
				<div>
					<UIBetslip />
				</div>
			</div>
		);
	}
}
