module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "JkW7");
/******/ })
/************************************************************************/
/******/ ({

/***/ "51Py":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"cardcontent":"cardcontent__3V-CP","bottombar":"bottombar___kKXJ","bottombar__details":"bottombar__details__3-kLs","bottombar__labels":"bottombar__labels__2k0kD"};

/***/ }),

/***/ "IpTH":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "JkW7":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: ./style/index.css
var style = __webpack_require__("rq4c");
var style_default = /*#__PURE__*/__webpack_require__.n(style);

// EXTERNAL MODULE: ../node_modules/preact/dist/preact.min.js
var preact_min = __webpack_require__("KM04");
var preact_min_default = /*#__PURE__*/__webpack_require__.n(preact_min);

// CONCATENATED MODULE: ../node_modules/preact-router/dist/preact-router.es.js


var EMPTY$1 = {};

function preact_router_es_assign(obj, props) {
	// eslint-disable-next-line guard-for-in
	for (var i in props) {
		obj[i] = props[i];
	}
	return obj;
}

function exec(url, route, opts) {
	var reg = /(?:\?([^#]*))?(#.*)?$/,
	    c = url.match(reg),
	    matches = {},
	    ret;
	if (c && c[1]) {
		var p = c[1].split('&');
		for (var i = 0; i < p.length; i++) {
			var r = p[i].split('=');
			matches[decodeURIComponent(r[0])] = decodeURIComponent(r.slice(1).join('='));
		}
	}
	url = segmentize(url.replace(reg, ''));
	route = segmentize(route || '');
	var max = Math.max(url.length, route.length);
	for (var i$1 = 0; i$1 < max; i$1++) {
		if (route[i$1] && route[i$1].charAt(0) === ':') {
			var param = route[i$1].replace(/(^\:|[+*?]+$)/g, ''),
			    flags = (route[i$1].match(/[+*?]+$/) || EMPTY$1)[0] || '',
			    plus = ~flags.indexOf('+'),
			    star = ~flags.indexOf('*'),
			    val = url[i$1] || '';
			if (!val && !star && (flags.indexOf('?') < 0 || plus)) {
				ret = false;
				break;
			}
			matches[param] = decodeURIComponent(val);
			if (plus || star) {
				matches[param] = url.slice(i$1).map(decodeURIComponent).join('/');
				break;
			}
		} else if (route[i$1] !== url[i$1]) {
			ret = false;
			break;
		}
	}
	if (opts.default !== true && ret === false) {
		return false;
	}
	return matches;
}

function pathRankSort(a, b) {
	return a.rank < b.rank ? 1 : a.rank > b.rank ? -1 : a.index - b.index;
}

// filter out VNodes without attributes (which are unrankeable), and add `index`/`rank` properties to be used in sorting.
function prepareVNodeForRanking(vnode, index) {
	vnode.index = index;
	vnode.rank = rankChild(vnode);
	return vnode.attributes;
}

function segmentize(url) {
	return url.replace(/(^\/+|\/+$)/g, '').split('/');
}

function rankSegment(segment) {
	return segment.charAt(0) == ':' ? 1 + '*+?'.indexOf(segment.charAt(segment.length - 1)) || 4 : 5;
}

function rank(path) {
	return segmentize(path).map(rankSegment).join('');
}

function rankChild(vnode) {
	return vnode.attributes.default ? 0 : rank(vnode.attributes.path);
}

var customHistory = null;

var ROUTERS = [];

var subscribers = [];

var EMPTY = {};

function isPreactElement(node) {
	return node.__preactattr_ != null || typeof Symbol !== 'undefined' && node[Symbol.for('preactattr')] != null;
}

function setUrl(url, type) {
	if (type === void 0) type = 'push';

	if (customHistory && customHistory[type]) {
		customHistory[type](url);
	} else if (typeof history !== 'undefined' && history[type + 'State']) {
		history[type + 'State'](null, null, url);
	}
}

function getCurrentUrl() {
	var url;
	if (customHistory && customHistory.location) {
		url = customHistory.location;
	} else if (customHistory && customHistory.getCurrentLocation) {
		url = customHistory.getCurrentLocation();
	} else {
		url = typeof location !== 'undefined' ? location : EMPTY;
	}
	return "" + (url.pathname || '') + (url.search || '');
}

function route(url, replace) {
	if (replace === void 0) replace = false;

	if (typeof url !== 'string' && url.url) {
		replace = url.replace;
		url = url.url;
	}

	// only push URL into history if we can handle it
	if (canRoute(url)) {
		setUrl(url, replace ? 'replace' : 'push');
	}

	return routeTo(url);
}

/** Check if the given URL can be handled by any router instances. */
function canRoute(url) {
	for (var i = ROUTERS.length; i--;) {
		if (ROUTERS[i].canRoute(url)) {
			return true;
		}
	}
	return false;
}

/** Tell all router instances to handle the given URL.  */
function routeTo(url) {
	var didRoute = false;
	for (var i = 0; i < ROUTERS.length; i++) {
		if (ROUTERS[i].routeTo(url) === true) {
			didRoute = true;
		}
	}
	for (var i$1 = subscribers.length; i$1--;) {
		subscribers[i$1](url);
	}
	return didRoute;
}

function routeFromLink(node) {
	// only valid elements
	if (!node || !node.getAttribute) {
		return;
	}

	var href = node.getAttribute('href'),
	    target = node.getAttribute('target');

	// ignore links with targets and non-path URLs
	if (!href || !href.match(/^\//g) || target && !target.match(/^_?self$/i)) {
		return;
	}

	// attempt to route, if no match simply cede control to browser
	return route(href);
}

function handleLinkClick(e) {
	if (e.button == 0) {
		routeFromLink(e.currentTarget || e.target || this);
		return prevent(e);
	}
}

function prevent(e) {
	if (e) {
		if (e.stopImmediatePropagation) {
			e.stopImmediatePropagation();
		}
		if (e.stopPropagation) {
			e.stopPropagation();
		}
		e.preventDefault();
	}
	return false;
}

function delegateLinkHandler(e) {
	// ignore events the browser takes care of already:
	if (e.ctrlKey || e.metaKey || e.altKey || e.shiftKey || e.button !== 0) {
		return;
	}

	var t = e.target;
	do {
		if (String(t.nodeName).toUpperCase() === 'A' && t.getAttribute('href') && isPreactElement(t)) {
			if (t.hasAttribute('native')) {
				return;
			}
			// if link is handled by the router, prevent browser defaults
			if (routeFromLink(t)) {
				return prevent(e);
			}
		}
	} while (t = t.parentNode);
}

var eventListenersInitialized = false;

function initEventListeners() {
	if (eventListenersInitialized) {
		return;
	}

	if (typeof addEventListener === 'function') {
		if (!customHistory) {
			addEventListener('popstate', function () {
				routeTo(getCurrentUrl());
			});
		}
		addEventListener('click', delegateLinkHandler);
	}
	eventListenersInitialized = true;
}

var preact_router_es_Router = function (Component$$1) {
	function Router(props) {
		Component$$1.call(this, props);
		if (props.history) {
			customHistory = props.history;
		}

		this.state = {
			url: props.url || getCurrentUrl()
		};

		initEventListeners();
	}

	if (Component$$1) Router.__proto__ = Component$$1;
	Router.prototype = Object.create(Component$$1 && Component$$1.prototype);
	Router.prototype.constructor = Router;

	Router.prototype.shouldComponentUpdate = function shouldComponentUpdate(props) {
		if (props.static !== true) {
			return true;
		}
		return props.url !== this.props.url || props.onChange !== this.props.onChange;
	};

	/** Check if the given URL can be matched against any children */
	Router.prototype.canRoute = function canRoute(url) {
		return this.getMatchingChildren(this.props.children, url, false).length > 0;
	};

	/** Re-render children with a new URL to match against. */
	Router.prototype.routeTo = function routeTo(url) {
		this._didRoute = false;
		this.setState({ url: url });

		// if we're in the middle of an update, don't synchronously re-route.
		if (this.updating) {
			return this.canRoute(url);
		}

		this.forceUpdate();
		return this._didRoute;
	};

	Router.prototype.componentWillMount = function componentWillMount() {
		ROUTERS.push(this);
		this.updating = true;
	};

	Router.prototype.componentDidMount = function componentDidMount() {
		var this$1 = this;

		if (customHistory) {
			this.unlisten = customHistory.listen(function (location) {
				this$1.routeTo("" + (location.pathname || '') + (location.search || ''));
			});
		}
		this.updating = false;
	};

	Router.prototype.componentWillUnmount = function componentWillUnmount() {
		if (typeof this.unlisten === 'function') {
			this.unlisten();
		}
		ROUTERS.splice(ROUTERS.indexOf(this), 1);
	};

	Router.prototype.componentWillUpdate = function componentWillUpdate() {
		this.updating = true;
	};

	Router.prototype.componentDidUpdate = function componentDidUpdate() {
		this.updating = false;
	};

	Router.prototype.getMatchingChildren = function getMatchingChildren(children, url, invoke) {
		return children.filter(prepareVNodeForRanking).sort(pathRankSort).map(function (vnode) {
			var matches = exec(url, vnode.attributes.path, vnode.attributes);
			if (matches) {
				if (invoke !== false) {
					var newProps = { url: url, matches: matches };
					preact_router_es_assign(newProps, matches);
					delete newProps.ref;
					delete newProps.key;
					return Object(preact_min["cloneElement"])(vnode, newProps);
				}
				return vnode;
			}
		}).filter(Boolean);
	};

	Router.prototype.render = function render(ref, ref$1) {
		var children = ref.children;
		var onChange = ref.onChange;
		var url = ref$1.url;

		var active = this.getMatchingChildren(children, url, true);

		var current = active[0] || null;
		this._didRoute = !!current;

		var previous = this.previousUrl;
		if (url !== previous) {
			this.previousUrl = url;
			if (typeof onChange === 'function') {
				onChange({
					router: this,
					url: url,
					previous: previous,
					active: active,
					current: current
				});
			}
		}

		return current;
	};

	return Router;
}(preact_min["Component"]);

var preact_router_es_Link = function Link(props) {
	return Object(preact_min["h"])('a', preact_router_es_assign({ onClick: handleLinkClick }, props));
};

var preact_router_es_Route = function Route(props) {
	return Object(preact_min["h"])(props.component, props);
};

preact_router_es_Router.subscribers = subscribers;
preact_router_es_Router.getCurrentUrl = getCurrentUrl;
preact_router_es_Router.route = route;
preact_router_es_Router.Router = preact_router_es_Router;
preact_router_es_Router.Route = preact_router_es_Route;
preact_router_es_Router.Link = preact_router_es_Link;

/* harmony default export */ var preact_router_es = (preact_router_es_Router);
//# sourceMappingURL=preact-router.es.js.map
// CONCATENATED MODULE: ../node_modules/@material/base/foundation.js
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @license
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @template A
 */
var MDCFoundation = function () {
  _createClass(MDCFoundation, null, [{
    key: "cssClasses",

    /** @return enum{cssClasses} */
    get: function get() {
      // Classes extending MDCFoundation should implement this method to return an object which exports every
      // CSS class the foundation class needs as a property. e.g. {ACTIVE: 'mdc-component--active'}
      return {};
    }

    /** @return enum{strings} */

  }, {
    key: "strings",
    get: function get() {
      // Classes extending MDCFoundation should implement this method to return an object which exports all
      // semantic strings as constants. e.g. {ARIA_ROLE: 'tablist'}
      return {};
    }

    /** @return enum{numbers} */

  }, {
    key: "numbers",
    get: function get() {
      // Classes extending MDCFoundation should implement this method to return an object which exports all
      // of its semantic numbers as constants. e.g. {ANIMATION_DELAY_MS: 350}
      return {};
    }

    /** @return {!Object} */

  }, {
    key: "defaultAdapter",
    get: function get() {
      // Classes extending MDCFoundation may choose to implement this getter in order to provide a convenient
      // way of viewing the necessary methods of an adapter. In the future, this could also be used for adapter
      // validation.
      return {};
    }

    /**
     * @param {A=} adapter
     */

  }]);

  function MDCFoundation() {
    var adapter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, MDCFoundation);

    /** @protected {!A} */
    this.adapter_ = adapter;
  }

  MDCFoundation.prototype.init = function init() {
    // Subclasses should override this method to perform initialization routines (registering events, etc.)
  };

  MDCFoundation.prototype.destroy = function destroy() {
    // Subclasses should override this method to perform de-initialization routines (de-registering events, etc.)
  };

  return MDCFoundation;
}();

/* harmony default export */ var foundation = (MDCFoundation);
// CONCATENATED MODULE: ../node_modules/@material/base/component.js
function component__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @license
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



/**
 * @template F
 */

var component_MDCComponent = function () {
  /**
   * @param {!Element} root
   * @return {!MDCComponent}
   */
  MDCComponent.attachTo = function attachTo(root) {
    // Subclasses which extend MDCBase should provide an attachTo() method that takes a root element and
    // returns an instantiated component with its root set to that element. Also note that in the cases of
    // subclasses, an explicit foundation class will not have to be passed in; it will simply be initialized
    // from getDefaultFoundation().
    return new MDCComponent(root, new foundation());
  };

  /**
   * @param {!Element} root
   * @param {F=} foundation
   * @param {...?} args
   */


  function MDCComponent(root) {
    var foundation = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

    component__classCallCheck(this, MDCComponent);

    /** @protected {!Element} */
    this.root_ = root;

    for (var _len = arguments.length, args = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
      args[_key - 2] = arguments[_key];
    }

    this.initialize.apply(this, args);
    // Note that we initialize foundation here and not within the constructor's default param so that
    // this.root_ is defined and can be used within the foundation class.
    /** @protected {!F} */
    this.foundation_ = foundation === undefined ? this.getDefaultFoundation() : foundation;
    this.foundation_.init();
    this.initialSyncWithDOM();
  }

  MDCComponent.prototype.initialize = function initialize() /* ...args */{}
  // Subclasses can override this to do any additional setup work that would be considered part of a
  // "constructor". Essentially, it is a hook into the parent constructor before the foundation is
  // initialized. Any additional arguments besides root and foundation will be passed in here.


  /**
   * @return {!F} foundation
   */
  ;

  MDCComponent.prototype.getDefaultFoundation = function getDefaultFoundation() {
    // Subclasses must override this method to return a properly configured foundation class for the
    // component.
    throw new Error('Subclasses must override getDefaultFoundation to return a properly configured ' + 'foundation class');
  };

  MDCComponent.prototype.initialSyncWithDOM = function initialSyncWithDOM() {
    // Subclasses should override this method if they need to perform work to synchronize with a host DOM
    // object. An example of this would be a form control wrapper that needs to synchronize its internal state
    // to some property or attribute of the host DOM. Please note: this is *not* the place to perform DOM
    // reads/writes that would cause layout / paint, as this is called synchronously from within the constructor.
  };

  MDCComponent.prototype.destroy = function destroy() {
    // Subclasses may implement this method to release any resources / deregister any listeners they have
    // attached. An example of this might be deregistering a resize event from the window object.
    this.foundation_.destroy();
  };

  /**
   * Wrapper method to add an event listener to the component's root element. This is most useful when
   * listening for custom events.
   * @param {string} evtType
   * @param {!Function} handler
   */


  MDCComponent.prototype.listen = function listen(evtType, handler) {
    this.root_.addEventListener(evtType, handler);
  };

  /**
   * Wrapper method to remove an event listener to the component's root element. This is most useful when
   * unlistening for custom events.
   * @param {string} evtType
   * @param {!Function} handler
   */


  MDCComponent.prototype.unlisten = function unlisten(evtType, handler) {
    this.root_.removeEventListener(evtType, handler);
  };

  /**
   * Fires a cross-browser-compatible custom event from the component root of the given type,
   * with the given data.
   * @param {string} evtType
   * @param {!Object} evtData
   * @param {boolean=} shouldBubble
   */


  MDCComponent.prototype.emit = function emit(evtType, evtData) {
    var shouldBubble = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    var evt = void 0;
    if (typeof CustomEvent === 'function') {
      evt = new CustomEvent(evtType, {
        detail: evtData,
        bubbles: shouldBubble
      });
    } else {
      evt = document.createEvent('CustomEvent');
      evt.initCustomEvent(evtType, shouldBubble, false, evtData);
    }

    this.root_.dispatchEvent(evt);
  };

  return MDCComponent;
}();

/* harmony default export */ var component = (component_MDCComponent);
// CONCATENATED MODULE: ../node_modules/@material/ripple/adapter.js
function adapter__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @license
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint no-unused-vars: [2, {"args": "none"}] */

/**
 * Adapter for MDC Ripple. Provides an interface for managing
 * - classes
 * - dom
 * - CSS variables
 * - position
 * - dimensions
 * - scroll position
 * - event handlers
 * - unbounded, active and disabled states
 *
 * Additionally, provides type information for the adapter to the Closure
 * compiler.
 *
 * Implement this adapter for your framework of choice to delegate updates to
 * the component in your framework of choice. See architecture documentation
 * for more details.
 * https://github.com/material-components/material-components-web/blob/master/docs/architecture.md
 *
 * @record
 */
var MDCRippleAdapter = function () {
  function MDCRippleAdapter() {
    adapter__classCallCheck(this, MDCRippleAdapter);
  }

  /** @return {boolean} */
  MDCRippleAdapter.prototype.browserSupportsCssVars = function browserSupportsCssVars() {};

  /** @return {boolean} */


  MDCRippleAdapter.prototype.isUnbounded = function isUnbounded() {};

  /** @return {boolean} */


  MDCRippleAdapter.prototype.isSurfaceActive = function isSurfaceActive() {};

  /** @return {boolean} */


  MDCRippleAdapter.prototype.isSurfaceDisabled = function isSurfaceDisabled() {};

  /** @param {string} className */


  MDCRippleAdapter.prototype.addClass = function addClass(className) {};

  /** @param {string} className */


  MDCRippleAdapter.prototype.removeClass = function removeClass(className) {};

  /**
   * @param {string} evtType
   * @param {!Function} handler
   */


  MDCRippleAdapter.prototype.registerInteractionHandler = function registerInteractionHandler(evtType, handler) {};

  /**
   * @param {string} evtType
   * @param {!Function} handler
   */


  MDCRippleAdapter.prototype.deregisterInteractionHandler = function deregisterInteractionHandler(evtType, handler) {};

  /**
   * @param {!Function} handler
   */


  MDCRippleAdapter.prototype.registerResizeHandler = function registerResizeHandler(handler) {};

  /**
   * @param {!Function} handler
   */


  MDCRippleAdapter.prototype.deregisterResizeHandler = function deregisterResizeHandler(handler) {};

  /**
   * @param {string} varName
   * @param {?number|string} value
   */


  MDCRippleAdapter.prototype.updateCssVariable = function updateCssVariable(varName, value) {};

  /** @return {!ClientRect} */


  MDCRippleAdapter.prototype.computeBoundingRect = function computeBoundingRect() {};

  /** @return {{x: number, y: number}} */


  MDCRippleAdapter.prototype.getWindowPageOffset = function getWindowPageOffset() {};

  return MDCRippleAdapter;
}();

/* harmony default export */ var ripple_adapter = (MDCRippleAdapter);
// CONCATENATED MODULE: ../node_modules/@material/ripple/constants.js
/**
 * @license
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var cssClasses = {
  // Ripple is a special case where the "root" component is really a "mixin" of sorts,
  // given that it's an 'upgrade' to an existing component. That being said it is the root
  // CSS class that all other CSS classes derive from.
  ROOT: 'mdc-ripple-upgraded',
  UNBOUNDED: 'mdc-ripple-upgraded--unbounded',
  BG_FOCUSED: 'mdc-ripple-upgraded--background-focused',
  BG_ACTIVE_FILL: 'mdc-ripple-upgraded--background-active-fill',
  FG_ACTIVATION: 'mdc-ripple-upgraded--foreground-activation',
  FG_DEACTIVATION: 'mdc-ripple-upgraded--foreground-deactivation'
};

var strings = {
  VAR_SURFACE_WIDTH: '--mdc-ripple-surface-width',
  VAR_SURFACE_HEIGHT: '--mdc-ripple-surface-height',
  VAR_FG_SIZE: '--mdc-ripple-fg-size',
  VAR_LEFT: '--mdc-ripple-left',
  VAR_TOP: '--mdc-ripple-top',
  VAR_FG_SCALE: '--mdc-ripple-fg-scale',
  VAR_FG_TRANSLATE_START: '--mdc-ripple-fg-translate-start',
  VAR_FG_TRANSLATE_END: '--mdc-ripple-fg-translate-end'
};

var numbers = {
  PADDING: 10,
  INITIAL_ORIGIN_SCALE: 0.6,
  DEACTIVATION_TIMEOUT_MS: 300,
  FG_DEACTIVATION_MS: 83
};


// CONCATENATED MODULE: ../node_modules/@material/ripple/util.js
/**
 * @license
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Stores result from supportsCssVariables to avoid redundant processing to detect CSS custom variable support.
 * @private {boolean|undefined}
 */
var supportsCssVariables_ = void 0;

/**
 * Stores result from applyPassive to avoid redundant processing to detect passive event listener support.
 * @private {boolean|undefined}
 */
var supportsPassive_ = void 0;

/**
 * @param {!Window} windowObj
 * @return {boolean}
 */
function detectEdgePseudoVarBug(windowObj) {
  // Detect versions of Edge with buggy var() support
  // See: https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/11495448/
  var document = windowObj.document;
  var node = document.createElement('div');
  node.className = 'mdc-ripple-surface--test-edge-var-bug';
  document.body.appendChild(node);

  // The bug exists if ::before style ends up propagating to the parent element.
  // Additionally, getComputedStyle returns null in iframes with display: "none" in Firefox,
  // but Firefox is known to support CSS custom properties correctly.
  // See: https://bugzilla.mozilla.org/show_bug.cgi?id=548397
  var computedStyle = windowObj.getComputedStyle(node);
  var hasPseudoVarBug = computedStyle !== null && computedStyle.borderTopStyle === 'solid';
  node.remove();
  return hasPseudoVarBug;
}

/**
 * @param {!Window} windowObj
 * @param {boolean=} forceRefresh
 * @return {boolean|undefined}
 */

function supportsCssVariables(windowObj) {
  var forceRefresh = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (typeof supportsCssVariables_ === 'boolean' && !forceRefresh) {
    return supportsCssVariables_;
  }

  var supportsFunctionPresent = windowObj.CSS && typeof windowObj.CSS.supports === 'function';
  if (!supportsFunctionPresent) {
    return;
  }

  var explicitlySupportsCssVars = windowObj.CSS.supports('--css-vars', 'yes');
  // See: https://bugs.webkit.org/show_bug.cgi?id=154669
  // See: README section on Safari
  var weAreFeatureDetectingSafari10plus = windowObj.CSS.supports('(--css-vars: yes)') && windowObj.CSS.supports('color', '#00000000');

  if (explicitlySupportsCssVars || weAreFeatureDetectingSafari10plus) {
    supportsCssVariables_ = !detectEdgePseudoVarBug(windowObj);
  } else {
    supportsCssVariables_ = false;
  }
  return supportsCssVariables_;
}

//
/**
 * Determine whether the current browser supports passive event listeners, and if so, use them.
 * @param {!Window=} globalObj
 * @param {boolean=} forceRefresh
 * @return {boolean|{passive: boolean}}
 */
function applyPassive() {
  var globalObj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;
  var forceRefresh = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (supportsPassive_ === undefined || forceRefresh) {
    var isSupported = false;
    try {
      globalObj.document.addEventListener('test', null, { get passive() {
          isSupported = true;
        } });
    } catch (e) {}

    supportsPassive_ = isSupported;
  }

  return supportsPassive_ ? { passive: true } : false;
}

/**
 * @param {!Object} HTMLElementPrototype
 * @return {!Array<string>}
 */
function getMatchesProperty(HTMLElementPrototype) {
  return ['webkitMatchesSelector', 'msMatchesSelector', 'matches'].filter(function (p) {
    return p in HTMLElementPrototype;
  }).pop();
}

/**
 * @param {!Event} ev
 * @param {!{x: number, y: number}} pageOffset
 * @param {!ClientRect} clientRect
 * @return {!{x: number, y: number}}
 */
function getNormalizedEventCoords(ev, pageOffset, clientRect) {
  var x = pageOffset.x,
      y = pageOffset.y;

  var documentX = x + clientRect.left;
  var documentY = y + clientRect.top;

  var normalizedX = void 0;
  var normalizedY = void 0;
  // Determine touch point relative to the ripple container.
  if (ev.type === 'touchstart') {
    normalizedX = ev.changedTouches[0].pageX - documentX;
    normalizedY = ev.changedTouches[0].pageY - documentY;
  } else {
    normalizedX = ev.pageX - documentX;
    normalizedY = ev.pageY - documentY;
  }

  return { x: normalizedX, y: normalizedY };
}


// CONCATENATED MODULE: ../node_modules/@material/ripple/foundation.js
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var foundation__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function foundation__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * @license
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */






/**
 * @typedef {!{
 *   isActivated: (boolean|undefined),
 *   hasDeactivationUXRun: (boolean|undefined),
 *   wasActivatedByPointer: (boolean|undefined),
 *   wasElementMadeActive: (boolean|undefined),
 *   activationStartTime: (number|undefined),
 *   activationEvent: Event,
 *   isProgrammatic: (boolean|undefined)
 * }}
 */
var ActivationStateType = void 0;

/**
 * @typedef {!{
 *   activate: (string|undefined),
 *   deactivate: (string|undefined),
 *   focus: (string|undefined),
 *   blur: (string|undefined)
 * }}
 */
var ListenerInfoType = void 0;

/**
 * @typedef {!{
 *   activate: function(!Event),
 *   deactivate: function(!Event),
 *   focus: function(),
 *   blur: function()
 * }}
 */
var ListenersType = void 0;

/**
 * @typedef {!{
 *   x: number,
 *   y: number
 * }}
 */
var PointType = void 0;

/**
 * @enum {string}
 */
var DEACTIVATION_ACTIVATION_PAIRS = {
  mouseup: 'mousedown',
  pointerup: 'pointerdown',
  touchend: 'touchstart',
  keyup: 'keydown',
  blur: 'focus'
};

/**
 * @extends {MDCFoundation<!MDCRippleAdapter>}
 */

var foundation_MDCRippleFoundation = function (_MDCFoundation) {
  _inherits(MDCRippleFoundation, _MDCFoundation);

  foundation__createClass(MDCRippleFoundation, null, [{
    key: 'cssClasses',
    get: function get() {
      return cssClasses;
    }
  }, {
    key: 'strings',
    get: function get() {
      return strings;
    }
  }, {
    key: 'numbers',
    get: function get() {
      return numbers;
    }
  }, {
    key: 'defaultAdapter',
    get: function get() {
      return {
        browserSupportsCssVars: function browserSupportsCssVars() /* boolean - cached */{},
        isUnbounded: function isUnbounded() /* boolean */{},
        isSurfaceActive: function isSurfaceActive() /* boolean */{},
        isSurfaceDisabled: function isSurfaceDisabled() /* boolean */{},
        addClass: function addClass() /* className: string */{},
        removeClass: function removeClass() /* className: string */{},
        registerInteractionHandler: function registerInteractionHandler() /* evtType: string, handler: EventListener */{},
        deregisterInteractionHandler: function deregisterInteractionHandler() /* evtType: string, handler: EventListener */{},
        registerResizeHandler: function registerResizeHandler() /* handler: EventListener */{},
        deregisterResizeHandler: function deregisterResizeHandler() /* handler: EventListener */{},
        updateCssVariable: function updateCssVariable() /* varName: string, value: string */{},
        computeBoundingRect: function computeBoundingRect() /* ClientRect */{},
        getWindowPageOffset: function getWindowPageOffset() /* {x: number, y: number} */{}
      };
    }
  }]);

  function MDCRippleFoundation(adapter) {
    foundation__classCallCheck(this, MDCRippleFoundation);

    /** @private {number} */
    var _this = _possibleConstructorReturn(this, _MDCFoundation.call(this, _extends(MDCRippleFoundation.defaultAdapter, adapter)));

    _this.layoutFrame_ = 0;

    /** @private {!ClientRect} */
    _this.frame_ = /** @type {!ClientRect} */{ width: 0, height: 0 };

    /** @private {!ActivationStateType} */
    _this.activationState_ = _this.defaultActivationState_();

    /** @private {number} */
    _this.xfDuration_ = 0;

    /** @private {number} */
    _this.initialSize_ = 0;

    /** @private {number} */
    _this.maxRadius_ = 0;

    /** @private {!Array<{ListenerInfoType}>} */
    _this.listenerInfos_ = [{ activate: 'touchstart', deactivate: 'touchend' }, { activate: 'pointerdown', deactivate: 'pointerup' }, { activate: 'mousedown', deactivate: 'mouseup' }, { activate: 'keydown', deactivate: 'keyup' }, { focus: 'focus', blur: 'blur' }];

    /** @private {!ListenersType} */
    _this.listeners_ = {
      activate: function activate(e) {
        return _this.activate_(e);
      },
      deactivate: function deactivate(e) {
        return _this.deactivate_(e);
      },
      focus: function focus() {
        return requestAnimationFrame(function () {
          return _this.adapter_.addClass(MDCRippleFoundation.cssClasses.BG_FOCUSED);
        });
      },
      blur: function blur() {
        return requestAnimationFrame(function () {
          return _this.adapter_.removeClass(MDCRippleFoundation.cssClasses.BG_FOCUSED);
        });
      }
    };

    /** @private {!Function} */
    _this.resizeHandler_ = function () {
      return _this.layout();
    };

    /** @private {!{left: number, top:number}} */
    _this.unboundedCoords_ = {
      left: 0,
      top: 0
    };

    /** @private {number} */
    _this.fgScale_ = 0;

    /** @private {number} */
    _this.activationTimer_ = 0;

    /** @private {number} */
    _this.fgDeactivationRemovalTimer_ = 0;

    /** @private {boolean} */
    _this.activationAnimationHasEnded_ = false;

    /** @private {!Function} */
    _this.activationTimerCallback_ = function () {
      _this.activationAnimationHasEnded_ = true;
      _this.runDeactivationUXLogicIfReady_();
    };
    return _this;
  }

  /**
   * We compute this property so that we are not querying information about the client
   * until the point in time where the foundation requests it. This prevents scenarios where
   * client-side feature-detection may happen too early, such as when components are rendered on the server
   * and then initialized at mount time on the client.
   * @return {boolean}
   * @private
   */


  MDCRippleFoundation.prototype.isSupported_ = function isSupported_() {
    return this.adapter_.browserSupportsCssVars();
  };

  /**
   * @return {!ActivationStateType}
   */


  MDCRippleFoundation.prototype.defaultActivationState_ = function defaultActivationState_() {
    return {
      isActivated: false,
      hasDeactivationUXRun: false,
      wasActivatedByPointer: false,
      wasElementMadeActive: false,
      activationStartTime: 0,
      activationEvent: null,
      isProgrammatic: false
    };
  };

  MDCRippleFoundation.prototype.init = function init() {
    var _this2 = this;

    if (!this.isSupported_()) {
      return;
    }
    this.addEventListeners_();

    var _MDCRippleFoundation$ = MDCRippleFoundation.cssClasses,
        ROOT = _MDCRippleFoundation$.ROOT,
        UNBOUNDED = _MDCRippleFoundation$.UNBOUNDED;

    requestAnimationFrame(function () {
      _this2.adapter_.addClass(ROOT);
      if (_this2.adapter_.isUnbounded()) {
        _this2.adapter_.addClass(UNBOUNDED);
      }
      _this2.layoutInternal_();
    });
  };

  /** @private */


  MDCRippleFoundation.prototype.addEventListeners_ = function addEventListeners_() {
    var _this3 = this;

    this.listenerInfos_.forEach(function (info) {
      Object.keys(info).forEach(function (k) {
        _this3.adapter_.registerInteractionHandler(info[k], _this3.listeners_[k]);
      });
    });
    this.adapter_.registerResizeHandler(this.resizeHandler_);
  };

  /**
   * @param {Event} e
   * @private
   */


  MDCRippleFoundation.prototype.activate_ = function activate_(e) {
    var _this4 = this;

    if (this.adapter_.isSurfaceDisabled()) {
      return;
    }

    var activationState = this.activationState_;

    if (activationState.isActivated) {
      return;
    }

    activationState.isActivated = true;
    activationState.isProgrammatic = e === null;
    activationState.activationEvent = e;
    activationState.wasActivatedByPointer = activationState.isProgrammatic ? false : e.type === 'mousedown' || e.type === 'touchstart' || e.type === 'pointerdown';
    activationState.activationStartTime = Date.now();

    requestAnimationFrame(function () {
      // This needs to be wrapped in an rAF call b/c web browsers
      // report active states inconsistently when they're called within
      // event handling code:
      // - https://bugs.chromium.org/p/chromium/issues/detail?id=635971
      // - https://bugzilla.mozilla.org/show_bug.cgi?id=1293741
      activationState.wasElementMadeActive = e && e.type === 'keydown' ? _this4.adapter_.isSurfaceActive() : true;
      if (activationState.wasElementMadeActive) {
        _this4.animateActivation_();
      } else {
        // Reset activation state immediately if element was not made active.
        _this4.activationState_ = _this4.defaultActivationState_();
      }
    });
  };

  /**
   * @param {?Event=} event Optional event containing position information.
   */


  MDCRippleFoundation.prototype.activate = function activate() {
    var event = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

    this.activate_(event);
  };

  /** @private */


  MDCRippleFoundation.prototype.animateActivation_ = function animateActivation_() {
    var _this5 = this;

    var _MDCRippleFoundation$2 = MDCRippleFoundation.strings,
        VAR_FG_TRANSLATE_START = _MDCRippleFoundation$2.VAR_FG_TRANSLATE_START,
        VAR_FG_TRANSLATE_END = _MDCRippleFoundation$2.VAR_FG_TRANSLATE_END;
    var _MDCRippleFoundation$3 = MDCRippleFoundation.cssClasses,
        BG_ACTIVE_FILL = _MDCRippleFoundation$3.BG_ACTIVE_FILL,
        FG_DEACTIVATION = _MDCRippleFoundation$3.FG_DEACTIVATION,
        FG_ACTIVATION = _MDCRippleFoundation$3.FG_ACTIVATION;
    var DEACTIVATION_TIMEOUT_MS = MDCRippleFoundation.numbers.DEACTIVATION_TIMEOUT_MS;


    var translateStart = '';
    var translateEnd = '';

    if (!this.adapter_.isUnbounded()) {
      var _getFgTranslationCoor = this.getFgTranslationCoordinates_(),
          startPoint = _getFgTranslationCoor.startPoint,
          endPoint = _getFgTranslationCoor.endPoint;

      translateStart = startPoint.x + 'px, ' + startPoint.y + 'px';
      translateEnd = endPoint.x + 'px, ' + endPoint.y + 'px';
    }

    this.adapter_.updateCssVariable(VAR_FG_TRANSLATE_START, translateStart);
    this.adapter_.updateCssVariable(VAR_FG_TRANSLATE_END, translateEnd);
    // Cancel any ongoing activation/deactivation animations
    clearTimeout(this.activationTimer_);
    clearTimeout(this.fgDeactivationRemovalTimer_);
    this.rmBoundedActivationClasses_();
    this.adapter_.removeClass(FG_DEACTIVATION);

    // Force layout in order to re-trigger the animation.
    this.adapter_.computeBoundingRect();
    this.adapter_.addClass(BG_ACTIVE_FILL);
    this.adapter_.addClass(FG_ACTIVATION);
    this.activationTimer_ = setTimeout(function () {
      return _this5.activationTimerCallback_();
    }, DEACTIVATION_TIMEOUT_MS);
  };

  /**
   * @private
   * @return {{startPoint: PointType, endPoint: PointType}}
   */


  MDCRippleFoundation.prototype.getFgTranslationCoordinates_ = function getFgTranslationCoordinates_() {
    var activationState = this.activationState_;
    var activationEvent = activationState.activationEvent,
        wasActivatedByPointer = activationState.wasActivatedByPointer;


    var startPoint = void 0;
    if (wasActivatedByPointer) {
      startPoint = getNormalizedEventCoords(
      /** @type {!Event} */activationEvent, this.adapter_.getWindowPageOffset(), this.adapter_.computeBoundingRect());
    } else {
      startPoint = {
        x: this.frame_.width / 2,
        y: this.frame_.height / 2
      };
    }
    // Center the element around the start point.
    startPoint = {
      x: startPoint.x - this.initialSize_ / 2,
      y: startPoint.y - this.initialSize_ / 2
    };

    var endPoint = {
      x: this.frame_.width / 2 - this.initialSize_ / 2,
      y: this.frame_.height / 2 - this.initialSize_ / 2
    };

    return { startPoint: startPoint, endPoint: endPoint };
  };

  /** @private */


  MDCRippleFoundation.prototype.runDeactivationUXLogicIfReady_ = function runDeactivationUXLogicIfReady_() {
    var _this6 = this;

    var FG_DEACTIVATION = MDCRippleFoundation.cssClasses.FG_DEACTIVATION;
    var _activationState_ = this.activationState_,
        hasDeactivationUXRun = _activationState_.hasDeactivationUXRun,
        isActivated = _activationState_.isActivated;

    var activationHasEnded = hasDeactivationUXRun || !isActivated;
    if (activationHasEnded && this.activationAnimationHasEnded_) {
      this.rmBoundedActivationClasses_();
      this.adapter_.addClass(FG_DEACTIVATION);
      this.fgDeactivationRemovalTimer_ = setTimeout(function () {
        _this6.adapter_.removeClass(FG_DEACTIVATION);
      }, numbers.FG_DEACTIVATION_MS);
    }
  };

  /** @private */


  MDCRippleFoundation.prototype.rmBoundedActivationClasses_ = function rmBoundedActivationClasses_() {
    var _MDCRippleFoundation$4 = MDCRippleFoundation.cssClasses,
        BG_ACTIVE_FILL = _MDCRippleFoundation$4.BG_ACTIVE_FILL,
        FG_ACTIVATION = _MDCRippleFoundation$4.FG_ACTIVATION;

    this.adapter_.removeClass(BG_ACTIVE_FILL);
    this.adapter_.removeClass(FG_ACTIVATION);
    this.activationAnimationHasEnded_ = false;
    this.adapter_.computeBoundingRect();
  };

  /**
   * @param {Event} e
   * @private
   */


  MDCRippleFoundation.prototype.deactivate_ = function deactivate_(e) {
    var _this7 = this;

    var activationState = this.activationState_;
    // This can happen in scenarios such as when you have a keyup event that blurs the element.

    if (!activationState.isActivated) {
      return;
    }
    // Programmatic deactivation.
    if (activationState.isProgrammatic) {
      var evtObject = null;
      var _state = /** @type {!ActivationStateType} */_extends({}, activationState);
      requestAnimationFrame(function () {
        return _this7.animateDeactivation_(evtObject, _state);
      });
      this.activationState_ = this.defaultActivationState_();
      return;
    }

    var actualActivationType = DEACTIVATION_ACTIVATION_PAIRS[e.type];
    var expectedActivationType = activationState.activationEvent.type;
    // NOTE: Pointer events are tricky - https://patrickhlauke.github.io/touch/tests/results/
    // Essentially, what we need to do here is decouple the deactivation UX from the actual
    // deactivation state itself. This way, touch/pointer events in sequence do not trample one
    // another.
    var needsDeactivationUX = actualActivationType === expectedActivationType;
    var needsActualDeactivation = needsDeactivationUX;
    if (activationState.wasActivatedByPointer) {
      needsActualDeactivation = e.type === 'mouseup';
    }

    var state = /** @type {!ActivationStateType} */_extends({}, activationState);
    requestAnimationFrame(function () {
      if (needsDeactivationUX) {
        _this7.activationState_.hasDeactivationUXRun = true;
        _this7.animateDeactivation_(e, state);
      }

      if (needsActualDeactivation) {
        _this7.activationState_ = _this7.defaultActivationState_();
      }
    });
  };

  /**
   * @param {?Event=} event Optional event containing position information.
   */


  MDCRippleFoundation.prototype.deactivate = function deactivate() {
    var event = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

    this.deactivate_(event);
  };

  /**
   * @param {Event} e
   * @param {!ActivationStateType} options
   * @private
   */


  MDCRippleFoundation.prototype.animateDeactivation_ = function animateDeactivation_(e, _ref) {
    var wasActivatedByPointer = _ref.wasActivatedByPointer,
        wasElementMadeActive = _ref.wasElementMadeActive;
    var BG_FOCUSED = MDCRippleFoundation.cssClasses.BG_FOCUSED;

    if (wasActivatedByPointer || wasElementMadeActive) {
      // Remove class left over by element being focused
      this.adapter_.removeClass(BG_FOCUSED);
      this.runDeactivationUXLogicIfReady_();
    }
  };

  MDCRippleFoundation.prototype.destroy = function destroy() {
    var _this8 = this;

    if (!this.isSupported_()) {
      return;
    }
    this.removeEventListeners_();

    var _MDCRippleFoundation$5 = MDCRippleFoundation.cssClasses,
        ROOT = _MDCRippleFoundation$5.ROOT,
        UNBOUNDED = _MDCRippleFoundation$5.UNBOUNDED;

    requestAnimationFrame(function () {
      _this8.adapter_.removeClass(ROOT);
      _this8.adapter_.removeClass(UNBOUNDED);
      _this8.removeCssVars_();
    });
  };

  /** @private */


  MDCRippleFoundation.prototype.removeEventListeners_ = function removeEventListeners_() {
    var _this9 = this;

    this.listenerInfos_.forEach(function (info) {
      Object.keys(info).forEach(function (k) {
        _this9.adapter_.deregisterInteractionHandler(info[k], _this9.listeners_[k]);
      });
    });
    this.adapter_.deregisterResizeHandler(this.resizeHandler_);
  };

  /** @private */


  MDCRippleFoundation.prototype.removeCssVars_ = function removeCssVars_() {
    var _this10 = this;

    var strings = MDCRippleFoundation.strings;

    Object.keys(strings).forEach(function (k) {
      if (k.indexOf('VAR_') === 0) {
        _this10.adapter_.updateCssVariable(strings[k], null);
      }
    });
  };

  MDCRippleFoundation.prototype.layout = function layout() {
    var _this11 = this;

    if (this.layoutFrame_) {
      cancelAnimationFrame(this.layoutFrame_);
    }
    this.layoutFrame_ = requestAnimationFrame(function () {
      _this11.layoutInternal_();
      _this11.layoutFrame_ = 0;
    });
  };

  /** @private */


  MDCRippleFoundation.prototype.layoutInternal_ = function layoutInternal_() {
    this.frame_ = this.adapter_.computeBoundingRect();

    var maxDim = Math.max(this.frame_.height, this.frame_.width);
    var surfaceDiameter = Math.sqrt(Math.pow(this.frame_.width, 2) + Math.pow(this.frame_.height, 2));

    // 60% of the largest dimension of the surface
    this.initialSize_ = maxDim * MDCRippleFoundation.numbers.INITIAL_ORIGIN_SCALE;

    // Diameter of the surface + 10px
    this.maxRadius_ = surfaceDiameter + MDCRippleFoundation.numbers.PADDING;
    this.fgScale_ = this.maxRadius_ / this.initialSize_;
    this.xfDuration_ = 1000 * Math.sqrt(this.maxRadius_ / 1024);
    this.updateLayoutCssVars_();
  };

  /** @private */


  MDCRippleFoundation.prototype.updateLayoutCssVars_ = function updateLayoutCssVars_() {
    var _MDCRippleFoundation$6 = MDCRippleFoundation.strings,
        VAR_SURFACE_WIDTH = _MDCRippleFoundation$6.VAR_SURFACE_WIDTH,
        VAR_SURFACE_HEIGHT = _MDCRippleFoundation$6.VAR_SURFACE_HEIGHT,
        VAR_FG_SIZE = _MDCRippleFoundation$6.VAR_FG_SIZE,
        VAR_LEFT = _MDCRippleFoundation$6.VAR_LEFT,
        VAR_TOP = _MDCRippleFoundation$6.VAR_TOP,
        VAR_FG_SCALE = _MDCRippleFoundation$6.VAR_FG_SCALE;


    this.adapter_.updateCssVariable(VAR_SURFACE_WIDTH, this.frame_.width + 'px');
    this.adapter_.updateCssVariable(VAR_SURFACE_HEIGHT, this.frame_.height + 'px');
    this.adapter_.updateCssVariable(VAR_FG_SIZE, this.initialSize_ + 'px');
    this.adapter_.updateCssVariable(VAR_FG_SCALE, this.fgScale_);

    if (this.adapter_.isUnbounded()) {
      this.unboundedCoords_ = {
        left: Math.round(this.frame_.width / 2 - this.initialSize_ / 2),
        top: Math.round(this.frame_.height / 2 - this.initialSize_ / 2)
      };

      this.adapter_.updateCssVariable(VAR_LEFT, this.unboundedCoords_.left + 'px');
      this.adapter_.updateCssVariable(VAR_TOP, this.unboundedCoords_.top + 'px');
    }
  };

  return MDCRippleFoundation;
}(foundation);

/* harmony default export */ var ripple_foundation = (foundation_MDCRippleFoundation);
// CONCATENATED MODULE: ../node_modules/@material/ripple/index.js
var ripple__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function ripple__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function ripple__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function ripple__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * @license
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */






/**
 * @extends MDCComponent<!MDCRippleFoundation>
 */

var ripple_MDCRipple = function (_MDCComponent) {
  ripple__inherits(MDCRipple, _MDCComponent);

  /** @param {...?} args */
  function MDCRipple() {
    ripple__classCallCheck(this, MDCRipple);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    /** @type {boolean} */
    var _this = ripple__possibleConstructorReturn(this, _MDCComponent.call.apply(_MDCComponent, [this].concat(args)));

    _this.disabled = false;

    /** @private {boolean} */
    _this.unbounded_;
    return _this;
  }

  /**
   * @param {!Element} root
   * @param {{isUnbounded: (boolean|undefined)}=} options
   * @return {!MDCRipple}
   */


  MDCRipple.attachTo = function attachTo(root) {
    var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        _ref$isUnbounded = _ref.isUnbounded,
        isUnbounded = _ref$isUnbounded === undefined ? undefined : _ref$isUnbounded;

    var ripple = new MDCRipple(root);
    // Only override unbounded behavior if option is explicitly specified
    if (isUnbounded !== undefined) {
      ripple.unbounded = /** @type {boolean} */isUnbounded;
    }
    return ripple;
  };

  /**
   * @param {!RippleCapableSurface} instance
   * @return {!MDCRippleAdapter}
   */


  MDCRipple.createAdapter = function createAdapter(instance) {
    var MATCHES = getMatchesProperty(HTMLElement.prototype);

    return {
      browserSupportsCssVars: function browserSupportsCssVars() {
        return supportsCssVariables(window);
      },
      isUnbounded: function isUnbounded() {
        return instance.unbounded;
      },
      isSurfaceActive: function isSurfaceActive() {
        return instance.root_[MATCHES](':active');
      },
      isSurfaceDisabled: function isSurfaceDisabled() {
        return instance.disabled;
      },
      addClass: function addClass(className) {
        return instance.root_.classList.add(className);
      },
      removeClass: function removeClass(className) {
        return instance.root_.classList.remove(className);
      },
      registerInteractionHandler: function registerInteractionHandler(evtType, handler) {
        return instance.root_.addEventListener(evtType, handler, applyPassive());
      },
      deregisterInteractionHandler: function deregisterInteractionHandler(evtType, handler) {
        return instance.root_.removeEventListener(evtType, handler, applyPassive());
      },
      registerResizeHandler: function registerResizeHandler(handler) {
        return window.addEventListener('resize', handler);
      },
      deregisterResizeHandler: function deregisterResizeHandler(handler) {
        return window.removeEventListener('resize', handler);
      },
      updateCssVariable: function updateCssVariable(varName, value) {
        return instance.root_.style.setProperty(varName, value);
      },
      computeBoundingRect: function computeBoundingRect() {
        return instance.root_.getBoundingClientRect();
      },
      getWindowPageOffset: function getWindowPageOffset() {
        return { x: window.pageXOffset, y: window.pageYOffset };
      }
    };
  };

  /** @return {boolean} */


  MDCRipple.prototype.activate = function activate() {
    this.foundation_.activate();
  };

  MDCRipple.prototype.deactivate = function deactivate() {
    this.foundation_.deactivate();
  };

  MDCRipple.prototype.layout = function layout() {
    this.foundation_.layout();
  };

  /** @return {!MDCRippleFoundation} */


  MDCRipple.prototype.getDefaultFoundation = function getDefaultFoundation() {
    return new ripple_foundation(MDCRipple.createAdapter(this));
  };

  MDCRipple.prototype.initialSyncWithDOM = function initialSyncWithDOM() {
    this.unbounded = 'mdcRippleIsUnbounded' in this.root_.dataset;
  };

  ripple__createClass(MDCRipple, [{
    key: 'unbounded',
    get: function get() {
      return this.unbounded_;
    }

    /** @param {boolean} unbounded */
    ,
    set: function set(unbounded) {
      var UNBOUNDED = ripple_foundation.cssClasses.UNBOUNDED;

      this.unbounded_ = Boolean(unbounded);
      if (this.unbounded_) {
        this.root_.classList.add(UNBOUNDED);
      } else {
        this.root_.classList.remove(UNBOUNDED);
      }
    }
  }]);

  return MDCRipple;
}(component);

/**
 * See Material Design spec for more details on when to use ripples.
 * https://material.io/guidelines/motion/choreography.html#choreography-creation
 * @record
 */


var RippleCapableSurface = function RippleCapableSurface() {
  ripple__classCallCheck(this, RippleCapableSurface);
};

/** @protected {!Element} */


RippleCapableSurface.prototype.root_;

/**
 * Whether or not the ripple bleeds out of the bounds of the element.
 * @type {boolean|undefined}
 */
RippleCapableSurface.prototype.unbounded;

/**
 * Whether or not the ripple is attached to a disabled component.
 * @type {boolean|undefined}
 */
RippleCapableSurface.prototype.disabled;


// CONCATENATED MODULE: ../node_modules/preact-material-components/MaterialComponent.js
var MaterialComponent__extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function MaterialComponent__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function MaterialComponent__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function MaterialComponent__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




/**
 * Base class for every Material component in this package
 * NOTE: every component should add a ref by the name of `control` to its root dom for autoInit Properties
 *
 * @export
 * @class MaterialComponent
 * @extends {Component}
 */

var MaterialComponent_MaterialComponent = function (_Component) {
  MaterialComponent__inherits(MaterialComponent, _Component);

  function MaterialComponent() {
    MaterialComponent__classCallCheck(this, MaterialComponent);

    // Attributes inside this array will be check for boolean value true
    // and will be converted to mdc classes
    var _this = MaterialComponent__possibleConstructorReturn(this, _Component.call(this));

    _this._mdcProps = [];
    // This will again be used to add apt classname to the component
    _this.componentName = "";
    // The final class name given to the dom
    _this.classText = "";
    return _this;
  }

  MaterialComponent.prototype.attachRipple = function attachRipple() {
    if (this.props.ripple && this.control) {
      ripple_MDCRipple.attachTo(this.control);
    }
  };
  // Build the className


  MaterialComponent.prototype.buildClassName = function buildClassName(props) {
    this.classText = "mdc-" + this.componentName;
    for (var propKey in this.props) {
      if (this.props.hasOwnProperty(propKey)) {
        var prop = this.props[propKey];
        if (typeof prop === "boolean" && prop) {
          if (this._mdcProps.indexOf(propKey) !== -1) {
            this.classText += " mdc-" + this.componentName + "--" + propKey;
          }
        }
      }
    }
  };

  MaterialComponent.prototype.getClassName = function getClassName(element) {
    if (!element) {
      return "";
    }
    var attrs = element.attributes = element.attributes || {};
    var classText = this.classText;
    if (attrs.class) {
      classText += " " + attrs.class;
    }
    if (attrs.className && attrs.className !== attrs.class) {
      classText += " " + attrs.className;
    }
    return classText;
  };
  // Components must implement this method for their specific DOM structure


  MaterialComponent.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("div", MaterialComponent__extends({}, props), props.children);
  };

  MaterialComponent.prototype.render = function render() {
    this.buildClassName();
    // Fetch a VNode
    var componentProps = this.props;
    if (componentProps.class) {
      // We delete class prop here so that any sub node's class doesn't get over-ridden from this
      delete componentProps.class;
    }
    var element = this.materialDom(componentProps);
    element.attributes = element.attributes || {};
    // Fix for className
    element.attributes.class = this.getClassName(element);
    element.attributes.className = this.getClassName(element);
    // Clean this shit of proxy attributes
    this._mdcProps.forEach(function (prop) {
      delete element.attributes[prop];
    });
    return element;
  };

  return MaterialComponent;
}(preact_min["Component"]);


// CONCATENATED MODULE: ../node_modules/@material/base/index.js
/**
 * @license
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */





// CONCATENATED MODULE: ../node_modules/@material/toolbar/constants.js
/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var constants_cssClasses = {
  FIXED: 'mdc-toolbar--fixed',
  FIXED_LASTROW: 'mdc-toolbar--fixed-lastrow-only',
  FIXED_AT_LAST_ROW: 'mdc-toolbar--fixed-at-last-row',
  TOOLBAR_ROW_FLEXIBLE: 'mdc-toolbar--flexible',
  FLEXIBLE_DEFAULT_BEHAVIOR: 'mdc-toolbar--flexible-default-behavior',
  FLEXIBLE_MAX: 'mdc-toolbar--flexible-space-maximized',
  FLEXIBLE_MIN: 'mdc-toolbar--flexible-space-minimized'
};

var constants_strings = {
  TITLE_SELECTOR: '.mdc-toolbar__title',
  FIRST_ROW_SELECTOR: '.mdc-toolbar__row:first-child',
  CHANGE_EVENT: 'MDCToolbar:change'
};

var constants_numbers = {
  MAX_TITLE_SIZE: 2.125,
  MIN_TITLE_SIZE: 1.25,
  TOOLBAR_ROW_HEIGHT: 64,
  TOOLBAR_ROW_MOBILE_HEIGHT: 56,
  TOOLBAR_MOBILE_BREAKPOINT: 600
};
// CONCATENATED MODULE: ../node_modules/@material/toolbar/foundation.js
var foundation__extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var toolbar_foundation__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function toolbar_foundation__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function foundation__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function foundation__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



var foundation_MDCToolbarFoundation = function (_MDCFoundation) {
  foundation__inherits(MDCToolbarFoundation, _MDCFoundation);

  toolbar_foundation__createClass(MDCToolbarFoundation, null, [{
    key: 'cssClasses',
    get: function get() {
      return constants_cssClasses;
    }
  }, {
    key: 'strings',
    get: function get() {
      return constants_strings;
    }
  }, {
    key: 'numbers',
    get: function get() {
      return constants_numbers;
    }
  }, {
    key: 'defaultAdapter',
    get: function get() {
      return {
        hasClass: function hasClass() {
          return (/* className: string */ /* boolean */false
          );
        },
        addClass: function addClass() /* className: string */{},
        removeClass: function removeClass() /* className: string */{},
        registerScrollHandler: function registerScrollHandler() /* handler: EventListener */{},
        deregisterScrollHandler: function deregisterScrollHandler() /* handler: EventListener */{},
        registerResizeHandler: function registerResizeHandler() /* handler: EventListener */{},
        deregisterResizeHandler: function deregisterResizeHandler() /* handler: EventListener */{},
        getViewportWidth: function getViewportWidth() {
          return (/* number */0
          );
        },
        getViewportScrollY: function getViewportScrollY() {
          return (/* number */0
          );
        },
        getOffsetHeight: function getOffsetHeight() {
          return (/* number */0
          );
        },
        getFirstRowElementOffsetHeight: function getFirstRowElementOffsetHeight() {
          return (/* number */0
          );
        },
        notifyChange: function notifyChange() /* evtData: {flexibleExpansionRatio: number} */{},
        setStyle: function setStyle() /* property: string, value: string */{},
        setStyleForTitleElement: function setStyleForTitleElement() /* property: string, value: string */{},
        setStyleForFlexibleRowElement: function setStyleForFlexibleRowElement() /* property: string, value: string */{},
        setStyleForFixedAdjustElement: function setStyleForFixedAdjustElement() /* property: string, value: string */{}
      };
    }
  }]);

  function MDCToolbarFoundation(adapter) {
    toolbar_foundation__classCallCheck(this, MDCToolbarFoundation);

    var _this = foundation__possibleConstructorReturn(this, _MDCFoundation.call(this, foundation__extends(MDCToolbarFoundation.defaultAdapter, adapter)));

    _this.resizeHandler_ = function () {
      return _this.checkRowHeight_();
    };
    _this.scrollHandler_ = function () {
      return _this.updateToolbarStyles_();
    };
    _this.checkRowHeightFrame_ = 0;
    _this.scrollFrame_ = 0;
    _this.executedLastChange_ = false;

    _this.calculations_ = {
      toolbarRowHeight: 0,
      // Calculated Height ratio. We use ratio to calculate corresponding heights in resize event.
      toolbarRatio: 0, // The ratio of toolbar height to row height
      flexibleExpansionRatio: 0, // The ratio of flexible space height to row height
      maxTranslateYRatio: 0, // The ratio of max toolbar move up distance to row height
      scrollThresholdRatio: 0, // The ratio of max scrollTop that we should listen to to row height
      // Derived Heights based on the above key ratios.
      toolbarHeight: 0,
      flexibleExpansionHeight: 0, // Flexible row minus toolbar height (derived)
      maxTranslateYDistance: 0, // When toolbar only fix last row (derived)
      scrollThreshold: 0
    };
    // Toolbar fixed behavior
    // If toolbar is fixed
    _this.fixed_ = false;
    // If fixed is targeted only at the last row
    _this.fixedLastrow_ = false;
    // Toolbar flexible behavior
    // If the first row is flexible
    _this.hasFlexibleRow_ = false;
    // If use the default behavior
    _this.useFlexDefaultBehavior_ = false;
    return _this;
  }

  MDCToolbarFoundation.prototype.init = function init() {
    this.fixed_ = this.adapter_.hasClass(MDCToolbarFoundation.cssClasses.FIXED);
    this.fixedLastrow_ = this.adapter_.hasClass(MDCToolbarFoundation.cssClasses.FIXED_LASTROW) & this.fixed_;
    this.hasFlexibleRow_ = this.adapter_.hasClass(MDCToolbarFoundation.cssClasses.TOOLBAR_ROW_FLEXIBLE);
    if (this.hasFlexibleRow_) {
      this.useFlexDefaultBehavior_ = this.adapter_.hasClass(MDCToolbarFoundation.cssClasses.FLEXIBLE_DEFAULT_BEHAVIOR);
    }
    this.initKeyRatio_();
    this.setKeyHeights_();
    this.adapter_.registerResizeHandler(this.resizeHandler_);
    this.adapter_.registerScrollHandler(this.scrollHandler_);
  };

  MDCToolbarFoundation.prototype.destroy = function destroy() {
    this.adapter_.deregisterResizeHandler(this.resizeHandler_);
    this.adapter_.deregisterScrollHandler(this.scrollHandler_);
  };

  MDCToolbarFoundation.prototype.updateAdjustElementStyles = function updateAdjustElementStyles() {
    if (this.fixed_) {
      this.adapter_.setStyleForFixedAdjustElement('margin-top', this.calculations_.toolbarHeight + 'px');
    }
  };

  MDCToolbarFoundation.prototype.getFlexibleExpansionRatio_ = function getFlexibleExpansionRatio_(scrollTop) {
    // To prevent division by zero when there is no flexibleExpansionHeight
    var delta = 0.0001;
    return Math.max(0, 1 - scrollTop / (this.calculations_.flexibleExpansionHeight + delta));
  };

  MDCToolbarFoundation.prototype.checkRowHeight_ = function checkRowHeight_() {
    var _this2 = this;

    cancelAnimationFrame(this.checkRowHeightFrame_);
    this.checkRowHeightFrame_ = requestAnimationFrame(function () {
      return _this2.setKeyHeights_();
    });
  };

  MDCToolbarFoundation.prototype.setKeyHeights_ = function setKeyHeights_() {
    var newToolbarRowHeight = this.getRowHeight_();
    if (newToolbarRowHeight !== this.calculations_.toolbarRowHeight) {
      this.calculations_.toolbarRowHeight = newToolbarRowHeight;
      this.calculations_.toolbarHeight = this.calculations_.toolbarRatio * this.calculations_.toolbarRowHeight;
      this.calculations_.flexibleExpansionHeight = this.calculations_.flexibleExpansionRatio * this.calculations_.toolbarRowHeight;
      this.calculations_.maxTranslateYDistance = this.calculations_.maxTranslateYRatio * this.calculations_.toolbarRowHeight;
      this.calculations_.scrollThreshold = this.calculations_.scrollThresholdRatio * this.calculations_.toolbarRowHeight;
      this.updateAdjustElementStyles();
      this.updateToolbarStyles_();
    }
  };

  MDCToolbarFoundation.prototype.updateToolbarStyles_ = function updateToolbarStyles_() {
    var _this3 = this;

    cancelAnimationFrame(this.scrollFrame_);
    this.scrollFrame_ = requestAnimationFrame(function () {
      var scrollTop = _this3.adapter_.getViewportScrollY();
      var hasScrolledOutOfThreshold = _this3.scrolledOutOfThreshold_(scrollTop);

      if (hasScrolledOutOfThreshold && _this3.executedLastChange_) {
        return;
      }

      var flexibleExpansionRatio = _this3.getFlexibleExpansionRatio_(scrollTop);

      _this3.updateToolbarFlexibleState_(flexibleExpansionRatio);
      if (_this3.fixedLastrow_) {
        _this3.updateToolbarFixedState_(scrollTop);
      }
      if (_this3.hasFlexibleRow_) {
        _this3.updateFlexibleRowElementStyles_(flexibleExpansionRatio);
      }
      _this3.executedLastChange_ = hasScrolledOutOfThreshold;
      _this3.adapter_.notifyChange({ flexibleExpansionRatio: flexibleExpansionRatio });
    });
  };

  MDCToolbarFoundation.prototype.scrolledOutOfThreshold_ = function scrolledOutOfThreshold_(scrollTop) {
    return scrollTop > this.calculations_.scrollThreshold;
  };

  MDCToolbarFoundation.prototype.initKeyRatio_ = function initKeyRatio_() {
    var toolbarRowHeight = this.getRowHeight_();
    var firstRowMaxRatio = this.adapter_.getFirstRowElementOffsetHeight() / toolbarRowHeight;
    this.calculations_.toolbarRatio = this.adapter_.getOffsetHeight() / toolbarRowHeight;
    this.calculations_.flexibleExpansionRatio = firstRowMaxRatio - 1;
    this.calculations_.maxTranslateYRatio = this.fixedLastrow_ ? this.calculations_.toolbarRatio - firstRowMaxRatio : 0;
    this.calculations_.scrollThresholdRatio = (this.fixedLastrow_ ? this.calculations_.toolbarRatio : firstRowMaxRatio) - 1;
  };

  MDCToolbarFoundation.prototype.getRowHeight_ = function getRowHeight_() {
    var breakpoint = MDCToolbarFoundation.numbers.TOOLBAR_MOBILE_BREAKPOINT;
    return this.adapter_.getViewportWidth() < breakpoint ? MDCToolbarFoundation.numbers.TOOLBAR_ROW_MOBILE_HEIGHT : MDCToolbarFoundation.numbers.TOOLBAR_ROW_HEIGHT;
  };

  MDCToolbarFoundation.prototype.updateToolbarFlexibleState_ = function updateToolbarFlexibleState_(flexibleExpansionRatio) {
    this.adapter_.removeClass(MDCToolbarFoundation.cssClasses.FLEXIBLE_MAX);
    this.adapter_.removeClass(MDCToolbarFoundation.cssClasses.FLEXIBLE_MIN);
    if (flexibleExpansionRatio === 1) {
      this.adapter_.addClass(MDCToolbarFoundation.cssClasses.FLEXIBLE_MAX);
    } else if (flexibleExpansionRatio === 0) {
      this.adapter_.addClass(MDCToolbarFoundation.cssClasses.FLEXIBLE_MIN);
    }
  };

  MDCToolbarFoundation.prototype.updateToolbarFixedState_ = function updateToolbarFixedState_(scrollTop) {
    var translateDistance = Math.max(0, Math.min(scrollTop - this.calculations_.flexibleExpansionHeight, this.calculations_.maxTranslateYDistance));
    this.adapter_.setStyle('transform', 'translateY(' + -translateDistance + 'px)');

    if (translateDistance === this.calculations_.maxTranslateYDistance) {
      this.adapter_.addClass(MDCToolbarFoundation.cssClasses.FIXED_AT_LAST_ROW);
    } else {
      this.adapter_.removeClass(MDCToolbarFoundation.cssClasses.FIXED_AT_LAST_ROW);
    }
  };

  MDCToolbarFoundation.prototype.updateFlexibleRowElementStyles_ = function updateFlexibleRowElementStyles_(flexibleExpansionRatio) {
    if (this.fixed_) {
      var height = this.calculations_.flexibleExpansionHeight * flexibleExpansionRatio;
      this.adapter_.setStyleForFlexibleRowElement('height', height + this.calculations_.toolbarRowHeight + 'px');
    }
    if (this.useFlexDefaultBehavior_) {
      this.updateElementStylesDefaultBehavior_(flexibleExpansionRatio);
    }
  };

  MDCToolbarFoundation.prototype.updateElementStylesDefaultBehavior_ = function updateElementStylesDefaultBehavior_(flexibleExpansionRatio) {
    var maxTitleSize = MDCToolbarFoundation.numbers.MAX_TITLE_SIZE;
    var minTitleSize = MDCToolbarFoundation.numbers.MIN_TITLE_SIZE;
    var currentTitleSize = (maxTitleSize - minTitleSize) * flexibleExpansionRatio + minTitleSize;

    this.adapter_.setStyleForTitleElement('font-size', currentTitleSize + 'rem');
  };

  return MDCToolbarFoundation;
}(foundation);


// CONCATENATED MODULE: ../node_modules/@material/toolbar/util.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var util_supportsPassive_ = void 0;

// Determine whether the current browser supports passive event listeners, and if so, use them.
function util_applyPassive() {
  var globalObj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;
  var forceRefresh = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (util_supportsPassive_ === undefined || forceRefresh) {
    var isSupported = false;
    try {
      globalObj.document.addEventListener('test', null, { get passive() {
          isSupported = true;
        } });
    } catch (e) {}

    util_supportsPassive_ = isSupported;
  }

  return util_supportsPassive_ ? { passive: true } : false;
}
// CONCATENATED MODULE: ../node_modules/@material/toolbar/index.js
var toolbar__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function toolbar__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function toolbar__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function toolbar__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */









var toolbar_MDCToolbar = function (_MDCComponent) {
  toolbar__inherits(MDCToolbar, _MDCComponent);

  function MDCToolbar() {
    toolbar__classCallCheck(this, MDCToolbar);

    return toolbar__possibleConstructorReturn(this, _MDCComponent.apply(this, arguments));
  }

  MDCToolbar.attachTo = function attachTo(root) {
    return new MDCToolbar(root);
  };

  MDCToolbar.prototype.getDefaultFoundation = function getDefaultFoundation() {
    var _this2 = this;

    return new foundation_MDCToolbarFoundation({
      hasClass: function hasClass(className) {
        return _this2.root_.classList.contains(className);
      },
      addClass: function addClass(className) {
        return _this2.root_.classList.add(className);
      },
      removeClass: function removeClass(className) {
        return _this2.root_.classList.remove(className);
      },
      registerScrollHandler: function registerScrollHandler(handler) {
        return window.addEventListener('scroll', handler, util_applyPassive());
      },
      deregisterScrollHandler: function deregisterScrollHandler(handler) {
        return window.removeEventListener('scroll', handler, util_applyPassive());
      },
      registerResizeHandler: function registerResizeHandler(handler) {
        return window.addEventListener('resize', handler);
      },
      deregisterResizeHandler: function deregisterResizeHandler(handler) {
        return window.removeEventListener('resize', handler);
      },
      getViewportWidth: function getViewportWidth() {
        return window.innerWidth;
      },
      getViewportScrollY: function getViewportScrollY() {
        return window.pageYOffset;
      },
      getOffsetHeight: function getOffsetHeight() {
        return _this2.root_.offsetHeight;
      },
      getFirstRowElementOffsetHeight: function getFirstRowElementOffsetHeight() {
        return _this2.firstRowElement_.offsetHeight;
      },
      notifyChange: function notifyChange(evtData) {
        return _this2.emit(foundation_MDCToolbarFoundation.strings.CHANGE_EVENT, evtData);
      },
      setStyle: function setStyle(property, value) {
        return _this2.root_.style.setProperty(property, value);
      },
      setStyleForTitleElement: function setStyleForTitleElement(property, value) {
        return _this2.titleElement_.style.setProperty(property, value);
      },
      setStyleForFlexibleRowElement: function setStyleForFlexibleRowElement(property, value) {
        return _this2.firstRowElement_.style.setProperty(property, value);
      },
      setStyleForFixedAdjustElement: function setStyleForFixedAdjustElement(property, value) {
        if (_this2.fixedAdjustElement) {
          _this2.fixedAdjustElement.style.setProperty(property, value);
        }
      }
    });
  };

  toolbar__createClass(MDCToolbar, [{
    key: 'firstRowElement_',
    get: function get() {
      return this.root_.querySelector(foundation_MDCToolbarFoundation.strings.FIRST_ROW_SELECTOR);
    }
  }, {
    key: 'titleElement_',
    get: function get() {
      return this.root_.querySelector(foundation_MDCToolbarFoundation.strings.TITLE_SELECTOR);
    }
  }, {
    key: 'fixedAdjustElement',
    set: function set(fixedAdjustElement) {
      this.fixedAdjustElement_ = fixedAdjustElement;
      this.foundation_.updateAdjustElementStyles();
    },
    get: function get() {
      return this.fixedAdjustElement_;
    }
  }]);

  return MDCToolbar;
}(component);
// CONCATENATED MODULE: ../node_modules/preact-material-components/Toolbar/index.js
function Toolbar__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Toolbar__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function Toolbar__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Toolbar__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};




/**
 * @prop fixed = false
 * @prop fixed-lastrow-only = false
 * @prop waterfall = false
 * @prop flexible = false
 * @prop flexible-default-behavior = false
 */

var Toolbar_Toolbar = function (_MaterialComponent) {
  Toolbar__inherits(Toolbar, _MaterialComponent);

  function Toolbar() {
    Toolbar__classCallCheck(this, Toolbar);

    var _this = Toolbar__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "toolbar";
    _this._mdcProps = ["fixed", "fixed-lastrow-only", "waterfall", "flexible", "flexible-default-behavior"];
    _this._onChange = _this._onChange.bind(_this);
    return _this;
  }

  Toolbar.prototype._onChange = function _onChange(e) {
    if (this.props.onChange) {
      this.props.onChange(e);
    }
  };

  Toolbar.prototype.componentDidMount = function componentDidMount() {
    this.MDComponent = new toolbar_MDCToolbar(this.control);
    this.MDComponent.listen("MDCToolbar:change", this._onChange);
  };

  Toolbar.prototype.componentWillUnmount = function componentWillUnmount() {
    this.MDComponent.unlisten("MDCToolbar:change", this._onChange);
    this.MDComponent.destroy && this.MDComponent.destroy();
  };

  Toolbar.prototype.materialDom = function materialDom(props) {
    var _this2 = this;

    return Object(preact_min["h"])("header", Toolbar__extends({
      ref: function ref(control) {
        _this2.control = control;
      }
    }, props), props.children);
  };

  return Toolbar;
}(MaterialComponent_MaterialComponent);

var ToolbarRow = function (_MaterialComponent2) {
  Toolbar__inherits(ToolbarRow, _MaterialComponent2);

  function ToolbarRow() {
    Toolbar__classCallCheck(this, ToolbarRow);

    var _this3 = Toolbar__possibleConstructorReturn(this, _MaterialComponent2.call(this));

    _this3.componentName = "toolbar__row";
    return _this3;
  }

  return ToolbarRow;
}(MaterialComponent_MaterialComponent);

/**
 * @prop align-end = false
 * @prop align-start = false
 * @prop shrink-to-fit = false
 */


var Toolbar_ToolbarSection = function (_MaterialComponent3) {
  Toolbar__inherits(ToolbarSection, _MaterialComponent3);

  function ToolbarSection() {
    Toolbar__classCallCheck(this, ToolbarSection);

    var _this4 = Toolbar__possibleConstructorReturn(this, _MaterialComponent3.call(this));

    _this4.componentName = "toolbar__section";
    _this4._mdcProps = ["align-start", "align-end", "shrink-to-fit"];
    return _this4;
  }

  ToolbarSection.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("section", props, props.children);
  };

  return ToolbarSection;
}(MaterialComponent_MaterialComponent);

/**
 * @prop menu = false
 */


var Toolbar_ToolbarIcon = function (_MaterialComponent4) {
  Toolbar__inherits(ToolbarIcon, _MaterialComponent4);

  function ToolbarIcon(props) {
    Toolbar__classCallCheck(this, ToolbarIcon);

    var _this5 = Toolbar__possibleConstructorReturn(this, _MaterialComponent4.call(this));

    _this5.componentName = "toolbar__icon";
    if (props.menu) {
      _this5.componentName = "toolbar__menu-icon";
    }
    return _this5;
  }

  ToolbarIcon.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("a", Toolbar__extends({ className: "material-icons" }, props), props.children || "menu");
  };

  return ToolbarIcon;
}(MaterialComponent_MaterialComponent);

/**
 * @prop title = ''
 */


var Toolbar_ToolbarTitle = function (_MaterialComponent5) {
  Toolbar__inherits(ToolbarTitle, _MaterialComponent5);

  function ToolbarTitle() {
    Toolbar__classCallCheck(this, ToolbarTitle);

    var _this6 = Toolbar__possibleConstructorReturn(this, _MaterialComponent5.call(this));

    _this6.componentName = "toolbar__title";
    return _this6;
  }

  ToolbarTitle.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("span", props, props.children);
  };

  return ToolbarTitle;
}(MaterialComponent_MaterialComponent);

Toolbar_Toolbar.Section = Toolbar_ToolbarSection;
Toolbar_Toolbar.Icon = Toolbar_ToolbarIcon;
Toolbar_Toolbar.Title = Toolbar_ToolbarTitle;
Toolbar_Toolbar.Row = ToolbarRow;

/* harmony default export */ var preact_material_components_Toolbar = (Toolbar_Toolbar);
// CONCATENATED MODULE: ../node_modules/@material/drawer/slidable/constants.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var constants_FOCUSABLE_ELEMENTS = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), ' + 'button:not([disabled]), iframe, object, embed, [tabindex], [contenteditable]';
// CONCATENATED MODULE: ../node_modules/@material/drawer/slidable/foundation.js
var slidable_foundation__extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var slidable_foundation__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function slidable_foundation__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function slidable_foundation__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function slidable_foundation__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



var MDCSlidableDrawerFoundation = function (_MDCFoundation) {
  slidable_foundation__inherits(MDCSlidableDrawerFoundation, _MDCFoundation);

  slidable_foundation__createClass(MDCSlidableDrawerFoundation, null, [{
    key: 'defaultAdapter',
    get: function get() {
      return {
        addClass: function addClass() /* className: string */{},
        removeClass: function removeClass() /* className: string */{},
        hasClass: function hasClass() /* className: string */{},
        hasNecessaryDom: function hasNecessaryDom() {
          return (/* boolean */false
          );
        },
        registerInteractionHandler: function registerInteractionHandler() /* evt: string, handler: EventListener */{},
        deregisterInteractionHandler: function deregisterInteractionHandler() /* evt: string, handler: EventListener */{},
        registerDrawerInteractionHandler: function registerDrawerInteractionHandler() /* evt: string, handler: EventListener */{},
        deregisterDrawerInteractionHandler: function deregisterDrawerInteractionHandler() /* evt: string, handler: EventListener */{},
        registerTransitionEndHandler: function registerTransitionEndHandler() /* handler: EventListener */{},
        deregisterTransitionEndHandler: function deregisterTransitionEndHandler() /* handler: EventListener */{},
        registerDocumentKeydownHandler: function registerDocumentKeydownHandler() /* handler: EventListener */{},
        deregisterDocumentKeydownHandler: function deregisterDocumentKeydownHandler() /* handler: EventListener */{},
        setTranslateX: function setTranslateX() /* value: number | null */{},
        getFocusableElements: function getFocusableElements() /* NodeList */{},
        saveElementTabState: function saveElementTabState() /* el: Element */{},
        restoreElementTabState: function restoreElementTabState() /* el: Element */{},
        makeElementUntabbable: function makeElementUntabbable() /* el: Element */{},
        notifyOpen: function notifyOpen() {},
        notifyClose: function notifyClose() {},
        isRtl: function isRtl() {
          return (/* boolean */false
          );
        },
        getDrawerWidth: function getDrawerWidth() {
          return (/* number */0
          );
        }
      };
    }
  }]);

  function MDCSlidableDrawerFoundation(adapter, rootCssClass, animatingCssClass, openCssClass) {
    slidable_foundation__classCallCheck(this, MDCSlidableDrawerFoundation);

    var _this = slidable_foundation__possibleConstructorReturn(this, _MDCFoundation.call(this, slidable_foundation__extends(MDCSlidableDrawerFoundation.defaultAdapter, adapter)));

    _this.rootCssClass_ = rootCssClass;
    _this.animatingCssClass_ = animatingCssClass;
    _this.openCssClass_ = openCssClass;

    _this.transitionEndHandler_ = function (evt) {
      return _this.handleTransitionEnd_(evt);
    };

    _this.inert_ = false;

    _this.drawerClickHandler_ = function (evt) {
      return evt.stopPropagation();
    };
    _this.componentTouchStartHandler_ = function (evt) {
      return _this.handleTouchStart_(evt);
    };
    _this.componentTouchMoveHandler_ = function (evt) {
      return _this.handleTouchMove_(evt);
    };
    _this.componentTouchEndHandler_ = function (evt) {
      return _this.handleTouchEnd_(evt);
    };
    _this.documentKeydownHandler_ = function (evt) {
      if (evt.key && evt.key === 'Escape' || evt.keyCode === 27) {
        _this.close();
      }
    };
    return _this;
  }

  MDCSlidableDrawerFoundation.prototype.init = function init() {
    var ROOT = this.rootCssClass_;
    var OPEN = this.openCssClass_;

    if (!this.adapter_.hasClass(ROOT)) {
      throw new Error(ROOT + ' class required in root element.');
    }

    if (!this.adapter_.hasNecessaryDom()) {
      throw new Error('Required DOM nodes missing in ' + ROOT + ' component.');
    }

    if (this.adapter_.hasClass(OPEN)) {
      this.isOpen_ = true;
    } else {
      this.detabinate_();
      this.isOpen_ = false;
    }

    this.adapter_.registerDrawerInteractionHandler('click', this.drawerClickHandler_);
    this.adapter_.registerDrawerInteractionHandler('touchstart', this.componentTouchStartHandler_);
    this.adapter_.registerInteractionHandler('touchmove', this.componentTouchMoveHandler_);
    this.adapter_.registerInteractionHandler('touchend', this.componentTouchEndHandler_);
  };

  MDCSlidableDrawerFoundation.prototype.destroy = function destroy() {
    this.adapter_.deregisterDrawerInteractionHandler('click', this.drawerClickHandler_);
    this.adapter_.deregisterDrawerInteractionHandler('touchstart', this.componentTouchStartHandler_);
    this.adapter_.deregisterInteractionHandler('touchmove', this.componentTouchMoveHandler_);
    this.adapter_.deregisterInteractionHandler('touchend', this.componentTouchEndHandler_);
    // Deregister the document keydown handler just in case the component is destroyed while the menu is open.
    this.adapter_.deregisterDocumentKeydownHandler(this.documentKeydownHandler_);
  };

  MDCSlidableDrawerFoundation.prototype.open = function open() {
    this.adapter_.registerTransitionEndHandler(this.transitionEndHandler_);
    this.adapter_.registerDocumentKeydownHandler(this.documentKeydownHandler_);
    this.adapter_.addClass(this.animatingCssClass_);
    this.adapter_.addClass(this.openCssClass_);
    this.retabinate_();
    // Debounce multiple calls
    if (!this.isOpen_) {
      this.adapter_.notifyOpen();
    }
    this.isOpen_ = true;
  };

  MDCSlidableDrawerFoundation.prototype.close = function close() {
    this.adapter_.deregisterDocumentKeydownHandler(this.documentKeydownHandler_);
    this.adapter_.registerTransitionEndHandler(this.transitionEndHandler_);
    this.adapter_.addClass(this.animatingCssClass_);
    this.adapter_.removeClass(this.openCssClass_);
    this.detabinate_();
    // Debounce multiple calls
    if (this.isOpen_) {
      this.adapter_.notifyClose();
    }
    this.isOpen_ = false;
  };

  MDCSlidableDrawerFoundation.prototype.isOpen = function isOpen() {
    return this.isOpen_;
  };

  /**
   *  Render all children of the drawer inert when it's closed.
   */


  MDCSlidableDrawerFoundation.prototype.detabinate_ = function detabinate_() {
    if (this.inert_) {
      return;
    }

    var elements = this.adapter_.getFocusableElements();
    if (elements) {
      for (var i = 0; i < elements.length; i++) {
        this.adapter_.saveElementTabState(elements[i]);
        this.adapter_.makeElementUntabbable(elements[i]);
      }
    }

    this.inert_ = true;
  };

  /**
   *  Make all children of the drawer tabbable again when it's open.
   */


  MDCSlidableDrawerFoundation.prototype.retabinate_ = function retabinate_() {
    if (!this.inert_) {
      return;
    }

    var elements = this.adapter_.getFocusableElements();
    if (elements) {
      for (var i = 0; i < elements.length; i++) {
        this.adapter_.restoreElementTabState(elements[i]);
      }
    }

    this.inert_ = false;
  };

  MDCSlidableDrawerFoundation.prototype.handleTouchStart_ = function handleTouchStart_(evt) {
    if (!this.adapter_.hasClass(this.openCssClass_)) {
      return;
    }
    if (evt.pointerType && evt.pointerType !== 'touch') {
      return;
    }

    this.direction_ = this.adapter_.isRtl() ? -1 : 1;
    this.drawerWidth_ = this.adapter_.getDrawerWidth();
    this.startX_ = evt.touches ? evt.touches[0].pageX : evt.pageX;
    this.currentX_ = this.startX_;

    this.updateRaf_ = requestAnimationFrame(this.updateDrawer_.bind(this));
  };

  MDCSlidableDrawerFoundation.prototype.handleTouchMove_ = function handleTouchMove_(evt) {
    if (evt.pointerType && evt.pointerType !== 'touch') {
      return;
    }

    this.currentX_ = evt.touches ? evt.touches[0].pageX : evt.pageX;
  };

  MDCSlidableDrawerFoundation.prototype.handleTouchEnd_ = function handleTouchEnd_(evt) {
    if (evt.pointerType && evt.pointerType !== 'touch') {
      return;
    }

    this.prepareForTouchEnd_();

    // Did the user close the drawer by more than 50%?
    if (Math.abs(this.newPosition_ / this.drawerWidth_) >= 0.5) {
      this.close();
    } else {
      // Triggering an open here means we'll get a nice animation back to the fully open state.
      this.open();
    }
  };

  MDCSlidableDrawerFoundation.prototype.prepareForTouchEnd_ = function prepareForTouchEnd_() {
    cancelAnimationFrame(this.updateRaf_);
    this.adapter_.setTranslateX(null);
  };

  MDCSlidableDrawerFoundation.prototype.updateDrawer_ = function updateDrawer_() {
    this.updateRaf_ = requestAnimationFrame(this.updateDrawer_.bind(this));
    this.adapter_.setTranslateX(this.newPosition_);
  };

  MDCSlidableDrawerFoundation.prototype.isRootTransitioningEventTarget_ = function isRootTransitioningEventTarget_() {
    // Classes extending MDCSlidableDrawerFoundation should implement this method to return true or false
    // if the event target is the root event target currently transitioning.
    return false;
  };

  MDCSlidableDrawerFoundation.prototype.handleTransitionEnd_ = function handleTransitionEnd_(evt) {
    if (this.isRootTransitioningEventTarget_(evt.target)) {
      this.adapter_.removeClass(this.animatingCssClass_);
      this.adapter_.deregisterTransitionEndHandler(this.transitionEndHandler_);
    }
  };

  slidable_foundation__createClass(MDCSlidableDrawerFoundation, [{
    key: 'newPosition_',
    get: function get() {
      var newPos = null;

      if (this.direction_ === 1) {
        newPos = Math.min(0, this.currentX_ - this.startX_);
      } else {
        newPos = Math.max(0, this.currentX_ - this.startX_);
      }

      return newPos;
    }
  }]);

  return MDCSlidableDrawerFoundation;
}(foundation);
// CONCATENATED MODULE: ../node_modules/@material/drawer/slidable/index.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



// CONCATENATED MODULE: ../node_modules/@material/drawer/temporary/constants.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



var temporary_constants_cssClasses = {
  ROOT: 'mdc-temporary-drawer',
  OPEN: 'mdc-temporary-drawer--open',
  ANIMATING: 'mdc-temporary-drawer--animating',
  SCROLL_LOCK: 'mdc-drawer-scroll-lock'
};

var temporary_constants_strings = {
  DRAWER_SELECTOR: '.mdc-temporary-drawer__drawer',
  OPACITY_VAR_NAME: '--mdc-temporary-drawer-opacity',
  FOCUSABLE_ELEMENTS: constants_FOCUSABLE_ELEMENTS,
  OPEN_EVENT: 'MDCTemporaryDrawer:open',
  CLOSE_EVENT: 'MDCTemporaryDrawer:close'
};
// CONCATENATED MODULE: ../node_modules/@material/drawer/temporary/foundation.js
var temporary_foundation__extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var temporary_foundation__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function temporary_foundation__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function temporary_foundation__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function temporary_foundation__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */




var foundation_MDCTemporaryDrawerFoundation = function (_MDCSlidableDrawerFou) {
  temporary_foundation__inherits(MDCTemporaryDrawerFoundation, _MDCSlidableDrawerFou);

  temporary_foundation__createClass(MDCTemporaryDrawerFoundation, null, [{
    key: 'cssClasses',
    get: function get() {
      return temporary_constants_cssClasses;
    }
  }, {
    key: 'strings',
    get: function get() {
      return temporary_constants_strings;
    }
  }, {
    key: 'defaultAdapter',
    get: function get() {
      return temporary_foundation__extends(MDCSlidableDrawerFoundation.defaultAdapter, {
        addBodyClass: function addBodyClass() /* className: string */{},
        removeBodyClass: function removeBodyClass() /* className: string */{},
        isDrawer: function isDrawer() {
          return false;
        },
        updateCssVariable: function updateCssVariable() /* value: string */{}
      });
    }
  }]);

  function MDCTemporaryDrawerFoundation(adapter) {
    temporary_foundation__classCallCheck(this, MDCTemporaryDrawerFoundation);

    var _this = temporary_foundation__possibleConstructorReturn(this, _MDCSlidableDrawerFou.call(this, temporary_foundation__extends(MDCTemporaryDrawerFoundation.defaultAdapter, adapter), MDCTemporaryDrawerFoundation.cssClasses.ROOT, MDCTemporaryDrawerFoundation.cssClasses.ANIMATING, MDCTemporaryDrawerFoundation.cssClasses.OPEN));

    _this.componentClickHandler_ = function () {
      return _this.close();
    };
    return _this;
  }

  MDCTemporaryDrawerFoundation.prototype.init = function init() {
    _MDCSlidableDrawerFou.prototype.init.call(this);

    // Make browser aware of custom property being used in this element.
    // Workaround for certain types of hard-to-reproduce heisenbugs.
    this.adapter_.updateCssVariable(0);
    this.adapter_.registerInteractionHandler('click', this.componentClickHandler_);
  };

  MDCTemporaryDrawerFoundation.prototype.destroy = function destroy() {
    _MDCSlidableDrawerFou.prototype.destroy.call(this);

    this.adapter_.deregisterInteractionHandler('click', this.componentClickHandler_);
    this.enableScroll_();
  };

  MDCTemporaryDrawerFoundation.prototype.open = function open() {
    this.disableScroll_();
    // Make sure custom property values are cleared before starting.
    this.adapter_.updateCssVariable('');

    _MDCSlidableDrawerFou.prototype.open.call(this);
  };

  MDCTemporaryDrawerFoundation.prototype.close = function close() {
    // Make sure custom property values are cleared before making any changes.
    this.adapter_.updateCssVariable('');

    _MDCSlidableDrawerFou.prototype.close.call(this);
  };

  MDCTemporaryDrawerFoundation.prototype.prepareForTouchEnd_ = function prepareForTouchEnd_() {
    _MDCSlidableDrawerFou.prototype.prepareForTouchEnd_.call(this);

    this.adapter_.updateCssVariable('');
  };

  MDCTemporaryDrawerFoundation.prototype.updateDrawer_ = function updateDrawer_() {
    _MDCSlidableDrawerFou.prototype.updateDrawer_.call(this);

    var newOpacity = Math.max(0, 1 + this.direction_ * (this.newPosition_ / this.drawerWidth_));
    this.adapter_.updateCssVariable(newOpacity);
  };

  MDCTemporaryDrawerFoundation.prototype.isRootTransitioningEventTarget_ = function isRootTransitioningEventTarget_(el) {
    return this.adapter_.isDrawer(el);
  };

  MDCTemporaryDrawerFoundation.prototype.handleTransitionEnd_ = function handleTransitionEnd_(evt) {
    _MDCSlidableDrawerFou.prototype.handleTransitionEnd_.call(this, evt);
    if (!this.isOpen_) {
      this.enableScroll_();
    }
  };

  MDCTemporaryDrawerFoundation.prototype.disableScroll_ = function disableScroll_() {
    this.adapter_.addBodyClass(temporary_constants_cssClasses.SCROLL_LOCK);
  };

  MDCTemporaryDrawerFoundation.prototype.enableScroll_ = function enableScroll_() {
    this.adapter_.removeBodyClass(temporary_constants_cssClasses.SCROLL_LOCK);
  };

  return MDCTemporaryDrawerFoundation;
}(MDCSlidableDrawerFoundation);


// CONCATENATED MODULE: ../node_modules/@material/drawer/util.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var TAB_DATA = 'data-mdc-tabindex';
var TAB_DATA_HANDLED = 'data-mdc-tabindex-handled';

var storedTransformPropertyName_ = void 0;
var drawer_util_supportsPassive_ = void 0;

// Remap touch events to pointer events, if the browser doesn't support touch events.
function remapEvent(eventName) {
  var globalObj = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : window;

  if (!('ontouchstart' in globalObj.document)) {
    switch (eventName) {
      case 'touchstart':
        return 'pointerdown';
      case 'touchmove':
        return 'pointermove';
      case 'touchend':
        return 'pointerup';
      default:
        return eventName;
    }
  }

  return eventName;
}

// Choose the correct transform property to use on the current browser.
function getTransformPropertyName() {
  var globalObj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;
  var forceRefresh = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (storedTransformPropertyName_ === undefined || forceRefresh) {
    var el = globalObj.document.createElement('div');
    var transformPropertyName = 'transform' in el.style ? 'transform' : '-webkit-transform';
    storedTransformPropertyName_ = transformPropertyName;
  }

  return storedTransformPropertyName_;
}

// Determine whether the current browser supports CSS properties.
function supportsCssCustomProperties() {
  var globalObj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;

  if ('CSS' in globalObj) {
    return globalObj.CSS.supports('(--color: red)');
  }
  return false;
}

// Determine whether the current browser supports passive event listeners, and if so, use them.
function drawer_util_applyPassive() {
  var globalObj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;
  var forceRefresh = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (drawer_util_supportsPassive_ === undefined || forceRefresh) {
    var isSupported = false;
    try {
      globalObj.document.addEventListener('test', null, { get passive() {
          isSupported = true;
        } });
    } catch (e) {}

    drawer_util_supportsPassive_ = isSupported;
  }

  return drawer_util_supportsPassive_ ? { passive: true } : false;
}

// Save the tab state for an element.
function util_saveElementTabState(el) {
  if (el.hasAttribute('tabindex')) {
    el.setAttribute(TAB_DATA, el.getAttribute('tabindex'));
  }
  el.setAttribute(TAB_DATA_HANDLED, true);
}

// Restore the tab state for an element, if it was saved.
function util_restoreElementTabState(el) {
  // Only modify elements we've already handled, in case anything was dynamically added since we saved state.
  if (el.hasAttribute(TAB_DATA_HANDLED)) {
    if (el.hasAttribute(TAB_DATA)) {
      el.setAttribute('tabindex', el.getAttribute(TAB_DATA));
      el.removeAttribute(TAB_DATA);
    } else {
      el.removeAttribute('tabindex');
    }
    el.removeAttribute(TAB_DATA_HANDLED);
  }
}
// CONCATENATED MODULE: ../node_modules/@material/drawer/temporary/index.js
var temporary__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function temporary__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function temporary__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function temporary__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */








var temporary_MDCTemporaryDrawer = function (_MDCComponent) {
  temporary__inherits(MDCTemporaryDrawer, _MDCComponent);

  function MDCTemporaryDrawer() {
    temporary__classCallCheck(this, MDCTemporaryDrawer);

    return temporary__possibleConstructorReturn(this, _MDCComponent.apply(this, arguments));
  }

  MDCTemporaryDrawer.attachTo = function attachTo(root) {
    return new MDCTemporaryDrawer(root);
  };

  MDCTemporaryDrawer.prototype.getDefaultFoundation = function getDefaultFoundation() {
    var _this2 = this;

    var _MDCTemporaryDrawerFo = foundation_MDCTemporaryDrawerFoundation.strings,
        FOCUSABLE_ELEMENTS = _MDCTemporaryDrawerFo.FOCUSABLE_ELEMENTS,
        OPACITY_VAR_NAME = _MDCTemporaryDrawerFo.OPACITY_VAR_NAME;


    return new foundation_MDCTemporaryDrawerFoundation({
      addClass: function addClass(className) {
        return _this2.root_.classList.add(className);
      },
      removeClass: function removeClass(className) {
        return _this2.root_.classList.remove(className);
      },
      hasClass: function hasClass(className) {
        return _this2.root_.classList.contains(className);
      },
      addBodyClass: function addBodyClass(className) {
        return document.body.classList.add(className);
      },
      removeBodyClass: function removeBodyClass(className) {
        return document.body.classList.remove(className);
      },
      hasNecessaryDom: function hasNecessaryDom() {
        return Boolean(_this2.drawer);
      },
      registerInteractionHandler: function registerInteractionHandler(evt, handler) {
        return _this2.root_.addEventListener(remapEvent(evt), handler, drawer_util_applyPassive());
      },
      deregisterInteractionHandler: function deregisterInteractionHandler(evt, handler) {
        return _this2.root_.removeEventListener(remapEvent(evt), handler, drawer_util_applyPassive());
      },
      registerDrawerInteractionHandler: function registerDrawerInteractionHandler(evt, handler) {
        return _this2.drawer.addEventListener(remapEvent(evt), handler);
      },
      deregisterDrawerInteractionHandler: function deregisterDrawerInteractionHandler(evt, handler) {
        return _this2.drawer.removeEventListener(remapEvent(evt), handler);
      },
      registerTransitionEndHandler: function registerTransitionEndHandler(handler) {
        return _this2.drawer.addEventListener('transitionend', handler);
      },
      deregisterTransitionEndHandler: function deregisterTransitionEndHandler(handler) {
        return _this2.drawer.removeEventListener('transitionend', handler);
      },
      registerDocumentKeydownHandler: function registerDocumentKeydownHandler(handler) {
        return document.addEventListener('keydown', handler);
      },
      deregisterDocumentKeydownHandler: function deregisterDocumentKeydownHandler(handler) {
        return document.removeEventListener('keydown', handler);
      },
      getDrawerWidth: function getDrawerWidth() {
        return _this2.drawer.offsetWidth;
      },
      setTranslateX: function setTranslateX(value) {
        return _this2.drawer.style.setProperty(getTransformPropertyName(), value === null ? null : 'translateX(' + value + 'px)');
      },
      updateCssVariable: function updateCssVariable(value) {
        if (supportsCssCustomProperties()) {
          _this2.root_.style.setProperty(OPACITY_VAR_NAME, value);
        }
      },
      getFocusableElements: function getFocusableElements() {
        return _this2.drawer.querySelectorAll(FOCUSABLE_ELEMENTS);
      },
      saveElementTabState: function saveElementTabState(el) {
        return util_saveElementTabState(el);
      },
      restoreElementTabState: function restoreElementTabState(el) {
        return util_restoreElementTabState(el);
      },
      makeElementUntabbable: function makeElementUntabbable(el) {
        return el.setAttribute('tabindex', -1);
      },
      notifyOpen: function notifyOpen() {
        return _this2.emit(foundation_MDCTemporaryDrawerFoundation.strings.OPEN_EVENT);
      },
      notifyClose: function notifyClose() {
        return _this2.emit(foundation_MDCTemporaryDrawerFoundation.strings.CLOSE_EVENT);
      },
      isRtl: function isRtl() {
        return getComputedStyle(_this2.root_).getPropertyValue('direction') === 'rtl';
      },
      isDrawer: function isDrawer(el) {
        return el === _this2.drawer;
      }
    });
  };

  temporary__createClass(MDCTemporaryDrawer, [{
    key: 'open',
    get: function get() {
      return this.foundation_.isOpen();
    },
    set: function set(value) {
      if (value) {
        this.foundation_.open();
      } else {
        this.foundation_.close();
      }
    }

    /* Return the drawer element inside the component. */

  }, {
    key: 'drawer',
    get: function get() {
      return this.root_.querySelector(foundation_MDCTemporaryDrawerFoundation.strings.DRAWER_SELECTOR);
    }
  }]);

  return MDCTemporaryDrawer;
}(component);
// CONCATENATED MODULE: ../node_modules/@material/drawer/persistent/constants.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



var persistent_constants_cssClasses = {
  ROOT: 'mdc-persistent-drawer',
  OPEN: 'mdc-persistent-drawer--open',
  ANIMATING: 'mdc-persistent-drawer--animating'
};

var persistent_constants_strings = {
  DRAWER_SELECTOR: '.mdc-persistent-drawer__drawer',
  FOCUSABLE_ELEMENTS: constants_FOCUSABLE_ELEMENTS,
  OPEN_EVENT: 'MDCPersistentDrawer:open',
  CLOSE_EVENT: 'MDCPersistentDrawer:close'
};
// CONCATENATED MODULE: ../node_modules/@material/drawer/persistent/foundation.js
var persistent_foundation__extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var persistent_foundation__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function persistent_foundation__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function persistent_foundation__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function persistent_foundation__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */




var foundation_MDCPersistentDrawerFoundation = function (_MDCSlidableDrawerFou) {
  persistent_foundation__inherits(MDCPersistentDrawerFoundation, _MDCSlidableDrawerFou);

  persistent_foundation__createClass(MDCPersistentDrawerFoundation, null, [{
    key: 'cssClasses',
    get: function get() {
      return persistent_constants_cssClasses;
    }
  }, {
    key: 'strings',
    get: function get() {
      return persistent_constants_strings;
    }
  }, {
    key: 'defaultAdapter',
    get: function get() {
      return persistent_foundation__extends(MDCSlidableDrawerFoundation.defaultAdapter, {
        isDrawer: function isDrawer() {
          return false;
        }
      });
    }
  }]);

  function MDCPersistentDrawerFoundation(adapter) {
    persistent_foundation__classCallCheck(this, MDCPersistentDrawerFoundation);

    return persistent_foundation__possibleConstructorReturn(this, _MDCSlidableDrawerFou.call(this, persistent_foundation__extends(MDCPersistentDrawerFoundation.defaultAdapter, adapter), MDCPersistentDrawerFoundation.cssClasses.ROOT, MDCPersistentDrawerFoundation.cssClasses.ANIMATING, MDCPersistentDrawerFoundation.cssClasses.OPEN));
  }

  MDCPersistentDrawerFoundation.prototype.isRootTransitioningEventTarget_ = function isRootTransitioningEventTarget_(el) {
    return this.adapter_.isDrawer(el);
  };

  return MDCPersistentDrawerFoundation;
}(MDCSlidableDrawerFoundation);


// CONCATENATED MODULE: ../node_modules/@material/drawer/persistent/index.js
var persistent__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function persistent__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function persistent__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function persistent__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */








var persistent_MDCPersistentDrawer = function (_MDCComponent) {
  persistent__inherits(MDCPersistentDrawer, _MDCComponent);

  function MDCPersistentDrawer() {
    persistent__classCallCheck(this, MDCPersistentDrawer);

    return persistent__possibleConstructorReturn(this, _MDCComponent.apply(this, arguments));
  }

  MDCPersistentDrawer.attachTo = function attachTo(root) {
    return new MDCPersistentDrawer(root);
  };

  MDCPersistentDrawer.prototype.getDefaultFoundation = function getDefaultFoundation() {
    var _this2 = this;

    var FOCUSABLE_ELEMENTS = foundation_MDCPersistentDrawerFoundation.strings.FOCUSABLE_ELEMENTS;


    return new foundation_MDCPersistentDrawerFoundation({
      addClass: function addClass(className) {
        return _this2.root_.classList.add(className);
      },
      removeClass: function removeClass(className) {
        return _this2.root_.classList.remove(className);
      },
      hasClass: function hasClass(className) {
        return _this2.root_.classList.contains(className);
      },
      hasNecessaryDom: function hasNecessaryDom() {
        return Boolean(_this2.drawer);
      },
      registerInteractionHandler: function registerInteractionHandler(evt, handler) {
        return _this2.root_.addEventListener(remapEvent(evt), handler, drawer_util_applyPassive());
      },
      deregisterInteractionHandler: function deregisterInteractionHandler(evt, handler) {
        return _this2.root_.removeEventListener(remapEvent(evt), handler, drawer_util_applyPassive());
      },
      registerDrawerInteractionHandler: function registerDrawerInteractionHandler(evt, handler) {
        return _this2.drawer.addEventListener(remapEvent(evt), handler);
      },
      deregisterDrawerInteractionHandler: function deregisterDrawerInteractionHandler(evt, handler) {
        return _this2.drawer.removeEventListener(remapEvent(evt), handler);
      },
      registerTransitionEndHandler: function registerTransitionEndHandler(handler) {
        return _this2.root_.addEventListener('transitionend', handler);
      },
      deregisterTransitionEndHandler: function deregisterTransitionEndHandler(handler) {
        return _this2.root_.removeEventListener('transitionend', handler);
      },
      registerDocumentKeydownHandler: function registerDocumentKeydownHandler(handler) {
        return document.addEventListener('keydown', handler);
      },
      deregisterDocumentKeydownHandler: function deregisterDocumentKeydownHandler(handler) {
        return document.removeEventListener('keydown', handler);
      },
      getDrawerWidth: function getDrawerWidth() {
        return _this2.drawer.offsetWidth;
      },
      setTranslateX: function setTranslateX(value) {
        return _this2.drawer.style.setProperty(getTransformPropertyName(), value === null ? null : 'translateX(' + value + 'px)');
      },
      getFocusableElements: function getFocusableElements() {
        return _this2.drawer.querySelectorAll(FOCUSABLE_ELEMENTS);
      },
      saveElementTabState: function saveElementTabState(el) {
        return util_saveElementTabState(el);
      },
      restoreElementTabState: function restoreElementTabState(el) {
        return util_restoreElementTabState(el);
      },
      makeElementUntabbable: function makeElementUntabbable(el) {
        return el.setAttribute('tabindex', -1);
      },
      notifyOpen: function notifyOpen() {
        return _this2.emit(foundation_MDCPersistentDrawerFoundation.strings.OPEN_EVENT);
      },
      notifyClose: function notifyClose() {
        return _this2.emit(foundation_MDCPersistentDrawerFoundation.strings.CLOSE_EVENT);
      },
      isRtl: function isRtl() {
        return getComputedStyle(_this2.root_).getPropertyValue('direction') === 'rtl';
      },
      isDrawer: function isDrawer(el) {
        return el === _this2.drawer;
      }
    });
  };

  persistent__createClass(MDCPersistentDrawer, [{
    key: 'open',
    get: function get() {
      return this.foundation_.isOpen();
    },
    set: function set(value) {
      if (value) {
        this.foundation_.open();
      } else {
        this.foundation_.close();
      }
    }

    // Return the drawer element inside the component.

  }, {
    key: 'drawer',
    get: function get() {
      return this.root_.querySelector(foundation_MDCPersistentDrawerFoundation.strings.DRAWER_SELECTOR);
    }
  }]);

  return MDCPersistentDrawer;
}(component);
// CONCATENATED MODULE: ../node_modules/preact-material-components/List/index.js
function List__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function List__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function List__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var List__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};




/**
 * @prop dense = false
 * @prop two-line = false
 * @prop interactive = false
 */

var List_List = function (_MaterialComponent) {
  List__inherits(List, _MaterialComponent);

  function List() {
    List__classCallCheck(this, List);

    var _this = List__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "list";
    _this._mdcProps = ["dense", "two-line"];
    return _this;
  }

  List.prototype.materialDom = function materialDom(props) {
    var _this2 = this;

    if (props.interactive) {
      return Object(preact_min["h"])("nav", List__extends({ ref: function ref(control) {
          return _this2.control = control;
        } }, props), props.children);
    }

    return Object(preact_min["h"])("ul", List__extends({}, props, { ref: function ref(control) {
        return _this2.control = control;
      } }), props.children);
  };

  return List;
}(MaterialComponent_MaterialComponent);

var List_ListItem = function (_MaterialComponent2) {
  List__inherits(ListItem, _MaterialComponent2);

  function ListItem() {
    List__classCallCheck(this, ListItem);

    var _this3 = List__possibleConstructorReturn(this, _MaterialComponent2.call(this));

    _this3.componentName = "list-item";
    return _this3;
  }

  ListItem.prototype.materialDom = function materialDom(props) {
    var _this4 = this;

    return Object(preact_min["h"])("li", List__extends({ role: "option" }, props, { ref: function ref(control) {
        return _this4.control = control;
      } }), props.children);
  };

  return ListItem;
}(MaterialComponent_MaterialComponent);

var List_LinkItem = function (_MaterialComponent3) {
  List__inherits(LinkItem, _MaterialComponent3);

  function LinkItem() {
    List__classCallCheck(this, LinkItem);

    var _this5 = List__possibleConstructorReturn(this, _MaterialComponent3.call(this));

    _this5.componentName = "list-item";
    return _this5;
  }

  LinkItem.prototype.componentDidMount = function componentDidMount() {
    _MaterialComponent3.prototype.attachRipple.call(this);
  };

  LinkItem.prototype.materialDom = function materialDom(props) {
    var _this6 = this;

    return Object(preact_min["h"])("a", List__extends({ role: "option" }, props, { ref: function ref(control) {
        return _this6.control = control;
      } }), props.children);
  };

  return LinkItem;
}(MaterialComponent_MaterialComponent);

/**
 * @prop start-detail = true
 * @prop end-detail = false
 */


var List_ListItemIcon = function (_MaterialComponent4) {
  List__inherits(ListItemIcon, _MaterialComponent4);

  function ListItemIcon() {
    List__classCallCheck(this, ListItemIcon);

    var _this7 = List__possibleConstructorReturn(this, _MaterialComponent4.call(this));

    _this7.componentName = "mdc-list-item__icon";
    return _this7;
  }

  ListItemIcon.prototype.getProxyClassName = function getProxyClassName(props) {
    var classNames = [];

    // default behavior
    props["start-detail"] = props["start-detail"] || true;

    // setting class names mutually exclusive
    if (props["end-detail"]) {
      classNames.push("mdc-list-item__end-detail");
    } else if (props["start-detail"]) {
      classNames.push("mdc-list-item__start-detail");
    }
    return classNames.join(" ");
  };

  ListItemIcon.prototype.materialDom = function materialDom(props) {
    var _this8 = this;

    var className = "material-icons " + this.getProxyClassName(props);
    return Object(preact_min["h"])("i", List__extends({
      className: className,
      "aria-hidden": "true"
    }, props, {
      ref: function ref(control) {
        return _this8.control = control;
      }
    }), props.children);
  };

  return ListItemIcon;
}(MaterialComponent_MaterialComponent);

/**
 * @prop start-detail = true
 * @prop end-detail = false
 */


var List_ListItemAvatar = function (_ListItemIcon) {
  List__inherits(ListItemAvatar, _ListItemIcon);

  function ListItemAvatar() {
    List__classCallCheck(this, ListItemAvatar);

    var _this9 = List__possibleConstructorReturn(this, _ListItemIcon.call(this));

    _this9.componentName = "mdc-list-item__avatar";
    return _this9;
  }

  ListItemAvatar.prototype.materialDom = function materialDom(props) {
    var _this10 = this;

    return Object(preact_min["h"])("img", List__extends({}, props, {
      className: _ListItemIcon.prototype.getProxyClassName.call(this, props)
    }, props, {
      ref: function ref(control) {
        return _this10.control = control;
      },
      width: props.width || "56",
      height: props.height || "56",
      alt: props.alt || ""
    }));
  };

  return ListItemAvatar;
}(List_ListItemIcon);

var List_ListDivider = function (_MaterialComponent5) {
  List__inherits(ListDivider, _MaterialComponent5);

  function ListDivider() {
    List__classCallCheck(this, ListDivider);

    var _this11 = List__possibleConstructorReturn(this, _MaterialComponent5.call(this));

    _this11.componentName = "list-divider";
    _this11._mdcProps = ["inset"];
    return _this11;
  }

  ListDivider.prototype.materialDom = function materialDom(props) {
    var _this12 = this;

    return Object(preact_min["h"])("li", List__extends({
      role: "separator"
    }, props, {
      ref: function ref(control) {
        return _this12.control = control;
      }
    }));
  };

  return ListDivider;
}(MaterialComponent_MaterialComponent);

var List_ListTextContainer = function (_MaterialComponent6) {
  List__inherits(ListTextContainer, _MaterialComponent6);

  function ListTextContainer() {
    List__classCallCheck(this, ListTextContainer);

    var _this13 = List__possibleConstructorReturn(this, _MaterialComponent6.call(this));

    _this13.componentName = "list-item__text";
    return _this13;
  }

  ListTextContainer.prototype.materialDom = function materialDom(props) {
    var _this14 = this;

    return Object(preact_min["h"])("span", List__extends({}, props, { ref: function ref(control) {
        return _this14.control = control;
      } }), props.children);
  };

  return ListTextContainer;
}(MaterialComponent_MaterialComponent);

var ListPrimaryText = function (_ListTextContainer) {
  List__inherits(ListPrimaryText, _ListTextContainer);

  function ListPrimaryText() {
    List__classCallCheck(this, ListPrimaryText);

    var _this15 = List__possibleConstructorReturn(this, _ListTextContainer.call(this));

    _this15.componentName = "list-item__text__primary";
    return _this15;
  }

  return ListPrimaryText;
}(List_ListTextContainer);

var ListSecondaryText = function (_ListTextContainer2) {
  List__inherits(ListSecondaryText, _ListTextContainer2);

  function ListSecondaryText() {
    List__classCallCheck(this, ListSecondaryText);

    var _this16 = List__possibleConstructorReturn(this, _ListTextContainer2.call(this));

    _this16.componentName = "list-item__text__secondary";
    return _this16;
  }

  return ListSecondaryText;
}(List_ListTextContainer);

List_List.Item = List_ListItem;
List_List.LinkItem = List_LinkItem;
List_List.ItemIcon = List_ListItemIcon;
List_List.ItemAvatar = List_ListItemAvatar;
List_List.Divider = List_ListDivider;
List_List.TextContainer = List_ListTextContainer;
List_List.PrimaryText = ListPrimaryText;
List_List.SecondaryText = ListSecondaryText;

/* harmony default export */ var preact_material_components_List = (List_List);
// CONCATENATED MODULE: ../node_modules/preact-material-components/Drawer/index.js
function Drawer__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Drawer__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function Drawer__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Drawer__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};







/*
 * Default props for drawers
 */
var defaultProps = {
  open: false
};

var Drawer_TemporaryDrawer = function (_MaterialComponent) {
  Drawer__inherits(TemporaryDrawer, _MaterialComponent);

  function TemporaryDrawer() {
    Drawer__classCallCheck(this, TemporaryDrawer);

    var _this = Drawer__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "temporary-drawer";
    _this._open = _this._open.bind(_this);
    _this._close = _this._close.bind(_this);
    return _this;
  }

  TemporaryDrawer.prototype._open = function _open() {
    if (this.props.onOpen) {
      this.props.onOpen();
    }
  };

  TemporaryDrawer.prototype._close = function _close() {
    if (this.props.onClose) {
      this.props.onClose();
    }
  };

  TemporaryDrawer.prototype.componentDidMount = function componentDidMount() {
    this.MDComponent = temporary_MDCTemporaryDrawer.attachTo(this.control);
    this.MDComponent.listen("MDCTemporaryDrawer:open", this._open);
    this.MDComponent.listen("MDCTemporaryDrawer:close", this._close);
    toggleDrawer(defaultProps, this.props, this.MDComponent);
  };

  TemporaryDrawer.prototype.componentWillUnmount = function componentWillUnmount() {
    this.MDComponent.unlisten("MDCTemporaryDrawer:close", this._close);
    this.MDComponent.unlisten("MDCTemporaryDrawer:open", this._open);
    this.MDComponent.destroy && this.MDComponent.destroy();
  };

  TemporaryDrawer.prototype.componentWillUpdate = function componentWillUpdate(nextProps) {
    toggleDrawer(this.props, nextProps, this.MDComponent);
  };

  TemporaryDrawer.prototype.materialDom = function materialDom(props) {
    var _this2 = this;

    return Object(preact_min["h"])("aside", Drawer__extends({
      className: "mdc-typography",
      ref: function ref(control) {
        _this2.control = control;
      }
    }, props), Object(preact_min["h"])("nav", { className: "mdc-temporary-drawer__drawer" }, props.children));
  };

  return TemporaryDrawer;
}(MaterialComponent_MaterialComponent);

var Drawer_TemporaryDrawerHeader = function (_MaterialComponent2) {
  Drawer__inherits(TemporaryDrawerHeader, _MaterialComponent2);

  function TemporaryDrawerHeader() {
    Drawer__classCallCheck(this, TemporaryDrawerHeader);

    var _this3 = Drawer__possibleConstructorReturn(this, _MaterialComponent2.call(this));

    _this3.componentName = "temporary-drawer__header";
    return _this3;
  }

  TemporaryDrawerHeader.prototype.materialDom = function materialDom(props) {
    var _this4 = this;

    return Object(preact_min["h"])("header", Drawer__extends({
      ref: function ref(control) {
        _this4.control = control;
      }
    }, props), Object(preact_min["h"])("div", { className: "mdc-temporary-drawer__header-content" }, props.children));
  };

  return TemporaryDrawerHeader;
}(MaterialComponent_MaterialComponent);

var Drawer_TemporaryDrawerContent = function (_MaterialComponent3) {
  Drawer__inherits(TemporaryDrawerContent, _MaterialComponent3);

  function TemporaryDrawerContent() {
    Drawer__classCallCheck(this, TemporaryDrawerContent);

    var _this5 = Drawer__possibleConstructorReturn(this, _MaterialComponent3.call(this));

    _this5.componentName = "temporary-drawer__content";
    return _this5;
  }

  TemporaryDrawerContent.prototype.materialDom = function materialDom(props) {
    var _this6 = this;

    return Object(preact_min["h"])("nav", Drawer__extends({
      className: "mdc-list",
      ref: function ref(control) {
        _this6.control = control;
      }
    }, props), props.children);
  };

  return TemporaryDrawerContent;
}(MaterialComponent_MaterialComponent);

/**
 * @prop spacer = false
 */


var Drawer_PermanentDrawer = function (_MaterialComponent4) {
  Drawer__inherits(PermanentDrawer, _MaterialComponent4);

  function PermanentDrawer() {
    Drawer__classCallCheck(this, PermanentDrawer);

    var _this7 = Drawer__possibleConstructorReturn(this, _MaterialComponent4.call(this));

    _this7.componentName = "permanent-drawer";
    return _this7;
  }

  PermanentDrawer.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("nav", Drawer__extends({ className: "mdc-typography" }, props), props.spacer && Object(preact_min["h"])("div", { className: "mdc-permanent-drawer__toolbar-spacer" }), Object(preact_min["h"])("div", { className: "mdc-permanent-drawer__content" }, Object(preact_min["h"])("nav", { className: "mdc-list" }, props.children)));
  };

  return PermanentDrawer;
}(MaterialComponent_MaterialComponent);

var Drawer_PermanentDrawerHeader = function (_MaterialComponent5) {
  Drawer__inherits(PermanentDrawerHeader, _MaterialComponent5);

  function PermanentDrawerHeader() {
    Drawer__classCallCheck(this, PermanentDrawerHeader);

    var _this8 = Drawer__possibleConstructorReturn(this, _MaterialComponent5.call(this));

    _this8.componentName = "permanent-drawer__header";
    return _this8;
  }

  PermanentDrawerHeader.prototype.materialDom = function materialDom(props) {
    var _this9 = this;

    return Object(preact_min["h"])("header", Drawer__extends({
      ref: function ref(control) {
        _this9.control = control;
      }
    }, props), Object(preact_min["h"])("div", { className: "mdc-permanent-drawer__header-content" }, props.children));
  };

  return PermanentDrawerHeader;
}(MaterialComponent_MaterialComponent);

var PermanentDrawerContent = function (_TemporaryDrawerConte) {
  Drawer__inherits(PermanentDrawerContent, _TemporaryDrawerConte);

  function PermanentDrawerContent() {
    Drawer__classCallCheck(this, PermanentDrawerContent);

    var _this10 = Drawer__possibleConstructorReturn(this, _TemporaryDrawerConte.call(this));

    _this10.componentName = "permanent-drawer__content";
    return _this10;
  }

  return PermanentDrawerContent;
}(Drawer_TemporaryDrawerContent);

var Drawer_PersistentDrawer = function (_MaterialComponent6) {
  Drawer__inherits(PersistentDrawer, _MaterialComponent6);

  function PersistentDrawer() {
    Drawer__classCallCheck(this, PersistentDrawer);

    var _this11 = Drawer__possibleConstructorReturn(this, _MaterialComponent6.call(this));

    _this11.componentName = "persistent-drawer";
    _this11._open = _this11._open.bind(_this11);
    _this11._close = _this11._close.bind(_this11);
    return _this11;
  }

  PersistentDrawer.prototype._open = function _open() {
    if (this.props.onOpen) {
      this.props.onOpen();
    }
  };

  PersistentDrawer.prototype._close = function _close() {
    if (this.props.onClose) {
      this.props.onClose();
    }
  };

  PersistentDrawer.prototype.componentDidMount = function componentDidMount() {
    this.MDComponent = persistent_MDCPersistentDrawer.attachTo(this.control);
    this.MDComponent.listen("MDCPersistentDrawer:open", this._open);
    this.MDComponent.listen("MDCPersistentDrawer:close", this._close);
    toggleDrawer(defaultProps, this.props, this.MDComponent);
  };

  PersistentDrawer.prototype.componentWillUnmount = function componentWillUnmount() {
    this.MDComponent.unlisten("MDCPersistentDrawer:close", this._close);
    this.MDComponent.unlisten("MDCPersistentDrawer:open", this._open);
    this.MDComponent.destroy && this.MDComponent.destroy();
  };

  PersistentDrawer.prototype.componentWillUpdate = function componentWillUpdate(nextProps) {
    toggleDrawer(this.props, nextProps, this.MDComponent);
  };

  PersistentDrawer.prototype.materialDom = function materialDom(props) {
    var _this12 = this;

    return Object(preact_min["h"])("aside", Drawer__extends({
      className: "mdc-typography",
      ref: function ref(control) {
        _this12.control = control;
      }
    }, props), Object(preact_min["h"])("nav", { className: "mdc-persistent-drawer__drawer" }));
  };

  return PersistentDrawer;
}(MaterialComponent_MaterialComponent);

var Drawer_PersistentDrawerHeader = function (_MaterialComponent7) {
  Drawer__inherits(PersistentDrawerHeader, _MaterialComponent7);

  function PersistentDrawerHeader() {
    Drawer__classCallCheck(this, PersistentDrawerHeader);

    var _this13 = Drawer__possibleConstructorReturn(this, _MaterialComponent7.call(this));

    _this13.componentName = "persistent-drawer__header";
    return _this13;
  }

  PersistentDrawerHeader.prototype.materialDom = function materialDom(props) {
    var _this14 = this;

    return Object(preact_min["h"])("header", Drawer__extends({
      ref: function ref(control) {
        _this14.control = control;
      }
    }, props), Object(preact_min["h"])("div", { className: "mdc-persistent-drawer__header-content" }, props.children));
  };

  return PersistentDrawerHeader;
}(MaterialComponent_MaterialComponent);

var PersistentDrawerContent = function (_TemporaryDrawerConte2) {
  Drawer__inherits(PersistentDrawerContent, _TemporaryDrawerConte2);

  function PersistentDrawerContent() {
    Drawer__classCallCheck(this, PersistentDrawerContent);

    var _this15 = Drawer__possibleConstructorReturn(this, _TemporaryDrawerConte2.call(this));

    _this15.componentName = "persistent-drawer__content";
    return _this15;
  }

  return PersistentDrawerContent;
}(Drawer_TemporaryDrawerContent);

/**
 * @prop selected = false
 */


var DrawerItem = function (_List$LinkItem) {
  Drawer__inherits(DrawerItem, _List$LinkItem);

  function DrawerItem() {
    Drawer__classCallCheck(this, DrawerItem);

    return Drawer__possibleConstructorReturn(this, _List$LinkItem.call(this));
  }

  DrawerItem.prototype.materialDom = function materialDom(props) {
    var returnedNode = _List$LinkItem.prototype.materialDom.call(this, props);
    /* Logic to add selected class */
    if (props.selected) {
      returnedNode.attributes["className"] = "mdc-temporary-drawer--selected mdc-permanent-drawer--selected";
    }
    return returnedNode;
  };

  return DrawerItem;
}(preact_material_components_List.LinkItem);

/*
 * Function to add declarative opening/closing to drawer
 */


function toggleDrawer(oldprops, newprops, drawer) {
  if ("open" in oldprops && "open" in newprops && oldprops.open !== newprops.open) {
    drawer.open = newprops.open;
  }
}

var Drawer = {};

Drawer.DrawerItem = DrawerItem;
Drawer.TemporaryDrawer = Drawer_TemporaryDrawer;
Drawer.TemporaryDrawerHeader = Drawer_TemporaryDrawerHeader;
Drawer.TemporaryDrawerContent = Drawer_TemporaryDrawerContent;
Drawer.PermanentDrawer = Drawer_PermanentDrawer;
Drawer.PermanentDrawerHeader = Drawer_PermanentDrawerHeader;
Drawer.PermanentDrawerContent = PermanentDrawerContent;
Drawer.PersistentDrawer = Drawer_PersistentDrawer;
Drawer.PersistentDrawerHeader = Drawer_PersistentDrawerHeader;
Drawer.PersistentDrawerContent = PersistentDrawerContent;

/* harmony default export */ var preact_material_components_Drawer = (Drawer);
// CONCATENATED MODULE: ../node_modules/@material/dialog/constants.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var dialog_constants_cssClasses = {
  ROOT: 'mdc-dialog',
  OPEN: 'mdc-dialog--open',
  ANIMATING: 'mdc-dialog--animating',
  BACKDROP: 'mdc-dialog__backdrop',
  SCROLL_LOCK: 'mdc-dialog-scroll-lock',
  ACCEPT_BTN: 'mdc-dialog__footer__button--accept',
  CANCEL_BTN: 'mdc-dialog__footer__button--cancel'
};

var dialog_constants_strings = {
  OPEN_DIALOG_SELECTOR: '.mdc-dialog--open',
  DIALOG_SURFACE_SELECTOR: '.mdc-dialog__surface',
  ACCEPT_SELECTOR: '.mdc-dialog__footer__button--accept',
  ACCEPT_EVENT: 'MDCDialog:accept',
  CANCEL_EVENT: 'MDCDialog:cancel'
};
// CONCATENATED MODULE: ../node_modules/@material/dialog/foundation.js
var dialog_foundation__extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var dialog_foundation__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function dialog_foundation__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function dialog_foundation__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function dialog_foundation__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */




var foundation_MDCDialogFoundation = function (_MDCFoundation) {
  dialog_foundation__inherits(MDCDialogFoundation, _MDCFoundation);

  dialog_foundation__createClass(MDCDialogFoundation, null, [{
    key: 'cssClasses',
    get: function get() {
      return dialog_constants_cssClasses;
    }
  }, {
    key: 'strings',
    get: function get() {
      return dialog_constants_strings;
    }
  }, {
    key: 'defaultAdapter',
    get: function get() {
      return {
        addClass: function addClass() /* className: string */{},
        removeClass: function removeClass() /* className: string */{},
        addBodyClass: function addBodyClass() /* className: string */{},
        removeBodyClass: function removeBodyClass() /* className: string */{},
        eventTargetHasClass: function eventTargetHasClass() {
          return (/* target: EventTarget, className: string */ /* boolean */false
          );
        },
        registerInteractionHandler: function registerInteractionHandler() /* evt: string, handler: EventListener */{},
        deregisterInteractionHandler: function deregisterInteractionHandler() /* evt: string, handler: EventListener */{},
        registerSurfaceInteractionHandler: function registerSurfaceInteractionHandler() /* evt: string, handler: EventListener */{},
        deregisterSurfaceInteractionHandler: function deregisterSurfaceInteractionHandler() /* evt: string, handler: EventListener */{},
        registerDocumentKeydownHandler: function registerDocumentKeydownHandler() /* handler: EventListener */{},
        deregisterDocumentKeydownHandler: function deregisterDocumentKeydownHandler() /* handler: EventListener */{},
        registerTransitionEndHandler: function registerTransitionEndHandler() /* handler: EventListener */{},
        deregisterTransitionEndHandler: function deregisterTransitionEndHandler() /* handler: EventListener */{},
        notifyAccept: function notifyAccept() {},
        notifyCancel: function notifyCancel() {},
        trapFocusOnSurface: function trapFocusOnSurface() {},
        untrapFocusOnSurface: function untrapFocusOnSurface() {},
        isDialog: function isDialog() {
          return (/* el: Element */ /* boolean */false
          );
        },
        layoutFooterRipples: function layoutFooterRipples() {}
      };
    }
  }]);

  function MDCDialogFoundation(adapter) {
    dialog_foundation__classCallCheck(this, MDCDialogFoundation);

    var _this = dialog_foundation__possibleConstructorReturn(this, _MDCFoundation.call(this, dialog_foundation__extends(MDCDialogFoundation.defaultAdapter, adapter)));

    _this.isOpen_ = false;
    _this.componentClickHandler_ = function (evt) {
      if (_this.adapter_.eventTargetHasClass(evt.target, dialog_constants_cssClasses.BACKDROP)) {
        _this.cancel(true);
      }
    };
    _this.dialogClickHandler_ = function (evt) {
      return _this.handleDialogClick_(evt);
    };
    _this.documentKeydownHandler_ = function (evt) {
      if (evt.key && evt.key === 'Escape' || evt.keyCode === 27) {
        _this.cancel(true);
      }
    };
    _this.transitionEndHandler_ = function (evt) {
      return _this.handleTransitionEnd_(evt);
    };
    return _this;
  }

  MDCDialogFoundation.prototype.destroy = function destroy() {
    // Ensure that dialog is cleaned up when destroyed
    if (this.isOpen_) {
      this.adapter_.deregisterSurfaceInteractionHandler('click', this.dialogClickHandler_);
      this.adapter_.deregisterDocumentKeydownHandler(this.documentKeydownHandler_);
      this.adapter_.deregisterInteractionHandler('click', this.componentClickHandler_);
      this.adapter_.untrapFocusOnSurface();
      this.adapter_.deregisterTransitionEndHandler(this.transitionEndHandler_);
      this.adapter_.removeClass(MDCDialogFoundation.cssClasses.ANIMATING);
      this.adapter_.removeClass(MDCDialogFoundation.cssClasses.OPEN);
      this.enableScroll_();
    }
  };

  MDCDialogFoundation.prototype.open = function open() {
    this.isOpen_ = true;
    this.disableScroll_();
    this.adapter_.registerDocumentKeydownHandler(this.documentKeydownHandler_);
    this.adapter_.registerSurfaceInteractionHandler('click', this.dialogClickHandler_);
    this.adapter_.registerInteractionHandler('click', this.componentClickHandler_);
    this.adapter_.registerTransitionEndHandler(this.transitionEndHandler_);
    this.adapter_.addClass(MDCDialogFoundation.cssClasses.ANIMATING);
    this.adapter_.addClass(MDCDialogFoundation.cssClasses.OPEN);
  };

  MDCDialogFoundation.prototype.close = function close() {
    this.isOpen_ = false;
    this.adapter_.deregisterSurfaceInteractionHandler('click', this.dialogClickHandler_);
    this.adapter_.deregisterDocumentKeydownHandler(this.documentKeydownHandler_);
    this.adapter_.deregisterInteractionHandler('click', this.componentClickHandler_);
    this.adapter_.untrapFocusOnSurface();
    this.adapter_.registerTransitionEndHandler(this.transitionEndHandler_);
    this.adapter_.addClass(MDCDialogFoundation.cssClasses.ANIMATING);
    this.adapter_.removeClass(MDCDialogFoundation.cssClasses.OPEN);
  };

  MDCDialogFoundation.prototype.isOpen = function isOpen() {
    return this.isOpen_;
  };

  MDCDialogFoundation.prototype.accept = function accept(shouldNotify) {
    if (shouldNotify) {
      this.adapter_.notifyAccept();
    }

    this.close();
  };

  MDCDialogFoundation.prototype.cancel = function cancel(shouldNotify) {
    if (shouldNotify) {
      this.adapter_.notifyCancel();
    }

    this.close();
  };

  MDCDialogFoundation.prototype.handleDialogClick_ = function handleDialogClick_(evt) {
    var target = evt.target;

    if (this.adapter_.eventTargetHasClass(target, dialog_constants_cssClasses.ACCEPT_BTN)) {
      this.accept(true);
    } else if (this.adapter_.eventTargetHasClass(target, dialog_constants_cssClasses.CANCEL_BTN)) {
      this.cancel(true);
    }
  };

  MDCDialogFoundation.prototype.handleTransitionEnd_ = function handleTransitionEnd_(evt) {
    if (this.adapter_.isDialog(evt.target)) {
      this.adapter_.deregisterTransitionEndHandler(this.transitionEndHandler_);
      this.adapter_.removeClass(MDCDialogFoundation.cssClasses.ANIMATING);
      if (this.isOpen_) {
        this.adapter_.trapFocusOnSurface();
        this.adapter_.layoutFooterRipples();
      } else {
        this.enableScroll_();
      };
    };
  };

  MDCDialogFoundation.prototype.disableScroll_ = function disableScroll_() {
    this.adapter_.addBodyClass(dialog_constants_cssClasses.SCROLL_LOCK);
  };

  MDCDialogFoundation.prototype.enableScroll_ = function enableScroll_() {
    this.adapter_.removeBodyClass(dialog_constants_cssClasses.SCROLL_LOCK);
  };

  return MDCDialogFoundation;
}(foundation);


// EXTERNAL MODULE: ../node_modules/focus-trap/index.js
var focus_trap = __webpack_require__("ySUw");
var focus_trap_default = /*#__PURE__*/__webpack_require__.n(focus_trap);

// CONCATENATED MODULE: ../node_modules/@material/dialog/util.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



function createFocusTrapInstance(surfaceEl, acceptButtonEl) {
  var focusTrapFactory = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : focus_trap_default.a;

  return focusTrapFactory(surfaceEl, {
    initialFocus: acceptButtonEl,
    clickOutsideDeactivates: true
  });
}
// CONCATENATED MODULE: ../node_modules/@material/dialog/index.js
var dialog__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function dialog__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function dialog__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function dialog__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */










var dialog_MDCDialog = function (_MDCComponent) {
  dialog__inherits(MDCDialog, _MDCComponent);

  function MDCDialog() {
    dialog__classCallCheck(this, MDCDialog);

    return dialog__possibleConstructorReturn(this, _MDCComponent.apply(this, arguments));
  }

  MDCDialog.attachTo = function attachTo(root) {
    return new MDCDialog(root);
  };

  MDCDialog.prototype.initialize = function initialize() {
    this.focusTrap_ = createFocusTrapInstance(this.dialogSurface_, this.acceptButton_);
    this.footerBtnRipples_ = [];

    var footerBtns = this.root_.querySelectorAll('.mdc-dialog__footer__button');
    for (var i = 0, footerBtn; footerBtn = footerBtns[i]; i++) {
      this.footerBtnRipples_.push(new ripple_MDCRipple(footerBtn));
    }
  };

  MDCDialog.prototype.destroy = function destroy() {
    this.footerBtnRipples_.forEach(function (ripple) {
      return ripple.destroy();
    });
    _MDCComponent.prototype.destroy.call(this);
  };

  MDCDialog.prototype.show = function show() {
    this.foundation_.open();
  };

  MDCDialog.prototype.close = function close() {
    this.foundation_.close();
  };

  MDCDialog.prototype.getDefaultFoundation = function getDefaultFoundation() {
    var _this2 = this;

    return new foundation_MDCDialogFoundation({
      addClass: function addClass(className) {
        return _this2.root_.classList.add(className);
      },
      removeClass: function removeClass(className) {
        return _this2.root_.classList.remove(className);
      },
      addBodyClass: function addBodyClass(className) {
        return document.body.classList.add(className);
      },
      removeBodyClass: function removeBodyClass(className) {
        return document.body.classList.remove(className);
      },
      eventTargetHasClass: function eventTargetHasClass(target, className) {
        return target.classList.contains(className);
      },
      registerInteractionHandler: function registerInteractionHandler(evt, handler) {
        return _this2.root_.addEventListener(evt, handler);
      },
      deregisterInteractionHandler: function deregisterInteractionHandler(evt, handler) {
        return _this2.root_.removeEventListener(evt, handler);
      },
      registerSurfaceInteractionHandler: function registerSurfaceInteractionHandler(evt, handler) {
        return _this2.dialogSurface_.addEventListener(evt, handler);
      },
      deregisterSurfaceInteractionHandler: function deregisterSurfaceInteractionHandler(evt, handler) {
        return _this2.dialogSurface_.removeEventListener(evt, handler);
      },
      registerDocumentKeydownHandler: function registerDocumentKeydownHandler(handler) {
        return document.addEventListener('keydown', handler);
      },
      deregisterDocumentKeydownHandler: function deregisterDocumentKeydownHandler(handler) {
        return document.removeEventListener('keydown', handler);
      },
      registerTransitionEndHandler: function registerTransitionEndHandler(handler) {
        return _this2.dialogSurface_.addEventListener('transitionend', handler);
      },
      deregisterTransitionEndHandler: function deregisterTransitionEndHandler(handler) {
        return _this2.dialogSurface_.removeEventListener('transitionend', handler);
      },
      notifyAccept: function notifyAccept() {
        return _this2.emit(foundation_MDCDialogFoundation.strings.ACCEPT_EVENT);
      },
      notifyCancel: function notifyCancel() {
        return _this2.emit(foundation_MDCDialogFoundation.strings.CANCEL_EVENT);
      },
      trapFocusOnSurface: function trapFocusOnSurface() {
        return _this2.focusTrap_.activate();
      },
      untrapFocusOnSurface: function untrapFocusOnSurface() {
        return _this2.focusTrap_.deactivate();
      },
      isDialog: function isDialog(el) {
        return el === _this2.dialogSurface_;
      },
      layoutFooterRipples: function layoutFooterRipples() {
        return _this2.footerBtnRipples_.forEach(function (ripple) {
          return ripple.layout();
        });
      }
    });
  };

  dialog__createClass(MDCDialog, [{
    key: 'open',
    get: function get() {
      return this.foundation_.isOpen();
    }
  }, {
    key: 'acceptButton_',
    get: function get() {
      return this.root_.querySelector(foundation_MDCDialogFoundation.strings.ACCEPT_SELECTOR);
    }
  }, {
    key: 'dialogSurface_',
    get: function get() {
      return this.root_.querySelector(foundation_MDCDialogFoundation.strings.DIALOG_SURFACE_SELECTOR);
    }
  }]);

  return MDCDialog;
}(component);
// CONCATENATED MODULE: ../node_modules/preact-material-components/Icon/index.js
function Icon__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Icon__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function Icon__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Icon__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};




/**
 * @prop disabled = false
 */

var Icon_Icon = function (_MaterialComponent) {
  Icon__inherits(Icon, _MaterialComponent);

  function Icon() {
    Icon__classCallCheck(this, Icon);

    var _this = Icon__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "icon";
    return _this;
  }

  Icon.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("i", Icon__extends({}, props, { className: "material-icons" }), props.children);
  };

  return Icon;
}(MaterialComponent_MaterialComponent);


// CONCATENATED MODULE: ../node_modules/preact-material-components/Button/index.js
function Button__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Button__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function Button__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Button__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};





/**
 *  @prop dense = false
 *  @prop raised = false
 *  @prop compact = false
 *  @prop disabled = false
 *  @prop unelevated = false
 *  @prop stroked = false
 */

var Button_Button = function (_MaterialComponent) {
  Button__inherits(Button, _MaterialComponent);

  function Button() {
    Button__classCallCheck(this, Button);

    var _this = Button__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "button";
    _this._mdcProps = ["dense", "raised", "compact", "unelevated", "stroked"];
    return _this;
  }

  Button.prototype.componentDidMount = function componentDidMount() {
    _MaterialComponent.prototype.attachRipple.call(this);
  };

  Button.prototype.materialDom = function materialDom(props) {
    var _this2 = this;

    var ButtonElement = props.href ? "a" : "button";

    return Object(preact_min["h"])(ButtonElement, Button__extends({
      ref: function ref(control) {
        _this2.control = control;
      }
    }, props), this.props.children);
  };

  return Button;
}(MaterialComponent_MaterialComponent);

var ButtonIcon = function (_Icon) {
  Button__inherits(ButtonIcon, _Icon);

  function ButtonIcon() {
    Button__classCallCheck(this, ButtonIcon);

    var _this3 = Button__possibleConstructorReturn(this, _Icon.call(this));

    _this3.componentName = "button__icon";
    return _this3;
  }

  return ButtonIcon;
}(Icon_Icon);

Button_Button.Icon = ButtonIcon;
/* harmony default export */ var preact_material_components_Button = (Button_Button);
// CONCATENATED MODULE: ../node_modules/preact-material-components/Dialog/index.js
function Dialog__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Dialog__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function Dialog__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Dialog__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};





/**
 */

var Dialog_Dialog = function (_MaterialComponent) {
  Dialog__inherits(Dialog, _MaterialComponent);

  function Dialog() {
    Dialog__classCallCheck(this, Dialog);

    var _this = Dialog__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "dialog";
    _this._onAccept = _this._onAccept.bind(_this);
    _this._onCancel = _this._onCancel.bind(_this);
    return _this;
  }

  Dialog.prototype.componentDidMount = function componentDidMount() {
    this.MDComponent = new dialog_MDCDialog(this.control);
    this.MDComponent.listen("MDCDialog:accept", this._onAccept);
    this.MDComponent.listen("MDCDialog:cancel", this._onCancel);
  };

  Dialog.prototype.componentWillUnmount = function componentWillUnmount() {
    this.MDComponent.unlisten("MDCDialog:accept", this._onAccept);
    this.MDComponent.unlisten("MDCDialog:cancel", this._onCancel);
    this.MDComponent.destroy && this.MDComponent.destroy();
  };

  Dialog.prototype._onAccept = function _onAccept(e) {
    if (this.props.onAccept) {
      this.props.onAccept(e);
    }
  };

  Dialog.prototype._onCancel = function _onCancel(e) {
    if (this.props.onCancel) {
      this.props.onCancel(e);
    }
  };

  Dialog.prototype.materialDom = function materialDom(props) {
    var _this2 = this;

    return Object(preact_min["h"])("aside", Dialog__extends({
      role: "alertdialog",
      ref: function ref(control) {
        _this2.control = control;
      }
    }, props), Object(preact_min["h"])("div", { className: "mdc-dialog__surface" }, props.children), Object(preact_min["h"])("div", { className: "mdc-dialog__backdrop" }));
  };

  return Dialog;
}(MaterialComponent_MaterialComponent);

var Dialog_DialogHeader = function (_MaterialComponent2) {
  Dialog__inherits(DialogHeader, _MaterialComponent2);

  function DialogHeader() {
    Dialog__classCallCheck(this, DialogHeader);

    var _this3 = Dialog__possibleConstructorReturn(this, _MaterialComponent2.call(this));

    _this3.componentName = "dialog__header";
    return _this3;
  }

  DialogHeader.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("header", props, Object(preact_min["h"])("h2", { className: "mdc-dialog__header__title" }, props.children));
  };

  return DialogHeader;
}(MaterialComponent_MaterialComponent);

/**
 * @prop scrollable = false
 */


var Dialog_DialogBody = function (_MaterialComponent3) {
  Dialog__inherits(DialogBody, _MaterialComponent3);

  function DialogBody() {
    Dialog__classCallCheck(this, DialogBody);

    var _this4 = Dialog__possibleConstructorReturn(this, _MaterialComponent3.call(this));

    _this4.componentName = "dialog__body";
    _this4._mdcProps = ["scrollable"];
    return _this4;
  }

  DialogBody.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("section", props, props.children);
  };

  return DialogBody;
}(MaterialComponent_MaterialComponent);

var Dialog_DialogFooter = function (_MaterialComponent4) {
  Dialog__inherits(DialogFooter, _MaterialComponent4);

  function DialogFooter() {
    Dialog__classCallCheck(this, DialogFooter);

    var _this5 = Dialog__possibleConstructorReturn(this, _MaterialComponent4.call(this));

    _this5.componentName = "dialog__footer";
    return _this5;
  }

  DialogFooter.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("footer", props, props.children);
  };

  return DialogFooter;
}(MaterialComponent_MaterialComponent);

/**
 * @prop cancel = false
 * @prop accept = false
 */


var Dialog_DialogFooterButton = function (_Button) {
  Dialog__inherits(DialogFooterButton, _Button);

  function DialogFooterButton() {
    Dialog__classCallCheck(this, DialogFooterButton);

    var _this6 = Dialog__possibleConstructorReturn(this, _Button.call(this));

    _this6.componentName = "dialog__footer__button";
    _this6._mdcProps = ["cancel", "accept"];
    return _this6;
  }

  DialogFooterButton.prototype.materialDom = function materialDom(props) {
    var _this7 = this;

    return Object(preact_min["h"])("button", Dialog__extends({}, props, {
      className: "mdc-button",
      ref: function ref(control) {
        _this7.control = control;
      }
    }), props.children);
  };

  return DialogFooterButton;
}(preact_material_components_Button);

Dialog_Dialog.Header = Dialog_DialogHeader;
Dialog_Dialog.Body = Dialog_DialogBody;
Dialog_Dialog.Footer = Dialog_DialogFooter;
Dialog_Dialog.FooterButton = Dialog_DialogFooterButton;

/* harmony default export */ var preact_material_components_Dialog = (Dialog_Dialog);
// CONCATENATED MODULE: ../node_modules/preact-material-components/Switch/index.js
function Switch__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Switch__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function Switch__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Switch__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

function _objectWithoutProperties(obj, keys) {
  var target = {};for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;target[i] = obj[i];
  }return target;
}




/**
 * @prop disabled = false
 */

var Switch_Switch = function (_MaterialComponent) {
  Switch__inherits(Switch, _MaterialComponent);

  function Switch() {
    Switch__classCallCheck(this, Switch);

    var _this = Switch__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "switch";
    _this._mdcProps = ["disabled"];
    return _this;
  }

  Switch.prototype.materialDom = function materialDom(allprops) {
    var className = allprops.className,
        props = _objectWithoutProperties(allprops, ["className"]);

    return Object(preact_min["h"])("div", { className: className }, Object(preact_min["h"])("input", Switch__extends({
      type: "checkbox",
      className: "mdc-switch__native-control"
    }, props)), Object(preact_min["h"])("div", { className: "mdc-switch__background" }, Object(preact_min["h"])("div", { className: "mdc-switch__knob" })));
  };

  return Switch;
}(MaterialComponent_MaterialComponent);


// EXTERNAL MODULE: ../node_modules/preact-material-components/Switch/style.css
var Switch_style = __webpack_require__("IpTH");
var Switch_style_default = /*#__PURE__*/__webpack_require__.n(Switch_style);

// EXTERNAL MODULE: ../node_modules/preact-material-components/Dialog/style.css
var Dialog_style = __webpack_require__("sEh6");
var Dialog_style_default = /*#__PURE__*/__webpack_require__.n(Dialog_style);

// EXTERNAL MODULE: ../node_modules/preact-material-components/Drawer/style.css
var Drawer_style = __webpack_require__("RYBc");
var Drawer_style_default = /*#__PURE__*/__webpack_require__.n(Drawer_style);

// EXTERNAL MODULE: ../node_modules/preact-material-components/List/style.css
var List_style = __webpack_require__("u+vq");
var List_style_default = /*#__PURE__*/__webpack_require__.n(List_style);

// EXTERNAL MODULE: ../node_modules/preact-material-components/Toolbar/style.css
var Toolbar_style = __webpack_require__("LbTS");
var Toolbar_style_default = /*#__PURE__*/__webpack_require__.n(Toolbar_style);

// EXTERNAL MODULE: ./components/header/style.css
var header_style = __webpack_require__("u3et");
var header_style_default = /*#__PURE__*/__webpack_require__.n(header_style);

// CONCATENATED MODULE: ./components/header/index.js


function header__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function header__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function header__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }















var header__ref = Object(preact_min["h"])(
	preact_material_components_Toolbar.Title,
	null,
	'Preact app'
);

var header__ref2 = Object(preact_min["h"])(
	preact_material_components_Toolbar.Icon,
	null,
	'settings'
);

var header__ref3 = Object(preact_min["h"])(
	preact_material_components_List.ItemIcon,
	null,
	'home'
);

var _ref4 = Object(preact_min["h"])(
	preact_material_components_List.ItemIcon,
	null,
	'account_circle'
);

var _ref5 = Object(preact_min["h"])(
	preact_material_components_Dialog.Header,
	null,
	'Settings'
);

var _ref6 = Object(preact_min["h"])(
	preact_material_components_Dialog.Footer,
	null,
	Object(preact_min["h"])(
		preact_material_components_Dialog.FooterButton,
		{ accept: true },
		'okay'
	)
);

var header_Header = function (_Component) {
	header__inherits(Header, _Component);

	function Header() {
		var _temp, _this, _ret;

		header__classCallCheck(this, Header);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = header__possibleConstructorReturn(this, _Component.call.apply(_Component, [this].concat(args))), _this), _this.openDrawer = function () {
			return _this.drawer.MDComponent.open = true;
		}, _this.openSettings = function () {
			return _this.dialog.MDComponent.show();
		}, _this.drawerRef = function (drawer) {
			return _this.drawer = drawer;
		}, _this.dialogRef = function (dialog) {
			return _this.dialog = dialog;
		}, _this.linkTo = function (path) {
			return function () {
				route(path);
				_this.closeDrawer();
			};
		}, _this.goHome = _this.linkTo('/'), _this.goToMyProfile = _this.linkTo('/profile'), _temp), header__possibleConstructorReturn(_this, _ret);
	}

	Header.prototype.closeDrawer = function closeDrawer() {
		this.drawer.MDComponent.open = false;
		this.state = {
			darkThemeEnabled: false
		};
	};

	Header.prototype.toggleDarkTheme = function toggleDarkTheme() {
		var _this2 = this;

		this.setState({
			darkThemeEnabled: !this.state.darkThemeEnabled
		}, function () {
			if (_this2.state.darkThemeEnabled) {
				document.body.classList.add('mdc-theme--dark');
			} else {
				document.body.classList.remove('mdc-theme--dark');
			}
		});
	};

	Header.prototype.render = function render() {
		return Object(preact_min["h"])(
			'div',
			null,
			Object(preact_min["h"])(
				preact_material_components_Toolbar,
				{ className: 'toolbar' },
				Object(preact_min["h"])(
					preact_material_components_Toolbar.Row,
					null,
					Object(preact_min["h"])(
						preact_material_components_Toolbar.Section,
						{ 'align-start': true },
						Object(preact_min["h"])(
							preact_material_components_Toolbar.Icon,
							{ menu: true, onClick: this.openDrawer },
							'menu'
						),
						header__ref
					),
					Object(preact_min["h"])(
						preact_material_components_Toolbar.Section,
						{ 'align-end': true, onClick: this.openSettings },
						header__ref2
					)
				)
			),
			Object(preact_min["h"])(
				preact_material_components_Drawer.TemporaryDrawer,
				{ ref: this.drawerRef },
				Object(preact_min["h"])(
					preact_material_components_Drawer.TemporaryDrawerContent,
					null,
					Object(preact_min["h"])(
						preact_material_components_List,
						null,
						Object(preact_min["h"])(
							preact_material_components_List.LinkItem,
							{ onClick: this.goHome },
							header__ref3,
							'Home'
						),
						Object(preact_min["h"])(
							preact_material_components_List.LinkItem,
							{ onClick: this.goToMyProfile },
							_ref4,
							'Profile'
						)
					)
				)
			),
			Object(preact_min["h"])(
				preact_material_components_Dialog,
				{ ref: this.dialogRef },
				_ref5,
				Object(preact_min["h"])(
					preact_material_components_Dialog.Body,
					null,
					Object(preact_min["h"])(
						'div',
						null,
						'Enable dark theme ',
						Object(preact_min["h"])(Switch_Switch, { onClick: this.toggleDarkTheme })
					)
				),
				_ref6
			)
		);
	};

	return Header;
}(preact_min["Component"]);


// EXTERNAL MODULE: ../node_modules/preact-material-components/Button/style.css
var Button_style = __webpack_require__("aqQ4");
var Button_style_default = /*#__PURE__*/__webpack_require__.n(Button_style);

// EXTERNAL MODULE: ../node_modules/preact-material-components/Theme/style.css
var Theme_style = __webpack_require__("a81U");
var Theme_style_default = /*#__PURE__*/__webpack_require__.n(Theme_style);

// EXTERNAL MODULE: ./routes/home/style.css
var home_style = __webpack_require__("ZAL5");
var home_style_default = /*#__PURE__*/__webpack_require__.n(home_style);

// CONCATENATED MODULE: ../node_modules/preact-material-components/Card/index.js
function Card__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Card__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function Card__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Card__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};





var Card = function (_MaterialComponent) {
  Card__inherits(Card, _MaterialComponent);

  function Card() {
    Card__classCallCheck(this, Card);

    var _this = Card__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "card";
    _this._mdcProps = ["theme-dark"];
    return _this;
  }

  return Card;
}(MaterialComponent_MaterialComponent);

var Card_CardSection = function (_MaterialComponent2) {
  Card__inherits(CardSection, _MaterialComponent2);

  function CardSection() {
    Card__classCallCheck(this, CardSection);

    var _this2 = Card__possibleConstructorReturn(this, _MaterialComponent2.call(this));

    _this2.componentName = "";
    return _this2;
  }

  CardSection.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("section", props, props.children);
  };

  return CardSection;
}(MaterialComponent_MaterialComponent);

var CardPrimary = function (_CardSection) {
  Card__inherits(CardPrimary, _CardSection);

  function CardPrimary() {
    Card__classCallCheck(this, CardPrimary);

    var _this3 = Card__possibleConstructorReturn(this, _CardSection.call(this));

    _this3.componentName = "card__primary";
    return _this3;
  }

  return CardPrimary;
}(Card_CardSection);

var CardSupportingText = function (_CardSection2) {
  Card__inherits(CardSupportingText, _CardSection2);

  function CardSupportingText() {
    Card__classCallCheck(this, CardSupportingText);

    var _this4 = Card__possibleConstructorReturn(this, _CardSection2.call(this));

    _this4.componentName = "card__supporting-text";
    return _this4;
  }

  return CardSupportingText;
}(Card_CardSection);

var CardActions = function (_CardSection3) {
  Card__inherits(CardActions, _CardSection3);

  function CardActions() {
    Card__classCallCheck(this, CardActions);

    var _this5 = Card__possibleConstructorReturn(this, _CardSection3.call(this));

    _this5.componentName = "card__actions";
    _this5._mdcProps = ["vertical"];
    return _this5;
  }

  return CardActions;
}(Card_CardSection);

var CardMedia = function (_CardSection4) {
  Card__inherits(CardMedia, _CardSection4);

  function CardMedia() {
    Card__classCallCheck(this, CardMedia);

    var _this6 = Card__possibleConstructorReturn(this, _CardSection4.call(this));

    _this6.componentName = "card__media";
    return _this6;
  }

  return CardMedia;
}(Card_CardSection);

var Card_CardAction = function (_Button) {
  Card__inherits(CardAction, _Button);

  function CardAction() {
    Card__classCallCheck(this, CardAction);

    var _this7 = Card__possibleConstructorReturn(this, _Button.call(this));

    _this7.componentName = "card__action";
    return _this7;
  }

  CardAction.prototype.materialDom = function materialDom(props) {
    var _this8 = this;

    return Object(preact_min["h"])("button", Card__extends({
      className: "mdc-button mdc-button--compact"
    }, props, {
      ref: function ref(control) {
        _this8.control = control;
      }
    }), props.children);
  };

  return CardAction;
}(preact_material_components_Button);

var Card_CardTitle = function (_MaterialComponent3) {
  Card__inherits(CardTitle, _MaterialComponent3);

  function CardTitle() {
    Card__classCallCheck(this, CardTitle);

    var _this9 = Card__possibleConstructorReturn(this, _MaterialComponent3.call(this));

    _this9.componentName = "card__title";
    _this9._mdcProps = ["large"];
    return _this9;
  }

  CardTitle.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("h1", props, props.children);
  };

  return CardTitle;
}(MaterialComponent_MaterialComponent);

var Card_CardSubtitle = function (_MaterialComponent4) {
  Card__inherits(CardSubtitle, _MaterialComponent4);

  function CardSubtitle() {
    Card__classCallCheck(this, CardSubtitle);

    var _this10 = Card__possibleConstructorReturn(this, _MaterialComponent4.call(this));

    _this10.componentName = "card__subtitle";
    return _this10;
  }

  CardSubtitle.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("h2", props, props.children);
  };

  return CardSubtitle;
}(MaterialComponent_MaterialComponent);

var CardHorizontalBlock = function (_CardSection5) {
  Card__inherits(CardHorizontalBlock, _CardSection5);

  function CardHorizontalBlock() {
    Card__classCallCheck(this, CardHorizontalBlock);

    var _this11 = Card__possibleConstructorReturn(this, _CardSection5.call(this));

    _this11.componentName = "card__horizontal-block";
    return _this11;
  }

  return CardHorizontalBlock;
}(Card_CardSection);

var Card_CardMediaItem = function (_MaterialComponent5) {
  Card__inherits(CardMediaItem, _MaterialComponent5);

  function CardMediaItem() {
    Card__classCallCheck(this, CardMediaItem);

    var _this12 = Card__possibleConstructorReturn(this, _MaterialComponent5.call(this));

    _this12.componentName = "card__media-item";
    _this12._mdcProps = [];
    return _this12;
  }

  CardMediaItem.prototype.materialDom = function materialDom(props) {
    var className = "";
    if (props.x) {
      className = "mdc-card__media-item--" + props.x + "x";
    }
    return Object(preact_min["h"])("img", Card__extends({ className: className }, props));
  };

  return CardMediaItem;
}(MaterialComponent_MaterialComponent);

Card.Primary = CardPrimary;
Card.SupportingText = CardSupportingText;
Card.Actions = CardActions;
Card.Action = Card_CardAction;
Card.Media = CardMedia;
Card.Title = Card_CardTitle;
Card.Subtitle = Card_CardSubtitle;
Card.HorizontalBlock = CardHorizontalBlock;
Card.MediaItem = Card_CardMediaItem;

/* harmony default export */ var preact_material_components_Card = (Card);
// CONCATENATED MODULE: ../node_modules/@material/snackbar/constants.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var snackbar_constants_cssClasses = {
  ROOT: 'mdc-snackbar',
  TEXT: 'mdc-snackbar__text',
  ACTION_WRAPPER: 'mdc-snackbar__action-wrapper',
  ACTION_BUTTON: 'mdc-snackbar__action-button',
  ACTIVE: 'mdc-snackbar--active',
  MULTILINE: 'mdc-snackbar--multiline',
  ACTION_ON_BOTTOM: 'mdc-snackbar--action-on-bottom'
};

var snackbar_constants_strings = {
  TEXT_SELECTOR: '.mdc-snackbar__text',
  ACTION_WRAPPER_SELECTOR: '.mdc-snackbar__action-wrapper',
  ACTION_BUTTON_SELECTOR: '.mdc-snackbar__action-button'
};

var snackbar_constants_numbers = {
  MESSAGE_TIMEOUT: 2750
};
// CONCATENATED MODULE: ../node_modules/@material/snackbar/foundation.js
var snackbar_foundation__extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var snackbar_foundation__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function snackbar_foundation__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function snackbar_foundation__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function snackbar_foundation__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */




var foundation_MDCSnackbarFoundation = function (_MDCFoundation) {
  snackbar_foundation__inherits(MDCSnackbarFoundation, _MDCFoundation);

  snackbar_foundation__createClass(MDCSnackbarFoundation, [{
    key: 'active',
    get: function get() {
      return this.active_;
    }
  }], [{
    key: 'cssClasses',
    get: function get() {
      return snackbar_constants_cssClasses;
    }
  }, {
    key: 'strings',
    get: function get() {
      return snackbar_constants_strings;
    }
  }, {
    key: 'defaultAdapter',
    get: function get() {
      return {
        addClass: function addClass() /* className: string */{},
        removeClass: function removeClass() /* className: string */{},
        setAriaHidden: function setAriaHidden() {},
        unsetAriaHidden: function unsetAriaHidden() {},
        setActionAriaHidden: function setActionAriaHidden() {},
        unsetActionAriaHidden: function unsetActionAriaHidden() {},
        setActionText: function setActionText() /* actionText: string */{},
        setMessageText: function setMessageText() /* message: string */{},
        setFocus: function setFocus() {},
        visibilityIsHidden: function visibilityIsHidden() {
          return (/* boolean */false
          );
        },
        registerCapturedBlurHandler: function registerCapturedBlurHandler() /* handler: EventListener */{},
        deregisterCapturedBlurHandler: function deregisterCapturedBlurHandler() /* handler: EventListener */{},
        registerVisibilityChangeHandler: function registerVisibilityChangeHandler() /* handler: EventListener */{},
        deregisterVisibilityChangeHandler: function deregisterVisibilityChangeHandler() /* handler: EventListener */{},
        registerCapturedInteractionHandler: function registerCapturedInteractionHandler() /* evtType: string, handler: EventListener */{},
        deregisterCapturedInteractionHandler: function deregisterCapturedInteractionHandler() /* evtType: string, handler: EventListener */{},
        registerActionClickHandler: function registerActionClickHandler() /* handler: EventListener */{},
        deregisterActionClickHandler: function deregisterActionClickHandler() /* handler: EventListener */{},
        registerTransitionEndHandler: function registerTransitionEndHandler() /* handler: EventListener */{},
        deregisterTransitionEndHandler: function deregisterTransitionEndHandler() /* handler: EventListener */{}
      };
    }
  }]);

  function MDCSnackbarFoundation(adapter) {
    snackbar_foundation__classCallCheck(this, MDCSnackbarFoundation);

    var _this = snackbar_foundation__possibleConstructorReturn(this, _MDCFoundation.call(this, snackbar_foundation__extends(MDCSnackbarFoundation.defaultAdapter, adapter)));

    _this.active_ = false;
    _this.actionWasClicked_ = false;
    _this.dismissOnAction_ = true;
    _this.firstFocus_ = true;
    _this.pointerDownRecognized_ = false;
    _this.snackbarHasFocus_ = false;
    _this.snackbarData_ = null;
    _this.queue_ = [];
    _this.actionClickHandler_ = function () {
      _this.actionWasClicked_ = true;
      _this.invokeAction_();
    };
    _this.visibilitychangeHandler_ = function () {
      clearTimeout(_this.timeoutId_);
      _this.snackbarHasFocus_ = true;

      if (!_this.adapter_.visibilityIsHidden()) {
        setTimeout(_this.cleanup_.bind(_this), _this.snackbarData_.timeout || snackbar_constants_numbers.MESSAGE_TIMEOUT);
      }
    };
    _this.interactionHandler_ = function (evt) {
      if (evt.type == 'touchstart' || evt.type == 'mousedown') {
        _this.pointerDownRecognized_ = true;
      }
      _this.handlePossibleTabKeyboardFocus_(evt);

      if (evt.type == 'focus') {
        _this.pointerDownRecognized_ = false;
      }
    };
    _this.blurHandler_ = function () {
      clearTimeout(_this.timeoutId_);
      _this.snackbarHasFocus_ = false;
      _this.timeoutId_ = setTimeout(_this.cleanup_.bind(_this), _this.snackbarData_.timeout || snackbar_constants_numbers.MESSAGE_TIMEOUT);
    };
    return _this;
  }

  MDCSnackbarFoundation.prototype.init = function init() {
    this.adapter_.registerActionClickHandler(this.actionClickHandler_);
    this.adapter_.setAriaHidden();
    this.adapter_.setActionAriaHidden();
  };

  MDCSnackbarFoundation.prototype.destroy = function destroy() {
    var _this2 = this;

    this.adapter_.deregisterActionClickHandler(this.actionClickHandler_);
    this.adapter_.deregisterCapturedBlurHandler(this.blurHandler_);
    this.adapter_.deregisterVisibilityChangeHandler(this.visibilitychangeHandler_);
    ['touchstart', 'mousedown', 'focus'].forEach(function (evtType) {
      _this2.adapter_.deregisterCapturedInteractionHandler(evtType, _this2.interactionHandler_);
    });
  };

  MDCSnackbarFoundation.prototype.dismissesOnAction = function dismissesOnAction() {
    return this.dismissOnAction_;
  };

  MDCSnackbarFoundation.prototype.setDismissOnAction = function setDismissOnAction(dismissOnAction) {
    this.dismissOnAction_ = !!dismissOnAction;
  };

  MDCSnackbarFoundation.prototype.show = function show(data) {
    var _this3 = this;

    if (!data) {
      throw new Error('Please provide a data object with at least a message to display.');
    }
    if (!data.message) {
      throw new Error('Please provide a message to be displayed.');
    }
    if (data.actionHandler && !data.actionText) {
      throw new Error('Please provide action text with the handler.');
    }
    if (this.active) {
      this.queue_.push(data);
      return;
    }
    clearTimeout(this.timeoutId_);
    this.snackbarData_ = data;
    this.firstFocus_ = true;
    this.adapter_.registerVisibilityChangeHandler(this.visibilitychangeHandler_);
    this.adapter_.registerCapturedBlurHandler(this.blurHandler_);
    ['touchstart', 'mousedown', 'focus'].forEach(function (evtType) {
      _this3.adapter_.registerCapturedInteractionHandler(evtType, _this3.interactionHandler_);
    });

    var ACTIVE = snackbar_constants_cssClasses.ACTIVE,
        MULTILINE = snackbar_constants_cssClasses.MULTILINE,
        ACTION_ON_BOTTOM = snackbar_constants_cssClasses.ACTION_ON_BOTTOM;


    this.adapter_.setMessageText(this.snackbarData_.message);

    if (this.snackbarData_.multiline) {
      this.adapter_.addClass(MULTILINE);
      if (this.snackbarData_.actionOnBottom) {
        this.adapter_.addClass(ACTION_ON_BOTTOM);
      }
    }

    if (this.snackbarData_.actionHandler) {
      this.adapter_.setActionText(this.snackbarData_.actionText);
      this.actionHandler_ = this.snackbarData_.actionHandler;
      this.setActionHidden_(false);
    } else {
      this.setActionHidden_(true);
      this.actionHandler_ = null;
      this.adapter_.setActionText(null);
    }

    this.active_ = true;
    this.adapter_.addClass(ACTIVE);
    this.adapter_.unsetAriaHidden();

    this.timeoutId_ = setTimeout(this.cleanup_.bind(this), this.snackbarData_.timeout || snackbar_constants_numbers.MESSAGE_TIMEOUT);
  };

  MDCSnackbarFoundation.prototype.handlePossibleTabKeyboardFocus_ = function handlePossibleTabKeyboardFocus_() {
    var hijackFocus = this.firstFocus_ && !this.pointerDownRecognized_;

    if (hijackFocus) {
      this.setFocusOnAction_();
    }

    this.firstFocus_ = false;
  };

  MDCSnackbarFoundation.prototype.setFocusOnAction_ = function setFocusOnAction_() {
    this.adapter_.setFocus();
    this.snackbarHasFocus_ = true;
    this.firstFocus_ = false;
  };

  MDCSnackbarFoundation.prototype.invokeAction_ = function invokeAction_() {
    try {
      if (!this.actionHandler_) {
        return;
      }

      this.actionHandler_();
    } finally {
      if (this.dismissOnAction_) {
        this.cleanup_();
      }
    }
  };

  MDCSnackbarFoundation.prototype.cleanup_ = function cleanup_() {
    var _this4 = this;

    var allowDismissal = !this.snackbarHasFocus_ || this.actionWasClicked_;

    if (allowDismissal) {
      var ACTIVE = snackbar_constants_cssClasses.ACTIVE,
          MULTILINE = snackbar_constants_cssClasses.MULTILINE,
          ACTION_ON_BOTTOM = snackbar_constants_cssClasses.ACTION_ON_BOTTOM;


      this.adapter_.removeClass(ACTIVE);

      var handler = function handler() {
        clearTimeout(_this4.timeoutId_);
        _this4.adapter_.deregisterTransitionEndHandler(handler);
        _this4.adapter_.removeClass(MULTILINE);
        _this4.adapter_.removeClass(ACTION_ON_BOTTOM);
        _this4.setActionHidden_(true);
        _this4.adapter_.setAriaHidden();
        _this4.active_ = false;
        _this4.snackbarHasFocus_ = false;
        _this4.showNext_();
      };

      this.adapter_.registerTransitionEndHandler(handler);
    }
  };

  MDCSnackbarFoundation.prototype.showNext_ = function showNext_() {
    if (!this.queue_.length) {
      return;
    }
    this.show(this.queue_.shift());
  };

  MDCSnackbarFoundation.prototype.setActionHidden_ = function setActionHidden_(isHidden) {
    if (isHidden) {
      this.adapter_.setActionAriaHidden();
    } else {
      this.adapter_.unsetActionAriaHidden();
    }
  };

  return MDCSnackbarFoundation;
}(foundation);


// CONCATENATED MODULE: ../node_modules/@material/animation/index.js
/**
 * @license
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @typedef {{
 *   noPrefix: string,
 *   webkitPrefix: string
 * }}
 */
var VendorPropertyMapType = void 0;

/** @const {Object<string, !VendorPropertyMapType>} */
var eventTypeMap = {
  'animationstart': {
    noPrefix: 'animationstart',
    webkitPrefix: 'webkitAnimationStart',
    styleProperty: 'animation'
  },
  'animationend': {
    noPrefix: 'animationend',
    webkitPrefix: 'webkitAnimationEnd',
    styleProperty: 'animation'
  },
  'animationiteration': {
    noPrefix: 'animationiteration',
    webkitPrefix: 'webkitAnimationIteration',
    styleProperty: 'animation'
  },
  'transitionend': {
    noPrefix: 'transitionend',
    webkitPrefix: 'webkitTransitionEnd',
    styleProperty: 'transition'
  }
};

/** @const {Object<string, !VendorPropertyMapType>} */
var cssPropertyMap = {
  'animation': {
    noPrefix: 'animation',
    webkitPrefix: '-webkit-animation'
  },
  'transform': {
    noPrefix: 'transform',
    webkitPrefix: '-webkit-transform'
  },
  'transition': {
    noPrefix: 'transition',
    webkitPrefix: '-webkit-transition'
  }
};

/**
 * @param {!Object} windowObj
 * @return {boolean}
 */
function hasProperShape(windowObj) {
  return windowObj['document'] !== undefined && typeof windowObj['document']['createElement'] === 'function';
}

/**
 * @param {string} eventType
 * @return {boolean}
 */
function eventFoundInMaps(eventType) {
  return eventType in eventTypeMap || eventType in cssPropertyMap;
}

/**
 * @param {string} eventType
 * @param {!Object<string, !VendorPropertyMapType>} map
 * @param {!Element} el
 * @return {string}
 */
function getJavaScriptEventName(eventType, map, el) {
  return map[eventType].styleProperty in el.style ? map[eventType].noPrefix : map[eventType].webkitPrefix;
}

/**
 * Helper function to determine browser prefix for CSS3 animation events
 * and property names.
 * @param {!Object} windowObj
 * @param {string} eventType
 * @return {string}
 */
function getAnimationName(windowObj, eventType) {
  if (!hasProperShape(windowObj) || !eventFoundInMaps(eventType)) {
    return eventType;
  }

  var map = /** @type {!Object<string, !VendorPropertyMapType>} */eventType in eventTypeMap ? eventTypeMap : cssPropertyMap;
  var el = windowObj['document']['createElement']('div');
  var eventName = '';

  if (map === eventTypeMap) {
    eventName = getJavaScriptEventName(eventType, map, el);
  } else {
    eventName = map[eventType].noPrefix in el.style ? map[eventType].noPrefix : map[eventType].webkitPrefix;
  }

  return eventName;
}

// Public functions to access getAnimationName() for JavaScript events or CSS
// property names.

var transformStyleProperties = ['transform', 'WebkitTransform', 'MozTransform', 'OTransform', 'MSTransform'];

/**
 * @param {!Object} windowObj
 * @param {string} eventType
 * @return {string}
 */
function getCorrectEventName(windowObj, eventType) {
  return getAnimationName(windowObj, eventType);
}

/**
 * @param {!Object} windowObj
 * @param {string} eventType
 * @return {string}
 */
function getCorrectPropertyName(windowObj, eventType) {
  return getAnimationName(windowObj, eventType);
}


// CONCATENATED MODULE: ../node_modules/@material/snackbar/index.js
var snackbar__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function snackbar__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function snackbar__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function snackbar__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */







var snackbar_MDCSnackbar = function (_MDCComponent) {
  snackbar__inherits(MDCSnackbar, _MDCComponent);

  function MDCSnackbar() {
    snackbar__classCallCheck(this, MDCSnackbar);

    return snackbar__possibleConstructorReturn(this, _MDCComponent.apply(this, arguments));
  }

  MDCSnackbar.attachTo = function attachTo(root) {
    return new MDCSnackbar(root);
  };

  MDCSnackbar.prototype.show = function show(data) {
    this.foundation_.show(data);
  };

  MDCSnackbar.prototype.getDefaultFoundation = function getDefaultFoundation() {
    var _this2 = this;

    var _MDCSnackbarFoundatio = foundation_MDCSnackbarFoundation.strings,
        TEXT_SELECTOR = _MDCSnackbarFoundatio.TEXT_SELECTOR,
        ACTION_BUTTON_SELECTOR = _MDCSnackbarFoundatio.ACTION_BUTTON_SELECTOR;

    var getText = function getText() {
      return _this2.root_.querySelector(TEXT_SELECTOR);
    };
    var getActionButton = function getActionButton() {
      return _this2.root_.querySelector(ACTION_BUTTON_SELECTOR);
    };

    /* eslint brace-style: "off" */
    return new foundation_MDCSnackbarFoundation({
      addClass: function addClass(className) {
        return _this2.root_.classList.add(className);
      },
      removeClass: function removeClass(className) {
        return _this2.root_.classList.remove(className);
      },
      setAriaHidden: function setAriaHidden() {
        return _this2.root_.setAttribute('aria-hidden', 'true');
      },
      unsetAriaHidden: function unsetAriaHidden() {
        return _this2.root_.removeAttribute('aria-hidden');
      },
      setActionAriaHidden: function setActionAriaHidden() {
        return getActionButton().setAttribute('aria-hidden', 'true');
      },
      unsetActionAriaHidden: function unsetActionAriaHidden() {
        return getActionButton().removeAttribute('aria-hidden');
      },
      setActionText: function setActionText(text) {
        getActionButton().textContent = text;
      },
      setMessageText: function setMessageText(text) {
        getText().textContent = text;
      },
      setFocus: function setFocus() {
        return getActionButton().focus();
      },
      visibilityIsHidden: function visibilityIsHidden() {
        return document.hidden;
      },
      registerCapturedBlurHandler: function registerCapturedBlurHandler(handler) {
        return getActionButton().addEventListener('blur', handler, true);
      },
      deregisterCapturedBlurHandler: function deregisterCapturedBlurHandler(handler) {
        return getActionButton().removeEventListener('blur', handler, true);
      },
      registerVisibilityChangeHandler: function registerVisibilityChangeHandler(handler) {
        return document.addEventListener('visibilitychange', handler);
      },
      deregisterVisibilityChangeHandler: function deregisterVisibilityChangeHandler(handler) {
        return document.removeEventListener('visibilitychange', handler);
      },
      registerCapturedInteractionHandler: function registerCapturedInteractionHandler(evt, handler) {
        return document.body.addEventListener(evt, handler, true);
      },
      deregisterCapturedInteractionHandler: function deregisterCapturedInteractionHandler(evt, handler) {
        return document.body.removeEventListener(evt, handler, true);
      },
      registerActionClickHandler: function registerActionClickHandler(handler) {
        return getActionButton().addEventListener('click', handler);
      },
      deregisterActionClickHandler: function deregisterActionClickHandler(handler) {
        return getActionButton().removeEventListener('click', handler);
      },
      registerTransitionEndHandler: function registerTransitionEndHandler(handler) {
        return _this2.root_.addEventListener(getCorrectEventName(window, 'transitionend'), handler);
      },
      deregisterTransitionEndHandler: function deregisterTransitionEndHandler(handler) {
        return _this2.root_.removeEventListener(getCorrectEventName(window, 'transitionend'), handler);
      }
    });
  };

  snackbar__createClass(MDCSnackbar, [{
    key: 'dismissesOnAction',
    get: function get() {
      return this.foundation_.dismissesOnAction();
    },
    set: function set(dismissesOnAction) {
      this.foundation_.setDismissOnAction(dismissesOnAction);
    }
  }]);

  return MDCSnackbar;
}(component);
// CONCATENATED MODULE: ../node_modules/preact-material-components/Snackbar/index.js
function Snackbar__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Snackbar__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function Snackbar__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Snackbar__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};





function shallowDiffers(a, b) {
  for (var i in a) {
    if (!(i in b)) return true;
  }for (var _i in b) {
    if (a[_i] !== b[_i]) return true;
  }return false;
}

/**
 * dismissesOnAction = true
 */

var Snackbar_Snackbar = function (_MaterialComponent) {
  Snackbar__inherits(Snackbar, _MaterialComponent);

  function Snackbar() {
    Snackbar__classCallCheck(this, Snackbar);

    var _this = Snackbar__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "snackbar";
    _this.isPureReactComponent = true;
    return _this;
  }

  Snackbar.prototype.componentDidMount = function componentDidMount() {
    this.MDComponent = snackbar_MDCSnackbar.attachTo(this.control);
    if (this.props.dismissesOnAction === undefined || this.props.dismissesOnAction === null) {
      this.MDComponent.dismissesOnAction = true;
    } else {
      this.MDComponent.dismissesOnAction = this.props.dismissesOnAction;
    }
  };

  Snackbar.prototype.componentWillUnmount = function componentWillUnmount() {
    this.MDComponent.destroy && this.MDComponent.destroy();
  };

  Snackbar.prototype.shouldComponentUpdate = function shouldComponentUpdate(props, state) {
    return shallowDiffers(this.props, props) || shallowDiffers(this.state, state);
  };

  Snackbar.prototype.materialDom = function materialDom(props) {
    var _this2 = this;

    return Object(preact_min["h"])("div", Snackbar__extends({
      "aria-live": "assertive",
      "aria-atomic": "true",
      "aria-hidden": "true",
      ref: function ref(control) {
        return _this2.control = control;
      }
    }, props), Object(preact_min["h"])("div", { className: "mdc-snackbar__text" }), Object(preact_min["h"])("div", { className: "mdc-snackbar__action-wrapper" }, Object(preact_min["h"])("button", { type: "button", className: "mdc-snackbar__action-button" })));
  };

  return Snackbar;
}(MaterialComponent_MaterialComponent);


// EXTERNAL MODULE: ../node_modules/preact-material-components/Card/style.css
var Card_style = __webpack_require__("UlEV");
var Card_style_default = /*#__PURE__*/__webpack_require__.n(Card_style);

// EXTERNAL MODULE: ../node_modules/preact-material-components/Snackbar/style.css
var Snackbar_style = __webpack_require__("pln1");
var Snackbar_style_default = /*#__PURE__*/__webpack_require__.n(Snackbar_style);

// CONCATENATED MODULE: ./models/Bet.js
function Bet__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Bet = function Bet() {
	Bet__classCallCheck(this, Bet);

	this.price = 0;
	this.stake = 0;
	this.outcomeName = '';
	this.eventName = '';
	this.betId = null;
	this.payout = 0;
	this.receipt = '';
};


// CONCATENATED MODULE: ../node_modules/@material/textfield/constants.js
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @enum {string} */
var textfield_constants_strings = {
  ARIA_HIDDEN: 'aria-hidden',
  ROLE: 'role',
  INPUT_SELECTOR: '.mdc-textfield__input',
  LABEL_SELECTOR: '.mdc-textfield__label',
  ICON_SELECTOR: '.mdc-textfield__icon',
  ICON_EVENT: 'MDCTextfield:icon',
  BOTTOM_LINE_SELECTOR: '.mdc-textfield__bottom-line'
};

/** @enum {string} */
var textfield_constants_cssClasses = {
  ROOT: 'mdc-textfield',
  UPGRADED: 'mdc-textfield--upgraded',
  DISABLED: 'mdc-textfield--disabled',
  FOCUSED: 'mdc-textfield--focused',
  INVALID: 'mdc-textfield--invalid',
  HELPTEXT_PERSISTENT: 'mdc-textfield-helptext--persistent',
  HELPTEXT_VALIDATION_MSG: 'mdc-textfield-helptext--validation-msg',
  LABEL_FLOAT_ABOVE: 'mdc-textfield__label--float-above',
  LABEL_SHAKE: 'mdc-textfield__label--shake',
  BOX: 'mdc-textfield--box',
  TEXT_FIELD_ICON: 'mdc-textfield__icon',
  TEXTAREA: 'mdc-textfield--textarea',
  BOTTOM_LINE_ACTIVE: 'mdc-textfield__bottom-line--active'
};


// CONCATENATED MODULE: ../node_modules/@material/textfield/adapter.js
function textfield_adapter__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @license
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint no-unused-vars: [2, {"args": "none"}] */

/**
 * @typedef {{
 *   value: string,
 *   disabled: boolean,
 *   badInput: boolean,
 *   checkValidity: (function(): boolean)
 * }}
 */
var NativeInputType = void 0;

/**
 * Adapter for MDC Textfield.
 *
 * Defines the shape of the adapter expected by the foundation. Implement this
 * adapter to integrate the Textfield into your framework. See
 * https://github.com/material-components/material-components-web/blob/master/docs/authoring-components.md
 * for more information.
 *
 * @record
 */

var MDCTextfieldAdapter = function () {
  function MDCTextfieldAdapter() {
    textfield_adapter__classCallCheck(this, MDCTextfieldAdapter);
  }

  /**
   * Adds a class to the root Element.
   * @param {string} className
   */
  MDCTextfieldAdapter.prototype.addClass = function addClass(className) {};

  /**
   * Removes a class from the root Element.
   * @param {string} className
   */


  MDCTextfieldAdapter.prototype.removeClass = function removeClass(className) {};

  /**
   * Adds a class to the label Element. We recommend you add a conditional
   * check here, and in removeClassFromLabel for whether or not the label is
   * present so that the JS component could be used with text fields that don't
   * require a label, such as the full-width text field.
   * @param {string} className
   */


  MDCTextfieldAdapter.prototype.addClassToLabel = function addClassToLabel(className) {};

  /**
   * Removes a class from the label Element.
   * @param {string} className
   */


  MDCTextfieldAdapter.prototype.removeClassFromLabel = function removeClassFromLabel(className) {};

  /**
   * Sets an attribute on the icon Element.
   * @param {string} name
   * @param {string} value
   */


  MDCTextfieldAdapter.prototype.setIconAttr = function setIconAttr(name, value) {};

  /**
   * Returns true if classname exists for a given target element.
   * @param {?EventTarget} target
   * @param {string} className
   * @return {boolean}
   */


  MDCTextfieldAdapter.prototype.eventTargetHasClass = function eventTargetHasClass(target, className) {};

  /**
   * Registers an event handler on the root element for a given event.
   * @param {string} type
   * @param {function(!Event): undefined} handler
   */


  MDCTextfieldAdapter.prototype.registerTextFieldInteractionHandler = function registerTextFieldInteractionHandler(type, handler) {};

  /**
   * Deregisters an event handler on the root element for a given event.
   * @param {string} type
   * @param {function(!Event): undefined} handler
   */


  MDCTextfieldAdapter.prototype.deregisterTextFieldInteractionHandler = function deregisterTextFieldInteractionHandler(type, handler) {};

  /**
   * Emits a custom event "MDCTextfield:icon" denoting a user has clicked the icon.
   */


  MDCTextfieldAdapter.prototype.notifyIconAction = function notifyIconAction() {};

  /**
   * Adds a class to the bottom line element.
   * @param {string} className
   */


  MDCTextfieldAdapter.prototype.addClassToBottomLine = function addClassToBottomLine(className) {};

  /**
   * Removes a class from the bottom line element.
   * @param {string} className
   */


  MDCTextfieldAdapter.prototype.removeClassFromBottomLine = function removeClassFromBottomLine(className) {};

  /**
   * Adds a class to the help text element. Note that in our code we check for
   * whether or not we have a help text element and if we don't, we simply
   * return.
   * @param {string} className
   */


  MDCTextfieldAdapter.prototype.addClassToHelptext = function addClassToHelptext(className) {};

  /**
   * Removes a class from the help text element.
   * @param {string} className
   */


  MDCTextfieldAdapter.prototype.removeClassFromHelptext = function removeClassFromHelptext(className) {};

  /**
   * Returns whether or not the help text element contains the given class.
   * @param {string} className
   * @return {boolean}
   */


  MDCTextfieldAdapter.prototype.helptextHasClass = function helptextHasClass(className) {};

  /**
   * Registers an event listener on the native input element for a given event.
   * @param {string} evtType
   * @param {function(!Event): undefined} handler
   */


  MDCTextfieldAdapter.prototype.registerInputInteractionHandler = function registerInputInteractionHandler(evtType, handler) {};

  /**
   * Deregisters an event listener on the native input element for a given event.
   * @param {string} evtType
   * @param {function(!Event): undefined} handler
   */


  MDCTextfieldAdapter.prototype.deregisterInputInteractionHandler = function deregisterInputInteractionHandler(evtType, handler) {};

  /**
   * Registers an event listener on the bottom line element for a "transitionend" event.
   * @param {function(!Event): undefined} handler
   */


  MDCTextfieldAdapter.prototype.registerTransitionEndHandler = function registerTransitionEndHandler(handler) {};

  /**
   * Deregisters an event listener on the bottom line element for a "transitionend" event.
   * @param {function(!Event): undefined} handler
   */


  MDCTextfieldAdapter.prototype.deregisterTransitionEndHandler = function deregisterTransitionEndHandler(handler) {};

  /**
   * Sets an attribute with a given value on the bottom line element.
   * @param {string} attr
   * @param {string} value
   */


  MDCTextfieldAdapter.prototype.setBottomLineAttr = function setBottomLineAttr(attr, value) {};

  /**
   * Sets an attribute with a given value on the help text element.
   * @param {string} name
   * @param {string} value
   */


  MDCTextfieldAdapter.prototype.setHelptextAttr = function setHelptextAttr(name, value) {};

  /**
   * Removes an attribute from the help text element.
   * @param {string} name
   */


  MDCTextfieldAdapter.prototype.removeHelptextAttr = function removeHelptextAttr(name) {};

  /**
   * Returns an object representing the native text input element, with a
   * similar API shape. The object returned should include the value, disabled
   * and badInput properties, as well as the checkValidity() function. We never
   * alter the value within our code, however we do update the disabled
   * property, so if you choose to duck-type the return value for this method
   * in your implementation it's important to keep this in mind. Also note that
   * this method can return null, which the foundation will handle gracefully.
   * @return {?Element|?NativeInputType}
   */


  MDCTextfieldAdapter.prototype.getNativeInput = function getNativeInput() {};

  return MDCTextfieldAdapter;
}();


// CONCATENATED MODULE: ../node_modules/@material/textfield/foundation.js
var textfield_foundation__extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var textfield_foundation__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function textfield_foundation__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function textfield_foundation__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function textfield_foundation__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */





/**
 * @extends {MDCFoundation<!MDCTextfieldAdapter>}
 * @final
 */

var foundation_MDCTextfieldFoundation = function (_MDCFoundation) {
  textfield_foundation__inherits(MDCTextfieldFoundation, _MDCFoundation);

  textfield_foundation__createClass(MDCTextfieldFoundation, null, [{
    key: 'cssClasses',

    /** @return enum {string} */
    get: function get() {
      return textfield_constants_cssClasses;
    }

    /** @return enum {string} */

  }, {
    key: 'strings',
    get: function get() {
      return textfield_constants_strings;
    }

    /**
     * {@see MDCTextfieldAdapter} for typing information on parameters and return
     * types.
     * @return {!MDCTextfieldAdapter}
     */

  }, {
    key: 'defaultAdapter',
    get: function get() {
      return (/** @type {!MDCTextfieldAdapter} */{
          addClass: function addClass() {},
          removeClass: function removeClass() {},
          addClassToLabel: function addClassToLabel() {},
          removeClassFromLabel: function removeClassFromLabel() {},
          setIconAttr: function setIconAttr() {},
          eventTargetHasClass: function eventTargetHasClass() {},
          registerTextFieldInteractionHandler: function registerTextFieldInteractionHandler() {},
          deregisterTextFieldInteractionHandler: function deregisterTextFieldInteractionHandler() {},
          notifyIconAction: function notifyIconAction() {},
          addClassToBottomLine: function addClassToBottomLine() {},
          removeClassFromBottomLine: function removeClassFromBottomLine() {},
          addClassToHelptext: function addClassToHelptext() {},
          removeClassFromHelptext: function removeClassFromHelptext() {},
          helptextHasClass: function helptextHasClass() {
            return false;
          },
          registerInputInteractionHandler: function registerInputInteractionHandler() {},
          deregisterInputInteractionHandler: function deregisterInputInteractionHandler() {},
          registerTransitionEndHandler: function registerTransitionEndHandler() {},
          deregisterTransitionEndHandler: function deregisterTransitionEndHandler() {},
          setBottomLineAttr: function setBottomLineAttr() {},
          setHelptextAttr: function setHelptextAttr() {},
          removeHelptextAttr: function removeHelptextAttr() {},
          getNativeInput: function getNativeInput() {}
        }
      );
    }

    /**
     * @param {!MDCTextfieldAdapter=} adapter
     */

  }]);

  function MDCTextfieldFoundation() {
    var adapter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : /** @type {!MDCTextfieldAdapter} */{};

    textfield_foundation__classCallCheck(this, MDCTextfieldFoundation);

    /** @private {boolean} */
    var _this = textfield_foundation__possibleConstructorReturn(this, _MDCFoundation.call(this, textfield_foundation__extends(MDCTextfieldFoundation.defaultAdapter, adapter)));

    _this.isFocused_ = false;
    /** @private {boolean} */
    _this.receivedUserInput_ = false;
    /** @private {boolean} */
    _this.useCustomValidityChecking_ = false;
    /** @private {function(): undefined} */
    _this.inputFocusHandler_ = function () {
      return _this.activateFocus_();
    };
    /** @private {function(): undefined} */
    _this.inputBlurHandler_ = function () {
      return _this.deactivateFocus_();
    };
    /** @private {function(): undefined} */
    _this.inputInputHandler_ = function () {
      return _this.autoCompleteFocus_();
    };
    /** @private {function(!Event): undefined} */
    _this.setPointerXOffset_ = function (evt) {
      return _this.setBottomLineTransformOrigin_(evt);
    };
    /** @private {function(!Event): undefined} */
    _this.textFieldInteractionHandler_ = function (evt) {
      return _this.handleTextFieldInteraction_(evt);
    };
    /** @private {function(!Event): undefined} */
    _this.transitionEndHandler_ = function (evt) {
      return _this.transitionEnd_(evt);
    };
    return _this;
  }

  MDCTextfieldFoundation.prototype.init = function init() {
    var _this2 = this;

    this.adapter_.addClass(MDCTextfieldFoundation.cssClasses.UPGRADED);
    // Ensure label does not collide with any pre-filled value.
    if (this.getNativeInput_().value) {
      this.adapter_.addClassToLabel(MDCTextfieldFoundation.cssClasses.LABEL_FLOAT_ABOVE);
    }

    this.adapter_.registerInputInteractionHandler('focus', this.inputFocusHandler_);
    this.adapter_.registerInputInteractionHandler('blur', this.inputBlurHandler_);
    this.adapter_.registerInputInteractionHandler('input', this.inputInputHandler_);
    ['mousedown', 'touchstart'].forEach(function (evtType) {
      _this2.adapter_.registerInputInteractionHandler(evtType, _this2.setPointerXOffset_);
    });
    ['click', 'keydown'].forEach(function (evtType) {
      _this2.adapter_.registerTextFieldInteractionHandler(evtType, _this2.textFieldInteractionHandler_);
    });
    this.adapter_.registerTransitionEndHandler(this.transitionEndHandler_);
  };

  MDCTextfieldFoundation.prototype.destroy = function destroy() {
    var _this3 = this;

    this.adapter_.removeClass(MDCTextfieldFoundation.cssClasses.UPGRADED);
    this.adapter_.deregisterInputInteractionHandler('focus', this.inputFocusHandler_);
    this.adapter_.deregisterInputInteractionHandler('blur', this.inputBlurHandler_);
    this.adapter_.deregisterInputInteractionHandler('input', this.inputInputHandler_);
    ['mousedown', 'touchstart'].forEach(function (evtType) {
      _this3.adapter_.deregisterInputInteractionHandler(evtType, _this3.setPointerXOffset_);
    });
    ['click', 'keydown'].forEach(function (evtType) {
      _this3.adapter_.deregisterTextFieldInteractionHandler(evtType, _this3.textFieldInteractionHandler_);
    });
    this.adapter_.deregisterTransitionEndHandler(this.transitionEndHandler_);
  };

  /**
   * Handles all user interactions with the Textfield.
   * @param {!Event} evt
   * @private
   */


  MDCTextfieldFoundation.prototype.handleTextFieldInteraction_ = function handleTextFieldInteraction_(evt) {
    if (this.adapter_.getNativeInput().disabled) {
      return;
    }

    this.receivedUserInput_ = true;

    var target = evt.target,
        type = evt.type;
    var TEXT_FIELD_ICON = MDCTextfieldFoundation.cssClasses.TEXT_FIELD_ICON;

    var targetIsIcon = this.adapter_.eventTargetHasClass(target, TEXT_FIELD_ICON);
    var eventTriggersNotification = type === 'click' || evt.key === 'Enter' || evt.keyCode === 13;

    if (targetIsIcon && eventTriggersNotification) {
      this.adapter_.notifyIconAction();
    }
  };

  /**
   * Activates the text field focus state.
   * @private
   */


  MDCTextfieldFoundation.prototype.activateFocus_ = function activateFocus_() {
    var _MDCTextfieldFoundati = MDCTextfieldFoundation.cssClasses,
        BOTTOM_LINE_ACTIVE = _MDCTextfieldFoundati.BOTTOM_LINE_ACTIVE,
        FOCUSED = _MDCTextfieldFoundati.FOCUSED,
        LABEL_FLOAT_ABOVE = _MDCTextfieldFoundati.LABEL_FLOAT_ABOVE,
        LABEL_SHAKE = _MDCTextfieldFoundati.LABEL_SHAKE;

    this.adapter_.addClass(FOCUSED);
    this.adapter_.addClassToBottomLine(BOTTOM_LINE_ACTIVE);
    this.adapter_.addClassToLabel(LABEL_FLOAT_ABOVE);
    this.adapter_.removeClassFromLabel(LABEL_SHAKE);
    this.showHelptext_();
    this.isFocused_ = true;
  };

  /**
   * Sets the transform-origin of the bottom line, causing it to animate out
   * from the user's click location.
   * @param {!Event} evt
   * @private
   */


  MDCTextfieldFoundation.prototype.setBottomLineTransformOrigin_ = function setBottomLineTransformOrigin_(evt) {
    var targetClientRect = evt.target.getBoundingClientRect();
    var evtCoords = { x: evt.clientX, y: evt.clientY };
    var normalizedX = evtCoords.x - targetClientRect.left;
    var attributeString = 'transform-origin: ' + normalizedX + 'px center';

    this.adapter_.setBottomLineAttr('style', attributeString);
  };

  /**
   * Activates the Textfield's focus state in cases when the input value
   * changes without user input (e.g. programatically).
   * @private
   */


  MDCTextfieldFoundation.prototype.autoCompleteFocus_ = function autoCompleteFocus_() {
    if (!this.receivedUserInput_) {
      this.activateFocus_();
    }
  };

  /**
   * Makes the help text visible to screen readers.
   * @private
   */


  MDCTextfieldFoundation.prototype.showHelptext_ = function showHelptext_() {
    var ARIA_HIDDEN = MDCTextfieldFoundation.strings.ARIA_HIDDEN;

    this.adapter_.removeHelptextAttr(ARIA_HIDDEN);
  };

  /**
   * Fires when animation transition ends, performing actions that must wait
   * for animations to finish.
   * @param {!Event} evt
   * @private
   */


  MDCTextfieldFoundation.prototype.transitionEnd_ = function transitionEnd_(evt) {
    var BOTTOM_LINE_ACTIVE = MDCTextfieldFoundation.cssClasses.BOTTOM_LINE_ACTIVE;

    // We need to wait for the bottom line to be entirely transparent
    // before removing the class. If we do not, we see the line start to
    // scale down before disappearing

    if (evt.propertyName === 'opacity' && !this.isFocused_) {
      this.adapter_.removeClassFromBottomLine(BOTTOM_LINE_ACTIVE);
    }
  };

  /**
   * Deactives the Textfield's focus state.
   * @private
   */


  MDCTextfieldFoundation.prototype.deactivateFocus_ = function deactivateFocus_() {
    var _MDCTextfieldFoundati2 = MDCTextfieldFoundation.cssClasses,
        FOCUSED = _MDCTextfieldFoundati2.FOCUSED,
        LABEL_FLOAT_ABOVE = _MDCTextfieldFoundati2.LABEL_FLOAT_ABOVE,
        LABEL_SHAKE = _MDCTextfieldFoundati2.LABEL_SHAKE;

    var input = this.getNativeInput_();

    this.isFocused_ = false;
    this.adapter_.removeClass(FOCUSED);
    this.adapter_.removeClassFromLabel(LABEL_SHAKE);

    if (!input.value && !this.isBadInput_()) {
      this.adapter_.removeClassFromLabel(LABEL_FLOAT_ABOVE);
      this.receivedUserInput_ = false;
    }

    if (!this.useCustomValidityChecking_) {
      this.changeValidity_(input.checkValidity());
    }
  };

  /**
   * Updates the Textfield's valid state based on the supplied validity.
   * @param {boolean} isValid
   * @private
   */


  MDCTextfieldFoundation.prototype.changeValidity_ = function changeValidity_(isValid) {
    var _MDCTextfieldFoundati3 = MDCTextfieldFoundation.cssClasses,
        INVALID = _MDCTextfieldFoundati3.INVALID,
        LABEL_SHAKE = _MDCTextfieldFoundati3.LABEL_SHAKE;

    if (isValid) {
      this.adapter_.removeClass(INVALID);
    } else {
      this.adapter_.addClassToLabel(LABEL_SHAKE);
      this.adapter_.addClass(INVALID);
    }
    this.updateHelptext_(isValid);
  };

  /**
   * Updates the state of the Textfield's help text based on validity and
   * the Textfield's options.
   * @param {boolean} isValid
   */


  MDCTextfieldFoundation.prototype.updateHelptext_ = function updateHelptext_(isValid) {
    var _MDCTextfieldFoundati4 = MDCTextfieldFoundation.cssClasses,
        HELPTEXT_PERSISTENT = _MDCTextfieldFoundati4.HELPTEXT_PERSISTENT,
        HELPTEXT_VALIDATION_MSG = _MDCTextfieldFoundati4.HELPTEXT_VALIDATION_MSG;
    var ROLE = MDCTextfieldFoundation.strings.ROLE;

    var helptextIsPersistent = this.adapter_.helptextHasClass(HELPTEXT_PERSISTENT);
    var helptextIsValidationMsg = this.adapter_.helptextHasClass(HELPTEXT_VALIDATION_MSG);
    var validationMsgNeedsDisplay = helptextIsValidationMsg && !isValid;

    if (validationMsgNeedsDisplay) {
      this.adapter_.setHelptextAttr(ROLE, 'alert');
    } else {
      this.adapter_.removeHelptextAttr(ROLE);
    }

    if (helptextIsPersistent || validationMsgNeedsDisplay) {
      return;
    }
    this.hideHelptext_();
  };

  /**
   * Hides the help text from screen readers.
   * @private
   */


  MDCTextfieldFoundation.prototype.hideHelptext_ = function hideHelptext_() {
    var ARIA_HIDDEN = MDCTextfieldFoundation.strings.ARIA_HIDDEN;

    this.adapter_.setHelptextAttr(ARIA_HIDDEN, 'true');
  };

  /**
   * @return {boolean} True if the Textfield input fails validity checks.
   * @private
   */


  MDCTextfieldFoundation.prototype.isBadInput_ = function isBadInput_() {
    var input = this.getNativeInput_();
    return input.validity ? input.validity.badInput : input.badInput;
  };

  /**
   * @return {boolean} True if the Textfield is disabled.
   */


  MDCTextfieldFoundation.prototype.isDisabled = function isDisabled() {
    return this.getNativeInput_().disabled;
  };

  /**
   * @param {boolean} disabled Sets the textfield disabled or enabled.
   */


  MDCTextfieldFoundation.prototype.setDisabled = function setDisabled(disabled) {
    var DISABLED = MDCTextfieldFoundation.cssClasses.DISABLED;

    this.getNativeInput_().disabled = disabled;
    if (disabled) {
      this.adapter_.addClass(DISABLED);
      this.adapter_.setIconAttr('tabindex', '-1');
    } else {
      this.adapter_.removeClass(DISABLED);
      this.adapter_.setIconAttr('tabindex', '0');
    }
  };

  /**
   * @return {!Element|!NativeInputType} The native text input from the
   * host environment, or a dummy if none exists.
   * @private
   */


  MDCTextfieldFoundation.prototype.getNativeInput_ = function getNativeInput_() {
    return this.adapter_.getNativeInput() ||
    /** @type {!NativeInputType} */{
      checkValidity: function checkValidity() {
        return true;
      },
      value: '',
      disabled: false,
      badInput: false
    };
  };

  /**
   * @param {boolean} isValid Sets the validity state of the Textfield.
   */


  MDCTextfieldFoundation.prototype.setValid = function setValid(isValid) {
    this.useCustomValidityChecking_ = true;
    this.changeValidity_(isValid);
  };

  return MDCTextfieldFoundation;
}(foundation);

/* harmony default export */ var textfield_foundation = (foundation_MDCTextfieldFoundation);
// CONCATENATED MODULE: ../node_modules/@material/textfield/index.js
var textfield__extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var textfield__createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function textfield__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function textfield__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function textfield__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */








/**
 * @extends {MDCComponent<!MDCTextfieldFoundation>}
 * @final
 */

var textfield_MDCTextfield = function (_MDCComponent) {
  textfield__inherits(MDCTextfield, _MDCComponent);

  /**
   * @param {...?} args
   */
  function MDCTextfield() {
    textfield__classCallCheck(this, MDCTextfield);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    /** @private {?Element} */
    var _this = textfield__possibleConstructorReturn(this, _MDCComponent.call.apply(_MDCComponent, [this].concat(args)));

    _this.input_;
    /** @private {?Element} */
    _this.label_;
    /** @type {?Element} */
    _this.helptextElement;
    /** @type {?MDCRipple} */
    _this.ripple;
    /** @private {?Element} */
    _this.bottomLine_;
    /** @private {?Element} */
    _this.icon_;
    return _this;
  }

  /**
   * @param {!Element} root
   * @return {!MDCTextfield}
   */


  MDCTextfield.attachTo = function attachTo(root) {
    return new MDCTextfield(root);
  };

  /**
   * @param {(function(!Element): !MDCRipple)=} rippleFactory A function which
   * creates a new MDCRipple.
   */


  MDCTextfield.prototype.initialize = function initialize() {
    var rippleFactory = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function (el) {
      return new ripple_MDCRipple(el);
    };

    this.input_ = this.root_.querySelector(textfield_constants_strings.INPUT_SELECTOR);
    this.label_ = this.root_.querySelector(textfield_constants_strings.LABEL_SELECTOR);
    this.helptextElement = null;
    this.ripple = null;
    if (this.input_.hasAttribute('aria-controls')) {
      this.helptextElement = document.getElementById(this.input_.getAttribute('aria-controls'));
    }
    if (this.root_.classList.contains(textfield_constants_cssClasses.BOX)) {
      this.ripple = rippleFactory(this.root_);
    };
    if (!this.root_.classList.contains(textfield_constants_cssClasses.TEXTAREA)) {
      this.bottomLine_ = this.root_.querySelector(textfield_constants_strings.BOTTOM_LINE_SELECTOR);
    };
    if (!this.root_.classList.contains(textfield_constants_cssClasses.TEXT_FIELD_ICON)) {
      this.icon_ = this.root_.querySelector(textfield_constants_strings.ICON_SELECTOR);
    };
  };

  MDCTextfield.prototype.destroy = function destroy() {
    if (this.ripple) {
      this.ripple.destroy();
    }
    _MDCComponent.prototype.destroy.call(this);
  };

  /**
   * Initiliazes the Textfield's internal state based on the environment's
   * state.
   */


  MDCTextfield.prototype.initialSyncWithDom = function initialSyncWithDom() {
    this.disabled = this.input_.disabled;
  };

  /**
   * @return {boolean} True if the Textfield is disabled.
   */


  /**
   * @return {!MDCTextfieldFoundation}
   */
  MDCTextfield.prototype.getDefaultFoundation = function getDefaultFoundation() {
    var _this2 = this;

    return new textfield_foundation( /** @type {!MDCTextfieldAdapter} */textfield__extends({
      addClass: function addClass(className) {
        return _this2.root_.classList.add(className);
      },
      removeClass: function removeClass(className) {
        return _this2.root_.classList.remove(className);
      },
      addClassToLabel: function addClassToLabel(className) {
        var label = _this2.label_;
        if (label) {
          label.classList.add(className);
        }
      },
      removeClassFromLabel: function removeClassFromLabel(className) {
        var label = _this2.label_;
        if (label) {
          label.classList.remove(className);
        }
      },
      eventTargetHasClass: function eventTargetHasClass(target, className) {
        return target.classList.contains(className);
      },
      registerTextFieldInteractionHandler: function registerTextFieldInteractionHandler(evtType, handler) {
        return _this2.root_.addEventListener(evtType, handler);
      },
      deregisterTextFieldInteractionHandler: function deregisterTextFieldInteractionHandler(evtType, handler) {
        return _this2.root_.removeEventListener(evtType, handler);
      },
      notifyIconAction: function notifyIconAction() {
        return _this2.emit(textfield_foundation.strings.ICON_EVENT, {});
      }
    }, this.getInputAdapterMethods_(), this.getHelptextAdapterMethods_(), this.getBottomLineAdapterMethods_(), this.getIconAdapterMethods_()));
  };

  /**
   * @return {!{
   *   setIconAttr: function(string, string): undefined,
   * }}
   */


  MDCTextfield.prototype.getIconAdapterMethods_ = function getIconAdapterMethods_() {
    var _this3 = this;

    return {
      setIconAttr: function setIconAttr(name, value) {
        if (_this3.icon_) {
          _this3.icon_.setAttribute(name, value);
        }
      }
    };
  };

  /**
   * @return {!{
   *   addClassToBottomLine: function(string): undefined,
   *   removeClassFromBottomLine: function(string): undefined,
   *   setBottomLineAttr: function(string, string): undefined,
   *   registerTransitionEndHandler: function(function()): undefined,
   *   deregisterTransitionEndHandler: function(function()): undefined,
   * }}
   */


  MDCTextfield.prototype.getBottomLineAdapterMethods_ = function getBottomLineAdapterMethods_() {
    var _this4 = this;

    return {
      addClassToBottomLine: function addClassToBottomLine(className) {
        if (_this4.bottomLine_) {
          _this4.bottomLine_.classList.add(className);
        }
      },
      removeClassFromBottomLine: function removeClassFromBottomLine(className) {
        if (_this4.bottomLine_) {
          _this4.bottomLine_.classList.remove(className);
        }
      },
      setBottomLineAttr: function setBottomLineAttr(attr, value) {
        if (_this4.bottomLine_) {
          _this4.bottomLine_.setAttribute(attr, value);
        }
      },
      registerTransitionEndHandler: function registerTransitionEndHandler(handler) {
        if (_this4.bottomLine_) {
          _this4.bottomLine_.addEventListener('transitionend', handler);
        }
      },
      deregisterTransitionEndHandler: function deregisterTransitionEndHandler(handler) {
        if (_this4.bottomLine_) {
          _this4.bottomLine_.removeEventListener('transitionend', handler);
        }
      }
    };
  };

  /**
   * @return {!{
   *   registerInputInteractionHandler: function(string, function()): undefined,
   *   deregisterInputInteractionHandler: function(string, function()): undefined,
   *   getNativeInput: function(): ?Element,
   * }}
   */


  MDCTextfield.prototype.getInputAdapterMethods_ = function getInputAdapterMethods_() {
    var _this5 = this;

    return {
      registerInputInteractionHandler: function registerInputInteractionHandler(evtType, handler) {
        return _this5.input_.addEventListener(evtType, handler);
      },
      deregisterInputInteractionHandler: function deregisterInputInteractionHandler(evtType, handler) {
        return _this5.input_.removeEventListener(evtType, handler);
      },
      getNativeInput: function getNativeInput() {
        return _this5.input_;
      }
    };
  };

  /**
   * @return {!{
   *   addClassToHelptext: function(string): undefined,
   *   removeClassFromHelptext: function(string): undefined,
   *   helptextHasClass: function(string): boolean,
   *   setHelptextAttr: function(string, string): undefined,
   *   removeHelptextAttr: function(string): undefined,
   * }}
   */


  MDCTextfield.prototype.getHelptextAdapterMethods_ = function getHelptextAdapterMethods_() {
    var _this6 = this;

    return {
      addClassToHelptext: function addClassToHelptext(className) {
        if (_this6.helptextElement) {
          _this6.helptextElement.classList.add(className);
        }
      },
      removeClassFromHelptext: function removeClassFromHelptext(className) {
        if (_this6.helptextElement) {
          _this6.helptextElement.classList.remove(className);
        }
      },
      helptextHasClass: function helptextHasClass(className) {
        if (!_this6.helptextElement) {
          return false;
        }
        return _this6.helptextElement.classList.contains(className);
      },
      setHelptextAttr: function setHelptextAttr(name, value) {
        if (_this6.helptextElement) {
          _this6.helptextElement.setAttribute(name, value);
        }
      },
      removeHelptextAttr: function removeHelptextAttr(name) {
        if (_this6.helptextElement) {
          _this6.helptextElement.removeAttribute(name);
        }
      }
    };
  };

  textfield__createClass(MDCTextfield, [{
    key: 'disabled',
    get: function get() {
      return this.foundation_.isDisabled();
    }

    /**
     * @param {boolean} disabled Sets the Textfield disabled or enabled.
     */
    ,
    set: function set(disabled) {
      this.foundation_.setDisabled(disabled);
    }

    /**
     * @param {boolean} valid Sets the Textfield valid or invalid.
     */

  }, {
    key: 'valid',
    set: function set(valid) {
      this.foundation_.setValid(valid);
    }
  }]);

  return MDCTextfield;
}(component);


// CONCATENATED MODULE: ../node_modules/preact-material-components/Textfield/index.js
function Textfield__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Textfield__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function Textfield__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Textfield__extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

function Textfield__objectWithoutProperties(obj, keys) {
  var target = {};for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;target[i] = obj[i];
  }return target;
}





/**
 * @prop persistent = false
 * @prop validation-msg = false
 */

var Textfield_Helptext = function (_MaterialComponent) {
  Textfield__inherits(Helptext, _MaterialComponent);

  function Helptext() {
    Textfield__classCallCheck(this, Helptext);

    var _this = Textfield__possibleConstructorReturn(this, _MaterialComponent.call(this));

    _this.componentName = "textfield-helptext";
    _this._mdcProps = ["persistent", "validation-msg"];
    return _this;
  }

  Helptext.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("p", Textfield__extends({}, props, { "aria-hidden": "true" }), props.children);
  };

  return Helptext;
}(MaterialComponent_MaterialComponent);

var Textfield_Label = function (_MaterialComponent2) {
  Textfield__inherits(Label, _MaterialComponent2);

  function Label() {
    Textfield__classCallCheck(this, Label);

    var _this2 = Textfield__possibleConstructorReturn(this, _MaterialComponent2.call(this));

    _this2.componentName = "textfield__label";
    return _this2;
  }

  Label.prototype.materialDom = function materialDom(props) {
    return Object(preact_min["h"])("label", props, props.children);
  };

  return Label;
}(MaterialComponent_MaterialComponent);

var Textfield_defaultProps = {
  valid: true
};

/**
 * @prop fullwidth = false
 * @prop multiline = false
 * @prop dense = false
 * @prop disabled = false
 * @prop box = false
 * @prop type = 'text'
 * @prop value = ''
 * @prop label = ''
 */

var Textfield_TextfieldInput = function (_MaterialComponent3) {
  Textfield__inherits(TextfieldInput, _MaterialComponent3);

  function TextfieldInput() {
    Textfield__classCallCheck(this, TextfieldInput);

    var _this3 = Textfield__possibleConstructorReturn(this, _MaterialComponent3.call(this));

    _this3.componentName = "textfield";
    _this3._mdcProps = ["fullwidth", "multiline", "dense", "disabled", "box"];
    _this3.state = {
      showFloatingLabel: false
    };
    return _this3;
  }

  TextfieldInput.prototype.componentDidMount = function componentDidMount() {
    var _this4 = this;

    this.setState({
      showFloatingLabel: true
    }, function () {
      _this4.MDComponent = new textfield_MDCTextfield(_this4.control);
      _this4.props.onInit && _this4.props.onInit(_this4.MDComponent);
      setValid(Textfield_defaultProps, _this4.props, _this4.MDComponent);
    });
  };

  TextfieldInput.prototype.componentWillUpdate = function componentWillUpdate(nextProps) {
    setValid(this.props, nextProps, this.MDComponent);
  };

  TextfieldInput.prototype.componentWillUnmount = function componentWillUnmount() {
    this.MDComponent && this.MDComponent.destroy && this.MDComponent.destroy();
  };

  TextfieldInput.prototype.materialDom = function materialDom(allprops) {
    var _this5 = this;

    var className = allprops.className,
        props = Textfield__objectWithoutProperties(allprops, ["className"]);


    if ("value" in props && this.state.showFloatingLabel) {
      className = [className, "mdc-textfield--upgraded"].join(" ");
    }

    return Object(preact_min["h"])("div", { className: className, ref: function ref(control) {
        return _this5.control = control;
      } }, props.multiline ? Object(preact_min["h"])("textarea", Textfield__extends({ className: "mdc-textfield__input" }, props)) : Object(preact_min["h"])("input", Textfield__extends({
      type: props.type || "text",
      className: "mdc-textfield__input"
    }, props)), props.label && this.state.showFloatingLabel && Object(preact_min["h"])(Textfield_Label, { "for": props.id }, props.label), props.multiline ? "" : Object(preact_min["h"])("div", { "class": "mdc-textfield__bottom-line" }));
  };

  return TextfieldInput;
}(MaterialComponent_MaterialComponent);

/**
 * @prop fullwidth = false
 * @prop multiline = false
 * @prop dense = false
 * @prop disabled = false
 * @prop box = false
 * @prop type = 'text'
 * @prop value = ''
 * @prop label = ''
 * @prop helptext = ''
 * @prop helptextPersistent = false
 * @prop helptextValidationMsg = false
 */


var Textfield_Textfield = function (_Component) {
  Textfield__inherits(Textfield, _Component);

  function Textfield() {
    Textfield__classCallCheck(this, Textfield);

    var _this6 = Textfield__possibleConstructorReturn(this, _Component.call(this));

    _this6.id = Textfield.uid();
    _this6.state = {
      showFloatingLabel: false
    };
    return _this6;
  }

  Textfield.prototype.componentDidMount = function componentDidMount() {
    this.setState({
      showFloatingLabel: true
    });
  };

  Textfield.uid = function uid() {
    if (!this.uidCounter) {
      this.uidCounter = 0;
    }
    return ++this.uidCounter;
  };

  Textfield.prototype.render = function render(allprops, _ref) {
    var _this7 = this;

    var showFloatingLabel = _ref.showFloatingLabel;

    var className = allprops.className,
        helptextPersistent = allprops.helptextPersistent,
        helptextValidationMsg = allprops.helptextValidationMsg,
        props = Textfield__objectWithoutProperties(allprops, ["className", "helptextPersistent", "helptextValidationMsg"]);

    var showDiv = props.helptext || props.label && !showFloatingLabel;

    if (showDiv && !props.id) {
      props.id = "tf-" + this.id;
    }

    // Helper text
    var helptextProps = {
      persistent: helptextPersistent,
      "validation-msg": helptextValidationMsg
    };

    return showDiv ? Object(preact_min["h"])("div", { className: className }, props.label && !showFloatingLabel && Object(preact_min["h"])("label", { "for": props.id }, props.cssLabel || props.label + ": "), Object(preact_min["h"])(Textfield_TextfieldInput, Textfield__extends({}, props, {
      onInit: function onInit(MDComponent) {
        _this7.MDComponent = MDComponent;
      },
      "aria-controls": props.helptext && props.id + "-helptext"
    })), props.helptext && Object(preact_min["h"])(Textfield_Helptext, Textfield__extends({ id: props.id + "-helptext" }, helptextProps), props.helptext)) : Object(preact_min["h"])(Textfield_TextfieldInput, Textfield__extends({}, props, {
      className: className,
      onInit: function onInit(MDComponent) {
        _this7.MDComponent = MDComponent;
      }
    }));
  };

  return Textfield;
}(preact_min["Component"]);

function setValid(oldprops, newprops, textfield) {
  if ("valid" in oldprops && "valid" in newprops && oldprops.valid !== newprops.valid) {
    textfield.valid = newprops.valid;
  }
}

Textfield_Textfield.Helptext = Textfield_Helptext;

/* harmony default export */ var preact_material_components_Textfield = (Textfield_Textfield);
// EXTERNAL MODULE: ../node_modules/preact-material-components/Textfield/style.css
var Textfield_style = __webpack_require__("Z+tv");
var Textfield_style_default = /*#__PURE__*/__webpack_require__.n(Textfield_style);

// CONCATENATED MODULE: ./components/uiBet/index.js


function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function uiBet__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function uiBet__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function uiBet__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







var uiBet_UIBet = function (_Component) {
	uiBet__inherits(UIBet, _Component);

	function UIBet(_ref) {
		var initialStake = _ref.initialStake;

		uiBet__classCallCheck(this, UIBet);

		var _this = uiBet__possibleConstructorReturn(this, _Component.call(this));

		_this.onTextChanged = _this.onTextChanged.bind(_this);
		_this.onDelete = _this.onDelete.bind(_this);
		return _this;
	}

	UIBet.prototype.onTextChanged = function onTextChanged(event) {
		if (isNaN(event.target.value) || !event.target.value || event.target.value === '') {
			this.props.callbackOnStakeChange({
				betId: this.props.betId,
				stake: 0
			});
			return;
		}
		this.props.callbackOnStakeChange({
			betId: this.props.betId,
			stake: event.target.value
		});
	};

	UIBet.prototype.onDelete = function onDelete(event) {
		this.props.callbackOnDelete({
			betId: this.props.betId
		});
	};

	UIBet.prototype.render = function render(_ref2, _ref3) {
		_objectDestructuringEmpty(_ref3);

		_objectDestructuringEmpty(_ref2);

		return Object(preact_min["h"])(
			preact_material_components_Card,
			null,
			Object(preact_min["h"])(
				preact_material_components_Card.HorizontalBlock,
				null,
				Object(preact_min["h"])(
					preact_material_components_Card.Primary,
					null,
					Object(preact_min["h"])(
						preact_material_components_Card.Title,
						{ large: true },
						this.props.outcomeName
					),
					Object(preact_min["h"])(
						preact_material_components_Card.Subtitle,
						null,
						this.props.eventName,
						' @',
						this.props.price
					),
					Object(preact_min["h"])(
						preact_material_components_Card.HorizontalBlock,
						null,
						' ',
						Object(preact_min["h"])(preact_material_components_Textfield, {
							type: 'number',
							step: '0.01',
							label: 'Enter Stake',
							value: this.props.initialStake,
							onChange: this.onTextChanged,
							onKeyUp: this.onTextChanged
						})
					)
				)
			),
			Object(preact_min["h"])(
				preact_material_components_Card.Actions,
				null,
				Object(preact_min["h"])(
					preact_material_components_Card.Action,
					{ onClick: this.onDelete },
					'REMOVE'
				),
				Object(preact_min["h"])(
					preact_material_components_Card.Title,
					null,
					Object(preact_min["h"])(
						'h2',
						null,
						this.props.receipt
					)
				)
			)
		);
	};

	return UIBet;
}(preact_min["Component"]);


// EXTERNAL MODULE: ./components/uiBetslip/style.css
var uiBetslip_style = __webpack_require__("51Py");
var uiBetslip_style_default = /*#__PURE__*/__webpack_require__.n(uiBetslip_style);

// CONCATENATED MODULE: ../node_modules/unfetch/dist/unfetch.es.js
var index = typeof fetch == 'function' ? fetch.bind() : function (url, options) {
	options = options || {};
	return new Promise(function (resolve, reject) {
		var request = new XMLHttpRequest();

		request.open(options.method || 'get', url);

		for (var i in options.headers) {
			request.setRequestHeader(i, options.headers[i]);
		}

		request.withCredentials = options.credentials == 'include';

		request.onload = function () {
			resolve(response());
		};

		request.onerror = reject;

		request.send(options.body);

		function response() {
			var _keys = [],
			    all = [],
			    headers = {},
			    header;

			request.getAllResponseHeaders().replace(/^(.*?):\s*([\s\S]*?)$/gm, function (m, key, value) {
				_keys.push(key = key.toLowerCase());
				all.push([key, value]);
				header = headers[key];
				headers[key] = header ? header + "," + value : value;
			});

			return {
				ok: (request.status / 200 | 0) == 1, // 200-299
				status: request.status,
				statusText: request.statusText,
				url: request.responseURL,
				clone: response,
				text: function text() {
					return Promise.resolve(request.responseText);
				},
				json: function json() {
					return Promise.resolve(request.responseText).then(JSON.parse);
				},
				blob: function blob() {
					return Promise.resolve(new Blob([request.response]));
				},
				headers: {
					keys: function keys() {
						return _keys;
					},
					entries: function entries() {
						return all;
					},
					get: function get(n) {
						return headers[n.toLowerCase()];
					},
					has: function has(n) {
						return n.toLowerCase() in headers;
					}
				}
			};
		}
	});
};

/* harmony default export */ var unfetch_es = (index);
//# sourceMappingURL=unfetch.es.js.map
// CONCATENATED MODULE: ./utils/fetchApi.js


/* harmony default export */ var fetchApi = (function (url, body, callback) {
	unfetch_es(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(body)
	}).then(function (response) {
		return response.json();
	}).then(function (data) {
		callback(data);
	});
});
// EXTERNAL MODULE: external "timers"
var external__timers_ = __webpack_require__("KrYd");
var external__timers__default = /*#__PURE__*/__webpack_require__.n(external__timers_);

// CONCATENATED MODULE: ./components/uiBetslip/index.js


function uiBetslip__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function uiBetslip__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function uiBetslip__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

















var uiBetslip__ref = Object(preact_min["h"])('hr', null);

var uiBetslip_UIBetslip = function (_Component) {
	uiBetslip__inherits(UIBetslip, _Component);

	function UIBetslip() {
		uiBetslip__classCallCheck(this, UIBetslip);

		var _this = uiBetslip__possibleConstructorReturn(this, _Component.call(this));

		_this.state = {
			bets: []
		};

		_this.wireup();
		_this.doSubmit = _this.doSubmit.bind(_this);
		return _this;
	}

	UIBetslip.prototype.doSubmit = function doSubmit() {
		var url = '//localhost:8080/';
		var self = this;
		Object(external__timers_["setTimeout"])(function () {
			var a = self.state.bets.slice();
			a.map(function (b) {
				b.receipt = 'ok';
			});
			//remove all successful bets
			self.setState({
				bets: []
			});
			self.bar.MDComponent.show({
				message: a.length + ' bets placed.'
			});
			localStorage.setItem('bets', JSON.stringify(self.state.bets));
		}, 100);

		// fetchApi(url, { }, (data) => {
		// 	  //callback(data);
		// 	  let a = this.state.bets.slice();
		// 	  a.map(b => {
		// 			  b.receipt = 'ok';
		// 	  });
		// 	  //remove all successful bets
		// 	  this.setState({
		// 		  bets: []
		// 	  });
		// 	  this.bar.MDComponent.show({
		// 		message: `${a.length} bets placed.`
		// 	  });
		// 	  localStorage.setItem('bets', JSON.stringify(this.state.bets));
		//   });
	};

	UIBetslip.prototype.wireup = function wireup() {
		var _this2 = this;

		this.totalStake = function () {
			var total = 0;
			if (!(_this2.state.bets && _this2.state.bets.length > 0)) return total;
			if (_this2.state.bets.length > 1) total = _this2.state.bets.reduce(function (a, b) {
				return a.stake + b.stake;
			});else total = _this2.state.bets[0].stake;
			return total;
		};

		this.betCount = function () {
			var total = 0;
			if (_this2.state.bets && _this2.state.bets.length > 0) total = _this2.state.bets.length;
			return total;
		};

		this.totalPayout = function () {
			var total = 0;
			if (!(_this2.state.bets && _this2.state.bets.length > 0)) return total;
			if (_this2.state.bets.length > 1) total = _this2.state.bets.reduce(function (a, b) {
				return a.payout + b.payout;
			});else total = _this2.state.bets[0].payout;
			return total;
		};

		this.totalTax = function () {
			var total = 0;
			return total;
		};
	};

	UIBetslip.prototype.componentWillMount = function componentWillMount() {
		//check if Preact is in SSR or CSR
		if (typeof window !== 'undefined') {
			var storedBets = localStorage.getItem('bets');
			if (storedBets !== null) this.setState({
				bets: JSON.parse(storedBets)
			});
		}
	};

	UIBetslip.prototype.componentDidMount = function componentDidMount() {
		var _this3 = this;

		window.addEventListener('add-bet', function (e) {
			var b = new Bet();
			b.eventName = e.detail.eventName;
			b.outcomeName = e.detail.outcomeName;
			b.price = e.detail.price;
			var existingBets = _this3.state.bets.slice();
			var maxId = existingBets.length ? Math.max.apply(null, existingBets.map(function (b) {
				return b.betId;
			})) : 0;
			b.betId = maxId + 1;
			b.stake = null;
			existingBets.push(b);
			_this3.setState({ bets: existingBets });
			localStorage.setItem('bets', JSON.stringify(_this3.state.bets));
		});
	};

	UIBetslip.prototype.onBetStakeChanged = function onBetStakeChanged(newState) {
		var a = this.state.bets.slice();
		a.map(function (b) {
			if (b.betId === newState.betId) {
				b.stake = parseFloat(newState.stake);
				b.payout = b.price * b.stake;
			}
		});
		this.setState({
			bets: a
		});
		localStorage.setItem('bets', JSON.stringify(this.state.bets));
	};

	UIBetslip.prototype.onBetDeleted = function onBetDeleted(newState) {
		var a = this.state.bets.slice();
		a = a.filter(function (e) {
			return e.betId !== newState.betId;
		});
		this.setState({
			bets: a
		});
		localStorage.setItem('bets', JSON.stringify(this.state.bets));
	};

	UIBetslip.prototype.render = function render() {
		var _this4 = this;

		var self = this;
		return Object(preact_min["h"])(
			'div',
			{ className: uiBetslip_style_default.a.betslip },
			Object(preact_min["h"])(
				preact_material_components_Card,
				{ className: uiBetslip_style_default.a.bottombar },
				Object(preact_min["h"])(
					preact_material_components_Card.Primary,
					null,
					Object(preact_min["h"])(
						preact_material_components_Card.Title,
						null,
						Object(preact_min["h"])(
							'h2',
							null,
							'Betslip (',
							this.betCount(),
							')'
						)
					)
				)
			),
			this.state.bets.map(function (d, idx) {
				return Object(preact_min["h"])(uiBet_UIBet, {
					key: idx,
					initialStake: d.stake,
					betId: d.betId,
					outcomeName: d.outcomeName,
					eventName: d.eventName,
					price: d.price,
					receipt: d.receipt,
					callbackOnStakeChange: function callbackOnStakeChange(newState) {
						return self.onBetStakeChanged(newState);
					},
					callbackOnDelete: function callbackOnDelete(newState) {
						return self.onBetDeleted(newState);
					},
					className: uiBetslip_style_default.a.cardcontent
				});
			}),
			uiBetslip__ref,
			Object(preact_min["h"])(
				'div',
				{ className: uiBetslip_style_default.a.bottombar },
				Object(preact_min["h"])(
					'div',
					{ className: uiBetslip_style_default.a.bottombar__details },
					Object(preact_min["h"])(
						'div',
						{ className: uiBetslip_style_default.a.bottombar__labels },
						Object(preact_min["h"])(
							'div',
							null,
							'Stake: \u20AC',
							this.totalStake()
						),
						Object(preact_min["h"])(
							'div',
							null,
							'Tax: \u20AC',
							this.totalTax()
						),
						Object(preact_min["h"])(
							'div',
							null,
							'Return: \u20AC',
							this.totalPayout()
						)
					)
				),
				Object(preact_min["h"])(
					preact_material_components_Button,
					{ ripple: true, raised: true, onClick: function onClick() {
							return _this4.doSubmit();
						} },
					'Bet \u20AC',
					this.totalStake()
				)
			),
			Object(preact_min["h"])(Snackbar_Snackbar, { ref: function ref(bar) {
					_this4.bar = bar;
				} })
		);
	};

	return UIBetslip;
}(preact_min["Component"]);


// CONCATENATED MODULE: ./routes/home/index.js


function home__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function home__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function home__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }









var home__ref = Object(preact_min["h"])('hr', null);

var home__ref2 = Object(preact_min["h"])('hr', null);

var home__ref3 = Object(preact_min["h"])('hr', null);

var home__ref4 = Object(preact_min["h"])(
	'div',
	{ 'class': 'bet-slip' },
	Object(preact_min["h"])(uiBetslip_UIBetslip, null)
);

var home_Home = function (_Component) {
	home__inherits(Home, _Component);

	function Home() {
		home__classCallCheck(this, Home);

		return home__possibleConstructorReturn(this, _Component.apply(this, arguments));
	}

	Home.prototype.sendBet = function sendBet(eventName, outcomeName, price) {
		var ev = new CustomEvent('add-bet', { detail: {
				eventName: eventName, outcomeName: outcomeName, price: price
			} });
		window.dispatchEvent(ev);
	};

	Home.prototype.render = function render() {
		var _this2 = this;

		return Object(preact_min["h"])(
			'div',
			{ 'class': 'container' },
			Object(preact_min["h"])(
				'div',
				{ 'class': 'markets' },
				home__ref,
				Object(preact_min["h"])(
					preact_material_components_Button,
					{ ripple: true, raised: true, onClick: function onClick() {
							return _this2.sendBet('MU vs CH', 'MU', 1.5);
						} },
					'Man United - 1.5'
				),
				home__ref2,
				Object(preact_min["h"])(
					preact_material_components_Button,
					{ ripple: true, raised: true, onClick: function onClick() {
							return _this2.sendBet('MU vs CH', 'CH', 3.0);
						} },
					'Chelsea - 3.0'
				),
				home__ref3
			),
			home__ref4
		);
	};

	return Home;
}(preact_min["Component"]);


// EXTERNAL MODULE: ./routes/profile/style.css
var profile_style = __webpack_require__("Tv6c");
var profile_style_default = /*#__PURE__*/__webpack_require__.n(profile_style);

// CONCATENATED MODULE: ./routes/profile/index.js


function profile__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function profile__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function profile__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var profile_Profile = function (_Component) {
	profile__inherits(Profile, _Component);

	function Profile() {
		var _temp, _this, _ret;

		profile__classCallCheck(this, Profile);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = profile__possibleConstructorReturn(this, _Component.call.apply(_Component, [this].concat(args))), _this), _this.state = {
			time: Date.now(),
			count: 10
		}, _this.updateTime = function () {
			_this.setState({ time: Date.now() });
		}, _this.increment = function () {
			_this.setState({ count: _this.state.count + 1 });
		}, _temp), profile__possibleConstructorReturn(_this, _ret);
	}

	// gets called when this route is navigated to
	Profile.prototype.componentDidMount = function componentDidMount() {
		// start a timer for the clock:
		this.timer = setInterval(this.updateTime, 1000);
	};

	// gets called just before navigating away from the route


	Profile.prototype.componentWillUnmount = function componentWillUnmount() {
		clearInterval(this.timer);
	};

	// update the current time


	// Note: `user` comes from the URL, courtesy of our router
	Profile.prototype.render = function render(_ref, _ref2) {
		var user = _ref.user;
		var time = _ref2.time,
		    count = _ref2.count;

		return Object(preact_min["h"])(
			'div',
			{ 'class': profile_style_default.a.profile },
			Object(preact_min["h"])(
				'h1',
				null,
				'Profile: ',
				user
			),
			Object(preact_min["h"])(
				'p',
				null,
				'This is the user profile for a user named ',
				user,
				'.'
			),
			Object(preact_min["h"])(
				'div',
				null,
				'Current time: ',
				new Date(time).toLocaleString()
			),
			Object(preact_min["h"])(
				'p',
				null,
				Object(preact_min["h"])(
					preact_material_components_Button,
					{ raised: true, primary: true, ripple: true, onClick: this.increment },
					'Click Me'
				),
				' ',
				'Clicked ',
				count,
				' times.'
			)
		);
	};

	return Profile;
}(preact_min["Component"]);


// CONCATENATED MODULE: ./components/app.js


function app__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function app__possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function app__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







// import Home from 'async!../routes/home';
// import Profile from 'async!../routes/profile';

var app__ref = Object(preact_min["h"])(header_Header, null);

var app__ref2 = Object(preact_min["h"])(home_Home, { path: '/' });

var app__ref3 = Object(preact_min["h"])(profile_Profile, { path: '/profile/', user: 'me' });

var app__ref4 = Object(preact_min["h"])(profile_Profile, { path: '/profile/:user' });

var app_App = function (_Component) {
	app__inherits(App, _Component);

	function App() {
		var _temp, _this, _ret;

		app__classCallCheck(this, App);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = app__possibleConstructorReturn(this, _Component.call.apply(_Component, [this].concat(args))), _this), _this.handleRoute = function (e) {
			_this.currentUrl = e.url;
		}, _temp), app__possibleConstructorReturn(_this, _ret);
	}
	/** Gets fired when the route changes.
  *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
  *	@param {string} event.url	The newly routed URL
  */


	App.prototype.render = function render() {
		return Object(preact_min["h"])(
			'div',
			{ id: 'app' },
			app__ref,
			Object(preact_min["h"])(
				preact_router_es_Router,
				{ onChange: this.handleRoute },
				app__ref2,
				app__ref3,
				app__ref4
			)
		);
	};

	return App;
}(preact_min["Component"]);


// CONCATENATED MODULE: ./index.js



/* harmony default export */ var index_0 = __webpack_exports__["default"] = (app_App);

/***/ }),

/***/ "KM04":
/***/ (function(module, exports, __webpack_require__) {

!function () {
  "use strict";
  function e() {}function t(t, n) {
    var o,
        r,
        i,
        l,
        a = E;for (l = arguments.length; l-- > 2;) {
      W.push(arguments[l]);
    }n && null != n.children && (W.length || W.push(n.children), delete n.children);while (W.length) {
      if ((r = W.pop()) && void 0 !== r.pop) for (l = r.length; l--;) {
        W.push(r[l]);
      } else "boolean" == typeof r && (r = null), (i = "function" != typeof t) && (null == r ? r = "" : "number" == typeof r ? r += "" : "string" != typeof r && (i = !1)), i && o ? a[a.length - 1] += r : a === E ? a = [r] : a.push(r), o = i;
    }var u = new e();return u.nodeName = t, u.children = a, u.attributes = null == n ? void 0 : n, u.key = null == n ? void 0 : n.key, void 0 !== S.vnode && S.vnode(u), u;
  }function n(e, t) {
    for (var n in t) {
      e[n] = t[n];
    }return e;
  }function o(e, o) {
    return t(e.nodeName, n(n({}, e.attributes), o), arguments.length > 2 ? [].slice.call(arguments, 2) : e.children);
  }function r(e) {
    !e.__d && (e.__d = !0) && 1 == A.push(e) && (S.debounceRendering || P)(i);
  }function i() {
    var e,
        t = A;A = [];while (e = t.pop()) {
      e.__d && k(e);
    }
  }function l(e, t, n) {
    return "string" == typeof t || "number" == typeof t ? void 0 !== e.splitText : "string" == typeof t.nodeName ? !e._componentConstructor && a(e, t.nodeName) : n || e._componentConstructor === t.nodeName;
  }function a(e, t) {
    return e.__n === t || e.nodeName.toLowerCase() === t.toLowerCase();
  }function u(e) {
    var t = n({}, e.attributes);t.children = e.children;var o = e.nodeName.defaultProps;if (void 0 !== o) for (var r in o) {
      void 0 === t[r] && (t[r] = o[r]);
    }return t;
  }function _(e, t) {
    var n = t ? document.createElementNS("http://www.w3.org/2000/svg", e) : document.createElement(e);return n.__n = e, n;
  }function p(e) {
    var t = e.parentNode;t && t.removeChild(e);
  }function c(e, t, n, o, r) {
    if ("className" === t && (t = "class"), "key" === t) ;else if ("ref" === t) n && n(null), o && o(e);else if ("class" !== t || r) {
      if ("style" === t) {
        if (o && "string" != typeof o && "string" != typeof n || (e.style.cssText = o || ""), o && "object" == typeof o) {
          if ("string" != typeof n) for (var i in n) {
            i in o || (e.style[i] = "");
          }for (var i in o) {
            e.style[i] = "number" == typeof o[i] && !1 === V.test(i) ? o[i] + "px" : o[i];
          }
        }
      } else if ("dangerouslySetInnerHTML" === t) o && (e.innerHTML = o.__html || "");else if ("o" == t[0] && "n" == t[1]) {
        var l = t !== (t = t.replace(/Capture$/, ""));t = t.toLowerCase().substring(2), o ? n || e.addEventListener(t, f, l) : e.removeEventListener(t, f, l), (e.__l || (e.__l = {}))[t] = o;
      } else if ("list" !== t && "type" !== t && !r && t in e) s(e, t, null == o ? "" : o), null != o && !1 !== o || e.removeAttribute(t);else {
        var a = r && t !== (t = t.replace(/^xlink\:?/, ""));null == o || !1 === o ? a ? e.removeAttributeNS("http://www.w3.org/1999/xlink", t.toLowerCase()) : e.removeAttribute(t) : "function" != typeof o && (a ? e.setAttributeNS("http://www.w3.org/1999/xlink", t.toLowerCase(), o) : e.setAttribute(t, o));
      }
    } else e.className = o || "";
  }function s(e, t, n) {
    try {
      e[t] = n;
    } catch (e) {}
  }function f(e) {
    return this.__l[e.type](S.event && S.event(e) || e);
  }function d() {
    var e;while (e = D.pop()) {
      S.afterMount && S.afterMount(e), e.componentDidMount && e.componentDidMount();
    }
  }function h(e, t, n, o, r, i) {
    H++ || (R = null != r && void 0 !== r.ownerSVGElement, j = null != e && !("__preactattr_" in e));var l = m(e, t, n, o, i);return r && l.parentNode !== r && r.appendChild(l), --H || (j = !1, i || d()), l;
  }function m(e, t, n, o, r) {
    var i = e,
        l = R;if (null != t && "boolean" != typeof t || (t = ""), "string" == typeof t || "number" == typeof t) return e && void 0 !== e.splitText && e.parentNode && (!e._component || r) ? e.nodeValue != t && (e.nodeValue = t) : (i = document.createTextNode(t), e && (e.parentNode && e.parentNode.replaceChild(i, e), b(e, !0))), i.__preactattr_ = !0, i;var u = t.nodeName;if ("function" == typeof u) return U(e, t, n, o);if (R = "svg" === u || "foreignObject" !== u && R, u += "", (!e || !a(e, u)) && (i = _(u, R), e)) {
      while (e.firstChild) {
        i.appendChild(e.firstChild);
      }e.parentNode && e.parentNode.replaceChild(i, e), b(e, !0);
    }var p = i.firstChild,
        c = i.__preactattr_,
        s = t.children;if (null == c) {
      c = i.__preactattr_ = {};for (var f = i.attributes, d = f.length; d--;) {
        c[f[d].name] = f[d].value;
      }
    }return !j && s && 1 === s.length && "string" == typeof s[0] && null != p && void 0 !== p.splitText && null == p.nextSibling ? p.nodeValue != s[0] && (p.nodeValue = s[0]) : (s && s.length || null != p) && v(i, s, n, o, j || null != c.dangerouslySetInnerHTML), g(i, t.attributes, c), R = l, i;
  }function v(e, t, n, o, r) {
    var i,
        a,
        u,
        _,
        c,
        s = e.childNodes,
        f = [],
        d = {},
        h = 0,
        v = 0,
        y = s.length,
        g = 0,
        w = t ? t.length : 0;if (0 !== y) for (var C = 0; C < y; C++) {
      var x = s[C],
          N = x.__preactattr_,
          k = w && N ? x._component ? x._component.__k : N.key : null;null != k ? (h++, d[k] = x) : (N || (void 0 !== x.splitText ? !r || x.nodeValue.trim() : r)) && (f[g++] = x);
    }if (0 !== w) for (var C = 0; C < w; C++) {
      _ = t[C], c = null;var k = _.key;if (null != k) h && void 0 !== d[k] && (c = d[k], d[k] = void 0, h--);else if (!c && v < g) for (i = v; i < g; i++) {
        if (void 0 !== f[i] && l(a = f[i], _, r)) {
          c = a, f[i] = void 0, i === g - 1 && g--, i === v && v++;break;
        }
      }c = m(c, _, n, o), u = s[C], c && c !== e && c !== u && (null == u ? e.appendChild(c) : c === u.nextSibling ? p(u) : e.insertBefore(c, u));
    }if (h) for (var C in d) {
      void 0 !== d[C] && b(d[C], !1);
    }while (v <= g) {
      void 0 !== (c = f[g--]) && b(c, !1);
    }
  }function b(e, t) {
    var n = e._component;n ? L(n) : (null != e.__preactattr_ && e.__preactattr_.ref && e.__preactattr_.ref(null), !1 !== t && null != e.__preactattr_ || p(e), y(e));
  }function y(e) {
    e = e.lastChild;while (e) {
      var t = e.previousSibling;b(e, !0), e = t;
    }
  }function g(e, t, n) {
    var o;for (o in n) {
      t && null != t[o] || null == n[o] || c(e, o, n[o], n[o] = void 0, R);
    }for (o in t) {
      "children" === o || "innerHTML" === o || o in n && t[o] === ("value" === o || "checked" === o ? e[o] : n[o]) || c(e, o, n[o], n[o] = t[o], R);
    }
  }function w(e) {
    var t = e.constructor.name;(I[t] || (I[t] = [])).push(e);
  }function C(e, t, n) {
    var o,
        r = I[e.name];if (e.prototype && e.prototype.render ? (o = new e(t, n), T.call(o, t, n)) : (o = new T(t, n), o.constructor = e, o.render = x), r) for (var i = r.length; i--;) {
      if (r[i].constructor === e) {
        o.__b = r[i].__b, r.splice(i, 1);break;
      }
    }return o;
  }function x(e, t, n) {
    return this.constructor(e, n);
  }function N(e, t, n, o, i) {
    e.__x || (e.__x = !0, (e.__r = t.ref) && delete t.ref, (e.__k = t.key) && delete t.key, !e.base || i ? e.componentWillMount && e.componentWillMount() : e.componentWillReceiveProps && e.componentWillReceiveProps(t, o), o && o !== e.context && (e.__c || (e.__c = e.context), e.context = o), e.__p || (e.__p = e.props), e.props = t, e.__x = !1, 0 !== n && (1 !== n && !1 === S.syncComponentUpdates && e.base ? r(e) : k(e, 1, i)), e.__r && e.__r(e));
  }function k(e, t, o, r) {
    if (!e.__x) {
      var i,
          l,
          a,
          _ = e.props,
          p = e.state,
          c = e.context,
          s = e.__p || _,
          f = e.__s || p,
          m = e.__c || c,
          v = e.base,
          y = e.__b,
          g = v || y,
          w = e._component,
          x = !1;if (v && (e.props = s, e.state = f, e.context = m, 2 !== t && e.shouldComponentUpdate && !1 === e.shouldComponentUpdate(_, p, c) ? x = !0 : e.componentWillUpdate && e.componentWillUpdate(_, p, c), e.props = _, e.state = p, e.context = c), e.__p = e.__s = e.__c = e.__b = null, e.__d = !1, !x) {
        i = e.render(_, p, c), e.getChildContext && (c = n(n({}, c), e.getChildContext()));var U,
            T,
            M = i && i.nodeName;if ("function" == typeof M) {
          var W = u(i);l = w, l && l.constructor === M && W.key == l.__k ? N(l, W, 1, c, !1) : (U = l, e._component = l = C(M, W, c), l.__b = l.__b || y, l.__u = e, N(l, W, 0, c, !1), k(l, 1, o, !0)), T = l.base;
        } else a = g, U = w, U && (a = e._component = null), (g || 1 === t) && (a && (a._component = null), T = h(a, i, c, o || !v, g && g.parentNode, !0));if (g && T !== g && l !== w) {
          var E = g.parentNode;E && T !== E && (E.replaceChild(T, g), U || (g._component = null, b(g, !1)));
        }if (U && L(U), e.base = T, T && !r) {
          var P = e,
              V = e;while (V = V.__u) {
            (P = V).base = T;
          }T._component = P, T._componentConstructor = P.constructor;
        }
      }if (!v || o ? D.unshift(e) : x || (e.componentDidUpdate && e.componentDidUpdate(s, f, m), S.afterUpdate && S.afterUpdate(e)), null != e.__h) while (e.__h.length) {
        e.__h.pop().call(e);
      }H || r || d();
    }
  }function U(e, t, n, o) {
    var r = e && e._component,
        i = r,
        l = e,
        a = r && e._componentConstructor === t.nodeName,
        _ = a,
        p = u(t);while (r && !_ && (r = r.__u)) {
      _ = r.constructor === t.nodeName;
    }return r && _ && (!o || r._component) ? (N(r, p, 3, n, o), e = r.base) : (i && !a && (L(i), e = l = null), r = C(t.nodeName, p, n), e && !r.__b && (r.__b = e, l = null), N(r, p, 1, n, o), e = r.base, l && e !== l && (l._component = null, b(l, !1))), e;
  }function L(e) {
    S.beforeUnmount && S.beforeUnmount(e);var t = e.base;e.__x = !0, e.componentWillUnmount && e.componentWillUnmount(), e.base = null;var n = e._component;n ? L(n) : t && (t.__preactattr_ && t.__preactattr_.ref && t.__preactattr_.ref(null), e.__b = t, p(t), w(e), y(t)), e.__r && e.__r(null);
  }function T(e, t) {
    this.__d = !0, this.context = t, this.props = e, this.state = this.state || {};
  }function M(e, t, n) {
    return h(n, e, {}, !1, t, !1);
  }var S = {},
      W = [],
      E = [],
      P = "function" == typeof Promise ? Promise.resolve().then.bind(Promise.resolve()) : setTimeout,
      V = /acit|ex(?:s|g|n|p|$)|rph|ows|mnc|ntw|ine[ch]|zoo|^ord/i,
      A = [],
      D = [],
      H = 0,
      R = !1,
      j = !1,
      I = {};n(T.prototype, { setState: function setState(e, t) {
      var o = this.state;this.__s || (this.__s = n({}, o)), n(o, "function" == typeof e ? e(o, this.props) : e), t && (this.__h = this.__h || []).push(t), r(this);
    }, forceUpdate: function forceUpdate(e) {
      e && (this.__h = this.__h || []).push(e), k(this, 2);
    }, render: function render() {} });var $ = { h: t, createElement: t, cloneElement: o, Component: T, render: M, rerender: i, options: S }; true ? module.exports = $ : self.preact = $;
}();
//# sourceMappingURL=preact.min.js.map

/***/ }),

/***/ "KrYd":
/***/ (function(module, exports) {

module.exports = require("timers");

/***/ }),

/***/ "LbTS":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "RYBc":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "TO+D":
/***/ (function(module, exports) {

module.exports = function (el, options) {
  options = options || {};

  var elementDocument = el.ownerDocument || el;
  var basicTabbables = [];
  var orderedTabbables = [];

  // A node is "available" if
  // - it's computed style
  var isUnavailable = createIsUnavailable(elementDocument);

  var candidateSelectors = ['input', 'select', 'a[href]', 'textarea', 'button', '[tabindex]'];

  var candidates = el.querySelectorAll(candidateSelectors);

  if (options.includeContainer) {
    var matches = Element.prototype.matches || Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;

    if (candidateSelectors.some(function (candidateSelector) {
      return matches.call(el, candidateSelector);
    })) {
      candidates = Array.prototype.slice.apply(candidates);
      candidates.unshift(el);
    }
  }

  var candidate, candidateIndex;
  for (var i = 0, l = candidates.length; i < l; i++) {
    candidate = candidates[i];
    candidateIndex = parseInt(candidate.getAttribute('tabindex'), 10) || candidate.tabIndex;

    if (candidateIndex < 0 || candidate.tagName === 'INPUT' && candidate.type === 'hidden' || candidate.disabled || isUnavailable(candidate, elementDocument)) {
      continue;
    }

    if (candidateIndex === 0) {
      basicTabbables.push(candidate);
    } else {
      orderedTabbables.push({
        index: i,
        tabIndex: candidateIndex,
        node: candidate
      });
    }
  }

  var tabbableNodes = orderedTabbables.sort(function (a, b) {
    return a.tabIndex === b.tabIndex ? a.index - b.index : a.tabIndex - b.tabIndex;
  }).map(function (a) {
    return a.node;
  });

  Array.prototype.push.apply(tabbableNodes, basicTabbables);

  return tabbableNodes;
};

function createIsUnavailable(elementDocument) {
  // Node cache must be refreshed on every check, in case
  // the content of the element has changed
  var isOffCache = [];

  // "off" means `display: none;`, as opposed to "hidden",
  // which means `visibility: hidden;`. getComputedStyle
  // accurately reflects visiblity in context but not
  // "off" state, so we need to recursively check parents.

  function isOff(node, nodeComputedStyle) {
    if (node === elementDocument.documentElement) return false;

    // Find the cached node (Array.prototype.find not available in IE9)
    for (var i = 0, length = isOffCache.length; i < length; i++) {
      if (isOffCache[i][0] === node) return isOffCache[i][1];
    }

    nodeComputedStyle = nodeComputedStyle || elementDocument.defaultView.getComputedStyle(node);

    var result = false;

    if (nodeComputedStyle.display === 'none') {
      result = true;
    } else if (node.parentNode) {
      result = isOff(node.parentNode);
    }

    isOffCache.push([node, result]);

    return result;
  }

  return function isUnavailable(node) {
    if (node === elementDocument.documentElement) return false;

    var computedStyle = elementDocument.defaultView.getComputedStyle(node);

    if (isOff(node, computedStyle)) return true;

    return computedStyle.visibility === 'hidden';
  };
}

/***/ }),

/***/ "Tv6c":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"profile":"profile__t2Dqz"};

/***/ }),

/***/ "UlEV":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "Z+tv":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "ZAL5":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"home":"home__MseGd"};

/***/ }),

/***/ "a81U":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "aqQ4":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "pln1":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "rq4c":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "sEh6":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "u+vq":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "u3et":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "ySUw":
/***/ (function(module, exports, __webpack_require__) {

var tabbable = __webpack_require__("TO+D");

var listeningFocusTrap = null;

function focusTrap(element, userOptions) {
  var tabbableNodes = [];
  var firstTabbableNode = null;
  var lastTabbableNode = null;
  var nodeFocusedBeforeActivation = null;
  var active = false;
  var paused = false;
  var tabEvent = null;

  var container = typeof element === 'string' ? document.querySelector(element) : element;

  var config = userOptions || {};
  config.returnFocusOnDeactivate = userOptions && userOptions.returnFocusOnDeactivate !== undefined ? userOptions.returnFocusOnDeactivate : true;
  config.escapeDeactivates = userOptions && userOptions.escapeDeactivates !== undefined ? userOptions.escapeDeactivates : true;

  var trap = {
    activate: activate,
    deactivate: deactivate,
    pause: pause,
    unpause: unpause
  };

  return trap;

  function activate(activateOptions) {
    if (active) return;

    var defaultedActivateOptions = {
      onActivate: activateOptions && activateOptions.onActivate !== undefined ? activateOptions.onActivate : config.onActivate
    };

    active = true;
    paused = false;
    nodeFocusedBeforeActivation = document.activeElement;

    if (defaultedActivateOptions.onActivate) {
      defaultedActivateOptions.onActivate();
    }

    addListeners();
    return trap;
  }

  function deactivate(deactivateOptions) {
    if (!active) return;

    var defaultedDeactivateOptions = {
      returnFocus: deactivateOptions && deactivateOptions.returnFocus !== undefined ? deactivateOptions.returnFocus : config.returnFocusOnDeactivate,
      onDeactivate: deactivateOptions && deactivateOptions.onDeactivate !== undefined ? deactivateOptions.onDeactivate : config.onDeactivate
    };

    removeListeners();

    if (defaultedDeactivateOptions.onDeactivate) {
      defaultedDeactivateOptions.onDeactivate();
    }

    if (defaultedDeactivateOptions.returnFocus) {
      setTimeout(function () {
        tryFocus(nodeFocusedBeforeActivation);
      }, 0);
    }

    active = false;
    paused = false;
    return this;
  }

  function pause() {
    if (paused || !active) return;
    paused = true;
    removeListeners();
  }

  function unpause() {
    if (!paused || !active) return;
    paused = false;
    addListeners();
  }

  function addListeners() {
    if (!active) return;

    // There can be only one listening focus trap at a time
    if (listeningFocusTrap) {
      listeningFocusTrap.pause();
    }
    listeningFocusTrap = trap;

    updateTabbableNodes();
    tryFocus(firstFocusNode());
    document.addEventListener('focus', checkFocus, true);
    document.addEventListener('click', checkClick, true);
    document.addEventListener('mousedown', checkPointerDown, true);
    document.addEventListener('touchstart', checkPointerDown, true);
    document.addEventListener('keydown', checkKey, true);

    return trap;
  }

  function removeListeners() {
    if (!active || listeningFocusTrap !== trap) return;

    document.removeEventListener('focus', checkFocus, true);
    document.removeEventListener('click', checkClick, true);
    document.removeEventListener('mousedown', checkPointerDown, true);
    document.removeEventListener('touchstart', checkPointerDown, true);
    document.removeEventListener('keydown', checkKey, true);

    listeningFocusTrap = null;

    return trap;
  }

  function getNodeForOption(optionName) {
    var optionValue = config[optionName];
    var node = optionValue;
    if (!optionValue) {
      return null;
    }
    if (typeof optionValue === 'string') {
      node = document.querySelector(optionValue);
      if (!node) {
        throw new Error('`' + optionName + '` refers to no known node');
      }
    }
    if (typeof optionValue === 'function') {
      node = optionValue();
      if (!node) {
        throw new Error('`' + optionName + '` did not return a node');
      }
    }
    return node;
  }

  function firstFocusNode() {
    var node;
    if (getNodeForOption('initialFocus') !== null) {
      node = getNodeForOption('initialFocus');
    } else if (container.contains(document.activeElement)) {
      node = document.activeElement;
    } else {
      node = tabbableNodes[0] || getNodeForOption('fallbackFocus');
    }

    if (!node) {
      throw new Error('You can\'t have a focus-trap without at least one focusable element');
    }

    return node;
  }

  // This needs to be done on mousedown and touchstart instead of click
  // so that it precedes the focus event
  function checkPointerDown(e) {
    if (config.clickOutsideDeactivates && !container.contains(e.target)) {
      deactivate({ returnFocus: false });
    }
  }

  function checkClick(e) {
    if (config.clickOutsideDeactivates) return;
    if (container.contains(e.target)) return;
    e.preventDefault();
    e.stopImmediatePropagation();
  }

  function checkFocus(e) {
    if (container.contains(e.target)) return;
    e.preventDefault();
    e.stopImmediatePropagation();
    // Checking for a blur method here resolves a Firefox issue (#15)
    if (typeof e.target.blur === 'function') e.target.blur();

    if (tabEvent) {
      readjustFocus(tabEvent);
    }
  }

  function checkKey(e) {
    if (e.key === 'Tab' || e.keyCode === 9) {
      handleTab(e);
    }

    if (config.escapeDeactivates !== false && isEscapeEvent(e)) {
      deactivate();
    }
  }

  function handleTab(e) {
    updateTabbableNodes();

    if (e.target.hasAttribute('tabindex') && Number(e.target.getAttribute('tabindex')) < 0) {
      return tabEvent = e;
    }

    e.preventDefault();
    var currentFocusIndex = tabbableNodes.indexOf(e.target);

    if (e.shiftKey) {
      if (e.target === firstTabbableNode || tabbableNodes.indexOf(e.target) === -1) {
        return tryFocus(lastTabbableNode);
      }
      return tryFocus(tabbableNodes[currentFocusIndex - 1]);
    }

    if (e.target === lastTabbableNode) return tryFocus(firstTabbableNode);

    tryFocus(tabbableNodes[currentFocusIndex + 1]);
  }

  function updateTabbableNodes() {
    tabbableNodes = tabbable(container);
    firstTabbableNode = tabbableNodes[0];
    lastTabbableNode = tabbableNodes[tabbableNodes.length - 1];
  }

  function readjustFocus(e) {
    if (e.shiftKey) return tryFocus(lastTabbableNode);

    tryFocus(firstTabbableNode);
  }
}

function isEscapeEvent(e) {
  return e.key === 'Escape' || e.key === 'Esc' || e.keyCode === 27;
}

function tryFocus(node) {
  if (!node || !node.focus) return;
  if (node === document.activeElement) return;

  node.focus();
  if (node.tagName.toLowerCase() === 'input') {
    node.select();
  }
}

module.exports = focusTrap;

/***/ })

/******/ });
//# sourceMappingURL=ssr-bundle.js.map