﻿Feature: BetCount
	To Verify bet count on the betslip counter.

Background: 
	Given user navigates to the sportsbook website
	Then user navigates to the betslip page
	Then clears the betslip page if any bets exists
	Then user navigates to the sports page


Scenario Outline: Verify the bet count 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then the user adds required number of markets <markets> by clicking different games
	Then verify the count on the betslip counter is <count>

	 Examples: 
        | markets | count |
        | 1       | 1     |
        | 2       | 2     |
        | 3       | 3     |

Scenario: Verify the bet count when the bets are added.
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game
	Then verify the count on the betslip counter is 1
	Then user clicks on the first price market for cricket game
	Then verify the count on the betslip counter is 2

Scenario: Verify the bet count when the bets are removed
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game
	Then user clicks on the first price market for cricket game
	Then verify the count on the betslip counter is 2
	Then the user clicks on the beslip
	Then the user removes the badminton game
	Then verify the count on the betslip counter is 1

Scenario: Verify the bet count when the bet page is refreshed.
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game
	Then verify the count on the betslip counter is 1
	Then user clicks on the first price market for cricket game
	Then verify the count on the betslip counter is 2
	Then the user refreshes the page
	Then verify the count on the betslip counter is 2

Scenario: Verify the bet count when the betslip is clear
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game
	Then user clicks on the first price market for cricket game
	Then verify the count on the betslip counter is 2
	Then the user clicks on the beslip
	Then the user clicks on Clear All button
	Then the user closes the betslip
	Then verify the count on the betslip counter is 0

Scenario: Verify the bet count After bet placement success
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game
	Then user clicks on the first price market for cricket game
	Then verify the count on the betslip counter is 2
	Then the user clicks on the beslip
	Then user enters 1 in first market stake box in the betslip
	Then user enters 1 in second market stake box in the betslip
	Then user clicks on Bet button	 
	Then verify the count on the betslip counter is 0

Scenario: Verify the bet count After bet placement success and keep bet
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game
	Then user clicks on the first price market for cricket game
	Then verify the count on the betslip counter is 2
	Then the user clicks on the beslip
	Then user enters 1 in first market stake box in the betslip
	Then user enters 1 in second market stake box in the betslip
	Then user clicks on Bet button	 
	Then verify the count on the betslip counter is 0
	Then the user clicks on Keep Bets button
	Then verify the count on the betslip counter is 2





