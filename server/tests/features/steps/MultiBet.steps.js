"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const cucumber_tsflow_1 = require("cucumber-tsflow");
const chai = require('chai');
const FeatureTestBase_1 = require("../utils/FeatureTestBase");
let MultiBet = class MultiBet extends FeatureTestBase_1.FeatureTestBase {
    constructor() {
        super(...arguments);
        this.betslipButtonXPath = "//paper-icon-button[@id='bet_btn']";
        this.clearAllBetsButtonXPath = "//*[@id='scroller_area']/section/div[1]/button";
        this.closeBetslipPageButtonXPath = "//paper-icon-button[@id='betslip-close']";
        this.otherMultiplesCollapseItemXPath = "//*[@id=\"system_bets_paper_collapse\"]/paper-item/paper-item-body/div/div";
        /// End - Verifications
    }
    //protected applyAllStakeInputXPath:string = "//*[@id=\"stake_applyall\"]/paper-input-container/div/div/input";
    /// Start - Click on markets
    addFirstBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89403, 1));
        });
    }
    addSecondBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89165, 1));
        });
    }
    addsideABet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89165, 1));
        });
    }
    addThirdBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            var betButtonXPath = "//button[@id='bet_btn_89153_1']";
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89153, 1));
        });
    }
    addFourthBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89157, 1));
        });
    }
    addFifthBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89161, 1));
        });
    }
    addSixthBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89399, 1));
        });
    }
    addSeventhBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89407, 1));
        });
    }
    addEighthBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89523, 1));
        });
    }
    addNinethBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89527, 1));
        });
    }
    addTenthBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89511, 1));
        });
    }
    addTenthsideABet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89153, 1));
        });
    }
    addEleventBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89519, 1));
        });
    }
    addTwelvethBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89375, 1));
        });
    }
    clickOnCricketSideA() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89165, 3));
            //var cricketSideBBetButtonXPath = "//button[@id='bet_btn_89165_3']";
            //var sideBStake = await FeatureTestBase.myElementHandler.findElementByXpath(cricketSideBBetButtonXPath).getText();
            //chai.assert.equal(sideBStake, "2.88");
            //await FeatureTestBase.myElementHandler.findElementByXpath(cricketSideBBetButtonXPath).click();
        });
    }
    /// End - Click on markets
    /// Start - Betslip handling
    navigateBetslipPage() {
        return __awaiter(this, void 0, void 0, function* () {
            // await FeatureTestBase.myElementHandler.getElementAndClick('document.querySelector("betslip-button").shadowRoot.querySelector("#bet_btn")');
        });
    }
    enterMultiStake(legCount, stake) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var position = 0;
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                    // (parseInt(legCount) == 3 ? 0 : 2);
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }
            }
        });
    }
    clickSubmitButton() {
        return __awaiter(this, void 0, void 0, function* () {
            //FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            // Leave enough time for any toaster to disappear from previous step, if any.
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getBetButtonPath());
        });
    }
    clickMultipleCollapesbutton(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getOtherMultiplesPath());
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
        });
    }
    enterApplyAllStake10(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            var parsedStake = parseFloat(stake);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1200);
            //await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.stackShadows(shadows) + '.querySelector("#stake_applyall")');
            //TODO:I know, it's code smell.. but it's because bloody paper-input doesnt respond to "clear" instead you have to get to the inner input to clear... :( 
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getApplyAllStakePath(), stake);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(10000);
            //await FeatureTestBase.myElementHandler.findElementByXpath(this.applyAllStakeInputXPath).clear();
            //await FeatureTestBase.myElementHandler.findElementByXpath(this.applyAllStakeInputXPath).sendKeys(parsedStake);
        });
    }
    enterBadmintonStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0), '1');
        });
    }
    enterCricketStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(1), '1');
        });
    }
    enterBaseballStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(2), '-1');
        });
    }
    enterAmericanFootballInvalidStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(3), '-1');
        });
    }
    clickOnKeepBets() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            var keepBetsText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastButtonPath());
            chai.assert.equal(keepBetsText, "KEEP BETS");
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastButtonPath());
        });
    }
    expandOtherMultiples() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var othermultiples = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElement(FeatureTestBase_1.FeatureTestBase.myElementHandler.getOtherMultiplesPath());
            if (othermultiples) {
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getOtherMultiplesPath());
            }
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
        });
    }
    verifyNoErrorMessagesAreShown() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);
            var multiConflictErrorMessage = "Multi Conflict";
            var isBet1ErrorMessageDisplayed = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(0), "hidden");
            var isBet2ErrorMessageDisplayed = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(1), "hidden");
            chai.assert.equal(isBet1ErrorMessageDisplayed, null);
            chai.assert.equal(isBet2ErrorMessageDisplayed, null);
        });
    }
    addReguriedNumberOfMarkest(markets) {
        return __awaiter(this, void 0, void 0, function* () {
            var parsedMarkets = parseInt(markets);
            var eventids = ["89403", "89165", "89153", "89157", "89161", "89399", "89407", "89523", "89527", "89511", "89519", "89375", "89379", "89383", "89395", "123456"];
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            for (var i = 0; i < parsedMarkets; i++) {
                //add markets
                FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(eventids[i], 1));
            }
        });
    }
    userEnterMultiStake(Singlestake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (Singlestake != "") {
                var parsedSinglestake = parseInt(Singlestake);
                var parsedMarkets = parseInt(markets);
                FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0), parsedSinglestake);
            }
        });
    }
    userEnterDoubleStake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedDoublestake = parseInt(stake);
                var parsedMarkets = parseInt(markets);
                FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                var position = (parsedMarkets == 2 ? 0 : 1);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }
            }
        });
    }
    doubleStakeExpectedstake(Doublestake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(6000);
            if (Doublestake != "") {
                var parsedDoublestake = parseInt(Doublestake);
                var parsedMarkets = parseInt(markets);
                FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                var position = (parsedMarkets == 2 ? 0 : 1);
                var expectedstake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                chai.assert.equal(expectedstake, Doublestake);
            }
        });
    }
    clickonmulitpesCollapes(markets) {
        return __awaiter(this, void 0, void 0, function* () {
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets > 1) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
                FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getOtherMultiplesPath());
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            }
        });
    }
    userEnterTreblestake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                var position = (parsedMarkets == 3 ? 0 : 2);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }
            }
        });
    }
    userEnterTrixiestake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 3) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                    var position = 2;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    trixieStakeExpectedstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(5000);
            if (stake != "") {
                var parsedDoublestake = parseInt(stake);
                var parsedMarkets = parseInt(markets);
                FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                var position = 2;
                var expectedstake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                chai.assert.equal(expectedstake, stake);
            }
        });
    }
    userEnterPatentstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 3) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                    var position = 3;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterYankeestake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 4) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                    var position = 3;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterLucky15stake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 4) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                    var position = 4;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterSuperYankeestake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 5) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
                    var position = 4;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterLucky31stake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 5) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = 5;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterHeinzstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 6) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = 5;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterSuperHeinzstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 7) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = 6;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterGoliathstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 8) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = 7;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterLucky63stake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets == 6) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = 6;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    //private async enterMultipleStake(stake: string, position: number): Promise<void> {
    //    FeatureTestBase.browser.sleep(1000);
    //    if (stake != "") {
    //        var baseballBetEventName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetEventNamePath(0));
    //        for (var i = 0; i < stake.length; i++) {
    //            FeatureTestBase.browser.sleep(100);
    //            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
    //        }
    //    }
    //}
    userEnterFourFoldsstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets >= 4) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = parsedMarkets == 4 ? 0 : 3;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterFiveFoldsstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets >= 5) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = parsedMarkets == 5 ? 0 : 4;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterSixFoldsstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets >= 6) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = parsedMarkets == 6 ? 0 : 5;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterSevenFoldsstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets >= 7) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = parsedMarkets == 7 ? 0 : 6;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterEightFoldsstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets >= 8) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = parsedMarkets == 8 ? 0 : 7;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    userEnterNineFoldsstake(stake, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var parsedMarkets = parseInt(markets);
                if (parsedMarkets >= 9) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var position = parsedMarkets == 9 ? 0 : 8;
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                    for (var i = 0; i < stake.length; i++) {
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                    }
                }
            }
        });
    }
    /// End - Betslip handling
    /// Start - Verifications  
    verifyNLegMultiVisible(legCount) {
        return __awaiter(this, void 0, void 0, function* () {
            var parsedLegCount = parseInt(legCount);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            var multiName = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesName(0));
            var expectedMultiName = ["Double", "Treble", "4 Fold", "5 Fold", "6 Fold", "7 Fold", "8 Fold", "9 Fold", "10 Fold", "11 Fold", "12 Fold"];
            chai.assert.equal(multiName, expectedMultiName[parsedLegCount - 2]);
        });
    }
    verifyDoubleBetVisible() {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyNLegMultiVisible("2");
        });
    }
    verifyTrebleBetVisible() {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyNLegMultiVisible("3");
        });
    }
    verifyNLegMultiBetPlaced(legCount) {
        return __awaiter(this, void 0, void 0, function* () {
            var parsedLegCount = parseInt(legCount);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(betTicketText, "Bet placed. Receipt #17W5EPF6");
        });
    }
    verify3BetsPlacedForSameAmount(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            // ISSUE: Cannot verify all bets are played for same stake
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var toastBetTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(toastBetTicketText, "3 bets placed.");
        });
    }
    verifyTotalStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var totalStake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalStakePath());
            chai.assert.equal(totalStake, " €2.00");
            //chai.assert.equal(totalStake, " 2.00");
        });
    }
    verifyReturnAmount() {
        return __awaiter(this, void 0, void 0, function* () {
            //FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var estimatedReturn = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalPayoutPath());
            chai.assert.equal(estimatedReturn, " €4.07");
            //chai.assert.equal(estimatedReturn, "4.07");
        });
    }
    verifyBetButtonAmount() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            var totalStakeOnBetButton = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalStakeOnBetButtonPath());
            chai.assert.equal(totalStakeOnBetButton, "BET €2.00");
        });
    }
    verifyAllValidBetsPlaced() {
        return __awaiter(this, void 0, void 0, function* () {
            // ISSUE: There is no way to check if the correct bet was placed with toast text
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1800);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.include(betTicketText, "2 bets placed.");
        });
    }
    verifyApplyAllSingleBoxIsEmpty() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(10000);
            var applyAllValue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getApplyAllStakePath());
            // FeatureTestBase.browser.sleep(10000);
            chai.assert.equal(applyAllValue, "");
        });
    }
    verifyApplyAllSingleBoxNotVisible() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(10000);
            var applyAllelements = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElement(FeatureTestBase_1.FeatureTestBase.myElementHandler.getApplyAllStakePath());
            chai.assert.equal(applyAllelements, null);
        });
    }
    verifyBaseballBetIsRemainingOnBetslip() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            var baseballBetTeamName = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetOutcomeNamePath(0));
            var baseballBetEventName = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetEventNamePath(0));
            var baseballBetStake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
            chai.assert.equal(baseballBetTeamName, "Chelsea");
            //chai.assert.equal(baseballBetOutcomeName, "Chelsea v Burnley");
            chai.assert.equal(baseballBetStake, "");
        });
    }
    verify2InalidBetsAreShownOnBetslip() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            this.verifyBaseballBetIsRemainingOnBetslip();
            var baseballBetTeamName = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetOutcomeNamePath(0));
            var baseballBetEventName = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetEventNamePath(0));
            var baseballBetStake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
            chai.assert.equal(baseballBetTeamName, "Chelsea");
            chai.assert.equal(baseballBetEventName, "Chelsea v Burnley");
            //chai.assert.equal(baseballBetStake, "-1");
            var baseballBetTeamName2 = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetOutcomeNamePath(1));
            var baseballBetEventName2 = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetEventNamePath(1));
            var baseballBetStake2 = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(1));
            chai.assert.equal(baseballBetTeamName2, "Crystal Palace");
            chai.assert.equal(baseballBetEventName2, "Crystal Palace v Huddersfield");
        });
    }
    verifyMultiConflictMessageShown() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1500);
            var multiConflictMessage = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.include(multiConflictMessage, "Multi conflict. Remove conflicted bets");
        });
    }
    verifyMultiConflictsShown() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);
            var multiConflictErrorMessage = "Multi Conflict";
            var bet1ErrorMessage = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(0));
            var bet2ErrorMessage = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(1));
            chai.assert.equal(bet1ErrorMessage, multiConflictErrorMessage);
            chai.assert.equal(bet2ErrorMessage, multiConflictErrorMessage);
        });
    }
    verifyAppropriateMultis(multis, markets) {
        return __awaiter(this, void 0, void 0, function* () {
            if (multis != "") {
                var parsedMultis = multis.split(",");
                var parsedMarkets = parseInt(markets);
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
                for (var i = 0, j = parsedMarkets + 1; i < parsedMultis.length; i++, j++) {
                    FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                    var multiName = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesName(i));
                    chai.assert.equal(multiName, parsedMultis[i].trim());
                }
            }
        });
    }
    verifySinglesReached() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(betTicketText, "Number of Singles Limit Reached");
        });
    }
    verifyStakeCourtCorrect(Count) {
        return __awaiter(this, void 0, void 0, function* () {
            var betscount = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalBetCountPath());
            chai.assert.equal(betscount, "(" + Count.trim() + ")");
        });
    }
    verifyReturnValueForEachMulti(Value) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var estimatedReturn = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalPayoutPath());
            chai.assert.equal(estimatedReturn, ' €' + Value.trim());
        });
    }
    verifyMultiBetPlaced(Count) {
        return __awaiter(this, void 0, void 0, function* () {
            var parsedCount = parseInt(Count);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            if (parsedCount == 1) {
                chai.assert.equal(betTicketText, "Bet placed. Receipt #17W5EPF6");
            }
        });
    }
    navigationToPage(Page) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
        });
    }
    keepbetTest() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            var badmintonSingleBetStakeInputXPath = "//*[@id=\"stake_1\"]/paper-input-container/div/div/input";
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).clear();
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).sendKeys(1);
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            var badmintonSingleBetStakeInputXPath = "//*[@id=\"stake_2\"]/paper-input-container/div/div/input";
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).clear();
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).sendKeys(1);
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            var badmintonSingleBetStakeInputXPath = "//*[@id=\"stake_3\"]/paper-input-container/div/div/input";
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).clear();
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).sendKeys(1);
            var betscountXPath = "//*[@id=\"stake_betcount\"]";
            var betscount = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.findElementByXpath(betscountXPath).getText();
            chai.assert.equal(betscount, "Stake (3)");
        });
    }
};
__decorate([
    cucumber_tsflow_1.given(/user clicks on the first price market for badminton game for price 1.57/),
    cucumber_tsflow_1.given(/user clicks on the first price market for badminton game$/)
], MultiBet.prototype, "addFirstBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for cricket game for price 2.50/),
    cucumber_tsflow_1.then(/user clicks on the first price market for cricket game$/)
], MultiBet.prototype, "addSecondBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for cricket game for side A to win/)
], MultiBet.prototype, "addsideABet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for baseball game/)
], MultiBet.prototype, "addThirdBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for american football game/),
    cucumber_tsflow_1.then(/ user clicks on the first price market for badminton game for price 1.40/)
], MultiBet.prototype, "addFourthBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for aussie rules game/)
], MultiBet.prototype, "addFifthBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for basketball game/)
], MultiBet.prototype, "addSixthBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for beach volleyball game/)
], MultiBet.prototype, "addSeventhBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for darts game/)
], MultiBet.prototype, "addEighthBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for esports game/)
], MultiBet.prototype, "addNinethBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for football game$/)
], MultiBet.prototype, "addTenthBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for football game for side A to win/)
], MultiBet.prototype, "addTenthsideABet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for handball game/)
], MultiBet.prototype, "addEleventBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for volleyball game/)
], MultiBet.prototype, "addTwelvethBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for cricket game for side B to win/)
], MultiBet.prototype, "clickOnCricketSideA", null);
__decorate([
    cucumber_tsflow_1.then(/clicks on the Betslip icon/)
], MultiBet.prototype, "navigateBetslipPage", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake value in the (.*)leg multi box for £(.*)/)
], MultiBet.prototype, "enterMultiStake", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on Bet button/)
], MultiBet.prototype, "clickSubmitButton", null);
__decorate([
    cucumber_tsflow_1.then(/user click on the multiples collapes button/)
], MultiBet.prototype, "clickMultipleCollapesbutton", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake value in apply all single box for £(.*)/)
], MultiBet.prototype, "enterApplyAllStake10", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake value in the badminton stake box for £1/)
], MultiBet.prototype, "enterBadmintonStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake value in the cricket stake box for £1/)
], MultiBet.prototype, "enterCricketStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake value in the baseball stake box for £-1/)
], MultiBet.prototype, "enterBaseballStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake value in the american football stake box for £-1/)
], MultiBet.prototype, "enterAmericanFootballInvalidStake", null);
__decorate([
    cucumber_tsflow_1.then(/the user clicks on Keep Bets button/)
], MultiBet.prototype, "clickOnKeepBets", null);
__decorate([
    cucumber_tsflow_1.then(/clicks on the Other Multiples drop down/)
], MultiBet.prototype, "expandOtherMultiples", null);
__decorate([
    cucumber_tsflow_1.then(/no error messages are shown/)
], MultiBet.prototype, "verifyNoErrorMessagesAreShown", null);
__decorate([
    cucumber_tsflow_1.then(/the user adds required number of markets (.*) by clicking different games/)
], MultiBet.prototype, "addReguriedNumberOfMarkest", null);
__decorate([
    cucumber_tsflow_1.then(/user enters single stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterMultiStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters double stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterDoubleStake", null);
__decorate([
    cucumber_tsflow_1.then(/user sees the expected double stake (.*) for (.*) markets/)
], MultiBet.prototype, "doubleStakeExpectedstake", null);
__decorate([
    cucumber_tsflow_1.then(/user click on the multiples collapes for (.*) markets/)
], MultiBet.prototype, "clickonmulitpesCollapes", null);
__decorate([
    cucumber_tsflow_1.then(/user enters treble stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterTreblestake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters trixie stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterTrixiestake", null);
__decorate([
    cucumber_tsflow_1.then(/user sees the expected trixie stake (.*) for (.*) markets/)
], MultiBet.prototype, "trixieStakeExpectedstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters patent stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterPatentstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters yankee stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterYankeestake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters lucky15 stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterLucky15stake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters super yankee stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterSuperYankeestake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters lucky31 stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterLucky31stake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters heinz stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterHeinzstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters super heinz stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterSuperHeinzstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters goliath stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterGoliathstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters lucky63 stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterLucky63stake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters fourfolds stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterFourFoldsstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters five folds stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterFiveFoldsstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters six fold stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterSixFoldsstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters seven fold stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterSevenFoldsstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters eight fold stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterEightFoldsstake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters nine fold stake (.*) for (.*) markets/)
], MultiBet.prototype, "userEnterNineFoldsstake", null);
__decorate([
    cucumber_tsflow_1.then(/verify the straight (.*)leg multi bet is visible on the betslip page/)
], MultiBet.prototype, "verifyNLegMultiVisible", null);
__decorate([
    cucumber_tsflow_1.then(/verify the bet placed is visible on the betslip page/),
    cucumber_tsflow_1.then(/verify the Double bet is visible on the betslip page/),
    cucumber_tsflow_1.then(/verify Double bet is visible/)
], MultiBet.prototype, "verifyDoubleBetVisible", null);
__decorate([
    cucumber_tsflow_1.then(/verify the Treble bet is visible on the betslip page/)
], MultiBet.prototype, "verifyTrebleBetVisible", null);
__decorate([
    cucumber_tsflow_1.then(/verify the straight (.*)leg multi bet is placed "Bet placed. Receipt #"/)
], MultiBet.prototype, "verifyNLegMultiBetPlaced", null);
__decorate([
    cucumber_tsflow_1.then(/verify the all bet are placed for the same £(.*) "3 bets placed"/)
], MultiBet.prototype, "verify3BetsPlacedForSameAmount", null);
__decorate([
    cucumber_tsflow_1.then(/verify the total stake value is £2/)
], MultiBet.prototype, "verifyTotalStake", null);
__decorate([
    cucumber_tsflow_1.then(/verify the total return value is £4.07/)
], MultiBet.prototype, "verifyReturnAmount", null);
__decorate([
    cucumber_tsflow_1.then(/verify the total stake value on the BET button is £2/)
], MultiBet.prototype, "verifyBetButtonAmount", null);
__decorate([
    cucumber_tsflow_1.then(/verify all the valid bets are placed "2 bets placed"/),
    cucumber_tsflow_1.then(/verify the all valid bets are placed for the same £1 "2 bets placed"/)
], MultiBet.prototype, "verifyAllValidBetsPlaced", null);
__decorate([
    cucumber_tsflow_1.then(/verify apply to single box value is 0 or empty/)
], MultiBet.prototype, "verifyApplyAllSingleBoxIsEmpty", null);
__decorate([
    cucumber_tsflow_1.then(/verify the apply all singles box is not visible/)
], MultiBet.prototype, "verifyApplyAllSingleBoxNotVisible", null);
__decorate([
    cucumber_tsflow_1.then(/verify the baseball game bet is visible on the betslip page with stake £0/)
], MultiBet.prototype, "verifyBaseballBetIsRemainingOnBetslip", null);
__decorate([
    cucumber_tsflow_1.then(/verify 2 invalid bets are shown in the betslip page/)
], MultiBet.prototype, "verify2InalidBetsAreShownOnBetslip", null);
__decorate([
    cucumber_tsflow_1.then(/verify "Multi conflict. Remove conflicted bets" message is shown/)
], MultiBet.prototype, "verifyMultiConflictMessageShown", null);
__decorate([
    cucumber_tsflow_1.then(/verify the indivdual selection for multi shows the "Multi Conflict"/)
], MultiBet.prototype, "verifyMultiConflictsShown", null);
__decorate([
    cucumber_tsflow_1.then(/verify appropriate (.*) for (.*) markets are shown in the betslip page/)
], MultiBet.prototype, "verifyAppropriateMultis", null);
__decorate([
    cucumber_tsflow_1.then(/verify "Number of Singles Limit Reached" message is shown on the homepage/)
], MultiBet.prototype, "verifySinglesReached", null);
__decorate([
    cucumber_tsflow_1.then(/verify the stake (.*) is correct/)
], MultiBet.prototype, "verifyStakeCourtCorrect", null);
__decorate([
    cucumber_tsflow_1.then(/verify the return value (.*) for each multi is correct/)
], MultiBet.prototype, "verifyReturnValueForEachMulti", null);
__decorate([
    cucumber_tsflow_1.then(/verify multi bet is placed "Bet placed. Receipt #" for (.*) bets/)
], MultiBet.prototype, "verifyMultiBetPlaced", null);
__decorate([
    cucumber_tsflow_1.then(/verify user navigates to (.*)/)
], MultiBet.prototype, "navigationToPage", null);
__decorate([
    cucumber_tsflow_1.then(/verify the bet placed is visible on the betslip page/)
], MultiBet.prototype, "keepbetTest", null);
MultiBet = __decorate([
    cucumber_tsflow_1.binding()
], MultiBet);
module.exports = MultiBet;
