"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const cucumber_tsflow_1 = require("cucumber-tsflow");
const FeatureTestBase_1 = require("../utils/FeatureTestBase");
const chai = require('chai');
let PriceUpdate = class PriceUpdate extends FeatureTestBase_1.FeatureTestBase {
    constructor() {
        super(...arguments);
        this.betslipButtonXPath = "//paper-icon-button[@id='bet_btn']";
        this.clearAllBetsButtonXPath = "//*[@id='scroller_area']/section/div[1]/paper-button";
        this.closeBetslipPageButtonXPath = "//paper-icon-button[@id='betslip-close']";
        this.homePageToastBetTicketLabelXPath = "//*[@id=\"label\"]";
        this.otherMultiplesCollapseItemXPath = "//*[@id=\"system_bets_paper_collapse\"]/paper-item/paper-item-body/div/div";
        this.applyAllStakeInputXPath = "//*[@id=\"stake_applyall\"]/paper-input-container/div/div/input";
        /// End - Verifications 
    }
    /// Start - Click on markets
    receivePriceUpdateForBet1() {
        return __awaiter(this, void 0, void 0, function* () {
            var signal = {
                selectionId: "89403_1",
                price: {
                    decimal: 3.5
                }
            };
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            var price = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetPricePath(0));
            chai.assert.equal(price, "3.50");
        });
    }
    receivePriceUpdateForBet1Odd(odd) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('=============', odd);
            var signal = {
                selectionId: "89403_1",
                price: {
                    decimal: odd
                }
            };
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        });
    }
    isUpdatedPriceShown() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    isGreenUpwardArrowShown() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.verifyPriceChangeIconForBet(true, 1);
        });
    }
    addcricketmarket() {
        return __awaiter(this, void 0, void 0, function* () {
            var signal = {
                selectionId: "89165_1",
                price: {
                    decimal: 1.5
                }
            };
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            var price = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetPricePath(0));
            chai.assert.equal(price, "1.50");
        });
    }
    isRedDownArrowShown() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.verifyPriceChangeIconForBet(false, 1);
        });
    }
    verifyRedDownArrowShownForCricketBet() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.verifyPriceChangeIconForBet(false, 2);
        });
    }
    verifyPriceChangeIconForBet(priceUp, betId) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            var iconCssContent = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetClass(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetPriceArrowPath(betId - 1));
            chai.assert.equal(iconCssContent, priceUp ? "odds is-up" : "odds is-down");
        });
    }
    enterStake(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
                }
            }
        });
    }
    stakeNotChange(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var dstake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
            chai.assert.equal(dstake, stake);
        });
    }
    multiStakeNotChange(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var dstake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(0));
            chai.assert.equal(dstake, stake);
        });
    }
    receiveNotification() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.verifyToastMessage("1 bet failed. Price updated", true);
        });
    }
    receivePriceUpdateForCricket() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    /// Start - Verifications 
    verifyMultiPayoutUpdate() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var estimatedReturn = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesPayoutPath(0));
            chai.assert.equal(estimatedReturn, "Return: €9,463.50");
        });
    }
    verifyTotalPayoutUpdate(payout) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var totalpayout = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalPayoutPath());
            chai.assert.equal(totalpayout, " €" + payout);
        });
    }
    verifyMultiTotalPayoutUpdate() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var totalpayout = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalPayoutPath());
            chai.assert.equal(totalpayout, " €9,463.50");
        });
    }
    verifyToasterWithYourbetslipgotupdated() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.verifyToastMessage("Your betslip got updated", true);
        });
    }
    verifyToasterWith1BetOK1BetFailedNotification() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.verifyToastMessage("1 bet placed. 1 bet failed.", true);
        });
    }
    verifyToasterWith1BetOK2BetsFailedNotification() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.verifyToastMessage("1 bet placed. 2 bets failed.", true);
        });
    }
    verifyToasterWith2BetsFailedNotification() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.verifyToastMessage("2 bets failed.", true);
        });
    }
    verifyToasterWithKeepBets() {
        return __awaiter(this, void 0, void 0, function* () {
            //TODO:
            // await this.verifyToastMessage("KEEP BETS", false);
        });
    }
    verifyToasterWithOk() {
        return __awaiter(this, void 0, void 0, function* () {
            // await this.verifyToastMessage("OK", false);
        });
    }
    verifyMultiPriceChangeError() {
        return __awaiter(this, void 0, void 0, function* () {
            var error = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesErrorMessagePath(0));
            chai.assert.equal(error, "Price Update");
        });
    }
    verifyToastMessage(message, exactMatch) {
        return __awaiter(this, void 0, void 0, function* () {
            if (exactMatch) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1800);
                var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
                chai.assert.include(betTicketText, message);
            }
            else {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1800);
                var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
                chai.assert.include(betTicketText, message);
            }
        });
    }
};
__decorate([
    cucumber_tsflow_1.then(/user receives a price update for the badminton game for price 3.50/)
], PriceUpdate.prototype, "receivePriceUpdateForBet1", null);
__decorate([
    cucumber_tsflow_1.then(/the user recives the price update for the badminton game for (.*)/)
], PriceUpdate.prototype, "receivePriceUpdateForBet1Odd", null);
__decorate([
    cucumber_tsflow_1.then(/verify an updated price is shown in the betslip page/)
], PriceUpdate.prototype, "isUpdatedPriceShown", null);
__decorate([
    cucumber_tsflow_1.then(/verify a green upward arrow is shown for the updated price change$/)
], PriceUpdate.prototype, "isGreenUpwardArrowShown", null);
__decorate([
    cucumber_tsflow_1.then(/user receives a price update for the badminton game for price 1.50/)
], PriceUpdate.prototype, "addcricketmarket", null);
__decorate([
    cucumber_tsflow_1.then(/verify a red downward arrow is shown for the updated price change$/)
], PriceUpdate.prototype, "isRedDownArrowShown", null);
__decorate([
    cucumber_tsflow_1.then(/verify a red downward arrow is shown for the updated price change for cricket game/)
], PriceUpdate.prototype, "verifyRedDownArrowShownForCricketBet", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake(.*) in the betslip/)
], PriceUpdate.prototype, "enterStake", null);
__decorate([
    cucumber_tsflow_1.then(/verify the stake(.*) box value is not changed/)
], PriceUpdate.prototype, "stakeNotChange", null);
__decorate([
    cucumber_tsflow_1.then(/verify the multi stake(.*) box value is not changed/)
], PriceUpdate.prototype, "multiStakeNotChange", null);
__decorate([
    cucumber_tsflow_1.then(/user receives notification toaster saying "1 bet failed. Price updated" for the price of 1.50/),
    cucumber_tsflow_1.then(/user receives notification toaster saying "1 bet failed. Price updated" for the price of 3.50/),
    cucumber_tsflow_1.then(/user receives notification toaster saying "1 bet failed. Price updated" for the price of 1.80/)
], PriceUpdate.prototype, "receiveNotification", null);
__decorate([
    cucumber_tsflow_1.then(/user receives a price update for the cricket game/)
], PriceUpdate.prototype, "receivePriceUpdateForCricket", null);
__decorate([
    cucumber_tsflow_1.then(/verify the multi est return below the stake box is updated to 9,463.50/)
], PriceUpdate.prototype, "verifyMultiPayoutUpdate", null);
__decorate([
    cucumber_tsflow_1.then(/verify the total est return value is updated to (.*)/)
], PriceUpdate.prototype, "verifyTotalPayoutUpdate", null);
__decorate([
    cucumber_tsflow_1.then(/verify the multi total est return value is updated to 9,463.50/)
], PriceUpdate.prototype, "verifyMultiTotalPayoutUpdate", null);
__decorate([
    cucumber_tsflow_1.then(/user receives notification toaster saying "Your betslip got updated"/)
], PriceUpdate.prototype, "verifyToasterWithYourbetslipgotupdated", null);
__decorate([
    cucumber_tsflow_1.then(/user receives notification toaster saying "1 bet placed. 1 bet failed."/)
], PriceUpdate.prototype, "verifyToasterWith1BetOK1BetFailedNotification", null);
__decorate([
    cucumber_tsflow_1.then(/user receives notification toaster saying "1 bet placed. 2 bets failed."/)
], PriceUpdate.prototype, "verifyToasterWith1BetOK2BetsFailedNotification", null);
__decorate([
    cucumber_tsflow_1.then(/user receives notification toaster saying "2 bets failed."/)
], PriceUpdate.prototype, "verifyToasterWith2BetsFailedNotification", null);
__decorate([
    cucumber_tsflow_1.then(/the user sees the Keep Bets button on the notificaiton toaster/)
], PriceUpdate.prototype, "verifyToasterWithKeepBets", null);
__decorate([
    cucumber_tsflow_1.then(/the user sees the Ok button on the notificaiton toaster/)
], PriceUpdate.prototype, "verifyToasterWithOk", null);
__decorate([
    cucumber_tsflow_1.then(/verify multi stake box shows an error message as "Price Change"/)
], PriceUpdate.prototype, "verifyMultiPriceChangeError", null);
PriceUpdate = __decorate([
    cucumber_tsflow_1.binding()
], PriceUpdate);
module.exports = PriceUpdate;
