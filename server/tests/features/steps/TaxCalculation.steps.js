"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const cucumber_tsflow_1 = require("cucumber-tsflow");
const chai = require('chai');
const FeatureTestBase_1 = require("../utils/FeatureTestBase");
let TaxCalculation = class TaxCalculation extends FeatureTestBase_1.FeatureTestBase {
    addBets(singleodds) {
        return __awaiter(this, void 0, void 0, function* () {
            if (singleodds != "") {
                var parsedMarkets = singleodds.split(",");
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
                var oddslist = [2.5, 500, 5, 100, 2, 5, 2, 5, 1, 2500, 5, 3, 1.5, 2, 1.80];
                var eventids = ["123448", "123456", "89383", "123455", "89379", "89375", "123454", "123453", "123452", "123451", "123451", "123450", "123450", "123449", "89395"];
                var outcomeids = ["1", "2", "1", "1", "1", "2", "1", "1", "1", "1", "2", "1", "2", "1", "1"];
                for (var i = 0; i < parsedMarkets.length; i++) {
                    console.log("=======price====", parsedMarkets[i]);
                    var parsedOdds = parseFloat(parsedMarkets[i]);
                    var index = oddslist.indexOf(parsedOdds);
                    console.log(parsedOdds + "-======" + index);
                    if (index > -1) {
                        console.log("*************click on " + eventids[index] + "_" + outcomeids[index]);
                        yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndScrollThenClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(eventids[index], outcomeids[index]));
                        oddslist.splice(index, 1);
                        eventids.splice(index, 1);
                        outcomeids.splice(index, 1);
                        FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
                    }
                }
            }
        });
    }
    verifyPayoutUpdate(estreturn) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var payout = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetPayoutPath(0));
            chai.assert.equal(payout, estreturn == "" ? "" : ("Return: €" + estreturn));
        });
    }
    verifyTaxUpdate(esttax) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var tax = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetTaxPath(0));
            chai.assert.equal(tax, esttax == "" ? "" : ("Tax: €" + esttax));
        });
    }
    verifyMultiPayoutUpdate(estreturn) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var payout = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesPayoutPath(0));
            chai.assert.equal(payout, estreturn == "" ? "" : ("Return: €" + estreturn));
        });
    }
    //@then(/verify the est return below the five folds stake box for (.*)markets is updated to (.*)/)
    //public async verifyMultiplePayoutUpdate(markets: string, exoticestreturn: string): Promise<void> {
    //    FeatureTestBase.browser.sleep(500);
    //    var position = parseInt(markets) == 5 ? 0 : 4;
    //    var payout = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesPayoutPath(position));
    //    chai.assert.equal(payout, exoticestreturn == "" ? "" : ("Return: €" + exoticestreturn));
    //}
    //@then(/verify the tax value below the five folds stake box for (.*)markets is updated to (.*)/)
    //public async verifyMultipleTaxUpdate(markets: string, exotictaxvalue: string): Promise<void> {
    //    FeatureTestBase.browser.sleep(500);
    //    var position = parseInt(markets) == 5 ? 0 : 4;
    //    var tax = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesTaxPath(position));
    //    chai.assert.equal(tax, exotictaxvalue == "" ? "" : ("Return: €" + exotictaxvalue));
    //}
    verifyMultiTaxUpdate(esttax) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var tax = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesTaxPath(0));
            chai.assert.equal(tax, esttax == "" ? "" : ("Tax: €" + esttax));
        });
    }
    verifyTotalReturnAmount(betslipestreturn) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var estimatedReturn = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalPayoutPath());
            chai.assert.equal(estimatedReturn, betslipestreturn == "" ? " " : (" €" + betslipestreturn));
        });
    }
    verifyTotalTaxAmount(betsliptaxvalue) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var estimatedTax = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalTaxPath());
            chai.assert.equal(estimatedTax, betsliptaxvalue == "" ? " " : (" €" + betsliptaxvalue));
        });
    }
    verifyTotalTaxNotShown() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var estimatedTax = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalTaxPath());
            chai.assert.equal(estimatedTax, " ");
        });
    }
    expectedStake(expectedstake) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(6000);
            var baseballBetStake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
            console.log("======1======", baseballBetStake);
            var stake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakeValuePath(0));
            console.log("======2======", stake);
            chai.assert.equal(stake, expectedstake);
        });
    }
    verifyToasterWith2BetsFailedNotification(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            if ((parseFloat(stake) * 100) % 100 == 0) {
                console.log("no toast check ....");
                yield this.verifyToastMessage("", true);
            }
            else {
                yield this.verifyToastMessage("Stake rounded for some selections", true);
            }
        });
    }
    verifyToastMessage(message, exactMatch) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var toastMessage = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            if (exactMatch) {
                chai.assert.equal(toastMessage, message);
            }
            else {
                chai.assert.include(toastMessage, message);
            }
        });
    }
};
__decorate([
    cucumber_tsflow_1.then(/user clicks on the markets for odds(.*)/)
], TaxCalculation.prototype, "addBets", null);
__decorate([
    cucumber_tsflow_1.then(/verify the est return below the stake box is updated to (.*)/)
], TaxCalculation.prototype, "verifyPayoutUpdate", null);
__decorate([
    cucumber_tsflow_1.then(/verify the tax value below the stake box is updated to (.*)/)
], TaxCalculation.prototype, "verifyTaxUpdate", null);
__decorate([
    cucumber_tsflow_1.then(/verify the est return below the multi stake box is updated to (.*)/)
], TaxCalculation.prototype, "verifyMultiPayoutUpdate", null);
__decorate([
    cucumber_tsflow_1.then(/verify the tax value below the multi stake box is updated to (.*)/)
], TaxCalculation.prototype, "verifyMultiTaxUpdate", null);
__decorate([
    cucumber_tsflow_1.then(/verify the total est return value is updated for the betstlip to (.*)/)
], TaxCalculation.prototype, "verifyTotalReturnAmount", null);
__decorate([
    cucumber_tsflow_1.then(/verify the total tax value is updated for the betstlip to (.*)/)
], TaxCalculation.prototype, "verifyTotalTaxAmount", null);
__decorate([
    cucumber_tsflow_1.then(/verify the total tax value is not shown on the betstlip/)
], TaxCalculation.prototype, "verifyTotalTaxNotShown", null);
__decorate([
    cucumber_tsflow_1.then(/user sees the expected stake(.*) in the stake box/)
], TaxCalculation.prototype, "expectedStake", null);
__decorate([
    cucumber_tsflow_1.then(/the user sees the notification toaster with text "Stake rounded for some selections" for (.*)/)
], TaxCalculation.prototype, "verifyToasterWith2BetsFailedNotification", null);
TaxCalculation = __decorate([
    cucumber_tsflow_1.binding()
], TaxCalculation);
module.exports = TaxCalculation;
