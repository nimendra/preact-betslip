﻿import { binding, given, then, before, after, when } from "cucumber-tsflow";
import { MyElementHandler } from "../utils/MyElementHandler";
import { FeatureTestBase } from "../utils/FeatureTestBase";
import * as webdriver from "selenium-webdriver";
const chai = require('chai');

@binding()
class PriceUpdate extends FeatureTestBase {
    protected betslipButtonXPath: string = "//paper-icon-button[@id='bet_btn']";
    protected clearAllBetsButtonXPath: string = "//*[@id='scroller_area']/section/div[1]/paper-button";
    protected closeBetslipPageButtonXPath: string = "//paper-icon-button[@id='betslip-close']";
    protected homePageToastBetTicketLabelXPath: string = "//*[@id=\"label\"]";
    protected otherMultiplesCollapseItemXPath: string = "//*[@id=\"system_bets_paper_collapse\"]/paper-item/paper-item-body/div/div";
    protected applyAllStakeInputXPath: string = "//*[@id=\"stake_applyall\"]/paper-input-container/div/div/input";

    /// Start - Click on markets
    @then(/user receives a price update for the badminton game for price 3.50/)
    public async receivePriceUpdateForBet1(): Promise<void> {
        var signal = {
            selectionId: "89403_1",
            price: {
                decimal: 3.5
            }
        };
        await FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");
        FeatureTestBase.browser.sleep(2000);         
        var price = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetPricePath(0));
        chai.assert.equal(price, "3.50");
    }
    @then(/the user recives the price update for the badminton game for (.*)/)
    public async receivePriceUpdateForBet1Odd(odd: number): Promise<void> {
        console.log('=============',odd);
        var signal = {
            selectionId: "89403_1",
            price: {
                decimal: odd
            }
        };

        await FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");
        FeatureTestBase.browser.sleep(2000);
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
    }

    @then(/verify an updated price is shown in the betslip page/)
    public async isUpdatedPriceShown(): Promise<void> {

    }

    @then(/verify a green upward arrow is shown for the updated price change$/)
    public async isGreenUpwardArrowShown(): Promise<void> {
       await this.verifyPriceChangeIconForBet(true, 1);
    }

    @then(/user receives a price update for the badminton game for price 1.50/)
    public async addcricketmarket(): Promise<void> {
        var signal =
            {
                selectionId: "89165_1",
                price: {
                    decimal: 1.5
                }
            };
        await FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");
        FeatureTestBase.browser.sleep(2000);
        var price = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetPricePath(0));
        chai.assert.equal(price, "1.50");
    }

    @then(/verify a red downward arrow is shown for the updated price change$/)
    public async isRedDownArrowShown(): Promise<void> {
        await this.verifyPriceChangeIconForBet(false, 1);
    }

    @then(/verify a red downward arrow is shown for the updated price change for cricket game/)
    public async verifyRedDownArrowShownForCricketBet(): Promise<void> {
        await this.verifyPriceChangeIconForBet(false, 2);
    }

    private async verifyPriceChangeIconForBet(priceUp: boolean, betId: number): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        var iconCssContent = await FeatureTestBase.myElementHandler.getElementAndGetClass(FeatureTestBase.myElementHandler.getSinglebetPriceArrowPath(betId-1));
        chai.assert.equal(iconCssContent, priceUp ? "odds is-up" : "odds is-down");
    }

    @then(/user enters the stake(.*) in the betslip/)
    public async enterStake(stake: string): Promise<void> {
        if (stake != "") {
             FeatureTestBase.browser.sleep(1000);
             await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
             for (var i = 0; i < stake.length; i++) {
                FeatureTestBase.browser.sleep(100);
                await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
            }
        }
    }

    @then(/verify the stake(.*) box value is not changed/)
    public async stakeNotChange(stake: string): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var dstake = await FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
         chai.assert.equal(dstake, stake);
    }

    @then(/verify the multi stake(.*) box value is not changed/)
    public async multiStakeNotChange(stake: string): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var dstake = await FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase.myElementHandler.getMultiplesStakePath(0));
        chai.assert.equal(dstake, stake);
    }

    @then(/user receives notification toaster saying "1 bet failed. Price updated" for the price of 1.50/)
    @then(/user receives notification toaster saying "1 bet failed. Price updated" for the price of 3.50/)
    @then(/user receives notification toaster saying "1 bet failed. Price updated" for the price of 1.80/)
    public async receiveNotification(): Promise<void> {
        await this.verifyToastMessage("1 bet failed. Price updated", true);
    }

    @then(/user receives a price update for the cricket game/)
    public async receivePriceUpdateForCricket(): Promise<void> {

    }


    /// Start - Verifications 
 
    @then(/verify the multi est return below the stake box is updated to 9,463.50/)
    public async verifyMultiPayoutUpdate(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var estimatedReturn = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesPayoutPath(0));
        chai.assert.equal(estimatedReturn, "Return: €9,463.50");
    }

    @then(/verify the total est return value is updated to (.*)/)
    public async verifyTotalPayoutUpdate(payout: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var totalpayout = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalPayoutPath());
        chai.assert.equal(totalpayout, " €" + payout);
    } 

    @then(/verify the multi total est return value is updated to 9,463.50/)
    public async verifyMultiTotalPayoutUpdate(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var totalpayout = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalPayoutPath());
        chai.assert.equal(totalpayout, " €9,463.50");
    }

    @then(/user receives notification toaster saying "Your betslip got updated"/)
    public async verifyToasterWithYourbetslipgotupdated(): Promise<void> {
       await this.verifyToastMessage("Your betslip got updated", true);
    }

    @then(/user receives notification toaster saying "1 bet placed. 1 bet failed."/)
    public async verifyToasterWith1BetOK1BetFailedNotification(): Promise<void> {
      await this.verifyToastMessage("1 bet placed. 1 bet failed.", true);
    }

    @then(/user receives notification toaster saying "1 bet placed. 2 bets failed."/)
    public async verifyToasterWith1BetOK2BetsFailedNotification(): Promise<void> {
        await this.verifyToastMessage("1 bet placed. 2 bets failed.", true);
    }

    @then(/user receives notification toaster saying "2 bets failed."/)
    public async verifyToasterWith2BetsFailedNotification(): Promise<void> {
        await this.verifyToastMessage("2 bets failed.", true);
    }

    @then(/the user sees the Keep Bets button on the notificaiton toaster/)
    public async verifyToasterWithKeepBets(): Promise<void> {
    　 //TODO:
       // await this.verifyToastMessage("KEEP BETS", false);
    }

    @then(/the user sees the Ok button on the notificaiton toaster/)
    public async verifyToasterWithOk(): Promise<void> {
       // await this.verifyToastMessage("OK", false);
    }

    @then(/verify multi stake box shows an error message as "Price Change"/)
    public async verifyMultiPriceChangeError(): Promise<void> {        
        var error = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesErrorMessagePath(0));
        chai.assert.equal(error, "Price Update");
    }

    private async verifyToastMessage(message: string, exactMatch: boolean) {
        if (exactMatch) {
            FeatureTestBase.browser.sleep(1800);
            var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.include(betTicketText, message);
        } else {
            FeatureTestBase.browser.sleep(1800);
            var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.include(betTicketText, message);
        }
    }

    /// End - Verifications 
}
export = PriceUpdate;
