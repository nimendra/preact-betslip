﻿# Few useful element dom paths for shadow doms


#### Toast

`var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(
            FeatureTestBase.myElementHandler.stackShadows(["#toast"]) + '.querySelector("#label")');`


#### Bet Buttons on the Sport Markets

 `await FeatureTestBase.myElementHandler.getElementAndClick(
           FeatureTestBase.myElementHandler.stackShadows(["my-sports-nextn","my-sports-button#bet_btn_89403_1"]) + '.querySelector("paper-button")');`


#### Multi Stakes

`var shadows= ["betslip-container","betslip-content","betslip-multi"];
        await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.stackShadows(shadows) + '.querySelector("#'+ multiBetStakeInputId +'")',stake);
        `

#### Get Team and Game Names
`var shadows = ["betslip-container", "betslip-content", "#bet-0"];`


`var americanFootballBetTeamName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.stackShadows(shadows)
            + '.querySelector("#outcomeName")');`

`var americanFootballBetOutcomeName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.stackShadows(shadows)
            + '.querySelector("div.event")');`


#### Total Stake
`   var shadows = ["betslip-container", "betslip-content"];
        var totalStake = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.stackShadows(shadows)
            + '.querySelector("span.stake-value")');`

       

>### Historically...
>Before the proper shadowdom, elements could be accessed with standard selectors, by forcing the documents to render with ShadyDom.

>### Polymer 2
>With Polymer2, this forced-fallback is no longer possible, and we have to use one of three approaches to find the elements through the Selenium web driver.
>
>###### Option 1:  
>`var elem = await this.browser.findElement(webdriver.By.tagName('my-email-component /deep/ #input'));`  
>###### Option 2: 
>`var elem = driver.executeScript("return $('body /deep/ my-email-component')")`
>
>###### Option 3:  
>`var elem = await driver.executeScript('return document.querySelector("my-email-component").shadowRoot.querySelector("paper-input")');`   
>`element.sendKeys("Hello, world!");`

> Although seemingly a complex way of writing a selector, the 3rd option is the best - it's more robust and will work across shadowdoms and page doms alike.  
> We have created a helper wrapper **stackShadows** that makes the notation a little bit simpler -  
>   
> ` var shadows = ["#betslip", "betslip-content", "betslip-multi"];`  
>`var multiName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.stackShadows(shadows)
            + '.querySelector("#title_1'")');`

>Something worth noting is, depending on how a polymer component is composed, you may find *slotted* content outside the shadowdom. One handy way to find the shadow root path is to check the Chrome console.
>
![alt text](https://lh3.googleusercontent.com/Z7HtYG3GqPG1fEHIU1ouR_bsk1lrqXpWql2DixC8rHSqWnjOvoyPlZcTXBhFDJRN2FWMRD-Qq6hCZTHgtkbxbOpsCRDhJyZXfA4vKfPse3dwCRoZiy-Bi_pldld2YLLF5Fdj2u4FucLmsQvNTmWG0JvUgvWp1_S3lMfRQO7IEIxU1fyiINNKuehMIMg9ksZsMEhkPBrazSc5vhKRKKxDI7QxGHNwO8QWQAKvC5dHvthq6L7Ba_BkNyAIc5rJImLy6rEiQQnGKn2Ameq02ZnhNBnntLEir9fa-FZlWxPUKjd3PfkSKj6o14NWPOoJBqAomH2rxT2eQ2jDunRGH6QnWL3Caa4h0r7WEkNRRQ78P7v3RxmYPaCohOcdOIiLNxbIzSklqpGHJG_I-Vo8XrKYy1m80FMK6sGNJYfsCtwBTUUuNaou8DZQ0Sd-5LhHiGoKcoQczl3DAu3tQIKUUcHvwdPYD8GosWRDpTZKMEn7lcrstEkOT1UlCchzXP5mqKROqtigGzgciX048uR7GqA6i5Z46Lyh9k6Ud39KtGAtgFnmNK6OeXDxNRAtzCirc7EOjca2YqxFBb-qbE7QLOQc-QZruBflefcMBwTB2QZeUlgFYfRm4I3npF3J74uSlGojUr0kNhoSeSLABq-68xwp7qyqd6sztNqzX9C_VYQZFw=w1788-h903-no "Logo Title Text 1")