"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const cucumber_tsflow_1 = require("cucumber-tsflow");
const chai = require('chai');
const FeatureTestBase_1 = require("../utils/FeatureTestBase");
let SuspendBet = class SuspendBet extends FeatureTestBase_1.FeatureTestBase {
    constructor() {
        super(...arguments);
        this.betslipButtonXPath = "//paper-icon-button[@id='bet_btn']";
        this.clearAllBetsButtonXPath = "//*[@id='scroller_area']/section/div[1]/paper-button";
        this.closeBetslipPageButtonXPath = "//paper-icon-button[@id='betslip-close']";
        this.homePageToastBetTicketLabelXPath = "//*[@id=\"label\"]";
        this.betslipPageToastXPath = "//*[@id=\"responseToast\"]";
        this.otherMultiplesCollapseItemXPath = "//*[@id=\"system_bets_paper_collapse\"]/paper-item/paper-item-body/div/div";
        this.applyAllStakeInputXPath = "//*[@id=\"stake_applyall\"]/paper-input-container/div/div/input";
        /// End - Verifications0
    }
    /// Start - Click on markets
    /// End - Click on markets
    /// Start - Betslip handling
    recieveBetSuspendedSignal() {
        return __awaiter(this, void 0, void 0, function* () {
            var othermultiples = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElement(FeatureTestBase_1.FeatureTestBase.myElementHandler.getOtherMultiplesPath());
            if (othermultiples) {
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getOtherMultiplesPath());
            }
            var signal = {
                selectionId: "89403_1",
                price: {
                    decimal: 1.57
                },
                suspended: true
            };
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");
        });
    }
    recieveBetSuspendedSignalfor500() {
        return __awaiter(this, void 0, void 0, function* () {
            var signal = {
                selectionId: "123456_2",
                price: {
                    decimal: 500.00
                },
                suspended: true
            };
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");
        });
    }
    enterBadmintonStake(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
                }
            }
        });
    }
    enterCricketStake(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(1));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(1), stake[i]);
                }
            }
        });
    }
    enterBaseballStake(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(2));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(2), stake[i]);
                }
            }
        });
    }
    removeSuspendedBet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetRemoveButtonPath(0));
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
        });
    }
    receivePriceUpdateForCricket() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
        });
    }
    /// End - Betslip handling
    /// Start - Verifications  
    verifyBetStakeInputIsDisabled() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetCardPath(0), 'disabled');
            chai.assert.equal(disabled, 'true');
        });
    }
    verifyBetButtonDisabled() {
        return __awaiter(this, void 0, void 0, function* () {
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getBetButtonPath(), 'disabled');
            chai.assert.equal(disabled, 'true');
        });
    }
    verifyBetSupspendedToastAppears() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var toastText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(toastText, "1 bet suspended");
        });
    }
    verifyMultiStakeInputDisabled() {
        return __awaiter(this, void 0, void 0, function* () {
            // FeatureTestBase.browser.sleep(20);
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesCardPath(0), 'disabled');
            chai.assert.equal(disabled, 'true');
        });
    }
    verifyApplyAllStakeInputDisabled() {
        return __awaiter(this, void 0, void 0, function* () {
            //await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getOtherMultiplesPath());
            //FeatureTestBase.browser.sleep(1000);
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getApplyAllCardPath(), 'disabled');
            chai.assert.equal(disabled, 'true');
        });
    }
    verifySecondBetStakeInputEnabled() {
        return __awaiter(this, void 0, void 0, function* () {
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetCardPath(1), 'disabled');
            chai.assert.isNull(disabled);
        });
    }
    verifyTotalStake(expectedTotalStake) {
        return __awaiter(this, void 0, void 0, function* () {
            var actualTotalStake = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalStakePath());
            chai.assert.equal(actualTotalStake, " €" + expectedTotalStake);
        });
    }
    verifyEstimatedReturn(expectedReturn) {
        return __awaiter(this, void 0, void 0, function* () {
            var actualReturn = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalPayoutPath());
            chai.assert.equal(actualReturn, " €" + expectedReturn);
        });
    }
    verifyBetCount() {
        return __awaiter(this, void 0, void 0, function* () {
            var betCount = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getTotalBetCountPath());
            chai.assert.equal(betCount, "(1)");
        });
    }
    verifyNLegMultiVisible(legCount) {
        return __awaiter(this, void 0, void 0, function* () {
            //FeatureTestBase.browser.sleep(1000);
            var multiName = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesName(legCount - 2));
            var expectedMultiName = ["Double", "Treble"];
            chai.assert.equal(multiName, expectedMultiName[legCount - 2]);
        });
    }
    verify2LegMultiVisible() {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyNLegMultiVisible(2);
        });
    }
    verify3LegMultiVisible() {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyNLegMultiVisible(3);
        });
    }
    verifyBetsPlacedToastAppears() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var toastBetTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(toastBetTicketText, "2 bets placed.");
        });
    }
    verifyMultiBetSuspendedError() {
        return __awaiter(this, void 0, void 0, function* () {
            var error = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesErrorMessagePath(0));
            chai.assert.equal(error, "Suspended");
        });
    }
    verifySingleBetSuspendedError() {
        return __awaiter(this, void 0, void 0, function* () {
            var error = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetSuspendedErrorMessagePath(1));
            chai.assert.equal(error, "Suspended");
        });
    }
};
__decorate([
    cucumber_tsflow_1.then(/user recieves a bet update for the badminton game as suspended/)
], SuspendBet.prototype, "recieveBetSuspendedSignal", null);
__decorate([
    cucumber_tsflow_1.then(/user recieves a bet update for the badminton game odd 500 as suspended/)
], SuspendBet.prototype, "recieveBetSuspendedSignalfor500", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake value in the badminton game box for £(.*)/),
    cucumber_tsflow_1.then(/user enters the stake amount(.*) for badminton game/)
], SuspendBet.prototype, "enterBadmintonStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake value in the cricket game box for £(.*)/),
    cucumber_tsflow_1.then(/user enters the stake amount(.*) for cricket game/)
], SuspendBet.prototype, "enterCricketStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake amount(.*) for baseball game/)
], SuspendBet.prototype, "enterBaseballStake", null);
__decorate([
    cucumber_tsflow_1.then(/user removes the suspended bet/)
], SuspendBet.prototype, "removeSuspendedBet", null);
__decorate([
    cucumber_tsflow_1.then(/user receives a suspend error for the cricket game/)
], SuspendBet.prototype, "receivePriceUpdateForCricket", null);
__decorate([
    cucumber_tsflow_1.then(/verify stake box is disabled on the betslip/),
    cucumber_tsflow_1.then(/verify stake box for the badminton game is disabled on the betslip/)
], SuspendBet.prototype, "verifyBetStakeInputIsDisabled", null);
__decorate([
    cucumber_tsflow_1.then(/verify bet button is disabled/)
], SuspendBet.prototype, "verifyBetButtonDisabled", null);
__decorate([
    cucumber_tsflow_1.then(/verify the notification toaster appear with text "1 bet suspended"/)
], SuspendBet.prototype, "verifyBetSupspendedToastAppears", null);
__decorate([
    cucumber_tsflow_1.then(/verify the stake box for the multi is disabled/)
], SuspendBet.prototype, "verifyMultiStakeInputDisabled", null);
__decorate([
    cucumber_tsflow_1.then(/verify apply all single box is disabled/)
], SuspendBet.prototype, "verifyApplyAllStakeInputDisabled", null);
__decorate([
    cucumber_tsflow_1.then(/verify bet button is enabled for placing the single bet for cricket game./)
], SuspendBet.prototype, "verifySecondBetStakeInputEnabled", null);
__decorate([
    cucumber_tsflow_1.then(/verify the total stake(.*) is correct/)
], SuspendBet.prototype, "verifyTotalStake", null);
__decorate([
    cucumber_tsflow_1.then(/verify the estimate return(.*) is correct/)
], SuspendBet.prototype, "verifyEstimatedReturn", null);
__decorate([
    cucumber_tsflow_1.then(/verify the bet count is 1/)
], SuspendBet.prototype, "verifyBetCount", null);
__decorate([
    cucumber_tsflow_1.then(/verify 2 leg multi is suggested/)
], SuspendBet.prototype, "verify2LegMultiVisible", null);
__decorate([
    cucumber_tsflow_1.then(/verify 3 leg multi is suggested/)
], SuspendBet.prototype, "verify3LegMultiVisible", null);
__decorate([
    cucumber_tsflow_1.then(/user receives notification toaster with text "2 bets placed./)
], SuspendBet.prototype, "verifyBetsPlacedToastAppears", null);
__decorate([
    cucumber_tsflow_1.then(/verify multi stake box shows an error message as "Suspended"/)
], SuspendBet.prototype, "verifyMultiBetSuspendedError", null);
__decorate([
    cucumber_tsflow_1.then(/verify single stake box shows an error message as "Suspended"/)
], SuspendBet.prototype, "verifySingleBetSuspendedError", null);
SuspendBet = __decorate([
    cucumber_tsflow_1.binding()
], SuspendBet);
module.exports = SuspendBet;
