"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const cucumber_tsflow_1 = require("cucumber-tsflow");
const chai = require('chai');
const FeatureTestBase_1 = require("../utils/FeatureTestBase");
let Others = class Others extends FeatureTestBase_1.FeatureTestBase {
    enters4010FirstStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var stake = "4010";
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
            }
        });
    }
    enters4000FirstStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var stake = "4000";
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
            }
        });
    }
    enters8000FirstStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var stake = "8000";
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
            }
        });
    }
    enters8000SecondStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var stake = "8000";
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(1), stake[i]);
            }
        });
    }
    enters8000ThirdStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var stake = "8000";
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(2), stake[i]);
            }
        });
    }
    enters8000ForthStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var stake = "8000";
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(3), stake[i]);
            }
        });
    }
    verifyBetButtonEnabled() {
        return __awaiter(this, void 0, void 0, function* () {
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getBetButtonPath(), 'disabled');
            chai.assert.equal(disabled, null);
        });
    }
    verifyBetSupspendedToastAppears() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1300);
            var toastText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(toastText, "Server or Network error: 401Unauthorized");
        });
    }
    verifyBetInterceptErrorToastAppears() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1300);
            var toastText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(toastText, "Server or Network error: 500Internal Server Error");
        });
    }
    afterfewSecond() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(4000);
            var toastText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(toastText, "");
        });
    }
    after1Second() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var toastText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
        });
    }
    verifybetPlaced() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(betTicketText, "Bet placed. Receipt #17W5EPF6");
        });
    }
    verifyBetStakeInputIsEnabled() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetCardPath(0), 'disabled');
            chai.assert.equal(disabled, null);
        });
    }
    verifyBetSecondStakeInputIsEnabled() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetCardPath(1), 'disabled');
            chai.assert.equal(disabled, null);
        });
    }
    verifyBetSecondStakeInputIsDisabled() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetCardPath(1), 'disabled');
            chai.assert.equal(disabled, 'true');
        });
    }
    verifyBetThirdStakeInputIsEnabled() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            var disabled = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetCardPath(2), 'disabled');
            chai.assert.equal(disabled, null);
        });
    }
    addSecondBet() {
        return __awaiter(this, void 0, void 0, function* () {
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89157, 2));
        });
    }
    addThirdBet() {
        return __awaiter(this, void 0, void 0, function* () {
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89157, 3));
        });
    }
    verifyMultiConflictsShown() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);
            var errorMessage = "Intercepted -> Rejected by Admin";
            var bet1ErrorMessage = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(0));
            chai.assert.equal(bet1ErrorMessage, errorMessage);
        });
    }
};
__decorate([
    cucumber_tsflow_1.then(/user enters 4010 in first market stake box in the betslip/)
], Others.prototype, "enters4010FirstStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters 4000 in first market stake box in the betslip/)
], Others.prototype, "enters4000FirstStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters 8000 in first market stake box in the betslip/)
], Others.prototype, "enters8000FirstStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters 8000 in second market stake box in the betslip/)
], Others.prototype, "enters8000SecondStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters 8000 in third market stake box in the betslip/)
], Others.prototype, "enters8000ThirdStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters 8000 in forth market stake box in the betslip/)
], Others.prototype, "enters8000ForthStake", null);
__decorate([
    cucumber_tsflow_1.then(/verify bet button is enabled/)
], Others.prototype, "verifyBetButtonEnabled", null);
__decorate([
    cucumber_tsflow_1.then(/verify the notification toaster appear with text "Server or Network error: 401Unauthorized"/)
], Others.prototype, "verifyBetSupspendedToastAppears", null);
__decorate([
    cucumber_tsflow_1.then(/verify the notification toaster appear with text "Server or Network error: 500Internal Server Error"/)
], Others.prototype, "verifyBetInterceptErrorToastAppears", null);
__decorate([
    cucumber_tsflow_1.then(/after few seconds/)
], Others.prototype, "afterfewSecond", null);
__decorate([
    cucumber_tsflow_1.then(/after 1 seconds/)
], Others.prototype, "after1Second", null);
__decorate([
    cucumber_tsflow_1.then(/verify user recives a notification toaster "Bet placed. Receipt #17W5EPF6"/)
], Others.prototype, "verifybetPlaced", null);
__decorate([
    cucumber_tsflow_1.then(/verify stake box is enabled on the betslip/)
], Others.prototype, "verifyBetStakeInputIsEnabled", null);
__decorate([
    cucumber_tsflow_1.then(/verify second stake box is enabled on the betslip/)
], Others.prototype, "verifyBetSecondStakeInputIsEnabled", null);
__decorate([
    cucumber_tsflow_1.then(/verify second stake box is disabled on the betslip/)
], Others.prototype, "verifyBetSecondStakeInputIsDisabled", null);
__decorate([
    cucumber_tsflow_1.then(/verify third stake box is enabled on the betslip/)
], Others.prototype, "verifyBetThirdStakeInputIsEnabled", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the second price market for the in play soccer game/)
], Others.prototype, "addSecondBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the third price market for the in play soccer game/)
], Others.prototype, "addThirdBet", null);
__decorate([
    cucumber_tsflow_1.then(/verify the error message for the first bet shows "Intercepted -> Rejected by Admin"/)
], Others.prototype, "verifyMultiConflictsShown", null);
Others = __decorate([
    cucumber_tsflow_1.binding()
], Others);
module.exports = Others;
