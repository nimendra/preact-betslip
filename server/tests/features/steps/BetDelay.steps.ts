﻿import { binding, given, then, before, after } from "cucumber-tsflow";
import { MyElementHandler } from "../utils/MyElementHandler";
import * as webdriver from "selenium-webdriver";
const chai = require('chai');
import { FeatureTestBase } from "../utils/FeatureTestBase";

@binding()
class BetDelay extends FeatureTestBase {  
    @then(/user clicks on the first price market for the pre match badminton game/)
    @then(/user clicks on the first price market for the in play badminton game/)
    public async addFirstBet(): Promise<void> {
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89403, 1));
    }

    @then(/user clicks on the first price market for the pre match cricket game/)
    @then(/user clicks on the first price market for the in play cricket game/)
    public async addSecondBet(): Promise<void> {
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89165, 1));
    }
    @then(/user clicks on the first price market for the pre match baseball game/)
    @then(/user clicks on the first price market for the in play baseball game/)
    public async addThirdBet(): Promise<void> {
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89153, 1));
    }

    @then(/user clicks on the first price market for the in play soccer game/)
    public async addFouthBet(): Promise<void> {
        await FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase.myElementHandler.getSportsButtonPath(89157, 1));
    }
    	

    @then(/verify the progress bar is displayed for 5sec/)
    public async verifyProgressbar5sec(): Promise<void> {        
        FeatureTestBase.browser.sleep(2000);
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
        chai.assert.equal(progressbar, null);

        FeatureTestBase.browser.sleep(5000); 
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
        chai.assert.equal(progressbar, 'true');
    }

    @then(/verify the progress bar is displayed for 6sec/)
    public async verifyProgressbar6sec(): Promise<void> {
        FeatureTestBase.browser.sleep(2000);
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
        chai.assert.equal(progressbar, null);

        FeatureTestBase.browser.sleep(3000);
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
      //  chai.assert.equal(progressbar, 'true');
    }

    @then(/verify the progress bar is displayed for 8sec/)
    public async verifyProgressbar8sec(): Promise<void> {
        FeatureTestBase.browser.sleep(2000);
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
        chai.assert.equal(progressbar, null);

        FeatureTestBase.browser.sleep(9000);
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
        chai.assert.equal(progressbar, 'true');
    }


    @then(/verify the multi progress bar is displayed for 5sec/)
    public async verifyMultiProgressbar5sec(): Promise<void> {
        FeatureTestBase.browser.sleep(2000);
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "hidden");
        chai.assert.equal(progressbar, null);

        FeatureTestBase.browser.sleep(4000);
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "hidden");
        chai.assert.equal(progressbar, null);

        //FeatureTestBase.browser.sleep(1000);
        //var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "value");
        //chai.assert.equal((progressbarvalue > 0), false);      
    }

    @then(/verify the no delay bet is placed "Bet placed. Receipt #"/)
    public async nodelaybetplaced(): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(betTicketText, "Bet placed. Receipt #17W5EPF6");
    }

    @then(/verify the bet is placed "Bet placed. Receipt #"/)
    public async betplaced(): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(betTicketText, "Bet placed. Receipt #A12345");
    } 

    @then(/verify the bet is rejected "1 bet failed"/)
    public async betfailed(): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        var hasBetFailed = betTicketText.indexOf("1 bet failed.") != -1;
        chai.assert.equal(hasBetFailed, true);
    }
     
    @then(/verify no progress bar is displayed/)
    public async verifynoProgressbar(): Promise<void> {        
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
        chai.assert.equal(progressbar, null);
    }
        
    @then(/verify the no multi progress bar is displayed/)
    public async verifynoMultiProgressbar(): Promise<void> {       
       // var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "hidden");
       // chai.assert.equal(progressbar, null);
    }


    @then(/user enters the stake(.*) in the betslip for the badminton game/)
    public async enterBadmintonStake(stake: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
        for (var i = 0; i < stake.length; i++) {
            FeatureTestBase.browser.sleep(100);
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
        } 
    }

    @then(/user enters the soccer stake(.*)in the betslip for the soccer game/)
    public async enterSoccerStake(stake: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getSinglebetStakePath(2));
        for (var i = 0; i < stake.length; i++) {
            FeatureTestBase.browser.sleep(100);
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(2), stake[i]);
        }
    }     

    @then(/user enters the cricket stake(.*) in the betslip for the cricket game/)
    public async enterCricketStake(stake: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getSinglebetStakePath(1));
        for (var i = 0; i < stake.length; i++) {
            FeatureTestBase.browser.sleep(100);
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(1), stake[i]);
        }
    }
    
    @then(/user enters the stake(.*) in the multistake box/)
    public async enterMultistake(stake: string): Promise<void> {
        if (stake != "") {
            var position = 0;
            FeatureTestBase.browser.sleep(1000);
            await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase.browser.sleep(100);               
                await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
            }
        }
    }

    @then(/verify the progress bar is still displayed for 5sec/)
    public async verifyProgressbar5sec2(): Promise<void> {
        FeatureTestBase.browser.sleep(2000);
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
        chai.assert.equal(progressbar, null);

        FeatureTestBase.browser.sleep(5000);
        var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
        chai.assert.equal((progressbarvalue > 0), true);
        var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
        //chai.assert.equal(progressbar, 'true');
    }

    @then(/verify delete button is not visible near the stake box while the progress bar is visible/)
    public async verifyDeleteButtonNotVisible(): Promise<void> {        
        var style = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetRemoveButtonPath(0), 'style');
        chai.assert.equal(style, "display: none;");
    }

    @then(/verify user recives a notification toaster "Bet placed. Receipt #"/)
    public async verifytoast1betplaced1betFailed(): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(betTicketText, "Bet placed. Receipt #A12345");
    }
    @then(/verify user recives a notification toaster "2 bets placed."/)
    public async verifytoast2betplaced(): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(betTicketText, "2 bets placed.");
    }
    
 
}

export = BetDelay;
