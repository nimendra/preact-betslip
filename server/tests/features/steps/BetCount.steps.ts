﻿import { binding, given, then, before, after } from "cucumber-tsflow";
import { MyElementHandler } from "../utils/MyElementHandler";
import * as webdriver from "selenium-webdriver";
const chai = require('chai');
import { FeatureTestBase } from "../utils/FeatureTestBase";
import * as protractor from "protractor";
@binding()
class BetCount extends FeatureTestBase {     

    // @then(/the user adds required number of markets (.*)by clicking different games/)
    // @then(/the user adds required number of markets (.*) by clicking different games/)
    // @given(/the user adds required number of markets by clicking different games$/)
    // public async addBetstobetslip(markets: string): Promise<void> {
    //     FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);   
    //     if (markets != "") {
    //         var parsedMarkets = parseInt(markets);  
    //         FeatureTestBase.browser.sleep(1000);
    //         var eventids = ["89403", "89165", "89153"];
    //         var outcomeids = ["1", "1", "1"];
    //         for (var i = 0; i < parsedMarkets; i++) {                
    //              console.log("*************click on " + eventids[i] + "_" + outcomeids[i]);
    //              await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(eventids[i], outcomeids[i]));
    //         }
    //     }        
    // }   

    @then(/verify the count on the betslip counter is (.*)/)
    public async checkbetcount(count: string): Promise<void> {
        if (count != "") {
            FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);   
            FeatureTestBase.browser.sleep(1000);
            var betcount = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getBetCount());
            chai.assert.equal(betcount, count);
        }
    }

   @then(/the user clicks on the beslip/)
    public async clickonbetslipbutton(): Promise<void> {
        
    }

    @then(/the user removes the badminton game/)
    public async removeFirstbet(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSinglebetRemoveButtonPath(0));
        FeatureTestBase.browser.sleep(500);
    }

    @then(/the user refreshes the page/)
    public async refreshPage(): Promise<void> {
        await FeatureTestBase.browser.driver.navigate().refresh();       
    }
   
    @then(/the user clicks on Clear All button/)
    public async clickClearAll(): Promise<void> {
         await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getRemoveAllCount());
    }

    @then(/the user closes the betslip/)
    public async closebetslip(): Promise<void> {
        
    }
    @then(/user enters 1 in first market stake box in the betslip/)
    public async entersFirstStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
       await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(0), '1');
    }

    @then(/user enters 1 in second market stake box in the betslip/)
    public async entersSecondStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(1), '1');
    }
}
export = BetCount;
