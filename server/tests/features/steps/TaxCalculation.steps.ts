﻿import { binding, given, then, before, after } from "cucumber-tsflow";
import { MyElementHandler } from "../utils/MyElementHandler";
import * as webdriver from "selenium-webdriver";
const chai = require('chai');
import { FeatureTestBase } from "../utils/FeatureTestBase";

@binding()
class TaxCalculation extends FeatureTestBase {  

    @then(/user clicks on the markets for odds(.*)/)
    public async addBets(singleodds: string): Promise<void> {
        if (singleodds != "") {
            var parsedMarkets = singleodds.split(",");
            FeatureTestBase.browser.sleep(1000);
            var oddslist = [2.5, 500, 5, 100, 2, 5, 2, 5, 1, 2500, 5, 3, 1.5, 2, 1.80];
            var eventids = ["123448", "123456", "89383", "123455", "89379", "89375", "123454", "123453", "123452", "123451", "123451", "123450", "123450", "123449", "89395"];
            var outcomeids = ["1", "2", "1", "1", "1", "2", "1", "1", "1", "1", "2", "1", "2", "1", "1"];
            for (var i = 0; i < parsedMarkets.length; i++) {
                console.log("=======price====", parsedMarkets[i]);
                var parsedOdds = parseFloat(parsedMarkets[i]);
                var index = oddslist.indexOf(parsedOdds);
                console.log(parsedOdds + "-======" + index);

                if (index > -1) {
                    console.log("*************click on " + eventids[index] + "_" + outcomeids[index]);
                    await FeatureTestBase.myElementHandler.getElementAndScrollThenClick(FeatureTestBase.myElementHandler.getSportsButtonPath(eventids[index], outcomeids[index]));
                    oddslist.splice(index, 1);
                    eventids.splice(index, 1);
                    outcomeids.splice(index, 1);
                    FeatureTestBase.browser.sleep(1000);
                }
            }
        }
    }

    @then(/verify the est return below the stake box is updated to (.*)/)
    public async verifyPayoutUpdate(estreturn: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var payout = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetPayoutPath(0));
        chai.assert.equal(payout, estreturn == "" ? "" : ("Return: €" + estreturn));
    }

    @then(/verify the tax value below the stake box is updated to (.*)/)
    public async verifyTaxUpdate(esttax: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var tax = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetTaxPath(0));

        chai.assert.equal(tax, esttax == "" ? "" : ("Tax: €" + esttax));
    }

    @then(/verify the est return below the multi stake box is updated to (.*)/)
    public async verifyMultiPayoutUpdate(estreturn: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var payout = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesPayoutPath(0));
        chai.assert.equal(payout, estreturn == "" ? "" : ("Return: €" + estreturn));
    }

    //@then(/verify the est return below the five folds stake box for (.*)markets is updated to (.*)/)
    //public async verifyMultiplePayoutUpdate(markets: string, exoticestreturn: string): Promise<void> {
    //    FeatureTestBase.browser.sleep(500);
    //    var position = parseInt(markets) == 5 ? 0 : 4;
    //    var payout = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesPayoutPath(position));
    //    chai.assert.equal(payout, exoticestreturn == "" ? "" : ("Return: €" + exoticestreturn));
    //}

    //@then(/verify the tax value below the five folds stake box for (.*)markets is updated to (.*)/)
    //public async verifyMultipleTaxUpdate(markets: string, exotictaxvalue: string): Promise<void> {
    //    FeatureTestBase.browser.sleep(500);
    //    var position = parseInt(markets) == 5 ? 0 : 4;
    //    var tax = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesTaxPath(position));
    //    chai.assert.equal(tax, exotictaxvalue == "" ? "" : ("Return: €" + exotictaxvalue));
    //}

    @then(/verify the tax value below the multi stake box is updated to (.*)/)
    public async verifyMultiTaxUpdate(esttax: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var tax = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesTaxPath(0));
        chai.assert.equal(tax, esttax == "" ? "" : ("Tax: €" + esttax));
    }

    @then(/verify the total est return value is updated for the betstlip to (.*)/)
    public async verifyTotalReturnAmount(betslipestreturn: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var estimatedReturn = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalPayoutPath());
        chai.assert.equal(estimatedReturn, betslipestreturn == "" ? " " : (" €" + betslipestreturn));
    }

    @then(/verify the total tax value is updated for the betstlip to (.*)/)
    public async verifyTotalTaxAmount(betsliptaxvalue: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var estimatedTax = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalTaxPath());
        chai.assert.equal(estimatedTax, betsliptaxvalue == "" ? " " : (" €" + betsliptaxvalue));
    }
    @then(/verify the total tax value is not shown on the betstlip/)
    public async verifyTotalTaxNotShown(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var estimatedTax = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalTaxPath());
        chai.assert.equal(estimatedTax, " ");
    }   

    @then(/user sees the expected stake(.*) in the stake box/)
    public async expectedStake(expectedstake:string): Promise<void> { 
        FeatureTestBase.browser.sleep(6000);
        var baseballBetStake = await FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
        console.log("======1======", baseballBetStake);

        var stake = await FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase.myElementHandler.getSinglebetStakeValuePath(0));
        console.log("======2======", stake);
        chai.assert.equal(stake, expectedstake);
    }

    @then(/the user sees the notification toaster with text "Stake rounded for some selections" for (.*)/)
    public async verifyToasterWith2BetsFailedNotification(stake:string): Promise<void> {
       if((parseFloat(stake) * 100) % 100 == 0){
          console.log("no toast check ....");
          await this.verifyToastMessage("", true);
       } else{
          await this.verifyToastMessage("Stake rounded for some selections", true);
       }
    }

    private async verifyToastMessage(message: string, exactMatch: boolean) {
        FeatureTestBase.browser.sleep(500);
        var toastMessage = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        if (exactMatch) {
            chai.assert.equal(toastMessage, message);
        } else {
            chai.assert.include(toastMessage, message);
        }
    }
}

export = TaxCalculation;
