﻿import { binding, given, then, before, after } from "cucumber-tsflow";
import { MyElementHandler } from "../utils/MyElementHandler";
import * as webdriver from "selenium-webdriver";
const chai = require('chai');
import { FeatureTestBase } from "../utils/FeatureTestBase";

@binding()
class SuspendBet extends FeatureTestBase {

    protected betslipButtonXPath: string = "//paper-icon-button[@id='bet_btn']";
    protected clearAllBetsButtonXPath: string = "//*[@id='scroller_area']/section/div[1]/paper-button";
    protected closeBetslipPageButtonXPath: string = "//paper-icon-button[@id='betslip-close']";
    protected homePageToastBetTicketLabelXPath: string = "//*[@id=\"label\"]";
    protected betslipPageToastXPath: string = "//*[@id=\"responseToast\"]"
    protected otherMultiplesCollapseItemXPath: string = "//*[@id=\"system_bets_paper_collapse\"]/paper-item/paper-item-body/div/div";
    protected applyAllStakeInputXPath: string = "//*[@id=\"stake_applyall\"]/paper-input-container/div/div/input";

    /// Start - Click on markets




    /// End - Click on markets

    /// Start - Betslip handling
    
    @then(/user recieves a bet update for the badminton game as suspended/)
    public async recieveBetSuspendedSignal(): Promise<void> {
        var othermultiples = await FeatureTestBase.myElementHandler.getElement(FeatureTestBase.myElementHandler.getOtherMultiplesPath());
        if (othermultiples) {
            await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getOtherMultiplesPath());
        }

        var signal = {
            selectionId: "89403_1",
            price: {
                decimal: 1.57
            },
            suspended: true
        };

        await FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");

    }

    @then(/user recieves a bet update for the badminton game odd 500 as suspended/)
    public async recieveBetSuspendedSignalfor500(): Promise<void> {
        var signal = {
            selectionId: "123456_2",
            price: {
                decimal: 500.00
            },
            suspended: true
        };

        await FeatureTestBase.myElementHandler.fireIronSignal(signal, "selection-update");
    }

    @then(/user enters the stake value in the badminton game box for £(.*)/)
    @then(/user enters the stake amount(.*) for badminton game/)
    public async enterBadmintonStake(stake: string): Promise<void> {
        if (stake != "") {
            await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase.browser.sleep(100);
                await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
            }
        }
     }

    @then(/user enters the stake value in the cricket game box for £(.*)/)
    @then(/user enters the stake amount(.*) for cricket game/)
    public async enterCricketStake(stake: string): Promise<void> {
        if (stake != "") {
            await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getSinglebetStakePath(1));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase.browser.sleep(100);
                await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(1), stake[i]);
            }
        }
    }

    @then(/user enters the stake amount(.*) for baseball game/)
    public async enterBaseballStake(stake: string): Promise<void> {
        if (stake != "") {
            await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getSinglebetStakePath(2));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase.browser.sleep(100);
                await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(2), stake[i]);
            }
        }
    }

    @then(/user removes the suspended bet/)
    public async removeSuspendedBet(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSinglebetRemoveButtonPath(0));
        FeatureTestBase.browser.sleep(500);
    }

    @then(/user receives a suspend error for the cricket game/)
    public async receivePriceUpdateForCricket(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
    }

    /// End - Betslip handling

    /// Start - Verifications  
    @then(/verify stake box is disabled on the betslip/)
    @then(/verify stake box for the badminton game is disabled on the betslip/)
    public async verifyBetStakeInputIsDisabled(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetCardPath(0), 'disabled');
        chai.assert.equal(disabled, 'true');
    }

    @then(/verify bet button is disabled/)
    public async verifyBetButtonDisabled(): Promise<void> {
        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getBetButtonPath(), 'disabled');
        chai.assert.equal(disabled, 'true');
    }

    @then(/verify the notification toaster appear with text "1 bet suspended"/)
    public async verifyBetSupspendedToastAppears(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var toastText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(toastText, "1 bet suspended");
    }

    @then(/verify the stake box for the multi is disabled/)
    public async verifyMultiStakeInputDisabled(): Promise<void> {
       // FeatureTestBase.browser.sleep(20);
        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getMultiplesCardPath(0), 'disabled');
        chai.assert.equal(disabled, 'true');
    }

    @then(/verify apply all single box is disabled/)
    public async verifyApplyAllStakeInputDisabled(): Promise<void> {
        //await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getOtherMultiplesPath());
        //FeatureTestBase.browser.sleep(1000);

        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(
            FeatureTestBase.myElementHandler.getApplyAllCardPath(), 'disabled');
        chai.assert.equal(disabled, 'true');
    }

    @then(/verify bet button is enabled for placing the single bet for cricket game./)
    public async verifySecondBetStakeInputEnabled(): Promise<void> {
        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetCardPath(1), 'disabled');
        chai.assert.isNull(disabled);        
    }

    @then(/verify the total stake(.*) is correct/)
    public async verifyTotalStake(expectedTotalStake: string): Promise<void> {
        var actualTotalStake = await FeatureTestBase.myElementHandler.getElementAndGetText(
            FeatureTestBase.myElementHandler.getTotalStakePath());
        chai.assert.equal(actualTotalStake, " €" + expectedTotalStake);
    }

    @then(/verify the estimate return(.*) is correct/)
    public async verifyEstimatedReturn(expectedReturn: string): Promise<void> {
        var actualReturn = await FeatureTestBase.myElementHandler.getElementAndGetText(
            FeatureTestBase.myElementHandler.getTotalPayoutPath());
        chai.assert.equal(actualReturn, " €" + expectedReturn);
    }

    @then(/verify the bet count is 1/)
    public async verifyBetCount(): Promise<void> {
        var betCount = await FeatureTestBase.myElementHandler.getElementAndGetText(
            FeatureTestBase.myElementHandler.getTotalBetCountPath());
        chai.assert.equal(betCount, "(1)");
    }

    public async verifyNLegMultiVisible(legCount: number): Promise<void> {
        //FeatureTestBase.browser.sleep(1000);
        var multiName = await FeatureTestBase.myElementHandler.getElementAndGetText(
            FeatureTestBase.myElementHandler.getMultiplesName(legCount -2));
        var expectedMultiName = ["Double", "Treble"];
        chai.assert.equal(multiName, expectedMultiName[legCount - 2]);
    }

    @then(/verify 2 leg multi is suggested/)
    public async verify2LegMultiVisible(): Promise<void> {
        this.verifyNLegMultiVisible(2);
    }

    @then(/verify 3 leg multi is suggested/)
    public async verify3LegMultiVisible(): Promise<void> {
        this.verifyNLegMultiVisible(3);
    }

    @then(/user receives notification toaster with text "2 bets placed./)
    public async verifyBetsPlacedToastAppears(): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var toastBetTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(
            FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(toastBetTicketText, "2 bets placed.");
    }

    @then(/verify multi stake box shows an error message as "Suspended"/)
    public async verifyMultiBetSuspendedError(): Promise<void> {        
        var error = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesErrorMessagePath(0));
        chai.assert.equal(error, "Suspended");
    }

    @then(/verify single stake box shows an error message as "Suspended"/)
    public async verifySingleBetSuspendedError(): Promise<void> {
        var error = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetSuspendedErrorMessagePath(1));
        chai.assert.equal(error, "Suspended");
    }


    /// End - Verifications0
}

export = SuspendBet;
