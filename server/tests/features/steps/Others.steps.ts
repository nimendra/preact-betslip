﻿import { binding, given, then, before, after } from "cucumber-tsflow";
import { MyElementHandler } from "../utils/MyElementHandler";
import * as webdriver from "selenium-webdriver";
const chai = require('chai');
import { FeatureTestBase } from "../utils/FeatureTestBase";

@binding()
class Others extends FeatureTestBase {  
    @then(/user enters 4010 in first market stake box in the betslip/)
    public async enters4010FirstStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var stake = "4010";
        for (var i = 0; i < stake.length; i++) {
            FeatureTestBase.browser.sleep(100);
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
        } 
    }

    @then(/user enters 4000 in first market stake box in the betslip/)
    public async enters4000FirstStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var stake = "4000";
        for (var i = 0; i < stake.length; i++) {
            FeatureTestBase.browser.sleep(100);
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
        } 
    }

    @then(/user enters 8000 in first market stake box in the betslip/)
    public async enters8000FirstStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var stake = "8000";
        for (var i = 0; i < stake.length; i++) {
            FeatureTestBase.browser.sleep(100);
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
        } 
    }

    
    @then(/user enters 8000 in second market stake box in the betslip/)
    public async enters8000SecondStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var stake = "8000";
        for (var i = 0; i < stake.length; i++) {
            FeatureTestBase.browser.sleep(100);
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(1), stake[i]);
        } 
    }

    
    @then(/user enters 8000 in third market stake box in the betslip/)
    public async enters8000ThirdStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var stake = "8000";
        for (var i = 0; i < stake.length; i++) {
            FeatureTestBase.browser.sleep(100);
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(2), stake[i]);
        } 
    }
    @then(/user enters 8000 in forth market stake box in the betslip/)
    public async enters8000ForthStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var stake = "8000";
        for (var i = 0; i < stake.length; i++) {
            FeatureTestBase.browser.sleep(100);
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(3), stake[i]);
        } 
    }
    @then(/verify bet button is enabled/)
    public async verifyBetButtonEnabled(): Promise<void> {
        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getBetButtonPath(), 'disabled');
        chai.assert.equal(disabled, null);
    }

    @then(/verify the notification toaster appear with text "Server or Network error: 401Unauthorized"/)
    public async verifyBetSupspendedToastAppears(): Promise<void> {
        FeatureTestBase.browser.sleep(1300);
        var toastText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(toastText, "Server or Network error: 401Unauthorized");
    }
    @then(/verify the notification toaster appear with text "Server or Network error: 500Internal Server Error"/)
    public async verifyBetInterceptErrorToastAppears(): Promise<void> {
        FeatureTestBase.browser.sleep(1300);
        var toastText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(toastText, "Server or Network error: 500Internal Server Error");
    }
    
    @then(/after few seconds/)
    public async afterfewSecond(): Promise<void> {
        FeatureTestBase.browser.sleep(4000);
        var toastText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(toastText, "");   
    }

    @then(/after 1 seconds/)
    public async after1Second(): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var toastText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        
    }

    @then(/verify user recives a notification toaster "Bet placed. Receipt #17W5EPF6"/)
    public async verifybetPlaced(): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(betTicketText, "Bet placed. Receipt #17W5EPF6");
    }

    @then(/verify stake box is enabled on the betslip/)
    public async verifyBetStakeInputIsEnabled(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetCardPath(0), 'disabled');
        chai.assert.equal(disabled, null);
    }

    @then(/verify second stake box is enabled on the betslip/)
    public async verifyBetSecondStakeInputIsEnabled(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetCardPath(1), 'disabled');
        chai.assert.equal(disabled, null);
    }

    @then(/verify second stake box is disabled on the betslip/)
    public async verifyBetSecondStakeInputIsDisabled(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetCardPath(1), 'disabled');
        chai.assert.equal(disabled, 'true');
    }

    @then(/verify third stake box is enabled on the betslip/)
    public async verifyBetThirdStakeInputIsEnabled(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var disabled = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetCardPath(2), 'disabled');
        chai.assert.equal(disabled, null);
    }

    @then(/user clicks on the second price market for the in play soccer game/)
    public async addSecondBet(): Promise<void> {
        await FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase.myElementHandler.getSportsButtonPath(89157, 2));
    }

    @then(/user clicks on the third price market for the in play soccer game/)
    public async addThirdBet(): Promise<void> {
        await FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase.myElementHandler.getSportsButtonPath(89157, 3));
    }   

    @then(/verify the error message for the first bet shows "Intercepted -> Rejected by Admin"/)
    public async verifyMultiConflictsShown(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);
        var errorMessage = "Intercepted -> Rejected by Admin";
        var bet1ErrorMessage = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(0));
        chai.assert.equal(bet1ErrorMessage, errorMessage);
    }
}

export = Others;
