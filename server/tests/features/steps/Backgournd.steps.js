"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const cucumber_tsflow_1 = require("cucumber-tsflow");
const MyElementHandler_1 = require("../utils/MyElementHandler");
const FeatureTestBase_1 = require("../utils/FeatureTestBase");
const webdriver = require("selenium-webdriver");
var path = require('chromedriver').path;
const chai = require('chai');
const protractor = require("protractor");
let Background = class Background extends FeatureTestBase_1.FeatureTestBase {
    constructor() {
        super(...arguments);
        this.betslipButtonXPath = "//paper-icon-button[@id='bet_btn']";
        this.clearAllBetsButtonXPath = "//*[@id='scroller_area']/section/div[1]/paper-button";
        this.closeBetslipPageButtonXPath = "//paper-icon-button[@id='betslip-close']";
        /// End - Background
    }
    beforeScenario() {
    }
    afterScenario(scenario) {
        return __awaiter(this, void 0, void 0, function* () {
            if (scenario.isFailed()) {
                yield FeatureTestBase_1.FeatureTestBase.browser.driver.takeScreenshot().then(function (screenShot) {
                    // screenShot is a base-64 encoded PNG
                    scenario.attach(new Buffer(screenShot, 'base64'), 'image/png');
                    scenario.attach('This is some text that also goes into the report');
                });
            }
            yield FeatureTestBase_1.FeatureTestBase.browser.quit();
        });
    }
    /// Start - Background
    navigateToWebsite() {
        return __awaiter(this, void 0, void 0, function* () {
            var t = this;
            var f = FeatureTestBase_1.FeatureTestBase;
            FeatureTestBase_1.FeatureTestBase.driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();
            FeatureTestBase_1.FeatureTestBase.browser = new protractor.ProtractorBrowser(FeatureTestBase_1.FeatureTestBase.driver);
            return yield FeatureTestBase_1.FeatureTestBase.browser.driver.get(this.testUrl(this.port, this.dom)).then(function () {
                FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(4000); // 4000 min for PhantomJS            
                FeatureTestBase_1.FeatureTestBase.browser.manage().window().setSize(1480, 1024); //maximize() not working on gitlab
                return FeatureTestBase_1.FeatureTestBase.browser.driver.findElement(webdriver.By.tagName('body')).then(function (element) {
                    FeatureTestBase_1.FeatureTestBase.myElementHandler = new MyElementHandler_1.MyElementHandler(element, f.browser.driver, (t.browsertype == "chrome" && t.dom == "shadow"));
                });
            });
        });
    }
    navigateToBetslipPage() {
        return __awaiter(this, void 0, void 0, function* () {
            //await FeatureTestBase.myElementHandler.getElementAndClick('document.querySelector("betslip-button").shadowRoot.querySelector("#bet_btn")');
        });
    }
    noBetsInBetslip() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            var noBetsLabelXPath = "/html/body/neon-animated-pages/betslip-container/div/iron-pages/betslip-content/div[1]/section/empty-state/p";
            var noBetsLabel;
            try {
                noBetsLabel = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.findElementByXpath(noBetsLabelXPath).getText();
                chai.assert.equal(noBetsLabel, "You have no bets");
            }
            catch (e) {
                // Threr are bets in betslip
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.clickWhenElementExist('Xpath', this.clearAllBetsButtonXPath);
            }
        });
    }
    navigateToSportsPage() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            // await FeatureTestBase.myElementHandler.getElementAndClick('document.querySelector("betslip-container").shadowRoot.querySelector("#betslip-close")');    
        });
    }
};
__decorate([
    cucumber_tsflow_1.before()
], Background.prototype, "beforeScenario", null);
__decorate([
    cucumber_tsflow_1.after()
], Background.prototype, "afterScenario", null);
__decorate([
    cucumber_tsflow_1.given(/user navigates to the sportsbook website/)
], Background.prototype, "navigateToWebsite", null);
__decorate([
    cucumber_tsflow_1.then(/user navigates to the betslip page/)
], Background.prototype, "navigateToBetslipPage", null);
__decorate([
    cucumber_tsflow_1.then(/clears the betslip page if any bets exists/)
], Background.prototype, "noBetsInBetslip", null);
__decorate([
    cucumber_tsflow_1.then(/user navigates to the sports page/)
], Background.prototype, "navigateToSportsPage", null);
Background = __decorate([
    cucumber_tsflow_1.binding()
], Background);
module.exports = Background;
