﻿import { binding, given, then, before, after } from "cucumber-tsflow";
import { MyElementHandler } from "../utils/MyElementHandler";
import * as webdriver from "selenium-webdriver";
const chai = require('chai');
import { FeatureTestBase } from "../utils/FeatureTestBase";

@binding()
class MultiBet extends FeatureTestBase {

    protected betslipButtonXPath: string = "//paper-icon-button[@id='bet_btn']";
    protected clearAllBetsButtonXPath: string = "//*[@id='scroller_area']/section/div[1]/button";
    protected closeBetslipPageButtonXPath: string = "//paper-icon-button[@id='betslip-close']";
    protected otherMultiplesCollapseItemXPath: string = "//*[@id=\"system_bets_paper_collapse\"]/paper-item/paper-item-body/div/div";
    //protected applyAllStakeInputXPath:string = "//*[@id=\"stake_applyall\"]/paper-input-container/div/div/input";

    /// Start - Click on markets
    @given(/user clicks on the first price market for badminton game for price 1.57/)
    @given(/user clicks on the first price market for badminton game$/)
    public async addFirstBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);   
        FeatureTestBase.browser.sleep(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89403,1));
    }

    @then(/user clicks on the first price market for cricket game for price 2.50/)
    @then(/user clicks on the first price market for cricket game$/)     
    public async addSecondBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);   
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89165, 1));
    }

    @then(/user clicks on the first price market for cricket game for side A to win/)
    public async addsideABet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);   
        await FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase.myElementHandler.getSportsButtonPath(89165, 1));
    }

    @then(/user clicks on the first price market for baseball game/)
    public async addThirdBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        var betButtonXPath = "//button[@id='bet_btn_89153_1']";
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89153, 1));
    }

    @then(/user clicks on the first price market for american football game/)
    @then(/ user clicks on the first price market for badminton game for price 1.40/)
    public async addFourthBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89157, 1));
    }

    @then(/user clicks on the first price market for aussie rules game/)
    public async addFifthBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89161, 1));
    }

    @then(/user clicks on the first price market for basketball game/)
    public async addSixthBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89399, 1));
    }

    @then(/user clicks on the first price market for beach volleyball game/)
    public async addSeventhBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89407, 1));
    }

    @then(/user clicks on the first price market for darts game/)
    public async addEighthBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89523, 1));
    }

    @then(/user clicks on the first price market for esports game/)
    public async addNinethBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89527, 1));
    }

    @then(/user clicks on the first price market for football game$/)
    public async addTenthBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89511, 1));
    }

    @then(/user clicks on the first price market for football game for side A to win/)
    public async addTenthsideABet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase.myElementHandler.getSportsButtonPath(89153, 1));
    }

    @then(/user clicks on the first price market for handball game/)
    public async addEleventBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89519, 1));
    }

    @then(/user clicks on the first price market for volleyball game/)
    public async addTwelvethBet(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89375, 1));
    }

    @then(/user clicks on the first price market for cricket game for side B to win/)
    public async clickOnCricketSideA(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(89165, 3));
        //var cricketSideBBetButtonXPath = "//button[@id='bet_btn_89165_3']";
        //var sideBStake = await FeatureTestBase.myElementHandler.findElementByXpath(cricketSideBBetButtonXPath).getText();
        //chai.assert.equal(sideBStake, "2.88");
        //await FeatureTestBase.myElementHandler.findElementByXpath(cricketSideBBetButtonXPath).click();
    }

    /// End - Click on markets

    /// Start - Betslip handling

    @then(/clicks on the Betslip icon/)
    public async navigateBetslipPage(): Promise<void> {
        // await FeatureTestBase.myElementHandler.getElementAndClick('document.querySelector("betslip-button").shadowRoot.querySelector("#bet_btn")');
    }

    @then(/user enters the stake value in the (.*)leg multi box for £(.*)/)
    public async enterMultiStake(legCount: string, stake: string): Promise<void> {
        if (stake != "") {
            var position = 0;
            FeatureTestBase.browser.sleep(1000);
            await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase.browser.sleep(100);
               // (parseInt(legCount) == 3 ? 0 : 2);
                await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
            }
        }
    } 

    @then(/user clicks on Bet button/)
    public async clickSubmitButton(): Promise<void> {
        //FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        // Leave enough time for any toaster to disappear from previous step, if any.
        FeatureTestBase.browser.sleep(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getBetButtonPath());
    }

    @then(/user click on the multiples collapes button/)
    public async clickMultipleCollapesbutton(stake: string): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getOtherMultiplesPath());
        FeatureTestBase.browser.sleep(1000);
    }

    @then(/user enters the stake value in apply all single box for £(.*)/)
    public async enterApplyAllStake10(stake: string): Promise<void> {
        var parsedStake = parseFloat(stake);
        FeatureTestBase.browser.sleep(1200);
        //await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.stackShadows(shadows) + '.querySelector("#stake_applyall")');
        //TODO:I know, it's code smell.. but it's because bloody paper-input doesnt respond to "clear" instead you have to get to the inner input to clear... :( 
        await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getApplyAllStakePath(), stake);
        FeatureTestBase.browser.sleep(10000);

        //await FeatureTestBase.myElementHandler.findElementByXpath(this.applyAllStakeInputXPath).clear();
        //await FeatureTestBase.myElementHandler.findElementByXpath(this.applyAllStakeInputXPath).sendKeys(parsedStake);
    }

    @then(/user enters the stake value in the badminton stake box for £1/)
    public async enterBadmintonStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(0), '1');
    }

    @then(/user enters the stake value in the cricket stake box for £1/)
    public async enterCricketStake(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(1), '1');
    }

    @then(/user enters the stake value in the baseball stake box for £-1/)
    public async enterBaseballStake(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(2), '-1');
    }

    @then(/user enters the stake value in the american football stake box for £-1/)
    public async enterAmericanFootballInvalidStake(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(3), '-1');
    }

    @then(/the user clicks on Keep Bets button/)
    public async clickOnKeepBets(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        var keepBetsText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastButtonPath());
        chai.assert.equal(keepBetsText, "KEEP BETS");
        await FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase.myElementHandler.getToastButtonPath());
    }

    @then(/clicks on the Other Multiples drop down/)
    public async expandOtherMultiples(): Promise<void> {
        FeatureTestBase.browser.sleep(1000);
        var othermultiples = await FeatureTestBase.myElementHandler.getElement(FeatureTestBase.myElementHandler.getOtherMultiplesPath());
        if (othermultiples) {            
            await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getOtherMultiplesPath());
        }      
        FeatureTestBase.browser.sleep(1000);
    }

    @then(/no error messages are shown/)
    public async verifyNoErrorMessagesAreShown(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);
        var multiConflictErrorMessage = "Multi Conflict";
        var isBet1ErrorMessageDisplayed = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(0), "hidden");
        var isBet2ErrorMessageDisplayed = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(1), "hidden");
        chai.assert.equal(isBet1ErrorMessageDisplayed, null);
        chai.assert.equal(isBet2ErrorMessageDisplayed, null);
    }

    @then(/the user adds required number of markets (.*) by clicking different games/)
    public async addReguriedNumberOfMarkest(markets: string): Promise<void> {
        var parsedMarkets = parseInt(markets);
        var eventids = ["89403", "89165", "89153", "89157", "89161", "89399", "89407", "89523", "89527", "89511", "89519", "89375", "89379", "89383", "89395", "123456"];
        FeatureTestBase.browser.sleep(1000);
        for (var i = 0; i < parsedMarkets; i++) {
            //add markets
            FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(eventids[i], 1));
        }
    }

    @then(/user enters single stake (.*) for (.*) markets/)
    public async userEnterMultiStake(Singlestake: string, markets: string): Promise<void> {
        if (Singlestake != "") {
            var parsedSinglestake = parseInt(Singlestake);
            var parsedMarkets = parseInt(markets);
            FeatureTestBase.browser.sleep(500);
            await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getSinglebetStakePath(0), parsedSinglestake);
        }
    }

    @then(/user enters double stake (.*) for (.*) markets/)
    public async userEnterDoubleStake(stake: string, markets: string): Promise<void> {
        if (stake != "") {
            var parsedDoublestake = parseInt(stake);
            var parsedMarkets = parseInt(markets);
            FeatureTestBase.browser.sleep(500);
            var position = (parsedMarkets == 2 ? 0 : 1);
            await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase.browser.sleep(100);
                await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
            } 
        }
    }

    @then(/user sees the expected double stake (.*) for (.*) markets/)
    public async doubleStakeExpectedstake(Doublestake: string, markets: string): Promise<void> {
        FeatureTestBase.browser.sleep(6000);
        if (Doublestake != "") {
            var parsedDoublestake = parseInt(Doublestake);
            var parsedMarkets = parseInt(markets);
            FeatureTestBase.browser.sleep(500);
            var position = (parsedMarkets == 2 ? 0 : 1);
            var expectedstake = await FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
            chai.assert.equal(expectedstake, Doublestake);
        }
    }

    @then(/user click on the multiples collapes for (.*) markets/)
    public async clickonmulitpesCollapes(markets: string): Promise<void> {
        var parsedMarkets = parseInt(markets);
        if (parsedMarkets > 1) {
            FeatureTestBase.browser.sleep(1000);
            FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
            await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getOtherMultiplesPath());
            FeatureTestBase.browser.sleep(1000);
        }
    }

    @then(/user enters treble stake (.*) for (.*) markets/)
    public async userEnterTreblestake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            FeatureTestBase.browser.sleep(500);
            var position = (parsedMarkets == 3 ? 0 : 2);
            await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase.browser.sleep(100);
                await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
            } 
        }
    }

    @then(/user enters trixie stake (.*) for (.*) markets/)
    public async userEnterTrixiestake(stake: string, markets: string): Promise<void> {
        if (stake != "") {             
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 3) {
                FeatureTestBase.browser.sleep(500);
                var position = 2;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                } 
            }
        }
    }

    @then(/user sees the expected trixie stake (.*) for (.*) markets/)
    public async trixieStakeExpectedstake(stake: string, markets: string): Promise<void> {
        FeatureTestBase.browser.sleep(5000);
        if (stake != "") {
            var parsedDoublestake = parseInt(stake);
            var parsedMarkets = parseInt(markets);
            FeatureTestBase.browser.sleep(500);
            var position = 2;
            var expectedstake = await FeatureTestBase.myElementHandler.getElementAndGetValue(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
            chai.assert.equal(expectedstake, stake);

        }
    }


    @then(/user enters patent stake (.*) for (.*) markets/)
    public async userEnterPatentstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {           
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 3) {
                FeatureTestBase.browser.sleep(500);
                var position = 3;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                } 
            }
        }
    }

    @then(/user enters yankee stake (.*) for (.*) markets/)
    public async userEnterYankeestake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 4) {
                FeatureTestBase.browser.sleep(500);
                var position = 3;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                } 
            }
        }
    }

    @then(/user enters lucky15 stake (.*) for (.*) markets/)
    public async userEnterLucky15stake(stake: string, markets: string): Promise<void> {
        if (stake != "") {           
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 4) {
                FeatureTestBase.browser.sleep(500);
                var position = 4;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                } 
            }
        }
    }

    @then(/user enters super yankee stake (.*) for (.*) markets/)
    public async userEnterSuperYankeestake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 5) {
                FeatureTestBase.browser.sleep(500);
                var position = 4;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                } 
            }
        }
    }

    @then(/user enters lucky31 stake (.*) for (.*) markets/)
    public async userEnterLucky31stake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 5) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = 5;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                } 
            }
        }
    }

    @then(/user enters heinz stake (.*) for (.*) markets/)
    public async userEnterHeinzstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 6) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = 5;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                } 
            }
        }
    }

    @then(/user enters super heinz stake (.*) for (.*) markets/)
    public async userEnterSuperHeinzstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 7) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = 6;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }  
            }
        }
    }

    @then(/user enters goliath stake (.*) for (.*) markets/)
    public async userEnterGoliathstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 8) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = 7;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }  
            }
        }
    }

    @then(/user enters lucky63 stake (.*) for (.*) markets/)
    public async userEnterLucky63stake(stake: string, markets: string): Promise<void> {
        if (stake != "") {           
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets == 6) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = 6;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }  
            }
        }
    }
    //private async enterMultipleStake(stake: string, position: number): Promise<void> {
    //    FeatureTestBase.browser.sleep(1000);
    //    if (stake != "") {

    //        var baseballBetEventName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetEventNamePath(0));

    //        for (var i = 0; i < stake.length; i++) {
    //            FeatureTestBase.browser.sleep(100);
    //            await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
    //        }
    //    }
    //}
    @then(/user enters fourfolds stake (.*) for (.*) markets/)
    public async userEnterFourFoldsstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets >= 4) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = parsedMarkets == 4 ? 0 : 3;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }  
            }
        }
    }

    @then(/user enters five folds stake (.*) for (.*) markets/)
    public async userEnterFiveFoldsstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets >= 5) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = parsedMarkets == 5 ? 0 : 4;               
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);                 
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }
            }
        }
    }

    @then(/user enters six fold stake (.*) for (.*) markets/)
    public async userEnterSixFoldsstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {           
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets >= 6) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = parsedMarkets == 6 ? 0 : 5;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }               
            }
        }
    }

    @then(/user enters seven fold stake (.*) for (.*) markets/)
    public async userEnterSevenFoldsstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {             
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets >= 7) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = parsedMarkets == 7 ? 0 : 6;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }   
            }
        }
    }

    @then(/user enters eight fold stake (.*) for (.*) markets/)
    public async userEnterEightFoldsstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {            
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets >= 8) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = parsedMarkets == 8 ? 0 : 7;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }   
            }
        }
    }

    @then(/user enters nine fold stake (.*) for (.*) markets/)
    public async userEnterNineFoldsstake(stake: string, markets: string): Promise<void> {
        if (stake != "") {             
            var parsedMarkets = parseInt(markets);
            if (parsedMarkets >= 9) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var position = parsedMarkets == 9 ? 0 : 8;
                await FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase.browser.sleep(100);
                    await FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }   
            }
        }
    }

    /// End - Betslip handling

    /// Start - Verifications  
    @then(/verify the straight (.*)leg multi bet is visible on the betslip page/)
    public async verifyNLegMultiVisible(legCount: string): Promise<void> {
        var parsedLegCount = parseInt(legCount);
        FeatureTestBase.browser.sleep(2000);
        var multiName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesName(0));
        var expectedMultiName = ["Double", "Treble", "4 Fold", "5 Fold", "6 Fold", "7 Fold", "8 Fold", "9 Fold", "10 Fold", "11 Fold", "12 Fold"];
        chai.assert.equal(multiName, expectedMultiName[parsedLegCount - 2]);
    }

    @then(/verify the bet placed is visible on the betslip page/)
    @then(/verify the Double bet is visible on the betslip page/)
    @then(/verify Double bet is visible/)
    public async verifyDoubleBetVisible(): Promise<void> {
        this.verifyNLegMultiVisible("2");
    }

    @then(/verify the Treble bet is visible on the betslip page/)
    public async verifyTrebleBetVisible(): Promise<void> {
        this.verifyNLegMultiVisible("3");
    }

    @then(/verify the straight (.*)leg multi bet is placed "Bet placed. Receipt #"/)
    public async verifyNLegMultiBetPlaced(legCount: string): Promise<void> {
        var parsedLegCount = parseInt(legCount);
        FeatureTestBase.browser.sleep(500);

        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(betTicketText, "Bet placed. Receipt #17W5EPF6");
    }

    @then(/verify the all bet are placed for the same £(.*) "3 bets placed"/)
    public async verify3BetsPlacedForSameAmount(stake: string): Promise<void> {
        // ISSUE: Cannot verify all bets are played for same stake
        FeatureTestBase.browser.sleep(500);
        var toastBetTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(toastBetTicketText, "3 bets placed.");
    }

    @then(/verify the total stake value is £2/)
    public async verifyTotalStake(): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var totalStake = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalStakePath());

        chai.assert.equal(totalStake, " €2.00");
        //chai.assert.equal(totalStake, " 2.00");
    }

    @then(/verify the total return value is £4.07/)
    public async verifyReturnAmount(): Promise<void> {
        //FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        FeatureTestBase.browser.sleep(500);
        var estimatedReturn = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalPayoutPath());
        chai.assert.equal(estimatedReturn, " €4.07");
        //chai.assert.equal(estimatedReturn, "4.07");
    }

    @then(/verify the total stake value on the BET button is £2/)
    public async verifyBetButtonAmount(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        var totalStakeOnBetButton = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalStakeOnBetButtonPath());
        chai.assert.equal(totalStakeOnBetButton, "BET €2.00");
    }

    @then(/verify all the valid bets are placed "2 bets placed"/)
    @then(/verify the all valid bets are placed for the same £1 "2 bets placed"/)
    public async verifyAllValidBetsPlaced(): Promise<void> {
        // ISSUE: There is no way to check if the correct bet was placed with toast text
        FeatureTestBase.browser.sleep(1800);
        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.include(betTicketText, "2 bets placed.");
    }

    @then(/verify apply to single box value is 0 or empty/)
    public async verifyApplyAllSingleBoxIsEmpty(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(10000);
        var applyAllValue = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getApplyAllStakePath());
        // FeatureTestBase.browser.sleep(10000);
        chai.assert.equal(applyAllValue, "");
    }

    @then(/verify the apply all singles box is not visible/)
    public async verifyApplyAllSingleBoxNotVisible(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(10000);
        var applyAllelements = await FeatureTestBase.myElementHandler.getElement(FeatureTestBase.myElementHandler.getApplyAllStakePath());
        chai.assert.equal(applyAllelements, null);
    }

    @then(/verify the baseball game bet is visible on the betslip page with stake £0/)
    public async verifyBaseballBetIsRemainingOnBetslip(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        var baseballBetTeamName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetOutcomeNamePath(0));
        var baseballBetEventName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetEventNamePath(0));
        var baseballBetStake = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
        chai.assert.equal(baseballBetTeamName, "Chelsea");
        //chai.assert.equal(baseballBetOutcomeName, "Chelsea v Burnley");
        chai.assert.equal(baseballBetStake, "");
    }

    @then(/verify 2 invalid bets are shown in the betslip page/)
    public async verify2InalidBetsAreShownOnBetslip(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        this.verifyBaseballBetIsRemainingOnBetslip();
        var baseballBetTeamName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetOutcomeNamePath(0));
        var baseballBetEventName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetEventNamePath(0));
        var baseballBetStake = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
        chai.assert.equal(baseballBetTeamName, "Chelsea");
        chai.assert.equal(baseballBetEventName, "Chelsea v Burnley");
        //chai.assert.equal(baseballBetStake, "-1");

        var baseballBetTeamName2 = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetOutcomeNamePath(1));
        var baseballBetEventName2 = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetEventNamePath(1));
        var baseballBetStake2 = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetStakePath(1));
        chai.assert.equal(baseballBetTeamName2, "Crystal Palace");
        chai.assert.equal(baseballBetEventName2, "Crystal Palace v Huddersfield");

    }

    @then(/verify "Multi conflict. Remove conflicted bets" message is shown/)
    public async verifyMultiConflictMessageShown(): Promise<void> {
        FeatureTestBase.browser.sleep(1500);
        var multiConflictMessage = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.include(multiConflictMessage, "Multi conflict. Remove conflicted bets");
    }

    @then(/verify the indivdual selection for multi shows the "Multi Conflict"/)
    public async verifyMultiConflictsShown(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);
        var multiConflictErrorMessage = "Multi Conflict";
        var bet1ErrorMessage = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(0));
        var bet2ErrorMessage = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getSinglebetErrorMessagePath(1));
        chai.assert.equal(bet1ErrorMessage, multiConflictErrorMessage);
        chai.assert.equal(bet2ErrorMessage, multiConflictErrorMessage);
    }

    @then(/verify appropriate (.*) for (.*) markets are shown in the betslip page/)
    public async verifyAppropriateMultis(multis: string, markets: string): Promise<void> {
        if (multis != "") {
            var parsedMultis = multis.split(",");
            var parsedMarkets = parseInt(markets);
            FeatureTestBase.browser.sleep(1000);
            for (var i = 0, j = parsedMarkets + 1; i < parsedMultis.length; i++ , j++) {
                FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
                var multiName = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getMultiplesName(i));
                chai.assert.equal(multiName, parsedMultis[i].trim());
            }
        }
    }

    @then(/verify "Number of Singles Limit Reached" message is shown on the homepage/)
    public async verifySinglesReached(): Promise<void> {
        FeatureTestBase.browser.sleep(2000);
        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        chai.assert.equal(betTicketText, "Number of Singles Limit Reached");
    }

    @then(/verify the stake (.*) is correct/)
    public async verifyStakeCourtCorrect(Count: string): Promise<void> {
        var betscount = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalBetCountPath());
        chai.assert.equal(betscount, "(" + Count.trim() + ")");
    }

    @then(/verify the return value (.*) for each multi is correct/)
    public async verifyReturnValueForEachMulti(Value: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
        var estimatedReturn = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getTotalPayoutPath());
        chai.assert.equal(estimatedReturn, ' €' + Value.trim());
    }

    @then(/verify multi bet is placed "Bet placed. Receipt #" for (.*) bets/)
    public async verifyMultiBetPlaced(Count: string): Promise<void> {
        var parsedCount = parseInt(Count);
        FeatureTestBase.browser.sleep(1000);
        var betTicketText = await FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase.myElementHandler.getToastLablePath());
        if (parsedCount == 1) {
            chai.assert.equal(betTicketText, "Bet placed. Receipt #17W5EPF6");
        }
    }
    @then(/verify user navigates to (.*)/)
    public async navigationToPage(Page: string): Promise<void> {
        FeatureTestBase.browser.sleep(500);
    }

    @then(/verify the bet placed is visible on the betslip page/)
    public async keepbetTest(): Promise<void> {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        var badmintonSingleBetStakeInputXPath = "//*[@id=\"stake_1\"]/paper-input-container/div/div/input";
        await FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).clear();
        await FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).sendKeys(1);

        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        var badmintonSingleBetStakeInputXPath = "//*[@id=\"stake_2\"]/paper-input-container/div/div/input";
        await FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).clear();
        await FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).sendKeys(1);

        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);
        var badmintonSingleBetStakeInputXPath = "//*[@id=\"stake_3\"]/paper-input-container/div/div/input";
        await FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).clear();
        await FeatureTestBase.myElementHandler.findElementByXpath(badmintonSingleBetStakeInputXPath).sendKeys(1);

        var betscountXPath = "//*[@id=\"stake_betcount\"]";
        var betscount = await FeatureTestBase.myElementHandler.findElementByXpath(betscountXPath).getText();
        chai.assert.equal(betscount, "Stake (3)");
    }

    /// End - Verifications
}

export = MultiBet;
