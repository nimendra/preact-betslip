﻿import { binding, given, then, before, after, when } from "cucumber-tsflow";
import { MyElementHandler } from "../utils/MyElementHandler";
import { FeatureTestBase } from "../utils/FeatureTestBase";
import * as webdriver from "selenium-webdriver";
var path = require('chromedriver').path;
const chai = require('chai');
import * as protractor from "protractor";
@binding()
class Background extends FeatureTestBase {
    protected betslipButtonXPath: string = "//paper-icon-button[@id='bet_btn']";
    protected clearAllBetsButtonXPath: string = "//*[@id='scroller_area']/section/div[1]/paper-button";
    protected closeBetslipPageButtonXPath: string = "//paper-icon-button[@id='betslip-close']";   
    @before()
    public beforeScenario(): void {
    }

    @after()
    public async afterScenario(scenario): Promise<void> {
        if (scenario.isFailed()) {
            await FeatureTestBase.browser.driver.takeScreenshot().then(function (screenShot) {
                // screenShot is a base-64 encoded PNG
                scenario.attach(new Buffer(screenShot, 'base64'), 'image/png');
                scenario.attach('This is some text that also goes into the report');
            });
        }
        await FeatureTestBase.browser.quit();
    }

    /// Start - Background
    @given(/user navigates to the sportsbook website/)
    public async navigateToWebsite(): Promise<void> {
        var t = this;
        var f = FeatureTestBase;
        FeatureTestBase.driver = new webdriver.Builder()           
            .forBrowser('chrome')
            //.withCapabilities({
            //    browserName: 'chrome'
            //    ,chromeOptions: {
            //        args: ["--headless", "--disable-gpu", "--window-size=800x600"]
            //    }
            //})
            .build();
        FeatureTestBase.browser = new protractor.ProtractorBrowser(FeatureTestBase.driver);
        return await FeatureTestBase.browser.driver.get(this.testUrl(this.port, this.dom)).then(function () {
            FeatureTestBase.browser.manage().timeouts().implicitlyWait(4000); // 4000 min for PhantomJS            
            FeatureTestBase.browser.manage().window().setSize(1480, 1024); //maximize() not working on gitlab
            return FeatureTestBase.browser.driver.findElement(webdriver.By.tagName('body')).then(function (element) {
                FeatureTestBase.myElementHandler = new MyElementHandler(element, f.browser.driver, (t.browsertype == "chrome" && t.dom == "shadow"));
            });
        });
    }

    @then(/user navigates to the betslip page/)
    public async navigateToBetslipPage() {
      //await FeatureTestBase.myElementHandler.getElementAndClick('document.querySelector("betslip-button").shadowRoot.querySelector("#bet_btn")');
    }

    @then(/clears the betslip page if any bets exists/)
    public async noBetsInBetslip() {
        FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);        
        var noBetsLabelXPath = "/html/body/neon-animated-pages/betslip-container/div/iron-pages/betslip-content/div[1]/section/empty-state/p";
        var noBetsLabel;
        try {
            noBetsLabel = await FeatureTestBase.myElementHandler.findElementByXpath(noBetsLabelXPath).getText();
            chai.assert.equal(noBetsLabel, "You have no bets");
        } catch (e) {
            // Threr are bets in betslip
            await FeatureTestBase.myElementHandler.clickWhenElementExist('Xpath',this.clearAllBetsButtonXPath);
        }
    }

    @then(/user navigates to the sports page/)
    public async navigateToSportsPage() {
       FeatureTestBase.browser.manage().timeouts().implicitlyWait(1000);   
       // await FeatureTestBase.myElementHandler.getElementAndClick('document.querySelector("betslip-container").shadowRoot.querySelector("#betslip-close")');    
    }

    /// End - Background
}

export = Background;