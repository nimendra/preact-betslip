"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const cucumber_tsflow_1 = require("cucumber-tsflow");
const chai = require('chai');
const FeatureTestBase_1 = require("../utils/FeatureTestBase");
let BetDelay = class BetDelay extends FeatureTestBase_1.FeatureTestBase {
    addFirstBet() {
        return __awaiter(this, void 0, void 0, function* () {
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89403, 1));
        });
    }
    addSecondBet() {
        return __awaiter(this, void 0, void 0, function* () {
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89165, 1));
        });
    }
    addThirdBet() {
        return __awaiter(this, void 0, void 0, function* () {
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89153, 1));
        });
    }
    addFouthBet() {
        return __awaiter(this, void 0, void 0, function* () {
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClickNotScroll(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSportsButtonPath(89157, 1));
        });
    }
    verifyProgressbar5sec() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
            chai.assert.equal(progressbar, null);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(5000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
            chai.assert.equal(progressbar, 'true');
        });
    }
    verifyProgressbar6sec() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
            chai.assert.equal(progressbar, null);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(3000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
            //  chai.assert.equal(progressbar, 'true');
        });
    }
    verifyProgressbar8sec() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
            chai.assert.equal(progressbar, null);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(9000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
            chai.assert.equal(progressbar, 'true');
        });
    }
    verifyMultiProgressbar5sec() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "hidden");
            chai.assert.equal(progressbar, null);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(4000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "hidden");
            chai.assert.equal(progressbar, null);
            //FeatureTestBase.browser.sleep(1000);
            //var progressbarvalue = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "value");
            //chai.assert.equal((progressbarvalue > 0), false);      
        });
    }
    nodelaybetplaced() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(betTicketText, "Bet placed. Receipt #17W5EPF6");
        });
    }
    betplaced() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(betTicketText, "Bet placed. Receipt #A12345");
        });
    }
    betfailed() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            var hasBetFailed = betTicketText.indexOf("1 bet failed.") != -1;
            chai.assert.equal(hasBetFailed, true);
        });
    }
    verifynoProgressbar() {
        return __awaiter(this, void 0, void 0, function* () {
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
            chai.assert.equal(progressbar, null);
        });
    }
    verifynoMultiProgressbar() {
        return __awaiter(this, void 0, void 0, function* () {
            // var progressbar = await FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase.myElementHandler.getMultipleProgressPath(0), "hidden");
            // chai.assert.equal(progressbar, null);
        });
    }
    enterBadmintonStake(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0), stake[i]);
            }
        });
    }
    enterSoccerStake(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(2));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(2), stake[i]);
            }
        });
    }
    enterCricketStake(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(1));
            for (var i = 0; i < stake.length; i++) {
                FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(1), stake[i]);
            }
        });
    }
    enterMultistake(stake) {
        return __awaiter(this, void 0, void 0, function* () {
            if (stake != "") {
                var position = 0;
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
                yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClear(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position));
                for (var i = 0; i < stake.length; i++) {
                    FeatureTestBase_1.FeatureTestBase.browser.sleep(100);
                    yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getMultiplesStakePath(position), stake[i]);
                }
            }
        });
    }
    verifyProgressbar5sec2() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(2000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
            chai.assert.equal(progressbar, null);
            FeatureTestBase_1.FeatureTestBase.browser.sleep(5000);
            var progressbarvalue = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "value");
            chai.assert.equal((progressbarvalue > 0), true);
            var progressbar = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetProgressPath(0), "hidden");
            //chai.assert.equal(progressbar, 'true');
        });
    }
    verifyDeleteButtonNotVisible() {
        return __awaiter(this, void 0, void 0, function* () {
            var style = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetAttribute(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetRemoveButtonPath(0), 'style');
            chai.assert.equal(style, "display: none;");
        });
    }
    verifytoast1betplaced1betFailed() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(betTicketText, "Bet placed. Receipt #A12345");
        });
    }
    verifytoast2betplaced() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
            var betTicketText = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getToastLablePath());
            chai.assert.equal(betTicketText, "2 bets placed.");
        });
    }
};
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for the pre match badminton game/),
    cucumber_tsflow_1.then(/user clicks on the first price market for the in play badminton game/)
], BetDelay.prototype, "addFirstBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for the pre match cricket game/),
    cucumber_tsflow_1.then(/user clicks on the first price market for the in play cricket game/)
], BetDelay.prototype, "addSecondBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for the pre match baseball game/),
    cucumber_tsflow_1.then(/user clicks on the first price market for the in play baseball game/)
], BetDelay.prototype, "addThirdBet", null);
__decorate([
    cucumber_tsflow_1.then(/user clicks on the first price market for the in play soccer game/)
], BetDelay.prototype, "addFouthBet", null);
__decorate([
    cucumber_tsflow_1.then(/verify the progress bar is displayed for 5sec/)
], BetDelay.prototype, "verifyProgressbar5sec", null);
__decorate([
    cucumber_tsflow_1.then(/verify the progress bar is displayed for 6sec/)
], BetDelay.prototype, "verifyProgressbar6sec", null);
__decorate([
    cucumber_tsflow_1.then(/verify the progress bar is displayed for 8sec/)
], BetDelay.prototype, "verifyProgressbar8sec", null);
__decorate([
    cucumber_tsflow_1.then(/verify the multi progress bar is displayed for 5sec/)
], BetDelay.prototype, "verifyMultiProgressbar5sec", null);
__decorate([
    cucumber_tsflow_1.then(/verify the no delay bet is placed "Bet placed. Receipt #"/)
], BetDelay.prototype, "nodelaybetplaced", null);
__decorate([
    cucumber_tsflow_1.then(/verify the bet is placed "Bet placed. Receipt #"/)
], BetDelay.prototype, "betplaced", null);
__decorate([
    cucumber_tsflow_1.then(/verify the bet is rejected "1 bet failed"/)
], BetDelay.prototype, "betfailed", null);
__decorate([
    cucumber_tsflow_1.then(/verify no progress bar is displayed/)
], BetDelay.prototype, "verifynoProgressbar", null);
__decorate([
    cucumber_tsflow_1.then(/verify the no multi progress bar is displayed/)
], BetDelay.prototype, "verifynoMultiProgressbar", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake(.*) in the betslip for the badminton game/)
], BetDelay.prototype, "enterBadmintonStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the soccer stake(.*)in the betslip for the soccer game/)
], BetDelay.prototype, "enterSoccerStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the cricket stake(.*) in the betslip for the cricket game/)
], BetDelay.prototype, "enterCricketStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters the stake(.*) in the multistake box/)
], BetDelay.prototype, "enterMultistake", null);
__decorate([
    cucumber_tsflow_1.then(/verify the progress bar is still displayed for 5sec/)
], BetDelay.prototype, "verifyProgressbar5sec2", null);
__decorate([
    cucumber_tsflow_1.then(/verify delete button is not visible near the stake box while the progress bar is visible/)
], BetDelay.prototype, "verifyDeleteButtonNotVisible", null);
__decorate([
    cucumber_tsflow_1.then(/verify user recives a notification toaster "Bet placed. Receipt #"/)
], BetDelay.prototype, "verifytoast1betplaced1betFailed", null);
__decorate([
    cucumber_tsflow_1.then(/verify user recives a notification toaster "2 bets placed."/)
], BetDelay.prototype, "verifytoast2betplaced", null);
BetDelay = __decorate([
    cucumber_tsflow_1.binding()
], BetDelay);
module.exports = BetDelay;
