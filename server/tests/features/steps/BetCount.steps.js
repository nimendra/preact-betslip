"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const cucumber_tsflow_1 = require("cucumber-tsflow");
const chai = require('chai');
const FeatureTestBase_1 = require("../utils/FeatureTestBase");
let BetCount = class BetCount extends FeatureTestBase_1.FeatureTestBase {
    // @then(/the user adds required number of markets (.*)by clicking different games/)
    // @then(/the user adds required number of markets (.*) by clicking different games/)
    // @given(/the user adds required number of markets by clicking different games$/)
    // public async addBetstobetslip(markets: string): Promise<void> {
    //     FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);   
    //     if (markets != "") {
    //         var parsedMarkets = parseInt(markets);  
    //         FeatureTestBase.browser.sleep(1000);
    //         var eventids = ["89403", "89165", "89153"];
    //         var outcomeids = ["1", "1", "1"];
    //         for (var i = 0; i < parsedMarkets; i++) {                
    //              console.log("*************click on " + eventids[i] + "_" + outcomeids[i]);
    //              await FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase.myElementHandler.getSportsButtonPath(eventids[i], outcomeids[i]));
    //         }
    //     }        
    // }   
    checkbetcount(count) {
        return __awaiter(this, void 0, void 0, function* () {
            if (count != "") {
                FeatureTestBase_1.FeatureTestBase.browser.manage().timeouts().implicitlyWait(2000);
                FeatureTestBase_1.FeatureTestBase.browser.sleep(1000);
                var betcount = yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndGetText(FeatureTestBase_1.FeatureTestBase.myElementHandler.getBetCount());
                chai.assert.equal(betcount, count);
            }
        });
    }
    clickonbetslipbutton() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    removeFirstbet() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetRemoveButtonPath(0));
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
        });
    }
    refreshPage() {
        return __awaiter(this, void 0, void 0, function* () {
            yield FeatureTestBase_1.FeatureTestBase.browser.driver.navigate().refresh();
        });
    }
    clickClearAll() {
        return __awaiter(this, void 0, void 0, function* () {
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndClick(FeatureTestBase_1.FeatureTestBase.myElementHandler.getRemoveAllCount());
        });
    }
    closebetslip() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    entersFirstStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(0), '1');
        });
    }
    entersSecondStake() {
        return __awaiter(this, void 0, void 0, function* () {
            FeatureTestBase_1.FeatureTestBase.browser.sleep(500);
            yield FeatureTestBase_1.FeatureTestBase.myElementHandler.getElementAndSendKeys(FeatureTestBase_1.FeatureTestBase.myElementHandler.getSinglebetStakePath(1), '1');
        });
    }
};
__decorate([
    cucumber_tsflow_1.then(/verify the count on the betslip counter is (.*)/)
], BetCount.prototype, "checkbetcount", null);
__decorate([
    cucumber_tsflow_1.then(/the user clicks on the beslip/)
], BetCount.prototype, "clickonbetslipbutton", null);
__decorate([
    cucumber_tsflow_1.then(/the user removes the badminton game/)
], BetCount.prototype, "removeFirstbet", null);
__decorate([
    cucumber_tsflow_1.then(/the user refreshes the page/)
], BetCount.prototype, "refreshPage", null);
__decorate([
    cucumber_tsflow_1.then(/the user clicks on Clear All button/)
], BetCount.prototype, "clickClearAll", null);
__decorate([
    cucumber_tsflow_1.then(/the user closes the betslip/)
], BetCount.prototype, "closebetslip", null);
__decorate([
    cucumber_tsflow_1.then(/user enters 1 in first market stake box in the betslip/)
], BetCount.prototype, "entersFirstStake", null);
__decorate([
    cucumber_tsflow_1.then(/user enters 1 in second market stake box in the betslip/)
], BetCount.prototype, "entersSecondStake", null);
BetCount = __decorate([
    cucumber_tsflow_1.binding()
], BetCount);
module.exports = BetCount;
