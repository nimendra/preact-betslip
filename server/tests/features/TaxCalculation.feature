﻿Feature: TaxCalculation
	Tax-free (Tax 0%): column net profits up to 100 euros
	Tax 15%: column net profit from 100.01 to 500 euros (The tax is charged on the profits which exceed 100 euros per column)
	Tax 20%: column net profit from 500.01 euros or more (The tax is charged on the profits which exceed 500.01 euros per column)

Background: 
	Given user navigates to the sportsbook website
	Then user navigates to the betslip page
	Then clears the betslip page if any bets exists
	Then user navigates to the sports page

@desktopsite
Scenario Outline: Tax Calc for single bets
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<odds>	
	Then user enters the stake<stake> in the betslip
	Then verify the est return below the stake box is updated to <estreturn>
	Then verify the tax value below the stake box is updated to <taxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>

Examples: 
	| odds   | stake | estreturn | taxvalue | betslipestreturn | betsliptaxvalue |
	| 500.0  | 10.0  | 4,851.50  | 148.50   | 4,851.50         | 148.50          |
	| 2500.0 | 10.0  | 20,802.00 | 4,198.00 | 20,802.00        | 4,198.00        |

@desktopsite
Scenario Outline: Tax Calc for multiple bets
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<singleodds>	 
	Then clicks on the Other Multiples drop down
	Then user enters the stake value in the <Markets>leg multi box for £<multistake>  
	Then verify the est return below the multi stake box is updated to <estreturn>
	Then verify the tax value below the multi stake box is updated to <taxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>

    Examples:
    | Markets | singleodds | multistake | odds   | estreturn | taxvalue | betslipestreturn | betsliptaxvalue |
    | 2       | 5.0,100.0  | 10         | 500.0  | 4,851.50  | 148.50   | 4,851.50         | 148.50          |
    | 2       | 5.0,500.0  | 10         | 2500.0 | 20,802.00 | 4,198.00 | 20,802.00        | 4,198.00        |

@desktopsite
Scenario Outline: Tax Calc for exotic bets
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<singleodds>
	Then clicks on the Other Multiples drop down
	Then user enters six fold stake <FiveFoldsstake> for <Markets> markets for odds<odds>
	Then verify the est return below the multi stake box is updated to <estreturn>
	Then verify the tax value below the multi stake box is updated to <taxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>
	
Examples: 
	| Markets | singleodds              | FiveFoldsstake | odds   | estreturn | taxvalue | betslipestreturn | betsliptaxvalue |
	| 6       | 2.0,5.0,2.0,5.0,2.5,2.0 | 10             | 500.0  | 4,851.50  | 148.50   | 4,851.50         | 148.50          |
	| 6       | 2.0,5.0,2.0,5.0,5.0,5.0 | 10             | 2500.0 | 20,802.00 | 4,198.00 | 20,802.00        | 4,198.00        |


@desktopsite
Scenario Outline: Tax Calc when single and multi bet is placed
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<singleodds>
	Then user enters the stake<singlestake> in the betslip
	Then clicks on the Other Multiples drop down
	Then user enters the stake value in the <Markets>leg multi box for £<multistake> 
	Then verify the est return below the stake box is updated to <singleestreturn>
	Then verify the tax value below the stake box is updated to <singletaxvalue>
	Then verify the est return below the multi stake box is updated to <multiestreturn>
	Then verify the tax value below the multi stake box is updated to <multitaxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>

Examples: 
	| Markets | singlestake | singleodds | multistake | multiodds | singleestreturn | singletaxvalue | multiestreturn | multitaxvalue | betslipestreturn | betsliptaxvalue |
	| 2       | 10.0        | 5.0,100.0  | 10.0       | 500.0     | 50.00           |                | 4,851.50       | 148.50        | 4,901.50         | 148.50          |
	| 2       | 10.0        | 5.0,500.0  | 10.0       | 2500.0    | 50.00           |                | 20,802.00      | 4,198.00      | 20,852.00        | 4,198.00        |
	| 2       | 10.0        | 500.0,2.0  | 10.0       | 1000.0    | 4,851.50        | 148.50         | 9,101.50       | 898.50        | 13,953.00        | 1,047.00        |

@desktopsite
Scenario Outline: Tax Calc when single and exotic bet is placed
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<singleodds>
	Then user enters the stake<singlestake> in the betslip for singleodds<singleodds>
	Then clicks on the Other Multiples drop down
	Then user enters five folds stake <FiveFoldsstake> for <Markets> markets for odds<exoticodds>
	Then verify the est return below the stake box is updated to <singleestreturn>
	Then verify the tax value below the stake box is updated to <singletaxvalue>
	#Then verify the est return below the five folds stake box for <Markets> markets is updated to <exoticestreturn>
	#Then verify the tax value below the five folds stake box for <Markets> markets is updated to <exotictaxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>

Examples: 
	| Markets | singlestake | singleodds                | FiveFoldsstake | exoticodds | singleestreturn | singletaxvalue | exoticestreturn | exotictaxvalue | betslipestreturn | betsliptaxvalue |
	| 6       | 10.0        | 2.0,5.0,2.0,5.0,2.5,2.0   | 10.0           | 500.0      | 20.00           |                |                 |                | 11,520.80        |                 |
	| 6       | 10.0        | 2.0,5.0,2.0,5.0,5.0,5.0   | 10.0           | 2500.0     | 20.00           |                |                 |                | 41,877.80        | 3,141.00        |
	| 6       | 10.0        | 500.0,5.0,2.0,5.0,2.5,2.0 | 10.0           | 75000.0    | 4,851.50        | 148.50         |                 |                | 1,811,362.30     | 446,138.50      |
	| 6       | 10.0        | 500.0,5.0,2.0,5.0,5.0,5.0 | 10.0           | 375000.0   | 4,851.50        | 148.50         |                 |                | 6,520,086.80     | 1,622,412.00    |
	#| 6       | 10.0        | 500.0      | 10.0           | 3.0        | 4851.5          | 148.5          | 30.0            | 0.0            | 4881.5           | 148.5           |
	#| 6       | 10.0        | 2500.0     | 10.0           | 3.0        | 20,802.0        | 4198           | 30.0            | 0.0            | 20,832.0         | 4198            |
	
@desktopsite
Scenario Outline: Tax Calc when multi and exotic bet is placed
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<singleodds>
	Then clicks on the Other Multiples drop down
	Then user enters the stake value in the <Markets>leg multi box for £<multistake>  
	Then user enters five folds stake <FiveFoldsstake> for <Markets> markets for odds<exoticodds>
	Then verify the est return below the multi stake box is updated to <multiestreturn>
	Then verify the tax value below the multi stake box is updated to <multitaxvalue>
	#Then verify the est return below the stake box is updated to <exoticestreturn>
	#Then verify the tax value below the stake box is updated to <exotictaxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>

Examples: 
	| Markets | singleodds                | multistake | multiodds | FiveFoldsstake | exoticodds | multiestreturn | multitaxvalue | exoticestreturn | exotictaxvalue | betslipestreturn | betsliptaxvalue |
	| 6       | 2.0,5.0,2.0,5.0,2.0,2.5   | 10.0       | 500.0     | 10.0           | 500.0      | 4,851.50       | 148.50        |                 |                | 16,352.30        | 148.50          |
	| 6       | 2.0,5.0,2.0,5.0,5.0,5.0   | 10.0       | 2500.0    | 10.0           | 2500.0     | 20,802.00      | 4,198.00      |                 |                | 62,659.80        | 7,339.00        |
	| 6       | 500.0,5.0,2.0,5.0,2.5,2.0 | 10.0       | 75000.0   | 10.0           | 3.0        | 1,000,802.00   | 249,198.00    |                 |                | 2,807,312.80     | 695,188.00      |
	| 6       | 500.0,2.0,5.0,5.0,5.0,5.0 | 10.0       | 375000.0  | 10.0           | 3.0        | 5,000,802.00   | 1,249,198.00  |                 |                | 11,516,037.30    | 2,871,461.50    |

@desktopsite
Scenario Outline: Tax row not shown for single bets when the tax value is 0
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<odds>	
	Then user enters the stake<stake> in the betslip
	Then verify the est return below the stake box is updated to <estreturn>
	Then verify the tax value below the stake box is updated to <taxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>

Examples: 
	| odds | stake | estreturn | taxvalue | betslipestreturn | betsliptaxvalue |
	| 3.0  | 10.0  | 30.00     |          | 30.00            |                 |

@desktopsite
Scenario Outline: Tax row not shown for multiple bets when the tax value is 0
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<odds>	
	Then clicks on the Other Multiples drop down
	Then user enters the stake value in the <Markets>leg multi box for £<multistake>  
	Then verify the est return below the multi stake box is updated to <estreturn>
	Then verify the tax value below the multi stake box is updated to <taxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>

    Examples:
    | Markets | multistake | odds    | estreturn | taxvalue | betslipestreturn | betsliptaxvalue |
    | 2       | 10         | 2.0,1.5 | 30.00     |          | 30.00            |                 |

@desktopsite
Scenario Outline: Tax row not shown for exotic bets when the tax value is 0
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then user clicks on the first price market for aussie rules game
	Then user clicks on the first price market for basketball game
	Then clicks on the Other Multiples drop down
	Then user enters five folds stake <FiveFoldsstake> for <Markets> markets 
	#Then verify the est return below the stake box is updated to <estreturn>
	#Then verify the tax value is not shown below the stake box
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is not shown on the betstlip
	
Examples: 
	| Markets | FiveFoldsstake | odds | estreturn | taxvalue | betslipestreturn | betsliptaxvalue |
	| 6       | 10             |      | 1,621.20  |          | 1,621.20         |                 |

@desktopsite
Scenario Outline: Tax calc updated when the price update signal recived for the single bet
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game for price 1.57
	Then user enters the stake<stake> in the betslip
	Then verify the est return below the stake box is updated to <estreturn>
	Then verify the tax value below the stake box is updated to <taxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is not shown on the betstlip
	Then the user recives the price update for the badminton game for <updodds>
	Then verify the est return below the stake box is updated to <updestreturn>
	Then verify the tax value below the stake box is updated to <updtaxvalue>
	Then verify the total est return value is updated for the betstlip to <updbetslipestreturn>
	Then verify the total tax value is updated for the betstlip to <updbetsliptaxvalue>

Examples: 
	| odds | stake | estreturn | taxvalue | betslipestreturn | betsliptaxvalue | updodds | updestreturn | updtaxvalue | updbetslipestreturn | updbetsliptaxvalue |
	| 1.57 | 10.0  | 15.71     |          | 15.71            |                 | 500.0   | 4,851.50     | 148.50      | 4,851.50            | 148.50             |


@desktopsite
Scenario Outline: Tax calc updated when the price update submit for the single bet
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<odds>
	Then user enters the stake<stake> in the betslip
	Then verify the est return below the stake box is updated to <estreturn>
	Then verify the tax value below the stake box is updated to <taxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is not shown on the betstlip
	Then user clicks on Bet button
	Then the user recives the price update for the badminton game for <updodds> along with the error message "1 Price changed" 
	Then verify the est return below the stake box is updated to <updestreturn>
	Then verify the tax value below the stake box is updated to <updtaxvalue>
	Then verify the total est return value is updated for the betstlip to <updbetslipestreturn>
	Then verify the total tax value is updated for the betstlip to <updbetsliptaxvalue>

Examples: 
	| odds | stake | estreturn | taxvalue | betslipestreturn | betsliptaxvalue | updodds | updestreturn | updtaxvalue | updbetslipestreturn | updbetsliptaxvalue |
	| 1.80 | 2103  | 3,785.40  |          | 3,785.40         |                 | 500.0   | 1,020,270.45 | 31,229.55   | 1,020,270.45        | 31,229.55          |

@desktopsite
Scenario Outline:  Tax calc updated when the suspend signal recived for the single bet
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<odds>	
	Then user enters the stake<stake> in the betslip
	Then verify the est return below the stake box is updated to <estreturn>
	Then verify the tax value below the stake box is updated to <taxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>
	Then user recieves a bet update for the badminton game odd 500 as suspended
	Then verify the est return below the stake box is updated to <updestreturn>
	Then verify the tax value below the stake box is updated to <updtaxvalue>
	Then verify the total est return value is updated for the betstlip to <updbetslipestreturn>
	Then verify the total tax value is updated for the betstlip to <updbetsliptaxvalue>

Examples: 
	| odds  | stake | estreturn | taxvalue | betslipestreturn | betsliptaxvalue | updestreturn | updtaxvalue | updbetslipestreturn | updbetsliptaxvalue |
	| 500.0 | 10.0  | 4,851.50  | 148.50   | 4,851.50         | 148.50          |              |             |                     |                    |


@desktopsite
Scenario Outline:  Tax calc updated when the suspend submit for the single bet
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the markets for odds<odds>	
	Then user enters the stake<stake> in the betslip
	Then verify the est return below the stake box is updated to <estreturn>
	Then verify the tax value below the stake box is updated to <taxvalue>
	Then verify the total est return value is updated for the betstlip to <betslipestreturn>
	Then verify the total tax value is updated for the betstlip to <betsliptaxvalue>
	Then user clicks on Bet button
	Then verify the notification toaster appear with text "1 bet suspended"
	Then verify the est return below the stake box is updated to <updestreturn>
	Then verify the tax value below the stake box is updated to <updtaxvalue>
	Then verify the total est return value is updated for the betstlip to <updbetslipestreturn>
	Then verify the total tax value is updated for the betstlip to <updbetsliptaxvalue>

Examples: 
	| odds  | stake | estreturn  | taxvalue  | betslipestreturn | betsliptaxvalue | updestreturn | updtaxvalue | updbetslipestreturn | updbetsliptaxvalue |
	| 500.0 | 2001  | 970,785.15 | 29,714.85 | 970,785.15       | 29,714.85       |              |             |                     |                    |


@desktopsite
Scenario Outline:  Rounding of stake into singles of 0.25
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game 
	Then user enters the stake<stake> in the betslip
	Then user sees the expected stake<expectedstake> in the stake box
	Then the user sees the notification toaster with text "Stake rounded for some selections" for <stake>

Examples: 
	| stake | expectedstake |
	| 0.23  | 0.25          |
	| 0.35  | 0.25          |
	| 1.0   | 1             |
	| 1.23  | 1             |
	| 1.95  | 1.75          |


@desktopsite
Scenario Outline:  Rounding of stake into multi bet of 0.25
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then the user adds required number of markets <legCount> by clicking different games
	Then verify the Double bet is visible on the betslip page
	Then user enters double stake <stake> for <legCount> markets
	Then user sees the expected double stake <expectedstake> for <legCount> markets
	Then the user sees the notification toaster with text "Stake rounded for some selections" for <stake>

Examples: 

	| legCount | stake | expectedstake |
	| 2        | 0.23  | 0.25          |
	| 2        | 0.35  | 0.25          |
	| 2        | 1.0   | 1             |
	| 2        | 1.23  | 1             |
	| 2        | 1.95  | 1.75          |


@desktopsite
Scenario Outline:  Rounding of stake into Parlay bet of 0.25
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then the user adds required number of markets <legCount> by clicking different games
	Then user click on the multiples collapes for <legCount> markets    	 
	Then user enters trixie stake <stake> for <legCount> markets
	Then user sees the expected trixie stake <expectedstake> for <legCount> markets
	Then the user sees the notification toaster with text "Stake rounded for some selections" for <stake>

Examples: 

	| legCount | stake | expectedstake |
	| 3        | 0.23  | 0.25          |
	| 3        | 0.35  | 0.25          |
	| 3        | 1.0   | 1             |
	| 3        | 1.23  | 1             |
	| 3        | 1.95  | 1.75          |