﻿Feature: PriceUpdate
	Verify the price update triggers the animations as per the price change
    
Background: 
	Given user navigates to the sportsbook website
	Then user navigates to the betslip page
	Then clears the betslip page if any bets exists
	Then user navigates to the sports page

@TB-49
Scenario: Increase in price for a particular market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game for price 1.57
	Then clicks on the Betslip icon
	Then user receives a price update for the badminton game for price 3.50
	Then verify an updated price is shown in the betslip page
	Then verify a green upward arrow is shown for the updated price change

@TB-49
Scenario: Decrease in price for a particular market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for cricket game for price 2.50
	Then clicks on the Betslip icon
	Then user receives a price update for the badminton game for price 1.50
	Then verify an updated price is shown in the betslip page
	Then verify a red downward arrow is shown for the updated price change

@TB-49
Scenario Outline: Increase in price for a particular market after bet placement
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game for price 1.57
	Then clicks on the Betslip icon
	Then user enters the stake<stake> in the betslip
	Then user clicks on Bet button
	Then user receives notification toaster saying "1 bet failed. Price updated" for the price of 3.50
	Then verify an updated price is shown in the betslip page
	Then verify a green upward arrow is shown for the updated price change
	Then verify the stake<stake> box value is not changed

Examples: 
			| stake |
			| 2103 |

@TB-49
Scenario Outline: Decrease in price for a particular market after bet placement
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for cricket game for price 2.50
	Then clicks on the Betslip icon
	Then user enters the stake<stake> in the betslip
	Then user clicks on Bet button
	Then user receives notification toaster saying "1 bet failed. Price updated" for the price of 1.50
	Then verify an updated price is shown in the betslip page
	Then verify a red downward arrow is shown for the updated price change
	Then verify the stake<stake> box value is not changed

Examples: 
			| stake |
			| 2103 |

@TB-174
Scenario Outline: Verify est return for single bet is updated when the price is increased
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game for price 1.57
	Then clicks on the Betslip icon
	Then user enters the stake<stake> in the betslip
	Then user clicks on Bet button
	Then user receives notification toaster saying "1 bet failed. Price updated" for the price of 1.80
	Then verify an updated price is shown in the betslip page
	Then verify a green upward arrow is shown for the updated price change
	Then verify the stake<stake> box value is not changed
	Then verify the est return below the stake box is updated to 3,785.40
	Then verify the total est return value is updated to 3,785.40

Examples: 
			| stake |
			| 2103 |

@TB-174
Scenario Outline: Verify est return for multi bet is updated when the price is increased
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then user receives notification toaster saying "1 bet failed. Price updated" for the price of 3.50
	Then verify an updated price is shown in the betslip page
	Then verify a green upward arrow is shown for the updated price change
	Then verify the multi stake<stake> box value is not changed
	Then verify the multi est return below the stake box is updated to 9,463.50 
	Then verify the multi total est return value is updated to 9,463.50

    Examples:
    | legCount | amount | stake |
    | 2        | 2103  | 2103 |

@TB-174
Scenario Outline: Verify est return for single bet is updated when the price is decreased
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for american football game for price 1.40
	Then clicks on the Betslip icon
	Then user enters the stake<stake> in the betslip
	Then user clicks on Bet button
	Then user receives notification toaster saying "1 bet failed. Price updated" for the price of 1.80
	Then verify an updated price is shown in the betslip page
	Then verify a green upward arrow is shown for the updated price change
	Then verify the stake<stake> box value is not changed
	Then verify the est return below the stake box is updated to 3,785.40
	Then verify the total est return value is updated to 3,785.40

Examples: 
			| stake |
			| 2103 |

@TB-174
Scenario Outline: Verify est return for multi bet is updated when the price is decreased
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then user receives notification toaster saying "1 bet failed. Price updated" for the price of 1.50
	Then verify an updated price is shown in the betslip page
	Then verify a green upward arrow is shown for the updated price change
	Then verify the multi stake<stake> box value is not changed
	Then verify the multi est return below the stake box is updated to 9,463.50
	Then verify the multi total est return value is updated to 9,463.50

    Examples:
    | legCount | amount | stake |
    | 2        | 2103  | 2103 |

@TB-56
Scenario Outline: Show Keep Bets and notification toast when a single bet is placed and other bet receives a price update 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user enters the stake value in the badminton game box for £<validBetAmount>
	Then user enters the stake value in the cricket game box for £<priceUpdateBetAmount>
	Then user clicks on Bet button
	Then user receives a price update for the cricket game
	Then user receives notification toaster saying "1 bet placed. 1 bet failed."
	Then the user sees the Keep Bets button on the notificaiton toaster

Examples: 
	| validBetAmount | priceUpdateBetAmount |
	| 10             | 2103                |

#@TB-333
#Scenario: Show Ok Button and notification toast when a single bet receives a price update 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	#Then user clicks on the first price market for badminton game	
	#Then user clicks on the first price market for cricket game
	#Then user receives a price update for the cricket game
	#Then user receives notification toaster saying "Your betslip got updated"
	#Then the user sees the Ok button on the notificaiton toaster

@TB-56
Scenario Outline: Show Keep Bets and notification toast when a single and mulit bet is placed and other bet receives a price update 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user enters the stake value in the badminton game box for £<validBetAmount>
	Then user enters the stake value in the cricket game box for £<priceUpdateBetAmount>
	Then user enters the stake value in the <legCount>leg multi box for £<validBetAmount>
	Then user clicks on Bet button
	Then user receives a price update for the cricket game
	Then user receives notification toaster saying "1 bet placed. 2 bets failed."
	Then the user sees the Keep Bets button on the notificaiton toaster
	Then verify a red downward arrow is shown for the updated price change for cricket game
	Then verify multi stake box shows an error message as "Price Change"

Examples: 
	 | legCount | validBetAmount | priceUpdateBetAmount |
	 | 2        | 10             | 2103                 |


@TB-56
Scenario Outline: Show Ok button and notification toast when a single and mulit bet is placed and other bet receives a price update 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user enters the stake value in the cricket game box for £<priceUpdateBetAmount>
	Then user enters the stake value in the <legCount>leg multi box for £<validBetAmount>
	Then user clicks on Bet button
	Then user receives a price update for the cricket game
	Then user receives notification toaster saying "2 bets failed."
	Then the user sees the Ok button on the notificaiton toaster
	Then verify a red downward arrow is shown for the updated price change for cricket game
	Then verify multi stake box shows an error message as "Price Change"

Examples: 
	 | legCount | validBetAmount | priceUpdateBetAmount |
	 | 2        | 10             | 2103                |