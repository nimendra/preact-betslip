﻿Feature: Others
	 To cover all bugs fix

Background: 
	Given user navigates to the sportsbook website
	Then user navigates to the betslip page
	Then clears the betslip page if any bets exists
	Then user navigates to the sports page

Scenario: after placing bet receive a 401 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game
	Then the user clicks on the beslip
	Then user enters 4010 in first market stake box in the betslip
	Then user clicks on Bet button	 
	Then verify stake box is disabled on the betslip
	Then verify bet button is disabled
	Then verify the notification toaster appear with text "Server or Network error: 401Unauthorized"
	Then verify bet button is enabled

Scenario: when placing bet call very slow 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game
	Then the user clicks on the beslip
	Then user enters 4000 in first market stake box in the betslip
	Then user clicks on Bet button	 
	Then verify stake box is disabled on the betslip
	Then verify bet button is disabled
	Then after few seconds 
	Then verify user recives a notification toaster "Bet placed. Receipt #17W5EPF6"
	Then verify bet button is disabled

@BM-219
Scenario: when intercept call throw error enable related error bet
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the in play soccer game
	Then the user clicks on the beslip
	Then user enters 8000 in first market stake box in the betslip
	Then user clicks on Bet button	 
	Then verify stake box is disabled on the betslip
	Then verify bet button is disabled
	Then verify the progress bar is displayed for 8sec
	Then verify the notification toaster appear with text "Server or Network error: 500Internal Server Error"
	Then verify stake box is enabled on the betslip
	Then verify bet button is enabled

@BM-219
Scenario: when intercept call throw error enable related error bets
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the in play soccer game
	Then user clicks on the second price market for the in play soccer game
	Then user clicks on the third price market for the in play soccer game
	Then the user clicks on the beslip
	Then user enters 8000 in first market stake box in the betslip
	Then user enters 8000 in second market stake box in the betslip
	Then user enters 8000 in third market stake box in the betslip
	Then user clicks on Bet button	 
	Then verify stake box is disabled on the betslip
	Then verify bet button is disabled
	Then verify the progress bar is displayed for 8sec
	Then verify the notification toaster appear with text "Server or Network error: 500Internal Server Error"
	Then verify stake box is enabled on the betslip
	Then verify second stake box is enabled on the betslip
    Then verify third stake box is enabled on the betslip
	Then verify bet button is enabled

@BM-219
Scenario: when intercept call throw error enable related error bets
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then the user clicks on the beslip
	Then user enters 8000 in first market stake box in the betslip
	Then user enters 8000 in second market stake box in the betslip
	Then user enters 8000 in third market stake box in the betslip
	Then user enters 8000 in forth market stake box in the betslip
	Then user clicks on Bet button	 
	Then verify stake box is disabled on the betslip
	Then verify bet button is disabled
	Then verify the progress bar is displayed for 6sec
    Then verify user recives a notification toaster "Bet placed. Receipt #"
	Then verify the notification toaster appear with text "Server or Network error: 500Internal Server Error"
	Then after 1 seconds
	Then user receives notification toaster saying "1 bet placed. 1 bet failed."
	Then verify stake box is disabled on the betslip
	Then verify second stake box is disabled on the betslip
    Then verify third stake box is enabled on the betslip
	Then verify bet button is enabled

@BM-219
Scenario: 5 sec bet delay for the in play game bet rejected and no bet details return
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for basketball game	
	Then user enters the stake<8000> in the betslip
	Then user clicks on Bet button
	Then verify the progress bar is displayed for 8sec
	Then verify the bet is rejected "1 bet failed"
	Then verify the error message for the first bet shows "Intercepted -> Rejected by Admin"