"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const selenium_webdriver_1 = require("selenium-webdriver");
class MyElementHandler {
    constructor(webElement, browser, supportshadow) {
        this.webElement = webElement;
        this.browser = browser;
        this.supportshadow = supportshadow;
        this.driver = browser;
    }
    findElementByClassName(classname) {
        return this.webElement.findElement(selenium_webdriver_1.By.className(classname));
    }
    findElementById(name) {
        return this.webElement.findElement(selenium_webdriver_1.By.id(name));
    }
    findElementByXpath(xpathlink) {
        return this.webElement.findElement(selenium_webdriver_1.By.xpath(xpathlink));
    }
    clickWhenElementExist(method, name) {
        if (method == "Xpath") {
            this.findElementByXpath(name).then(function (webElement) {
                console.log('Element exists');
                if (webElement.isDisplayed()) {
                    webElement.click();
                }
            }, function (err) {
                console.log('Element not found');
            });
        }
        else if (method == "Id") {
            this.findElementById(name).then(function (webElement) {
                console.log('Element exists');
                if (webElement.isDisplayed()) {
                    webElement.click();
                }
            }, function (err) {
                console.log('Element not found');
            });
        }
        else if (method == "ClassName") {
            this.findElementByClassName(name).then(function (webElement) {
                console.log('Element exists');
                if (webElement.isDisplayed()) {
                    webElement.click();
                }
            }, function (err) {
                console.log('Element not found');
            });
        }
    }
    getElementByXPath(xpath) {
        return this.findElementByXpath(xpath);
    }
    getElementAndGetText(path) {
        return __awaiter(this, void 0, void 0, function* () {
            var e2 = yield this.driver.executeScript('return ' + path + ';');
            return e2.getText();
            // this.driver.executeScript("arguments[0].click();", e2);      
        });
    }
    getElementAndGetValue(path) {
        return __awaiter(this, void 0, void 0, function* () {
            var e2 = yield this.driver.executeScript('return ' + path + '.value;');
            return e2;
            // this.driver.executeScript("arguments[0].click();", e2);      
        });
    }
    getElementAndGetClass(path) {
        return __awaiter(this, void 0, void 0, function* () {
            var e2 = yield this.driver.executeScript('return ' + path + ';');
            return e2.getAttribute('class');
            // this.driver.executeScript("arguments[0].click();", e2);      
        });
    }
    getElementAndGetAttribute(path, attribute) {
        return __awaiter(this, void 0, void 0, function* () {
            var el = this.findElementByXpath(path);
            return el.getAttribute(attribute);
        });
    }
    getElementAndScrollThenClick(path) {
        return __awaiter(this, void 0, void 0, function* () {
            var e2 = yield this.driver.executeScript('return ' + path + ';');
            this.driver.executeScript("arguments[0].scrollIntoView(true);arguments[0].click();", e2);
        });
    }
    getElementAndClick(path) {
        return __awaiter(this, void 0, void 0, function* () {
            var el = this.findElementByXpath(path);
            el.click();
        });
    }
    getElementAndClickNotScroll(path) {
        return __awaiter(this, void 0, void 0, function* () {
            var e2 = yield this.driver.executeScript('return ' + path + ';');
            e2.click();
        });
    }
    getElement(path) {
        return __awaiter(this, void 0, void 0, function* () {
            var e2 = yield this.driver.executeScript('return ' + path + ';');
            return e2;
        });
    }
    getElementAndSendKeys(path, keys) {
        return __awaiter(this, void 0, void 0, function* () {
            var js = 'return ' + path + ';';
            //console.log("----------JS------------", js);
            var e = yield this.driver.executeScript(js);
            e.sendKeys(keys);
        });
    }
    getElementAndClear(path) {
        return __awaiter(this, void 0, void 0, function* () {
            var e2 = yield this.driver.executeScript('return ' + path + ';');
            e2.clear();
        });
    }
    stackShadows(shadows) {
        var ret = 'document';
        for (var i = 0; i < shadows.length; i++) {
            ret += '.querySelector("' + shadows[i] + '").shadowRoot';
        }
        return ret;
    }
    fireIronSignal(signal, type) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.driver.executeScript('return document.dispatchEvent(new CustomEvent("iron-signal", { "detail": { name: "' + type + '", data: ['
                + JSON.stringify(signal)
                + ']}}));');
        });
    }
    //sport button
    getSportsButtonPath(eventId, outcomeId) {
        return this.stackShadows(["bg-sports-nextn", "bg-sports-button#bet_btn_" + eventId + "_" + outcomeId]) + '.querySelector("button")';
    }
    //toast label
    getToastLablePath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip", "bg-betslip-toast", "paper-toast"]) + '.querySelector("span#label")';
    }
    //toast button 
    getToastButtonPath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip", "bg-betslip-toast"]) + '.querySelector("a#toastButton")';
    }
    //singlebet stake 
    getSinglebetStakePath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#stakeInput").shadowRoot.querySelector("#nativeInput")';
    }
    //singlebet stake 
    getSinglebetStakeValuePath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#stakeInput")';
    }
    getSinglebetCardPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("paper-card")';
    }
    //singlebet progress 
    getSinglebetProgressPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("timed-paper-progress").shadowRoot.querySelector("#progress")';
    }
    //singlebet price 
    getSinglebetPricePath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("span#price")';
    }
    //singlebet price arrow
    getSinglebetPriceArrowPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("div#priceChangeStyle")';
    }
    //singlebet eventname 
    getSinglebetEventNamePath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("div.event")';
    }
    //singlebet Outcome Name
    getSinglebetOutcomeNamePath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("div.outcome")';
    }
    //singlebet error message 
    getSinglebetErrorMessagePath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#error_label")';
    }
    //singlebet suspended error message
    getSinglebetSuspendedErrorMessagePath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#error_label")';
    }
    //singlebet payout  
    getSinglebetPayoutPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#returns_label")';
    }
    //singlebet tax  
    getSinglebetTaxPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#tax_label")';
    }
    //singlebet remove button 
    getSinglebetRemoveButtonPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#btn_remove")';
    }
    //apply all stake 
    getApplyAllStakePath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("#stake_applyall")';
    }
    getApplyAllCardPath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("div.apply-all-singles")';
    }
    //other multiples
    getOtherMultiplesPath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("paper-collapse-item")';
    }
    //multiple name
    getMultiplesName(id) {
        if (id == 0) {
            return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("div.title-column")';
        }
        else {
            return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("div.title")';
        }
    }
    getMultiplesPayoutPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("#returns_label")';
    }
    getMultiplesTaxPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("#tax_label")';
    }
    getMultiplesStakePath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("#stakeInput").shadowRoot.querySelector("#nativeInput")';
    }
    getMultiplesCardPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + ']';
    }
    getMultipleProgressPath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("timed-paper-progress").shadowRoot.querySelector("#progress")';
    }
    //multi error message 
    getMultiplesErrorMessagePath(id) {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("div#error_label")';
    }
    getTotalBetCountPath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("span#totalbetcount")';
    }
    getTotalStakePath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("div#total-stake")';
    }
    getTotalStakeOnBetButtonPath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("#btnBet")';
    }
    getTotalPayoutPath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("div#payout_total")';
    }
    getTotalTaxPath() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("div#tax_total")';
    }
    //bet button path
    getBetButtonPath() {
        return '//*[@id="app"]/div[2]/div[2]/div/div[2]/button';
    }
    //bet count
    getBetCount() {
        return this.stackShadows(["bg-betslip-betcount"]) + '.querySelector("#bet-count")';
    }
    //remove all button
    getRemoveAllCount() {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("paper-button.separator__btn")';
    }
}
exports.MyElementHandler = MyElementHandler;
