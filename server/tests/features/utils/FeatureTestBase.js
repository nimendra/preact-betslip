"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor = require("protractor");
class Browser {
    get driver() { return Browser.driver; }
    get browser() { return new protractor.ProtractorBrowser(this.driver); }
}
Browser.driver = null;
exports.Browser = Browser;
class FeatureTestBase {
    constructor() {
        this.browsertype = process.env.BROWSER || "chrome";
        this.dom = process.env.DOM;
        this.port = 8080;
        //protected browser = new webdriver.Builder()
        //.withCapabilities({ browserName: this.browsertype }).build();
        //, chromeOptions: { args: ['--headless', '--disable-gpu'] } });  
        this.testUrl = (port, dom) => `https://localhost:${port}/`; //sample-pages/feature-tests/desktop-
    }
}
FeatureTestBase.browser = null;
FeatureTestBase.driver = null;
exports.FeatureTestBase = FeatureTestBase;
