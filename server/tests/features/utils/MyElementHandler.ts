﻿import { By, WebElementPromise, WebElement } from "selenium-webdriver";
import * as webdriver from "selenium-webdriver";

export class MyElementHandler {

    public driver;

    constructor(public webElement: WebElement, public browser, public supportshadow) {
        this.driver = browser;
    }

    public findElementByClassName(classname: string): WebElementPromise {
        return this.webElement.findElement(By.className(classname));
    }

    public findElementById(name: string): WebElementPromise {
        return this.webElement.findElement(By.id(name));
    }

    public findElementByXpath(xpathlink: string): WebElementPromise {
        return this.webElement.findElement(By.xpath(xpathlink));
    }

    public clickWhenElementExist(method: string, name: string): void {
        if (method == "Xpath") {
            this.findElementByXpath(name).then(function (webElement) {
                console.log('Element exists');
                if (webElement.isDisplayed()) {
                    webElement.click();
                }
            }, function (err) {
                console.log('Element not found');
            });
        } else if (method == "Id") {
            this.findElementById(name).then(function (webElement) {
                console.log('Element exists');
                if (webElement.isDisplayed()) {
                    webElement.click();
                }
            }, function (err) {
                console.log('Element not found');
            });
        } else if (method == "ClassName") {
            this.findElementByClassName(name).then(function (webElement) {
                console.log('Element exists');
                if (webElement.isDisplayed()) {
                    webElement.click();
                }
            }, function (err) {
                console.log('Element not found');
            });
        }
    }

    public getElementByXPath(xpath: string): WebElementPromise {
        return this.findElementByXpath(xpath);
    }

    public async getElementAndGetText(path: string) {
        var e2 = await this.driver.executeScript('return ' + path + ';');
        return e2.getText();
        // this.driver.executeScript("arguments[0].click();", e2);      
    }

    public async getElementAndGetValue(path: string) {
        var e2 = await this.driver.executeScript('return ' + path + '.value;');
        return e2;
        // this.driver.executeScript("arguments[0].click();", e2);      
    }

    public async getElementAndGetClass(path: string) {
        var e2 = await this.driver.executeScript('return ' + path + ';');
        return e2.getAttribute('class');
        // this.driver.executeScript("arguments[0].click();", e2);      
    }

    public async getElementAndGetAttribute(path: string, attribute: string) {
        var el = this.findElementByXpath(path);        
        return el.getAttribute(attribute);
    }

    public async getElementAndScrollThenClick(path: string) {
        var e2 = await this.driver.executeScript('return ' + path + ';');
        this.driver.executeScript("arguments[0].scrollIntoView(true);arguments[0].click();", e2);
    }

    public async getElementAndClick(path: string) {
        var el = this.findElementByXpath(path);
        el.click();
    }

    public async getElementAndClickNotScroll(path: string) {
        var e2 = await this.driver.executeScript('return ' + path + ';');
        e2.click();
    }

    public async getElement(path: string) {
        var e2 = await this.driver.executeScript('return ' + path + ';');
        return e2;
    }

    public async getElementAndSendKeys(path: string, keys: string) {
        var js = 'return ' + path + ';';
        //console.log("----------JS------------", js);
        var e =   await this.driver.executeScript(js);
        e.sendKeys(keys);
    }

    public async getElementAndClear(path: string) {
        var e2 = await this.driver.executeScript('return ' + path + ';');
        e2.clear();
    }

    public stackShadows(shadows: string[]): string {
        var ret = 'document';
        for (var i = 0; i < shadows.length; i++) {
            ret += '.querySelector("' + shadows[i] + '").shadowRoot';
        }
        return ret;
    }

    public async fireIronSignal(signal: Object, type: string) {
        await this.driver.executeScript('return document.dispatchEvent(new CustomEvent("iron-signal", { "detail": { name: "' + type + '", data: ['
            + JSON.stringify(signal)
            + ']}}));');
    }

    //sport button
    public getSportsButtonPath(eventId, outcomeId): string {
        return this.stackShadows(["bg-sports-nextn", "bg-sports-button#bet_btn_" + eventId + "_" + outcomeId]) + '.querySelector("button")';
    }

    //toast label
    public getToastLablePath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip", "bg-betslip-toast", "paper-toast"]) + '.querySelector("span#label")';
    }

    //toast button 
    public getToastButtonPath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip", "bg-betslip-toast"]) + '.querySelector("a#toastButton")';
    }

    //singlebet stake 
    public getSinglebetStakePath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#stakeInput").shadowRoot.querySelector("#nativeInput")';
    }

    //singlebet stake 
    public getSinglebetStakeValuePath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#stakeInput")';
    }

    public getSinglebetCardPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("paper-card")';
    }

    //singlebet progress 
    public getSinglebetProgressPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("timed-paper-progress").shadowRoot.querySelector("#progress")';
    }

    //singlebet price 
    public getSinglebetPricePath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("span#price")';
    }

    //singlebet price arrow
    public getSinglebetPriceArrowPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("div#priceChangeStyle")';
    }

    //singlebet eventname 
    public getSinglebetEventNamePath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("div.event")';
    }

    //singlebet Outcome Name
    public getSinglebetOutcomeNamePath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("div.outcome")';
    }

    //singlebet error message 
    public getSinglebetErrorMessagePath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#error_label")';
    }

    //singlebet suspended error message
    public getSinglebetSuspendedErrorMessagePath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#error_label")';
    }

    //singlebet payout  
    public getSinglebetPayoutPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#returns_label")';
    }

    //singlebet tax  
    public getSinglebetTaxPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#tax_label")';
    }

    //singlebet remove button 
    public getSinglebetRemoveButtonPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-singlebet")[' + id + '].shadowRoot' + '.querySelector("#btn_remove")';
    }

    //apply all stake 
    public getApplyAllStakePath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("#stake_applyall")';
    }

    public getApplyAllCardPath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("div.apply-all-singles")';
    }

    //other multiples
    public getOtherMultiplesPath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("paper-collapse-item")';
    }
    //multiple name
    public getMultiplesName(id): string {
        if (id == 0) {
            return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("div.title-column")';
        } else {
            return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("div.title")';
        }
    }

    public getMultiplesPayoutPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("#returns_label")';
    }

    public getMultiplesTaxPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("#tax_label")';
    }

    public getMultiplesStakePath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("#stakeInput").shadowRoot.querySelector("#nativeInput")';
    }

    public getMultiplesCardPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + ']';
    }

    public getMultipleProgressPath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("timed-paper-progress").shadowRoot.querySelector("#progress")';
    }

    //multi error message 
    public getMultiplesErrorMessagePath(id): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelectorAll("bg-betslip-multibet")[' + id + '].shadowRoot' + '.querySelector("div#error_label")';
    }

    public getTotalBetCountPath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("span#totalbetcount")';
    }

    public getTotalStakePath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("div#total-stake")';
    }

    public getTotalStakeOnBetButtonPath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("#btnBet")';
    }

    public getTotalPayoutPath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("div#payout_total")';
    }

    public getTotalTaxPath(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("div#tax_total")';
    }

    //bet button path
    public getBetButtonPath(): string {
        return '//*[@id="app"]/div[2]/div[2]/div/div[2]/button';
    }

    //bet count
    public getBetCount(): string {
        return this.stackShadows(["bg-betslip-betcount"]) + '.querySelector("#bet-count")';
    }

    //remove all button
    public getRemoveAllCount(): string {
        return this.stackShadows(["bg-betslip-app", "bg-betslip"]) + '.querySelector("paper-button.separator__btn")';
    }
}
