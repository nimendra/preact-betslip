﻿import { binding, given, then, before, after } from "cucumber-tsflow";
import * as webdriver from "selenium-webdriver";
import * as protractor from "protractor";

import { MyElementHandler } from "../utils/MyElementHandler";

export class Browser {
    static driver = null;

    get driver(): any { return Browser.driver; }
    get browser(): any { return new protractor.ProtractorBrowser(this.driver); }
}

export class FeatureTestBase {
    protected browsertype = process.env.BROWSER || "chrome";
    protected dom: string = process.env.DOM;
    static browser = null;
    static driver = null;

    protected port: number = 8080;
    static myElementHandler;

    //protected browser = new webdriver.Builder()
    //.withCapabilities({ browserName: this.browsertype }).build();
    //, chromeOptions: { args: ['--headless', '--disable-gpu'] } });  

    protected testUrl = (port: number, dom: string) => `https://localhost:${port}/`; //sample-pages/feature-tests/desktop-
}