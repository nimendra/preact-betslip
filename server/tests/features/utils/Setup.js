﻿var WebServer = require("../../../servers/web/WebServer");
var APIServer = require("../../../servers/api/APIServer");
var MultiSuggestionAPIServer = require("../../../servers/multi-suggestion-api/APIServer");  
var reporter = require('cucumber-html-reporter');

var setUp = function () {

    var webServerPort = 8765,
        apiServerPort = 3000,
        multiSugestionApiServerPort = 3001,
        webServer = new WebServer.default(webServerPort, "../"),
        apiServer = new APIServer.default(apiServerPort),
        multiSugestionApiServer = new MultiSuggestionAPIServer.default(multiSugestionApiServerPort);

    this.setDefaultTimeout(60000);
    process.setMaxListeners(0);

    this.Before(function (scenario, callback) {
        callback();
    });

    this.After(function (scenario, callback) {
        callback();
    });

    this.BeforeFeature(function (event, callback) {
        callback();
    });

    this.AfterFeature(function (event, callback) {
        callback();
    });

};

module.exports = setUp;