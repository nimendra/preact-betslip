﻿Feature: SuspendBet
	Suspend Bets

Background: 
	Given user navigates to the sportsbook website
	Then user navigates to the betslip page
	Then clears the betslip page if any bets exists
	Then user navigates to the sports page

Scenario: Place bet button disabled when bets are suspended 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game for price 1.57
	Then clicks on the Betslip icon
	Then user recieves a bet update for the badminton game as suspended
	Then verify stake box is disabled on the betslip
	Then verify bet button is disabled
	Then verify the notification toaster appear with text "1 bet suspended"

Scenario: Verify all multi is disabled when 1 one of the bet is suspended
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user recieves a bet update for the badminton game as suspended
	Then verify stake box for the badminton game is disabled on the betslip
	Then verify the stake box for the multi is disabled
	Then verify apply all single box is disabled
	Then verify the notification toaster appear with text "1 bet suspended"
	Then verify bet button is enabled for placing the single bet for cricket game.

Scenario Outline: Verify the total payout return estimate, and bet count when the bet is suspended
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game for price 1.57
	Then user clicks on the first price market for cricket game for price 2.50
	Then clicks on the Betslip icon
	Then user enters the stake amount<amount> for badminton game
	Then user enters the stake amount<amount> for cricket game
	Then user recieves a bet update for the badminton game as suspended
	Then verify stake box for the badminton game is disabled on the betslip
	Then verify the stake box for the multi is disabled
	Then verify apply all single box is disabled
	Then verify bet button is enabled for placing the single bet for cricket game.
	Then verify the notification toaster appear with text "1 bet suspended"
	Then verify the total stake<totalstake> is correct
	Then verify the estimate return<estimatereturn> is correct
	Then verify the bet count is 1.

Examples: 
	| amount | totalstake | estimatereturn |
	| 10     | 10.00      | 25.00          |


Scenario Outline: Verify after removing the suspended bet user is able to place all available bets
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then clicks on the Betslip icon
	Then verify 3 leg multi is suggested
	Then user enters the stake amount<amount> for badminton game
	Then user enters the stake amount<amount> for cricket game
	Then user enters the stake amount<amount> for baseball game
	Then user recieves a bet update for the badminton game as suspended
	Then user removes the suspended bet
	Then verify 2 leg multi is suggested
	Then user clicks on Bet button
	Then user receives notification toaster with text "2 bets placed."

Examples: 
	| amount |
	| 10     |

Scenario: Verify the mutli bet does not get enabled if one of the additional leg is added after one of the bet on the betslip is suspended.
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user recieves a bet update for the badminton game as suspended
	Then user navigates to the sports page
	Then user clicks on the first price market for baseball game
	Then verify stake box for the badminton game is disabled on the betslip
	Then verify the stake box for the multi is disabled
	Then verify apply all single box is disabled
	Then verify 3 leg multi is suggested and disabled

@TB-56
Scenario Outline: Show Keep Bets and notification toast when a single bet is placed and other bet recieves a suspend bet message
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user enters the stake value in the badminton game box for £<amount>
	Then user enters the stake value in the cricket game box for £<suspendBetAmount>
	Then user clicks on Bet button	 
	Then user receives a suspend error for the cricket game
	Then user receives notification toaster saying "1 bet placed. 1 bet failed."
	Then the user sees the Keep Bets button on the notificaiton toaster

Examples: 
	| amount |suspendBetAmount|
	| 10     |2001            |

@TB-333
Scenario: Show Ok Button and notification toast when a single bet recieves a suspend bet message 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user receives a suspend error for the cricket game
	Then user receives notification toaster saying "1 bet placed. 1 bet failed."
	Then the user sees the Ok button on the notificaiton toaster

@TB-56
Scenario Outline: Show Keep Bets and notification toast when a single and mulit bet is placed and other bet recieves a suspend bet message
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user enters the stake value in the badminton game box for £<validBetAmount>
	Then user enters the stake value in the cricket game box for £<suspendBetAmount>
	Then user enters the stake value in the <legCount>leg multi box for £<validBetAmount>
	Then user clicks on Bet button
	Then user receives a suspend error for the cricket game
	Then user receives notification toaster saying "1 bet placed. 2 bets failed."
	Then the user sees the Keep Bets button on the notificaiton toaster
	Then verify single stake box shows an error message as "Suspended"  
	Then verify multi stake box shows an error message as "Suspended"

Examples: 
	 | legCount | validBetAmount | suspendBetAmount | 
	 | 2        | 10             | 2001             |


@TB-56
Scenario Outline: Show Ok button and notification toast when a single and mulit bet is placed and other bet recieves a suspend bet message 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then user enters the stake value in the cricket game box for £<suspendBetAmount>
	Then user enters the stake value in the <legCount>leg multi box for £<validBetAmount>
	Then user clicks on Bet button
	Then user receives a suspend error for the cricket game
	Then user receives notification toaster saying "2 bets failed."
	Then the user sees the Ok button on the notificaiton toaster
	Then verify single stake box shows an error message as "Suspended"
	Then verify multi stake box shows an error message as "Suspended"

Examples: 
	 | legCount | validBetAmount | suspendBetAmount |
	 | 2        | 10             | 2001             |