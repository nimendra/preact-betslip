﻿Feature: BetDelay
	Calculate bet delay for the inplay bets

Background: 
	Given user navigates to the sportsbook website
	Then user navigates to the betslip page
	Then clears the betslip page if any bets exists
	Then user navigates to the sports page

Scenario: 5 sec bet delay for the in play game bet accepted
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the in play badminton game	
	Then user enters the stake<8000> in the betslip
	Then user clicks on Bet button
	Then verify the progress bar is displayed for 5sec
	Then verify the bet is placed "Bet placed. Receipt #"

Scenario: 0 sec bet delay for the pre match selection bet accepted
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the pre match cricket game	
	Then user enters the stake<5.0> in the betslip
	Then user clicks on Bet button
	Then verify no progress bar is displayed
	Then verify the no delay bet is placed "Bet placed. Receipt #"

Scenario: 5 sec bet delay for the in play game bet rejected
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the pre match cricket game		
	Then user enters the stake<8000> in the betslip
	Then user clicks on Bet button
	Then verify the progress bar is displayed for 8sec
	Then verify the bet is rejected "1 bet failed"

Scenario: 0 sec bet delay for the pre match selection bet rejected
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the pre match cricket game	
	Then user enters the stake<1009> in the betslip
	Then user clicks on Bet button
	Then verify no progress bar is displayed
	Then verify the bet is rejected "1 bet failed"

Scenario: Multi bet 5sec delay bet accepted
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the in play badminton game	
	Then user clicks on the first price market for the pre match cricket game	
	Then user enters the stake<8000> in the multistake box
	Then user clicks on Bet button
	Then verify the multi progress bar is displayed for 5sec
	Then verify the bet is placed "Bet placed. Receipt #"

Scenario: Multi bet 0sec delay bet accepted
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the pre match badminton game	
	Then user clicks on the first price market for the pre match cricket game	
	Then user enters the stake<5.0> in the multistake box
	Then user clicks on Bet button
	Then verify the no multi progress bar is displayed
	Then verify the no delay bet is placed "Bet placed. Receipt #"

Scenario: Multi bet 5sec delay bet accepted for both in play games
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the in play badminton game	
	Then user clicks on the first price market for the in play cricket game	
	Then user enters the stake<8000> in the multistake box
	Then user clicks on Bet button
	Then verify the multi progress bar is displayed for 5sec
	Then verify the bet is placed "Bet placed. Receipt #"

Scenario: Multi bet 5sec delay bet rejected
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the in play badminton game	
	Then user clicks on the first price market for the pre match baseball game	
	Then user enters the stake<8000> in the multistake box
	Then user clicks on Bet button
	Then verify the multi progress bar is displayed for 5sec
	Then verify the bet is rejected "1 bet failed"

Scenario: Multi bet 0sec delay bet rejected for both prematch game
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the pre match badminton game	
	Then user clicks on the first price market for the pre match baseball game	
	Then user enters the stake<1009> in the multistake box
	Then user clicks on Bet button
	Then verify the no multi progress bar is displayed
	Then verify the bet is rejected "1 bet failed"

Scenario: Multi bet 5sec delay bet rejected for both inplay games
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the in play badminton game	
	Then user clicks on the first price market for the in play baseball game	
	Then user enters the stake<8000> in the multistake box
	Then user clicks on Bet button
	Then verify the multi progress bar is displayed for 5sec
	Then verify the bet is rejected "1 bet failed"

Scenario: 1 Bet Placed, 1 Bet Failed for bet delay
	# Given the bdaminton game bet will be rejected and cricket game bet will be accepted
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the in play badminton game	
	Then user clicks on the first price market for the in play cricket game
	Then user enters the stake<8000> in the betslip	for the badminton game
	Then user enters the cricket stake<8000> in the betslip for the cricket game
	Then user clicks on Bet button
	Then verify delete button is not visible near the stake box while the progress bar is visible
	Then verify the progress bar is still displayed for 5sec
	Then verify user recives a notification toaster "Bet placed. Receipt #"

Scenario: Allow placing bet while some bets are in progress/delay
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for the in play badminton game	
	Then user clicks on the first price market for the in play cricket game
	Then user enters the stake<8000> in the betslip	for the badminton game
	Then user enters the cricket stake<8000> in the betslip for the cricket game
	Then user clicks on Bet button
	Then user clicks on the first price market for the in play soccer game	
	Then user enters the soccer stake<5.0> in the betslip for the soccer game
	Then user clicks on Bet button
	Then verify the progress bar is still displayed for 5sec
	#Then verify user recives a notification toaster "Bet placed. Receipt #"
	Then verify user recives a notification toaster "2 bets placed."
