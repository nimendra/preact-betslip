﻿Feature: MultiBet
	To Verify we can place multi bets

Background: 
	Given user navigates to the sportsbook website
	Then user navigates to the betslip page
	Then clears the betslip page if any bets exists
	Then user navigates to the sports page

@TB-54
Scenario Outline: 2leg straight multibet suggested and bet is placed
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then verify the Double bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £9
	Then user enters the stake value in the <legCount>leg multi box for £<amount>

	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"

    Examples:
    | legCount | amount |
    | 2        | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 3 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then clicks on the Betslip icon
	Then verify the Treble bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"

    Examples:
    | legCount | amount |
    | 3        | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 4 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"

    Examples:
    | legCount | amount |
    | 4        | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 5 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then user clicks on the first price market for aussie rules game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"
    
    Examples:
    | legCount | amount |
    | 5        | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 6 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then user clicks on the first price market for aussie rules game
	Then user clicks on the first price market for basketball game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"

    Examples:
    | legCount | amount |
    | 6        | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 7 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then user clicks on the first price market for aussie rules game
	Then user clicks on the first price market for basketball game
	Then user clicks on the first price market for beach volleyball game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"
    
    Examples:
    | legCount | amount |
    | 7        | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 8 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then user clicks on the first price market for aussie rules game
	Then user clicks on the first price market for basketball game
	Then user clicks on the first price market for beach volleyball game
	Then user clicks on the first price market for darts game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"
    
    Examples:
    | legCount | amount |
    | 8        | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 9 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then user clicks on the first price market for aussie rules game
	Then user clicks on the first price market for basketball game
	Then user clicks on the first price market for beach volleyball game
	Then user clicks on the first price market for darts game
	Then user clicks on the first price market for esports game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"
    
    Examples:
    | legCount | amount |
    | 9        | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 10 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then user clicks on the first price market for aussie rules game
	Then user clicks on the first price market for basketball game
	Then user clicks on the first price market for beach volleyball game
	Then user clicks on the first price market for darts game
	Then user clicks on the first price market for esports game
	Then user clicks on the first price market for football game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"
    
    Examples:
    | legCount | amount |
    | 10       | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 11 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then user clicks on the first price market for aussie rules game
	Then user clicks on the first price market for basketball game
	Then user clicks on the first price market for beach volleyball game
	Then user clicks on the first price market for darts game
	Then user clicks on the first price market for esports game
	Then user clicks on the first price market for football game
	Then user clicks on the first price market for handball game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"
    
    Examples:
    | legCount | amount |
    | 11       | 10     |

@TB-54
Scenario Outline: To verify different bet types when we place 12 different bets from different market
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then user clicks on the first price market for aussie rules game
	Then user clicks on the first price market for basketball game
	Then user clicks on the first price market for beach volleyball game
	Then user clicks on the first price market for darts game
	Then user clicks on the first price market for esports game
	Then user clicks on the first price market for football game
	Then user clicks on the first price market for handball game
	Then user clicks on the first price market for volleyball game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"
    
    Examples:
    | legCount | amount |
    | 12       | 10     |

@TB-54
Scenario Outline: Place single using apply all singles Box along with 2leg multi bet.
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then verify the Double bet is visible on the betslip page
	Then clicks on the Other Multiples drop down
	Then user enters the stake value in apply all single box for £<amount>
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the all bet are placed for the same £<amount> "3 bets placed"

    Examples:
    | legCount | amount |
    |  2       | 10     |

@TB-54
Scenario: Verify the stake count is correct when the one of the bet is not valid
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then clicks on the Betslip icon
	Then verify the Treble bet is visible on the betslip page
	Then user enters the stake value in the badminton stake box for £1
	Then user enters the stake value in the cricket stake box for £1
	Then user enters the stake value in the baseball stake box for £-1
	Then verify the total stake value is £2

@TB-54
Scenario: Verify the est return is correct when the one of the bet is not valid
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then clicks on the Betslip icon
	Then verify the Treble bet is visible on the betslip page
	Then user enters the stake value in the badminton stake box for £1
	Then user enters the stake value in the cricket stake box for £1
	Then user enters the stake value in the baseball stake box for £-1
	Then verify the total return value is £4.07
	Then verify the total stake value on the BET button is £2

@TB-54
Scenario: Verify the correct bets are placed
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then clicks on the Betslip icon
	Then verify the Treble bet is visible on the betslip page
	Then user enters the stake value in the badminton stake box for £1
	Then user enters the stake value in the cricket stake box for £1
	Then user enters the stake value in the baseball stake box for £-1
	Then user clicks on Bet button
	Then verify the all valid bets are placed for the same £1 "2 bets placed"
	Then verify the baseball game bet is visible on the betslip page with stake £0
	
@TB-54
Scenario Outline: Verify all single stake box becomes null or empty when one of the stake value is changed
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then verify the Double bet is visible on the betslip page
	Then clicks on the Other Multiples drop down
	Then user enters the stake value in apply all single box for £<amount>
	Then user enters the stake value in the cricket stake box for £1
	Then verify apply to single box value is 0 or empty   
    
    Examples:
    |amount|
    |5|

@TB-54	
Scenario: When we Place 1 single bet we do not see Apply All Singles Box
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then clicks on the Betslip icon
	Then verify the apply all singles box is not visible

@TB-54
Scenario: Verify the the correct bets are displayed after placing the correct bets.
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then user clicks on the first price market for baseball game
	Then user clicks on the first price market for american football game
	Then clicks on the Betslip icon
	Then verify the straight 4leg multi bet is visible on the betslip page
	Then user enters the stake value in the badminton stake box for £1
	Then user enters the stake value in the cricket stake box for £1
	Then user enters the stake value in the baseball stake box for £-1
	Then user enters the stake value in the american football stake box for £-1
	Then user clicks on Bet button
	Then verify all the valid bets are placed "2 bets placed"
	Then verify 2 invalid bets are shown in the betslip page
	Then verify Double bet is visible

@TB-173
Scenario: 2leg straight multibet conflict suggested
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for cricket game for side A to win
	Then user clicks on the first price market for cricket game for side B to win
	Then clicks on the Betslip icon
	Then clicks on the Other Multiples drop down
	Then verify "Multi conflict. Remove conflicted bets" message is shown 
	Then verify the indivdual selection for multi shows the "Multi Conflict"

@TB-173
Scenario: 2leg straight multibet conflict suggested with two different games
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for cricket game for side A to win
	Then user clicks on the first price market for cricket game for side B to win
	Then user clicks on the first price market for football game for side A to win
	Then clicks on the Betslip icon
	Then clicks on the Other Multiples drop down
	Then verify "Multi conflict. Remove conflicted bets" message is shown 
	Then verify the indivdual selection for multi shows the "Multi Conflict"

@TB-173
Scenario: 2leg straight multibet conflict suggested with two different games and removed when collapsed the errors are removed
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for cricket game for side A to win
	Then user clicks on the first price market for cricket game for side B to win
	Then user clicks on the first price market for football game for side A to win
	Then clicks on the Betslip icon
	Then clicks on the Other Multiples drop down
	Then verify "Multi conflict. Remove conflicted bets" message is shown 
	Then verify the indivdual selection for multi shows the "Multi Conflict"
	Then clicks on the Other Multiples drop down
	Then no error messages are shown

@TB-247
Scenario Outline: Suggesting Different Leg Multis 
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then the user adds required number of markets <Markets> by clicking different games
	Then clicks on the Betslip icon
	Then clicks on the Other Multiples drop down
	Then verify appropriate <Multis> for <Markets> markets are shown in the betslip page

       Examples: 
        | Markets | Multis                                                                |
        | 1       |                                                                       |
        | 2       | Double                                                                |
        | 3       | Treble, Doubles, Trixie, Patent                                       |
        | 4       | 4 Fold, Doubles, Trebles, Yankee, Lucky 15                            |
        | 6       | 6 Fold, Doubles, Trebles, 4 Folds, 5 Folds, Heinz, Lucky 63           |
        | 7       | 7 Fold, Doubles, Trebles, 4 Folds, 5 Folds, 6 Folds, Super Heinz      |
        | 8       | 8 Fold, Doubles, Trebles, 4 Folds, 5 Folds, 6 Folds, 7 Folds, Goliath |

@TB-247
Scenario Outline: Placing Different Multis
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then the user adds required number of markets <Markets> by clicking different games
	Then clicks on the Betslip icon	 
    Then user click on the multiples collapes for <Markets> markets    
    Then user enters single stake <Singlestake> for <Markets> markets	
    Then user enters double stake <Doublestake> for <Markets> markets	
    Then user enters treble stake <Treblestake> for <Markets> markets	
	Then user enters trixie stake <Trixiestake> for <Markets> markets	
	Then user enters patent stake <Patentstake> for <Markets> markets	
	Then user enters yankee stake <Yankeestake> for <Markets> markets	
	Then user enters lucky15 stake <Lucky15stake> for <Markets> markets	
	Then user enters super yankee stake <SuperYankeestake> for <Markets> markets	
	Then user enters lucky31 stake <Lucky31stake> for <Markets> markets	
	Then user enters fourfolds stake <FourFoldsstake> for <Markets> markets	
	Then user enters lucky63 stake <Lucky63stake> for <Markets> markets	
	Then user enters heinz stake <Heinzstake> for <Markets> markets	
	Then user enters five folds stake <FiveFoldsstake> for <Markets> markets	
	Then user enters super heinz stake <SuperHeinzstake> for <Markets> markets	
	Then user enters six fold stake <SixFoldsstake> for <Markets> markets	
	Then user enters goliath stake <Goliathstake> for <Markets> markets	
	Then user enters seven fold stake <SevenFoldsstake> for <Markets> markets	
    Then user enters eight fold stake <EightFoldsstake> for <Markets> markets	
	Then user enters nine fold stake <NineFoldsstake> for <Markets> markets	
	Then verify the stake <Count> is correct
	Then verify the return value <Value> for each multi is correct
	Then user clicks on Bet button 
	Then verify multi bet is placed "Bet placed. Receipt #" for <Count> bets
	Then verify user navigates to <Page>

	Examples: 
	| Markets | Singlestake | Doublestake | Treblestake | Trixiestake | Patentstake | Yankeestake | Lucky15stake | SuperYankeestake | Lucky31stake | FourFoldsstake | Lucky63stake | Heinzstake | FiveFoldsstake | SuperHeinzstake | SixFoldsstake | Goliathstake | SevenFoldsstake | EightFoldsstake | NineFoldsstake | Count | Value    | Page        |
	| 1       | 1           |             |             |             |             |             |              |                  |              |                |              |            |                |                 |               |              |                 |                 |                | 1     | 1.57     | Homepage    |
	| 2       |             | 1           |             |             |             |             |              |                  |              |                |              |            |                |                 |               |              |                 |                 |                | 1     | 3.93     | Homepage    |
	| 3       |             |             | 1           |             |             |             |              |                  |              |                |              |            |                |                 |               |              |                 |                 |                | 1     | 9.03     | Homepage    |
	| 3       |             |             |             | 1           |             |             |              |                  |              |                |              |            |                |                 |               |              |                 |                 |                | 1     | 22.32    | Homepage    |
	| 3       |             |             |             |             | 1           |             |              |                  |              |                |              |            |                |                 |               |              |                 |                 |                | 1     | 28.70    | Homepage    |
	| 4       |             |             |             |             |             | 1           |              |                  |              |                |              |            |                |                 |               |              |                 |                 |                | 1     | 62.48    | Homepage    |
	| 4       |             |             |             |             |             |             | 1            |                  |              |                |              |            |                |                 |               |              |                 |                 |                | 1     | 70.05    | Homepage    |
	| 5       |             |             |             |             |             |             |              | 1                |              |                |              |            |                |                 |               |              |                 |                 |                | 1     | 169.52   | Homepage    |
	| 5       |             |             |             |             |             |             |              |                  | 1            |                |              |            |                |                 |               |              |                 |                 |                | 1     | 179.18   | Homepage    |
	| 5       |             |             |             |             |             |             |              |                  |              | 1              |              |            |                |                 |               |              |                 |                 |                | 1     | 54.90    | Homepage    |
	| 6       |             |             |             |             |             |             |              |                  |              |                | 1            |            |                |                 |               |              |                 |                 |                | 1     | 647.64   | Homepage    |
	| 6       |             |             |             |             |             |             |              |                  |              |                |              | 1          |                |                 |               |              |                 |                 |                | 1     | 634.41   | Homepage    |
	| 6       |             |             |             |             |             |             |              |                  |              |                |              |            | 1              |                 |               |              |                 |                 |                | 1     | 162.12   | Homepage    |
	| 7       |             |             |             |             |             |             |              |                  |              |                |              |            |                | 1               |               |              |                 |                 |                | 1     | 1,522.80 | Homepage    |
	| 7       |             |             |             |             |             |             |              |                  |              |                |              |            |                |                 | 1             |              |                 |                 |                | 1     | 270.76   | Homepage    |
	| 8       |             |             |             |             |             |             |              |                  |              |                |              |            |                |                 |               | 1            |                 |                 |                | 1     | 3,388.84 | Homepage    |
	| 8       |             |             |             |             |             |             |              |                  |              |                |              |            |                |                 |               |              | 1               |                 |                | 1     | 398.72   | Homepage    |
	| 9       |             |             |             |             |             |             |              |                  |              |                |              |            |                |                 |               |              |                 |                 | 1              | 1     | 130.97   | Homepage    |
	| 9       | 1           | 1           | 1           |             |             |             |              |                  |              | 1              |              |            | 1              |                 | 1             |              | 1               | 1               | 1              | 9     | 8,710.38 | Homepage    |
	| 4       | 1           |             |             |             |             | 1           |              |                  |              |                |              |            |                |                 |               |              |                 |                 |                | 2     | 64.05    | Betslippage |
	| 4       |             | 1           |             |             |             | 1           |              |                  |              |                |              |            |                |                 |               |              |                 |                 |                | 2     | 84.68    | Homepage    |

@TB-247
Scenario Outline: Keep Bets shows the bets placed in the betslip again
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then user clicks on the first price market for badminton game	
	Then user clicks on the first price market for cricket game
	Then clicks on the Betslip icon
	Then verify the straight <legCount>leg multi bet is visible on the betslip page
	Then user enters the stake value in the <legCount>leg multi box for £<amount>
	Then user clicks on Bet button
	Then verify the straight <legCount>leg multi bet is placed "Bet placed. Receipt #"
	Then the user clicks on Keep Bets button
	Then verify the bet placed is visible on the betslip page

    Examples:
    | legCount | amount |
    | 2        | 10     |

@TB-247
Scenario: Max Limits for Markets
	# Given user is not logged in
	# Then user clicks on Login link
	# Then user provide the email address and password and click on Login Button
	Then the user adds required number of markets 16 by clicking different games
	Then verify "Number of Singles Limit Reached" message is shown on the homepage