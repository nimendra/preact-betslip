﻿exports.config = {
  // set to "custom" instead of cucumber.
  framework: 'custom',
  restartBrowserBetweenTests: false,
  // path relative to the current config file
  frameworkPath: require.resolve('./node_modules/protractor-cucumber-framework'),

  // relevant cucumber command line options
  cucumberOpts: {
        require: [
          conf.paths.e2e + '/steps/**/*.steps.js',
          conf.paths.e2e + '/utils/SetUp.js'
        ],
        format: 'pretty'
      }
};