﻿"use strict";
var reporter = require('cucumber-html-reporter');
var find = require('find');
var fs = require('fs-extra');
var outputDirectory = 'tests/report/';
function removeReports() {
    var files = find.fileSync(/\.html/, outputDirectory);
    files.map(function (file) {
        fs.unlinkSync(file);
    });
}
function generateReport() {
    var options = {
        theme: 'bootstrap',
        jsonDir: outputDirectory,
        //jsonFile: 'tests/report/cucumber_report.json',
        output: outputDirectory + 'cucumber_report_snapshot.html',
        reportSuiteAsScenarios: false,
        launchReport: true,
        metadata: {
            "App Version": "0.3.2",
            "Test Environment": "STAGING",
            "Browser": "Chrome  54.0.2840.98",
            "Platform": "Windows 10",
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    };
    reporter.generate(options);

}
removeReports();
generateReport();