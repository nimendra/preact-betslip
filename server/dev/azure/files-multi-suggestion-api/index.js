var APIServer = require("./server/servers/multi-suggestion-api/APIServer"),
    apiServerPort = process.env.PORT || 1337,
    apiServer = new APIServer.default(apiServerPort);

apiServer.start();
