#!/bin/bash

# Check if below four variable values are correct for your environment.
sourcePath="/c/Projects/bg-betting-components/" # Source path of bg-betting-components project
azureRemoteName="origin" # git remote -v
module="API"    # Web / API

declare -a apiItems=(
    "server/servers/api/data"
    "server/servers/api/routes"
    "server/servers/api/APIServer.js"
    "server/servers/api/App.js"
)

function getLatest {
    echo -e "\n### Getting latest from Azure repository..."
    git stash
    git pull ${azureRemoteName} master
}

function copyItems {
    echo -e "\n### Copying code from source..."
	
	targetPath=$(pwd)
	cd ${sourcePath}
	
    for item in "${apiItems[@]}"
    do
        echo "${item}..."
        cp --parents -rf ./${item} ${targetPath}
    done
	
	cd ${targetPath}
    echo -e "Copying complete."
}

function commitAndPush {
    echo -e "\n### Pushing latest changes to Azure..."
    git add -A
    git commit -m "Added latest code to Azure - ${module}"
    git push ${azureRemoteName} master
    echo -e "\nDone!"
}

function startCopy {
    echo -e "\nPublishing module : ${module}" 
    echo "Source repository path : ${sourcePath}" 
    echo "Azure repository remote name : ${azureRemoteName}"
    getLatest
    copyItems
    commitAndPush
}

startCopy
