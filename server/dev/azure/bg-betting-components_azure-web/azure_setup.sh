#!/bin/bash

echo -e "\n### Initializing Git repository..."
git init

echo -e "\n### Setting up remote..."
git remote add origin https://tbs-dev@bg-betting-components-web.scm.azurewebsites.net:443/bg-betting-components-web.git

echo -e "\n### Getting latest code..."
git pull origin master
