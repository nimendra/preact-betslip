var WebServer = require("./server/servers/web/WebServer"),
webServerPort = process.env.PORT || 1337,
webServerRootPath = "./server/temp/build/es5-unbundled"
webServer = new WebServer.default(webServerPort, webServerRootPath);

webServer.start();
