var APIServer = require("./server/servers/api/APIServer"),
    apiServerPort = process.env.PORT || 1337,
    apiServer = new APIServer.default(apiServerPort);

apiServer.start();
