#!/bin/bash

echo -e "\n### Initializing Git repository..."
git init

echo -e "\n### Setting up remote..."
git remote add origin https://tbs-dev@bg-betting-components-multi-suggestion-api.scm.azurewebsites.net:443/bg-betting-components-multi-suggestion-api.git

echo -e "\n### Getting latest code..."
git pull origin master
