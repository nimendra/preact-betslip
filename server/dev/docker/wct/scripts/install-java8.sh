
#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive
  apt-get install -y software-properties-common python-software-properties
  add-apt-repository ppa:webupd8team/java -y
  apt-get -y update

echo debconf shared/accepted-oracle-license-v1-1 select true | \
  sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | \
  sudo debconf-set-selections

  apt-get install -y oracle-java8-installer -y
