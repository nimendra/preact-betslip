echo "Invoking Polymer build...";

tempDirectory=${1}
currentDirectory=$(pwd)

rm -rf ${tempDirectory}
mkdir -p ${tempDirectory}

cd ..
cp --parents -rf "client/source" server/${tempDirectory} 
cp --parents -rf "server/public" server/${tempDirectory} 
cp "server/polymer.json" server/${tempDirectory} 

cd server/${tempDirectory}

polymer build
rm -rf "client"
rm -rf "server"
rm -f "polymer.json"

cd ${currentDirectory}