﻿#!/usr/bin/env bash
#set -e;
export PATH=$PATH:$(npm bin):$(npm bin)/../node
tempDirectory="temp"

./dev/scripts/npm/polymer_build.sh ${tempDirectory}

webServerPort=9876
webServerRootPath="./${tempDirectory}/build/es5-unbundled"
url="http://localhost:${webServerPort}/"

node ./servers/serve_api.js ${webServerPort} &
node ./servers/serve_multi_suggestion_api.js ${webServerPort} &
node ./servers/open_url.js ${url} &
node ./servers/serve_web.js ${webServerPort} ${webServerRootPath}
