﻿#!/usr/bin/env bash
#set -e;
export PATH=$PATH:$(npm bin):$(npm bin)/../node

webServerPort=9876
webServerRootPath="../"
url="http://localhost:${webServerPort}/"

node ./servers/serve_api.js ${webServerPort} &
node ./servers/serve_multi_suggestion_api.js ${webServerPort} &
node ./servers/open_url.js ${url} &
node ./servers/serve_web.js ${webServerPort} ${webServerRootPath}
