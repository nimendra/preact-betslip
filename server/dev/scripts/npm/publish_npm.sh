﻿#!/bin/bash
tempDirectory="temp"
outputDirectory="${tempDirectory}/publish/bg-betting-components"
module=${1,,} 
confirmationDelay=10

function resolveInputs {

    declare -a modules=(
        "betslip" 
        "bethistory"
        "all"
    )

    case ${module} in
        ${modules[0]})      
                publishVersionDirectory="betslip"
                ;;
        ${modules[1]})   
                publishVersionDirectory="bethistory"
                ;;
        ${modules[2]})   
                publishVersionDirectory="all"
                ;;
        *)      
                echo -e "\nERROR:\n======" 
                echo -e " Invalid publishing module(s) '${module}'\n"
                echo " Usage  : npm run publish MODULE_NAME"
                echo " Modules: ${modules[0]}, ${modules[1]}, ${modules[2]}"
                exit 0
                ;;
    esac

    publishVersionPackageJsonFile="../client/version/${publishVersionDirectory}/package.json"
    publishVersion=$(node -pe "require('${publishVersionPackageJsonFile}').version")
}

function confirmPublish {
    echo -e "\n### Starting publish in ${confirmationDelay} seconds." 
    echo -e "\tPublishing module(s): ${module}"
    echo -e "\tPackage version: ${publishVersion}"
    echo "Press Ctrl-C to abort"
    read -t ${confirmationDelay}
}

function closureCompile {
    rm -rf ${tempDirectory}
    gulp closure --closure_module ${module}
}

function prepareOutput {
    mkdir -p ${outputDirectory}

    declare -a copyItems=(
        "${tempDirectory}/client/source/*" 
        "${publishVersionPackageJsonFile}" 
        "../client/.npmrc"
    )

    echo -e "\n### Copying items..."
    for item in "${copyItems[@]}"
        do
            echo "${item}"
            cp -rf ${item} ${outputDirectory}
        done
}

function publish {
    echo -e "\n### Publishing..."
    cd ${outputDirectory}
    #npm pack
    npm publish
    echo -e "\nDone!"
}

resolveInputs
confirmPublish
closureCompile
prepareOutput
publish
