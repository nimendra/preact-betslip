﻿#!/usr/bin/env bash
set -e;
export PATH=$PATH:$(npm bin);
export BROWSER=$2
export DOM=$3

echo "Compiling TypeScript files in tests/features";
tsc -p tests/features/;

echo "Running CucumberJs";

selectedFeature="./tests/";
tagName="";

if [[ $1 ]] && [[ ${1} != "all" ]]; then
    selectedFeature+="features/";
    OLDIFS=$IFS;

    if [[ $1 == *"@"* ]]; then
        IFS='@';
    else
        IFS=':';
    fi
    
    structure=( $1 );
    selectedFeature+=${structure[0]}".feature";
    
    if [[ ${structure[1]} ]]; then
        if [[ $IFS == '@' ]]; then
            tagName+="--tags @"${structure[1]};
        else
            selectedFeature+=":"${structure[1]};
        fi
    fi

    IFS=$OLDIFS;
fi

cucumberjs $selectedFeature $tagName --tags ~@outofscope --tags ~@clarificationpending -f json:tests/report/cucumber_report.json;