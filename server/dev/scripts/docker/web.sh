﻿#!/usr/bin/env bash
#set -e;
export PATH=$PATH:$(npm bin):$(npm bin)/../node

webServerPort=9876
webServerRootPath="./"

node ./servers/serve_web.js ${webServerPort} ${webServerRootPath}
