﻿#!/usr/bin/env bash
#set -e;
export PATH=$PATH:$(npm bin):$(npm bin)/../node

webServerCorsPort=9876
node ./servers/serve_multi_suggestion_api.js ${webServerCorsPort}