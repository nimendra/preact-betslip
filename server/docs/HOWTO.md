# BG-BETTING-COMPONENTS &trade;

[![Build Status](http://vld-bsbuild02.betgenius.net:8080/buildStatus/icon?job=TradingPlatform-Frontend)](http://vld-bsbuild02.betgenius.net:8080/job/TradingPlatform-Frontend)

_bg-betting-components_&trade; are a set of components that can be used in a web application to compose a betting slip. They provide functionality to store and submit a set of bets to a betting api. The host element is `bg-betlip-app`. It's made up of:

  - Betslip container `bg-betslip`
  - Single Bet components `bg-betslip-singlebet`
  - All-up or Multi bet components `bg-betslip-multibet`. These also support system bets and parlays.
  - And helper components that deal with state management, data storage, ajax calls etc.

# Adding a `bg-betslip` to your web application

Adding a betslip component to your web page is quite straightforward.  
The GeniusBet betslip components are hosted in an `npm` repository. Client applications can simply install them  via `npm`.

#### Bring the element in
`bg-betting-components` requires [Node.js](https://nodejs.org/) v6+ to run.

Assuming your web application is in a folder called *my-app*, and using Visual Studio Code (or your favourite editor)

```sh
$ cd my-app
$ npm install bg-betting-components --registry=http://nexus.geniussports.net/repository/GeniusNpmGroup/
$ code index.html
```
Add a reference to the `bg-betslip-app` Polymer component to the `<head>` of your page.

```
   <link rel="import" href="node_modules/bg-betting-components/betslip/bg-betslip-app/bg-betslip-app.html">
```

#### Add the element to your page
Next, add an instance of the component to the page's HTML.

```
 <bg-betslip-app api-uri="//bg-betting-components-api.azurewebsites.net/api/v1/" progress-api-uri="http://127.0.0.1:3001/api/v1/"
                    currency-symbol="&euro;" unresolved>
 </bg-betslip-app>
```
>Note: the `api-uri` mentioned here is a mock api. In production, update the uri to your production betting-api url.

That's it! you are all set to start using the betslip functionality now.
It would be a good idea to run your application now, using your favourite http-server. For example, using `static-server`

```
$ npm i -g static-server
$ static-server
```

# Adding Bets to the slip
The betslip accepts bets through a standard **CustomEvent** interface. 
You can submit a single bet or a set of bets to the betslip.  
The simplest form of submitting a single bet is shown below:

```
let _bet = {
            betInfo: "89403|Arsenal v Leicester|0|1|Arsenal|1.57142857||100|WIN|0|Arsenal v Leicester|263490|0|0|true|false|false|true|false|0|100|0|0"
            };

document.dispatchEvent(new CustomEvent('iron-signal', 
           {
                detail: { 
                    name: "addbet",
                    data: {
                        bet:_bet
                    }
                }
            }));
```
  
> [Deprecation warning] Version 1.0 of the betslip Public API was based on iron-signals. For backwards-compatibility that interface is maintained. 
> The new api accepts bets in the form `new CustomEvent('add-bets'....)`
> It is recommended that you switch to the new event contract.


### Appendix

The public api definition for the betting components are best documented through the Polymer api-docs hosted in the component repository. Once you `http` serve the bg-betting-components source code, the documentation will be accessible at

[http://localhost:9080/source/betslip/docs.html](http://localhost:9080/source/betslip/docs.html)

| Signal | README | Example |
| ------ | ------ | ------- |
| add-bets | Adds one or more bets to slip | `{betInfo: "xxx...xxx" }` |
| iron-signal { name:'addbet'} | Deprecated, same as above | `detail:{name:"addbet", data : {betInfo: "xxx...xxx" } }` |


