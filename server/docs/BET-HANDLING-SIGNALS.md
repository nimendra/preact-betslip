# Handling Bets in Betslip with Custom Events

- Run `npm run serve` / `npm run serve-linux` 
- On the opened page, go to the browser console 
- Use below commands as necessary to test bet functionality

### Adding a bet (Legacy)
```
var _bet = {               
                betInfo: "89403|Arsenal v Leicester|0|1|Arsenal|1.57142857||100|WIN|0|0|Arsenal v Leicester|263490|0|0|true|false|false|true|false|0|100|0|0"
            };

document.dispatchEvent(new CustomEvent('iron-signal', 
           {
                detail: { 
                    name: "addbet",
                    data: {
                        bet:_bet
                    }
                }
            }));
```

### Adding a bet / a list of bets (New)
```
var _bet = {               
                betInfo: "89403|Arsenal v Leicester|0|1|Arsenal|1.57142857||100|WIN|0|0|Arsenal v Leicester|263490|0|0|true|false|false|true|false|0|100|0|0"
            };

document.dispatchEvent(new CustomEvent('iron-signal', {
                bubbles: true,
                composed: true,
                detail: {
                    name: "add-bets",
                    data: [_bet]
                }
}));
```

### Removing a bet
```
document.dispatchEvent(new CustomEvent('remove-bet', {
                bubbles: true,
                composed: true,
                detail: {     
                  Guid: "76c78556-5540-4ce0-b73b-0fa555cddcbe"     
                }
}));
```

### Remove all bets
```
document.dispatchEvent(new CustomEvent('clear-bets', {
                bubbles: true,
                composed: true,
                detail: { 
                }
}));
```

### Updating bet price
``` 
var signal = {
            selectionId: "89403_1",
            price: {
                decimal: 3.5
            }
        };

document.dispatchEvent(new CustomEvent('iron-signal', {             
            detail: {
                name: "selection-update",
                data: [signal]
            }
        }));
```

### Suspending a bet 
```
var signal = {
            selectionId: "89403_1",
            price: {
                decimal: 3.5
            }, 
            suspended: true
        };

document.dispatchEvent(new CustomEvent('iron-signal', {             
            detail: {
                name: "selection-update",
                data: [signal]
            }
}));
```

### Unsuspending a bet
```
var signal = {
            selectionId: "89403_1",
            price: {
                decimal: 3.5
            },
            suspended: false
        };

document.dispatchEvent(new CustomEvent('iron-signal', {             
            detail: {
                name: "selection-update",
                data: [signal]
            }
}));
```

### bet count details
```
document.dispatchEvent(new CustomEvent('bet-count-changed', {
                bubbles: true,
                composed: true,
                detail: {
                    name: "bet-count-changed",                     
                    singleBetsCount: 2,
                    singleBetsWithStakeCount: 1,
                    multiBetsCount: 1,
                    multiBetsWithStakeCount: 0,
                    parlaysCount:1,
                    parlaysWithStakeCount:0,
                    arlayLegCount:1,
                    parlaysWithStakeLegCount:0

                }
}));
```

OR

```
document.dispatchEvent(new CustomEvent("geniussportsmessagebus", 
    {
        detail: {
            "routing": "",
            "type": "bet-count-changed",
            "body": {
                detail:
                {   
                    name: "bet-count-changed", 
                    singleBetsCount: 2, 
                    singleBetsWithStakeCount: 1,
                    multiBetsCount: 1, 
                    multiBetsWithStakeCount: 0, 
                    parlaysCount:1, 
                    parlaysWithStakeCount:0, 
                    parlayLegCount:1, 
                    parlaysWithStakeLegCount:0 
                }
            }
        }
    }));
```

### bet placed details
```
document.dispatchEvent(new CustomEvent('bet-placed', {
                bubbles: true,
                composed: true,
                detail: { 
                }
}));
```
## betInfo object format 

```
 "BetInfo": "{EventId}|{EventName}|{EventClassId}|{OutcomeId}|{OutcomeName}|{Price}|{EventClass}|{MarketTypeCode}|{BetDetailTypeCode}|{Points}|{GroupingId}|{MarketDesc}|{FixedMarketId}|{MasterEventClassId}|{CategoryClassId}|{IsCashoutAllowed}|{GroupByHeader}|{GroupByCode}|{IsOpenForBetting}|{IsEventStarted}|{MasterEventId}|{EventTypeId}|{MasterCategoryId}|{CategoryId}"

 "BetInfo": "22160|Team A vs Team B - H2H|2549|2|Team B|2.10000000||WIN|WIN|0.00|0|Win|90882|0|0|False|Team A vs Team B - H2H|22160WIN|True|False|5133|500|28|38"  
 ```

## Requesting current betslip status  
 ```
document.dispatchEvent(new CustomEvent('iron-signal', {
                bubbles: true,
                composed: true,
                detail: {
                    name: "get-current-state",
                    data: []
                }
}));
 ```