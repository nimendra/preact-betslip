# How to use components 

##### bg-betslip-openbets
* [For Openbets]  
  <bg-betslip-openbets api-uri="http://127.0.0.1:3000/api/v1/" currency-symbol="�"></bg-betslip-openbets>


* [For Cashout]  
  <bg-betslip-openbets api-uri="http://127.0.0.1:3000/api/v1/" currency-symbol="�" is-cashout-tab></bg-betslip-openbets>

    * api-uri is the api url to get openbets data ( Eg: http://127.0.0.1:3000/api/v1/bethistory?filter=open&pagesize=20&page=1)
    * is-cashout-tab to show cashout bets only
    * currency-symbol to show which currency to use