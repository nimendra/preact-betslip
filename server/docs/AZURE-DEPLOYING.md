# Publishing API and Web Projects to Azure

This document describes how to setup `bg-betting-components-web` and `bg-betting-components-api` projects locally, which are hosted as local Git repositories in Azure. Once setup is complete, changes made to these projects can be published to Azure App services through each commit.

Refer to `Azure Node.js Hosting.docx` for details on how above projects have been deployed to Azure as Node JS applications.

## API Project
### Setting up
* Copy the `bg-betting-components/dev/azure/bg-betting-components_azure-api` directory content (2 .sh files) to a new directory, for example `bg-bc-azure-api`
* Update the `sourcePath` variable in `bg-bc-azure-api/azure_deploy.sh` file to reflect your local path of `bg-betting-components` project
* Open bash at `bg-bc-azure-api` and execute:
 `./azure_setup.sh` when prompted, enter the git *username:tbs-dev, password:password123*
* This will bring the latest code to `bg-bc-azure-api` from Azure local Git repository
* You may remove the `bg-bc-azure-api/azure_setup.sh` file now

### Publishing latest to Azure
* Make sure `bg-betting-components` directory has the latest code
* Open bash at `bg-bc-azure-api` and execute:
 `./azure_deploy.sh`
* This will copy the latest API code from `bg-betting-components` to `bg-bc-azure-api`, commit and push the changes to Azure local Git repository
* Latest commit would trigger a build on Azure and new changes should be available at   
`http://bg-betting-components-api.azurewebsites.net`  
`http://bg-betting-components-web.azurewebsites.net/`  
within few minutes

## Web Project
Setting up and publishing the latest of Web project is same as the steps of API project. Just replace the word `api` with `web` in each step.
