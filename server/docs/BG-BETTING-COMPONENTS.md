﻿How to serve Platform Frontend Components Demos on your local machine
===================


**TL;DR** Using `[git](http://git-scm.com)` to clone the latest code, and `[Docker](http://docker.com)` to host a container, you can get a local demo of the Platform frontend components running in your PC or Mac in no time.

----------


Prerequisites
-------------

You would need `git` and `Docker` installed in your machine. Depending on the kind of OS you use, `git` may already be installed in your machine by default (ex: Mac OS).
Open a terminal window (or command line) and type `git` to find out if it's already installed.


> - Go to [git-scm.com](http://git-scm.com) and follow the instruction to install `git`.
> - Go to [docker.com](http://docker.com) and follow the instructions to install docker. The docker container will host both the components demo pages and mock data api.


Running the web application
-------------
Clone (get the latest code) the repository, install the dependancies and fire up the container in docker to get going.

> - mkdir `dev`
> -  cd `dev`
> - clone the development branch `git clone -b development git@gitlab.betgenius.net:tradingplatform/sample-element-build.git`
> this will get the code in to a folder called `sample-element-build`
> - Install dependancies `npm install`
> - Install `bower` to get the client-side libraries like `polymer` which is required for compiling and serving out the web application.
>`npm install -g bower` 
>in `linux` you may have to use `sudo npm install -g bower`
> - `bower install`
> - You are now ready to build the application
> On windows `npm run build-win`
> On MacOS/linux `npm run build`
> - To start the web application
> On windows `npm run start-win`
> On MacOS/linux `npm run start`
> 
> The web application along with the mock data api should now be running in your local environment. The web application loads up on port `9876` by default and the mock data api on port `3000`

#### Try it out
Open up your favourite browser and navigate to 
[http://localhost:9876/components/project/public/sample-pages/](http://localhost:9876/components/project/public/sample-pages/)

As new features are built, you can get the latest code over to your machine and re-build and re-load following these instructions. (You don't have to `clone` again, you simply have to `pull` the latest code over.

> - `git pull origin development`
> - On windows `npm run build-win`
>  On MacOS/linux `npm run build`

### Using Docker
> - docker build -t *your-username*/platformfe .
> - docker run -p 9876:9876 -p 3000:3000 -d *your-username*/platformfe

navigate to [http://localhost:9876/components/project/dev/sample-pages/index.html](http://localhost:9876/components/project/dev/sample-pages/index.html)