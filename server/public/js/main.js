var htmlEl = document.querySelector('html');

// Event Polyfill for IE11
(function () {
	if ( typeof window.CustomEvent === "function" ) return false; //If not IE

	function CustomEvent ( event, params ) {
		params = params || { bubbles: false, cancelable: false, detail: undefined };
		var evt = document.createEvent( 'CustomEvent' );
		evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
		return evt;
	}

	CustomEvent.prototype = window.Event.prototype;

	window.CustomEvent = CustomEvent;
})();

// MDC Fixed Toolbar
var toolbar = document.querySelector('.mdc-toolbar');
if (toolbar) {
	mdc.toolbar.MDCToolbar.attachTo(toolbar);
	toolbar.fixedAdjustElement = document.querySelector('.mdc-toolbar-fixed-adjust');
}

// MDC Drawer
var drawerEl = document.querySelector('.mdc-temporary-drawer');
if (drawerEl) {
	let drawer = new mdc.drawer.MDCTemporaryDrawer(drawerEl);
	document.querySelector('.menu').addEventListener('click', () => drawer.open = true);
}

// Snackbar

(function(global) {
  'use strict';
  var snackbarEl = document.getElementById('mdc-js-snackbar');

  if (snackbarEl) {
  	var MDCSnackbar = global.mdc.snackbar.MDCSnackbar;
  	var snackbar = new MDCSnackbar(snackbarEl);
  	var snackbars = document.getElementsByClassName('js-open-snackbar');
  	for (var i = 0; i < snackbars.length; i++) {
  		snackbars[i].addEventListener('click', openSnackBar, false);
  	}
  }

  function openSnackBar(el) {
  	var el = this;
  	const messageText = el.dataset.snackbarMessage;
  	const actionText = el.dataset.snackbarAction;
  	const timeout = el.dataset.snackbarTimeout;
  	const multiline = el.dataset.snackbarMultiline;
  	const actionOnBottom = el.dataset.snackbarActionOnBottom;
  	var actionHandlerFoo = el.dataset.snackbarHandler;

  	const dataObj = {
  		message: messageText,
  		actionText: actionText,
  		timeout: timeout,
  		multiline: multiline,
  		actionOnBottom: actionOnBottom,
  		actionHandler: function () {
  			eval(actionHandlerFoo);
  		}
  	};

  	snackbar.show(dataObj);
  }

})(this);

// Odds Toggle Button
var oddsButton = document.getElementsByClassName("c-odds-toggle-button");
var betAddedEvent = new CustomEvent("betAdded", {'bubbles': true});
var betRemovedEvent = new CustomEvent("betRemoved", {'bubbles': true});
var betRemoveAllEvent = new CustomEvent("betRemoveAll", {'bubbles': true});

var oddsButtonToggle = function() {
	if(this.classList.contains('is-active')) {
		this.classList.remove('is-active');
		document.dispatchEvent(betRemovedEvent);
	} else {
		this.classList.add('is-active');
		document.dispatchEvent(betAddedEvent);
	}
};

for (var i = 0; i < oddsButton.length; i++) {
	oddsButton[i].addEventListener('click', oddsButtonToggle, false);
}

function oddsButtonPriceChange(el, signal) {
	var button = el;
	var direction = signal;
	// Check to see if it already has flashing class
	if(button.classList.contains('is-price-up')) {
		button.classList.remove('is-price-up');
		// Reset the css animation //
		void button.offsetWidth;
	}
	if(button.classList.contains('is-price-down')) {
		button.classList.remove('is-price-down');
		// Reset the css animation //
		void button.offsetWidth;
	}
	// Change icon based on up or down signal
	if(direction == "up") {
		button.classList.add('is-price-up');
	} else {
		button.classList.add('is-price-down');
	}
	// Remove classes after animation is done
	button.addEventListener('animationend', function(e) {
		this.classList.remove('is-price-up', 'is-price-down');
	}, false);
}

// Icon Toggle Buttons
var iconToggleEl = document.querySelectorAll('.mdc-icon-toggle');
for (var i = 0, node; node = iconToggleEl[i]; i++) {
	mdc.iconToggle.MDCIconToggle.attachTo(node);
}

// Stop Mouse Wheel Propagation
var scrollEl = document.querySelectorAll('.js-stop-scroll-prop');
for (var i = 0, node; node = scrollEl[i]; i++) {
	var element = node;

	var isMacWebkit = (navigator.userAgent.indexOf("Macintosh") !== -1 && navigator.userAgent.indexOf("WebKit") !== -1);
	var isFirefox = (navigator.userAgent.indexOf("firefox") !== -1);

	// Register mousewheel event handlers.
	element.onwheel = wheelHandler;       // Future browsers
	element.onmousewheel = wheelHandler;  // Most current browsers
	if (isFirefox) {              // Firefox only
		element.scrollTop = 0;
		element.addEventListener("DOMMouseScroll", wheelHandler, false);
	}
	// prevent from scrolling parrent elements
	function wheelHandler(event) {
		var e = event || window.event;  // Standard or IE event object

		// Extract the amount of rotation from the event object, looking
		// for properties of a wheel event object, a mousewheel event object
		// (in both its 2D and 1D forms), and the Firefox DOMMouseScroll event.
		// Scale the deltas so that one "click" toward the screen is 30 pixels.
		// If future browsers fire both "wheel" and "mousewheel" for the same
		// event, we'll end up double-counting it here. Hopefully, however,
		// cancelling the wheel event will prevent generation of mousewheel.
		var deltaX = e.deltaX * -30 ||  // wheel event
		e.wheelDeltaX / 4 ||  // mousewheel
		0;    // property not defined
		var deltaY = e.deltaY * -30 ||  // wheel event
		e.wheelDeltaY / 4 ||  // mousewheel event in Webkit
		(e.wheelDeltaY === undefined &&      // if there is no 2D property then
			e.wheelDelta / 4) ||  // use the 1D wheel property
			e.detail * -10 ||  // Firefox DOMMouseScroll event
			0;     // property not defined

			// Most browsers generate one event with delta 120 per mousewheel click.
			// On Macs, however, the mousewheels seem to be velocity-sensitive and
			// the delta values are often larger multiples of 120, at
			// least with the Apple Mouse. Use browser-testing to defeat this.
			if (isMacWebkit) {
				deltaX /= 30;
				deltaY /= 30;
			}
			e.currentTarget.scrollTop -= deltaY;
			// If we ever get a mousewheel or wheel event in (a future version of)
			// Firefox, then we don't need DOMMouseScroll anymore.
			if (isFirefox && e.type !== "DOMMouseScroll")
				element.removeEventListener("DOMMouseScroll", wheelHandler, false);

			// Don't let this event bubble. Prevent any default action.
			// This stops the browser from using the mousewheel event to scroll
			// the document. Hopefully calling preventDefault() on a wheel event
			// will also prevent the generation of a mousewheel event for the
			// same rotation.
			if (e.preventDefault) e.preventDefault();
			if (e.stopPropagation) e.stopPropagation();
			e.cancelBubble = true;  // IE events
			e.returnValue = false;  // IE events
			return false;
		}
	}

	if (document.getElementById('js-bets-content-scroll')) {
		var element = document.getElementById('js-bets-content-scroll');

		var isMacWebkit = (navigator.userAgent.indexOf("Macintosh") !== -1 && navigator.userAgent.indexOf("WebKit") !== -1);
		var isFirefox = (navigator.userAgent.indexOf("firefox") !== -1);

		// Register mousewheel event handlers.
		element.onwheel = wheelHandler;       // Future browsers
		element.onmousewheel = wheelHandler;  // Most current browsers
		if (isFirefox) {              // Firefox only
			element.scrollTop = 0;
			element.addEventListener("DOMMouseScroll", wheelHandler, false);
		}
		// prevent from scrolling parrent elements
		function wheelHandler(event) {
			var e = event || window.event;  // Standard or IE event object

			// Extract the amount of rotation from the event object, looking
			// for properties of a wheel event object, a mousewheel event object
			// (in both its 2D and 1D forms), and the Firefox DOMMouseScroll event.
			// Scale the deltas so that one "click" toward the screen is 30 pixels.
			// If future browsers fire both "wheel" and "mousewheel" for the same
			// event, we'll end up double-counting it here. Hopefully, however,
			// cancelling the wheel event will prevent generation of mousewheel.
			var deltaX = e.deltaX * -30 ||  // wheel event
			e.wheelDeltaX / 4 ||  // mousewheel
			0;    // property not defined
			var deltaY = e.deltaY * -30 ||  // wheel event
			e.wheelDeltaY / 4 ||  // mousewheel event in Webkit
			(e.wheelDeltaY === undefined &&      // if there is no 2D property then
				e.wheelDelta / 4) ||  // use the 1D wheel property
				e.detail * -10 ||  // Firefox DOMMouseScroll event
				0;     // property not defined

				// Most browsers generate one event with delta 120 per mousewheel click.
				// On Macs, however, the mousewheels seem to be velocity-sensitive and
				// the delta values are often larger multiples of 120, at
				// least with the Apple Mouse. Use browser-testing to defeat this.
				if (isMacWebkit) {
					deltaX /= 30;
					deltaY /= 30;
				}
				e.currentTarget.scrollTop -= deltaY;
				// If we ever get a mousewheel or wheel event in (a future version of)
				// Firefox, then we don't need DOMMouseScroll anymore.
				if (isFirefox && e.type !== "DOMMouseScroll")
					element.removeEventListener("DOMMouseScroll", wheelHandler, false);

				// Don't let this event bubble. Prevent any default action.
				// This stops the browser from using the mousewheel event to scroll
				// the document. Hopefully calling preventDefault() on a wheel event
				// will also prevent the generation of a mousewheel event for the
				// same rotation.
				if (e.preventDefault) e.preventDefault();
				if (e.stopPropagation) e.stopPropagation();
				e.cancelBubble = true;  // IE events
				e.returnValue = false;  // IE events
				return false;
			}

		}


/**
* Collapsible
*/

function collapseSection(element) {
	// get the height of the element's inner content, regardless of its actual size
	var sectionHeight = element.scrollHeight;

	// temporarily disable all css transitions
	var elementTransition = element.style.transition;
	element.style.transition = '';

	// on the next frame (as soon as the previous style change has taken effect),
	// explicitly set the element's height to its current pixel height, so we
	// aren't transitioning out of 'auto'
	requestAnimationFrame(function () {
		element.style.height = sectionHeight + 'px';
		element.style.transition = elementTransition;

		// on the next frame (as soon as the previous style change has taken effect),
		// have the element transition to height: 0
		requestAnimationFrame(function () {
			element.style.height = 0 + 'px';
		});
	});

	// mark the section as "currently collapsed"
	element.setAttribute('data-collapsed', 'true');
}

function expandSection(element) {
	// get the height of the element's inner content, regardless of its actual size
	var sectionHeight = element.scrollHeight;

	// have the element transition to the height of its inner content
	element.style.height = sectionHeight + 'px';

	// when the next css transition finishes (which should be the one we just triggered)
	element.addEventListener('transitionend', function (e) {
		// remove this event listener so it only gets triggered once
		element.removeEventListener('transitionend', arguments.callee);

		// remove "height" from the element's inline styles, so it can return to its initial value
		element.style.height = null;
	});

	// mark the section as "currently not collapsed"
	element.setAttribute('data-collapsed', 'false');
}


function findAncestor (el, cls) {
	while ((el = el.parentElement) && !el.classList.contains(cls));
	return el;
}

function initCollapse(el) {

	const collapsible = document.getElementById(el);
	const collapseBtn = document.getElementById('toggleCollapse');
	const expandBtn = document.getElementById('toggleExpand');

	if(collapsible) {

		function init() {
			initEvents();
		}

		function initEvents() {

			var btns = collapsible.querySelectorAll('.jsCollapseBtn'),
			btnsArray = [].slice.call(btns);

			btnsArray.forEach(function (item) {
				item.addEventListener('click', toggleView, false);
			});
		}

		function toggleView() {

			this.classList.toggle('is-inactive');

			var parentContainer = findAncestor(this, 'jsCollapseItem'),
			collapsibleArea = parentContainer.querySelector('.is-collapsible'),
			isCollapsed 	= collapsibleArea.getAttribute('data-collapsed') === 'true';

			if (isCollapsed) {
				parentContainer.classList.remove('is-collapsed');
				expandSection(collapsibleArea);
				collapsibleArea.setAttribute('data-collapsed', 'false');
				if(collapseBtn !== null) {
					collapseBtn.style.display = 'initial';
					expandBtn.style.display = 'none';
				}
			} else {
				parentContainer.classList.add('is-collapsed');
				collapseSection(collapsibleArea);
				collapsibleArea.setAttribute('data-collapsed', 'true');
				if(collapseBtn !== null) {
					collapseBtn.style.display = 'none';
					expandBtn.style.display = 'initial';
				}
			}

		}

		init();

	}
}

initCollapse('jsMarketCards');
initCollapse('jsSportMenu');
initCollapse('jsPrematchSport');
initCollapse('jsLiveVision');
initCollapse('jsSideNav');

// only required for in-play menu prototype
initCollapse('jsSportMenu1');
initCollapse('jsSportMenu2');

/*
* Demo Ruler
*/
/* <div class="demo-ruler"><div id="js-demo-ruler"></div></div> */

(function(global) {
	'use strict';
	var ruler = document.querySelector('#js-demo-ruler');
	var update = function() {
		var size = '(phone)';
		if (window.innerWidth >= 840) {
			size = '(desktop)';
		} else if (window.innerWidth >= 480) {
			size = '(tablet)';
		}
		ruler.textContent = window.innerWidth + 'px ' + size;
	};
	if (ruler) {
		update();
		window.addEventListener('resize', update);
	}
})(this);

function hasSomeParentTheClass(element, classname) {
	// If we are here we didn't find the searched class in any parents node
	if (!element.parentNode) return false;
	// If the current node has the class return true, otherwise we will search
	// it in the parent node
	if (element.className.split(' ').indexOf(classname)>=0) return true;
	return hasSomeParentTheClass(element.parentNode, classname);
}

//Tabs

(function() {

	function initTabs(el) {

		var tabBarEl = document.querySelector('#' + el);

		// Dirty method, feel free to replace with eligant solution.

		if(hasSomeParentTheClass(tabBarEl, 'mdc-tab-bar-scroller')) {
			var scrollerEl = tabBarEl.closest('.mdc-tab-bar-scroller');
			var tabBarScroller = new mdc.tabs.MDCTabBarScroller(scrollerEl);
			var tabs = tabBarScroller.tabBar_;
		} else {
			var tabs = new mdc.tabs.MDCTabBar(tabBarEl);
		}

		var panels = document.querySelector("[for=" + el + "]");

		tabs.preventDefaultOnClick = true;

		function updatePanel(index) {

			var activePanel = panels.querySelector("[for=" + el + "] > .js-panel.is-active");

			if (activePanel) {
				activePanel.classList.remove("is-active");
			}
			var newActivePanel = panels.querySelector(
				"[for=" + el + "] > .js-panel:nth-child(" + (index + 1) + ")"
			);
			if (newActivePanel) {
				newActivePanel.classList.add("is-active");
			}
		}

		tabs.listen("MDCTabBar:change", function(t) {
			var tabs = t.detail;
			var nthChildIndex = tabs.activeTabIndex;
			updatePanel(nthChildIndex);
		});
	}

	// MDC Tabs
	var allTabs = document.getElementsByClassName("mdc-tab-bar");

	if (allTabs) {
		for (var i = 0; i < allTabs.length; i++) {
			var j = allTabs[i].id;
			initTabs(j);
		}
	}

})();

/* Grid list  */

function initCollapseAll(el) {

	const myPanel = document.getElementById(el);

	if( myPanel !== null ) {

		const collapseBtn = document.getElementById('toggleCollapse');
		const expandBtn = document.getElementById('toggleExpand');
		const collapsibles = myPanel.querySelectorAll('.is-collapsible');
		const collapsiblesArray = [].slice.call(collapsibles);

		function init() {
			initEvents();
		}

		function initEvents() {
			expandBtn.addEventListener('click', expandAll, false);
			collapseBtn.addEventListener('click', collapseAll, false);
		}

		function collapseAll() {
			collapsiblesArray.forEach(function (item) {
				var parent = findAncestor(item, 'jsCollapseItem');
				item.style.height = 0;
				item.setAttribute('data-collapsed', true);
				parent.classList.add('is-collapsed');
			});
			this.style.display = 'none';
			expandBtn.style.display = 'initial';
		}

		function expandAll() {
			collapsiblesArray.forEach(function (item) {
				var parent = findAncestor(item, 'jsCollapseItem');
				item.style.height = null;
				item.setAttribute('data-collapsed', 'false');
				parent.classList.remove('is-collapsed');
			});
			this.style.display = 'none';
			collapseBtn.style.display = 'initial';
		}

		init();

	}
}

initCollapseAll('jsPrematchSport');

// Bet Slip Button
var betSlipButton = document.querySelector(".c-bet-slip-button");
var betSlip = document.querySelector("bet-slip");
var betCounter = document.querySelector(".c-bet-slip-button__counter");

if (betSlipButton) {
	// Remove classes after animation is done
	betSlipButton.addEventListener('animationend', function(e) {
		this.classList.remove('is-counting');
	}, false);
}

// MDC Simple Menu

(function() {

	function initMenus(el) {

		var menuEl = document.querySelector('#' + el);
		var menuButton = document.querySelector("[for=" + el + "]");

		var menu = new mdc.menu.MDCSimpleMenu.attachTo(menuEl);
		if(menuButton) {
			menuButton.addEventListener('click', () => menu.open = !menu.open);
		}

	}

	var allMenus = document.getElementsByClassName("mdc-simple-menu");

	if (allMenus) {
		for (var i = 0; i < allMenus.length; i++) {
			var j = allMenus[i].id;
			initMenus(j);
		}
	}

})();

// Dialog

(() => {

	if (!document.querySelector('#my-mdc-dialog'))
		return;

	const d = document.querySelector('#my-mdc-dialog');
	const dialog = new mdc.dialog.MDCDialog(document.querySelector('#my-mdc-dialog'));
	const surface = d.querySelector('.mdc-dialog__surface');
	const activateBtns = document.querySelectorAll('.js-dialog-activation');
	let dialogSize;

	dialog.listen('MDCDialog:accept', function() {
		console.log('accepted');

		/* This is used to switch out the dialog content - presentational only */
		document.querySelector('.c-dialog-content.is-visible').classList.remove('is-visible');

	})

	dialog.listen('MDCDialog:cancel', function() {
		console.log('cancelled');

		/* This is used to switch out the dialog content - presentational only */
		document.querySelector('.c-dialog-content.is-visible').classList.remove('is-visible');

		if(dialogSize) {
			surface.classList.remove(`is-${dialogSize}`);
		}

	})

	/* Look for all the dialog activation buttons */

	for (const btn of activateBtns) {
		btn.addEventListener('click', function(evt) {

			dialog.lastFocusedTarget = evt.target;

			/* This is used to switch out the dialog content - presentational only */
			let dialogContent = `#dialog-${this.getAttribute('data-dialog')}`;
			dialogSize = this.getAttribute('data-dialog-size');
			document.querySelector(dialogContent).classList.add('is-visible');

			if(dialogSize != null) {
				surface.classList.add(`is-${dialogSize}`);
			}
			dialog.show();
		});
	}

})();

// Bet Slip Dialog

var betSlipDialog = document.querySelector('.c-bet-slip-full-screen-dialog');
var betSlipDialogCloseBtn = document.querySelector('.c-bet-slip-full-screen-dialog__close');

if (betSlipButton) {
	betSlipButton.addEventListener('click', betSlipDialogOpen, false);
}

if (betSlipDialogCloseBtn) {
	betSlipDialogCloseBtn.addEventListener('click', betSlipDialogClose, false);
}

function betSlipDialogOpen() {
	betSlipDialog.style.display = "block";
	htmlEl.classList.add('c-dialog-scroll-lock');
}

function betSlipDialogClose() {
	betSlipDialog.style.display = "none";
	htmlEl.classList.remove('c-dialog-scroll-lock');
}

// Betslip functions and events

function incrementBetCounter() {
	var value = parseInt(betCounter.innerHTML);
	value = isNaN(value) ? 0 : value;
	value+= 1;
	betCounter.innerHTML = value;
}

function decrementBetCounter() {
	var value = parseInt(betCounter.innerHTML);
	value = isNaN(value) ? 0 : value;
	value-= 1;
	betCounter.innerHTML = value;
}

document.addEventListener('betAdded', function (e) {
	if (betSlipButton) {
		betSlipButton.classList.add('is-counting');
		incrementBetCounter();
	}
	betSlip._addBet();
}, false);

document.addEventListener('betRemoved', function (e) {
	if (betSlipButton) {
		decrementBetCounter();
	}
	betSlip._removeBet();
}, false);

document.addEventListener('betRemoveAll', function (e) {
	if (betSlipButton) {
		betCounter.innerHTML = 0;
	}
	for (var i = 0; i < oddsButton.length; i++) {
		oddsButton[i].classList.remove('is-active');
	}
	if (betSlipDialog) {
		betSlipDialogClose();
	}
}, false);

// Cash Out (Bet Card)

var cashOutButtons = document.getElementsByClassName('js-cashout-button');
for (var i = 0; i < cashOutButtons.length; i++) {
	cashOutButtons[i].addEventListener('click', cashOut, false);
}

function cashOut() {
	var card = this.closest('.c-bet-card');
	if (card.parentElement.classList.contains("mdc-layout-grid__cell")) {
		card.parentElement.style.display="none";
	}
	card.style.display="none";
}