(function () {
    'use strict';
    window.addEventListener('WebComponentsReady', () => {

        class BgSportsNextn extends Polymer.Element {

            static get is() {
                return 'bg-sports-nextn';
            }

            static get properties() {
                return {
                    result: {
                        type: Object
                    },
                    apiUri: {
                        type: String
                    },
                    toolsOpen: {
                        type: Boolean,
                        notify: true,
                        value: false
                    }
                }
            }

            constructor() {
                super();
            }

            ready() {
                super.ready();
                this.$.sportsAjax.url = this.apiUri;
                this.$.sportsAjax.generateRequest();
            }

            onResponse(response) {
                var response = response.detail.response;
                this.result = response;
            }

            priceUpAthleticBilbao() {
                var self = this;
                //use this for predicatable feature test
                setTimeout(function () {
                    document.dispatchEvent(new CustomEvent('iron-signal', {
                        'detail': {
                            name: 'selection-update',
                            data: [{
                                selectionId: "89403_1",
                                price: {
                                    decimal: 3.5
                                }
                            }]
                        }
                    }));
                }, 3000);
            };

            priceDownWatford() {
                var self = this;
                //use this for predicatable feature test
                setTimeout(function () {
                    document.dispatchEvent(new CustomEvent('iron-signal', {
                        'detail': {
                            name: 'selection-update',
                            data: [{
                                selectionId: "89165_1",
                                price: {
                                    decimal: 1.5
                                }
                            }]
                        }
                    }));
                }, 3000);
            }

            suspendAthleticBilbao() {
                var self = this;
                //use this for predicatable feature test
                setTimeout(function () {
                    document.dispatchEvent(new CustomEvent('iron-signal', {
                        'detail': {
                            name: 'selection-update',
                            data: [{
                                selectionId: "89403_1",
                                price: {
                                    decimal: 1.57
                                },
                                suspended: true
                            }]
                        }
                    }));
                }, 3000);
            };

            unsuspendAthleticBilbao() {
                var self = this;
                //use this for predicatable feature test
                setTimeout(function () {
                    document.dispatchEvent(new CustomEvent('iron-signal', {
                        'detail': {
                            name: 'selection-update',
                            data: [{
                                selectionId: "89403_1",
                                price: {
                                    decimal: 1.57
                                },
                                suspended: false
                            }]
                        }
                    }));

                }, 3000);
            };

            suspendWatford() {
                var self = this;
                //use this for predicatable feature test
                setTimeout(function () {
                    document.dispatchEvent(new CustomEvent('iron-signal', {
                        'detail': {
                            name: 'selection-update',
                            data: [{
                                selectionId: "89165_1",
                                price: {
                                    decimal: 2.50
                                },
                                suspended: true
                            }]
                        }
                    }));

                }, 3000);
            };

            unsuspendWatford() {
                var self = this;
                //use this for predicatable feature test
                setTimeout(function () {
                    document.dispatchEvent(new CustomEvent('iron-signal', {
                        'detail': {
                            name: 'selection-update',
                            data: [{
                                selectionId: "89165_1",
                                price: {
                                    decimal: 2.50
                                },
                                suspended: false
                            }]
                        }
                    }));

                }, 3000);
            }

            toggleTools() {
                this.toolsOpen = !this.toolsOpen;
            }

        }

        customElements.define(BgSportsNextn.is, BgSportsNextn);


    });
})();