(function () {
    'use strict';
    window.addEventListener('WebComponentsReady', () => {
        class BgSportsButton extends Polymer.Element {

            static get is() {
                return 'bg-sports-button';
            }

            static get properties() {
                return {
                    event: {
                        type: Object
                    },

                    outcome: {
                        type: Object
                    }
                }
            }

            constructor() {
                super();
                // window.performance && performance.mark && performance.mark('bg-sports-button.created');
            }

            ready() {
                super.ready();
            }

            _formatPrice(price) {
                return price ? price.toFixed(2) : '';
            }

            _getPrice() {
                return this._formatPrice(this.outcome.Price);
            }
            addBet(e) {
                e.cancelBubble = true;
                e.stopPropagation();
                var _bet = {
                    betInfo: this.outcome.BetInfo
                };
                //mock audio video
                if (this.outcome.OutcomeName == 'Crystal Palace') {
                    _bet.videoInfo = "ESPN";
                    _bet.audioInfo = "BBC";
                }

                document.dispatchEvent(new CustomEvent('iron-signal', {
                    bubbles: true,
                    composed: true,
                    detail: {
                        name: "add-bets",
                        data: [_bet]
                    }
                }));
            }

            removeBet() {
                this.fire('iron-signal', {
                    name: 'removebet',
                    data: {
                        bet: {
                            eventId: this.event.EventId,
                            outcomeId: this.outcome.OutcomeId
                        }
                    }
                });
            }
        }

        customElements.define(BgSportsButton.is, BgSportsButton);

    });
})();