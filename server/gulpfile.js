/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http:polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http:polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http:polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http:polymer.github.io/PATENTS.txt
 */

/* eslint-env node */
'use strict';

const gulp = require('gulp');
const gulpif = require('gulp-if');
const del = require('del');
const mergeStream = require('merge-stream');
const closure = require('google-closure-compiler').gulp();
const Polymer = require('polymer-build');
const lazypipe = require('lazypipe');
const size = require('gulp-size');
const argv = require('yargs').argv;

const {Transform} = require('stream');

/**
 * Closure will only emit one file, but polymer-build is expecting an equal number of files in and out
 * Given a list of files, emit the missing ones back into the stream as empty files
 */
class BackfillStream extends Transform {
    constructor(fileList) {
        super({ objectMode: true });
        this.fileList = fileList;
    }
    _transform(file, enc, cb) {
        if (this.fileList) {
            const origFile = this.fileList.shift();
            file.path = origFile.path;
        }
        cb(null, file);
    }
    _flush(cb) {
        if (this.fileList && this.fileList.length > 0) {
            this.fileList.forEach((oldFile) => {
                let newFile = oldFile.clone({ deep: true, contents: false });
                newFile.contents = new Buffer('');
                this.push(newFile);
            });
        }
        cb();
    }
}

gulp.task('clean', () => del('temp'));

const dom5 = require('dom5'); const path = require('path');

let firstImportFinder = dom5.predicates.AND(dom5.predicates.hasTagName('link'), dom5.predicates.hasAttrValue('rel', 'import'));

class AddClosureTypeImport extends Transform {
    constructor(entryFileName, typeFileName) {
        super({ objectMode: true });
        this.target = path.resolve(entryFileName);
        this.importPath = path.resolve(typeFileName);
    }
    _transform(file, enc, cb) {
        if (file.path === this.target) {
            let contents = file.contents.toString();
            let html = dom5.parse(contents, { locationInfo: true });
            let firstImport = dom5.query(html, firstImportFinder);
            if (firstImport) {
                let importPath = path.relative(path.dirname(this.target), this.importPath);
                let importLink = dom5.constructors.element('link');
                dom5.setAttribute(importLink, 'rel', 'import');
                dom5.setAttribute(importLink, 'href', importPath);
                dom5.insertBefore(firstImport.parentNode, firstImport, importLink);
                file.contents = Buffer(dom5.serialize(html));
            }
        }
        cb(null, file);
    }
}

gulp.task('closure', ['clean'], () => {

    let shell, joinRx, addClosureTypes;
    let closureModule = argv.closure_module;
    let moduleSettings = {
        betslip : {
            shell: "client/source/betslip/bg-betslip-app/bg-betslip-app.html",
            entryPoint: "server/public/index-betslip.html",
            fragments: []
        },
        bethistory : {
            shell: "client/source/bethistory/bg-betslip-openbets/bg-betslip-openbets.html",
            entryPoint: "server/public/index-bethistory.html",
            fragments: []
        },
        all : {
            shell: "client/source/betslip/bg-betslip-app/bg-betslip-app.html",
            entryPoint: "server/public/index-betslip.html",
            fragments: [
                'client/source/bethistory/bg-betslip-openbets/bg-betslip-openbets.html'
            ]
        },
    };

    function config(path) {
        shell = path;
        joinRx = new RegExp(path.split('/').join('\\/'));
        addClosureTypes = new AddClosureTypeImport(shell, 'client/source/lib/polymer/externs/polymer-closure-types.html');
    }

    process.chdir('../');
    config(moduleSettings[closureModule].shell);

    const project = new Polymer.PolymerProject({
        entrypoint: moduleSettings[closureModule].entryPoint,
        shell: `./${shell}`,
        fragments: moduleSettings[closureModule].fragments,
        extraDependencies: [
            addClosureTypes.importPath,
            "client/source/lib/webcomponentsjs/custom-elements-es5-adapter.js",
            "client/source/lib/webcomponentsjs/webcomponents-hi.js",
            "client/source/lib/webcomponentsjs/webcomponents-hi-ce.js",
            "client/source/lib/webcomponentsjs/webcomponents-hi-sd-ce.js",
            "client/source/lib/webcomponentsjs/webcomponents-lite.js",
            "client/source/lib/webcomponentsjs/webcomponents-loader.js",
            "client/source/lib/webcomponentsjs/webcomponents-sd-ce.js"
        ],
    });

    const closureStream = closure({
        compilation_level: 'SIMPLE',
        language_in: 'ES6_STRICT',
        language_out: 'ES5_STRICT',
        isolation_mode: 'IIFE',
        assume_function_wrapper: true,
        rewrite_polyfills: false,
        new_type_inf: true,
        jscomp_off: ['checkTypes', 'missingProperties', 'typeInvalidation'],
        polymer_version: 2,
        externs: [
            'client/source/lib/polymer/externs/webcomponents-externs.js',
            'client/source/lib/polymer/externs/closure-types.js',
            'client/source/lib/polymer/externs/polymer-externs.js',
        ]
    });

    const closurePipeline = lazypipe()
        .pipe(() => closureStream)
        .pipe(() => new BackfillStream(closureStream.fileList_))

    // process source files in the project
    const sources = project.sources();

    // process dependencies
    const dependencies = project.dependencies();

    // merge the source and dependencies streams to we can analyze the project
    const mergedFiles = mergeStream(sources, dependencies);

    function closureCompiled(file) {
        return new RegExp('bg-betslip-.*\.html_script_\\d+\\.js$').test(file.path);
    };

    const splitter = new Polymer.HtmlSplitter();
    return mergedFiles
        .pipe(addClosureTypes)
        .pipe(project.bundler())
        .pipe(splitter.split())
        .pipe(gulpif(closureCompiled, closurePipeline()))
        .pipe(splitter.rejoin())
        .pipe(project.addCustomElementsEs5Adapter())
        .pipe(gulpif(joinRx, size({ title: 'bundle size', gzip: true, showTotal: false, showFiles: true })))
        .pipe(gulp.dest('server/temp'))
});

gulp.task('default', ['closure']);