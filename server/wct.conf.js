﻿var path = require('path');

var ret = {
    verbose: true,
    testTimeout: 30 * 1000,
    suites: ['tests'],
    webserver: {
        'pathMappings': []
    },
    plugins: {
        "local": {
            "browsers": ["firefox"]
        }
    }
};

var mapping = {};
var rootPath = (__dirname).split(path.sep).slice(-1)[0];
ret.webserver.pathMappings.push(mapping);
module.exports = ret;
