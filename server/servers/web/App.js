"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const path = require("path");
const express = require("express");
const compression = require("compression");
const bodyParser = require("body-parser");

// Creates and configures an ExpressJS web server.
class App {

    //Run configuration methods on the Express instance.
    constructor(rootPath) {
        this.express = express();
        this.rootPath = rootPath || ".";
        this.middleware();
        this.staticRoutes();
    }

    // Configure Express middleware.
    middleware() {
        this.express.use(compression());
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({
            extended: false
        }));
    }

    staticRoutes() {
        var rootDir = path.resolve(this.rootPath);
        var publicDir = path.resolve(this.rootPath + '/server/public');
        var publicRoute = express.static(publicDir);
        var sourceRoute = express.static(path.resolve(rootDir, "./client/source"));
        var testComponentsRoute = express.static(path.resolve(rootDir, "./server/public/test-components"));
        var extraLibRoute = express.static(path.resolve(rootDir, "./server/lib"));
        this.express.use("/", publicRoute);
        this.express.use("/components/", sourceRoute);
        this.express.use("/components/", testComponentsRoute);
        this.express.use("/components/lib", extraLibRoute);
    }
}

exports.App = App;