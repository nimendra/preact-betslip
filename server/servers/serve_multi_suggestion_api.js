"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const detect = require("detect-port");
const APIServer_1 = require("./multi-suggestion-api/APIServer");

var multiSugestionApiServerPort = 3001,
    multiSugestionApiServer = new APIServer_1.default(multiSugestionApiServerPort);

function startMultiSuggestionAPIServer() {
    detect(multiSugestionApiServerPort, (err, availablePort) => {
        if (err) {
            console.log(err);
            return;
        }
        if (multiSugestionApiServerPort === availablePort) {
            multiSugestionApiServer.start();
        } else {
            console.log("Multi Suggestion API server already listening on port", multiSugestionApiServerPort);
        }
    });
}

startMultiSuggestionAPIServer();