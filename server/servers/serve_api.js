"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const detect = require("detect-port");
const APIServer_1 = require("./api/APIServer");

var apiServerPort = 3000,
    apiServer = new APIServer_1.default(apiServerPort);

function startAPIServer() {
    detect(apiServerPort, (err, availablePort) => {
        if (err) {
            console.log(err);
            return;
        }
        if (apiServerPort === availablePort) {
            apiServer.start();
        } else {
            console.log("API server already listening on port", apiServerPort);
        }
    });
}

startAPIServer();