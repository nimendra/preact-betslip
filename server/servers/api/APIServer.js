"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const http = require("http");
const debug = require("debug");
const App_1 = require("./App");

class APIServer {

    constructor(port) {
        this.port = port || 3000;
        this.app = new App_1.default();
        this.server = http.createServer(this.app.express);
        this.app.express.use('/close', this.stop);
        debug('ts-express:server');
    }

    start() {
        console.log("Starting API server on port %s...", this.port);
        this.server.listen(this.port);
        this.server.on('error', this.onError);
        //this.server.on('listening', this.onListening);
    }

    stop() {
        console.log("Stopping API server...");
        this.server.close();
    }

    onError(error) {
        if (error.syscall !== 'listen')
            throw error;
        let bind = (typeof this.port === 'string') ? 'Pipe ' + this.port : 'Port ' + this.port;
        switch (error.code) {
            case 'EACCES':
                console.error(`${bind} requires elevated privileges`);
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(`${bind} is already in use`);
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    onListening() {
        let addr = this.server.address();
        let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
        debug(`Listening on ${bind}`);
    }
}

exports.default = APIServer;