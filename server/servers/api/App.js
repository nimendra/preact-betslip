"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const SportsRouter_1 = require("./routes/SportsRouter");
const BetslipRouter_1 = require("./routes/BetslipRouter");
const BetHistoryRouter_1 = require("./routes/BetHistoryRouter");
const InterceptRouter_1 = require("./routes/InterceptRouter");

// Creates and configures an ExpressJS web server.
class App {

    //Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        this.middleware();
        this.cors();
        this.routes();
        //this.express.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    }

    // Configure Express middleware.
    middleware() {
        //this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({
            extended: false
        }));
    }

    // Configure API endpoints.
    routes() {
        /* This is just to get up and running, and to make sure what we've got is
         * working so far. This function will change when we start to add more
         * API endpoints */
        let router = express.Router();
        // placeholder route handler
        router.get('/', (req, res, next) => {
            res.json({
                message: 'Hello World!'
            });
        });
        this.express.use('/api/v1/sports', SportsRouter_1.default);
        this.express.use('/api/v1/sportsbook/bets', BetslipRouter_1.default);
        this.express.use('/api/v1/bethistory', BetHistoryRouter_1.default);
        this.express.use('/api/v1/sportsbook/intercepts', InterceptRouter_1.default);
    }

    cors() {
        this.express.use(cors());
    }
}

exports.default = App;