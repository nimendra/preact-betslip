"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const express_1 = require("express");
const OpenBets = require('../data/openbets');
const CashoutBets = require('../data/cashoutbets');
const BetDetailsMulti = require('../data/betdetailsMulti');
const BetDetailsSingle = require('../data/betdetailsSingle');

class BetHistoryRouter {

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }

    getBetHistory(req, res, next) {
        var filter = req.query.filter;
        var pagesize = req.query.pagesize;
        var page = req.query.page;
        var result = null;
        if (filter == 'cashout') {
            result = JSON.parse(JSON.stringify(CashoutBets));
        } else if (filter == 'open') {
            result = JSON.parse(JSON.stringify(OpenBets));
        } else {
            //default open
            result = JSON.parse(JSON.stringify(OpenBets));
        }
        res.send(result);
    }

    getBetDetails(req, res, next) {
        var betid = req.query.betid;
        var result = null;
        if (betid == 911046) {
            result = JSON.parse(JSON.stringify(BetDetailsMulti));
        } else if (betid == 911042) {
            result = JSON.parse(JSON.stringify(BetDetailsSingle));
        } else {
            //default single
            result = JSON.parse(JSON.stringify(BetDetailsSingle));
        }
        res.send(result);
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/', this.getBetHistory);
        this.router.get('/details', this.getBetDetails);
        this.router.post('/details', this.getBetDetails);
    }
}

exports.BetHistoryRouter = BetHistoryRouter;

// Create the HeroRouter, and export its configured Express.Router
const bethistoryRoutes = new BetHistoryRouter();
bethistoryRoutes.init();

exports.default = bethistoryRoutes.router;