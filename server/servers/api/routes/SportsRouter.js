"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const express_1 = require("express");
const SportsNextN = require('../data/sportsnextn');
const RealSportsNextN = require('../data/sportsnextnreal');
const SqlConnection_1 = require("../data/SqlConnection");

class SportsRouter {

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    
    getBetInfo(sport, outcome) {
        var EventId = sport.EventId;
        var EventName = sport.EventName;
        var EventClassId = 0;
        var EventClass = "";
        var OutcomeId = outcome.OutcomeId;
        var OutcomeName = outcome.OutcomeName;
        var Price = outcome.Price;
        var MarketTypeCode = outcome.MarketTypeCode;
        var BetDetailTypeCode = outcome.MarketTypeCode;
        var Points = outcome.Points;
        var GroupingId = 0;
        var MarketDesc = sport.EventName;
        var FixedMarketId = outcome.FixedMarketId;
        var MasterEventClassId = 0;
        var CategoryClassId = 0;
        var IsCashoutAllowed = true;
        var GroupByHeader = false;
        var GroupByCode = false;
        var IsOpenForBetting = true;
        var IsEventStarted = false;
        var MasterEventId = 0;
        var EventTypeId = sport.EventTypeId;
        var MasterCategoryId = 0;
        var CategoryId = 0;
        var betInfo = EventId + "|" + EventName + "|" + EventClassId + "|" + OutcomeId + "|" + OutcomeName + "|" + Price + "|" +
            EventClass + "|" + MarketTypeCode + "|" + BetDetailTypeCode + "|" + Points + "|" + GroupingId +
            "|" + MarketDesc + "|" + FixedMarketId + "|" + MasterEventClassId + "|" + CategoryClassId + "|" +
            IsCashoutAllowed + "|" + GroupByHeader + "|" + GroupByCode + "|" + IsOpenForBetting + "|" +
            IsEventStarted + "|" + MasterEventId + "|" + EventTypeId + "|" + MasterCategoryId + "|" + CategoryId;
        return betInfo;
    }

    getSportsNextN(req, res, next) {
        var self = this;
        SportsNextN.Sports.map(function (sport) {
            sport.Outcomes.map(function (outcome) {
                outcome.BetInfo = SportsRouter.prototype.getBetInfo(sport, outcome);
            });
        });
        res.send(SportsNextN);
    }

    getRealSportsNextN(req, res, next) {
        return new SqlConnection_1.default().executeStatement(res);
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/nextn', this.getSportsNextN);
        this.router.get('/realnextn', this.getRealSportsNextN);
    }
}

exports.SportsRouter = SportsRouter;

// Create the HeroRouter, and export its configured Express.Router
const sportsRoutes = new SportsRouter();
sportsRoutes.init();

exports.default = sportsRoutes.router;