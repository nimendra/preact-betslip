"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const express_1 = require("express");

class InterceptRouter {

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }

    init() {
        //this.router.post('/confirm', this.getBetslipConfirm);
        this.router.post('/:interceptId?', this.getIntercepts);
        this.router.get('/:interceptId?', this.getIntercepts);
    }

    getIntercepts(req, res, next) {
        var interceptId = req.params.interceptId;
        var hasDelay = false;
        var has500 = false;
        //occasional success
        var ticket = null;
        var interceptData = {};
        if (parseInt(interceptId) === 90404 || parseInt(interceptId) === 90405 || parseInt(interceptId) === 90406
         || parseInt(interceptId) === 178570 || parseInt(interceptId) === 178320) {
            ticket = "A12345";
            interceptData = {
                "intercepts": [{
                    "interceptId": interceptId,
                    "interceptStatusId": 0,
                    "interceptStatusDescription": "string",
                    "totalBetCost": 0,
                    "maxAllowedTotalStake": 0,
                    "currentFixedPrice": 0,
                    "currentFixedPricePlace": 0,
                    "currentPoints": 0,
                    "calculatedTotalLines": 0,
                    "bet": {
                        "errorNo": 0,
                        "errorText": "string",
                        "betId": 0,
                        "transactionId": 0,
                        "totalBetCost": 0,
                        "maxAllowedTotalStake": 0,
                        "currentFixedPrice": 0,
                        "currentFixedPricePlace": 0,
                        "currentPoints": 0,
                        "currentBalance": 0,
                        "calculatedTotalLines": 0,
                        "ticketNumber": ticket
                    }
                }],
                "errors": [{
                    "interceptId": 0,
                    "status": "Accepted",
                    "description": "string"
                }]
            };
        } else if (parseInt(interceptId) === 90154  || parseInt(interceptId) === 90155 || parseInt(interceptId) === 90156) {
            interceptData = {
                "intercepts": [{
                    "interceptId": interceptId,
                    "interceptStatusId": 0,
                    "interceptStatusDescription": "continue",
                    "totalBetCost": 0,
                    "maxAllowedTotalStake": 0,
                    "currentFixedPrice": 0,
                    "currentFixedPricePlace": 0,
                    "currentPoints": 0,
                    "calculatedTotalLines": 0,
                    "bet": {
                        "errorNo": 8001,
                        "errorText": "Delayed -> Waiting",
                        "betId": 0,
                        "transactionId": 0,
                        "totalBetCost": 0,
                        "maxAllowedTotalStake": 0,
                        "currentFixedPrice": 0,
                        "currentFixedPricePlace": 0,
                        "currentPoints": 0,
                        "currentBalance": 0,
                        "calculatedTotalLines": 0,
                        "ticketNumber": null
                    }
                }],
                "errors": [{
                    "interceptId": 0,
                    "status": "Accepted",
                    "description": "string"
                }]
            };
        } else if (parseInt(interceptId) === 90158 || parseInt(interceptId) === 90159 || parseInt(interceptId) === 90160) {
            has500 = true;            
        } 
        else if (parseInt(interceptId) === 90162 || parseInt(interceptId) === 90163|| parseInt(interceptId) === 90164) {
            interceptData = {
                "intercepts": [{
                    "interceptId": interceptId,
                    "interceptStatusId": 0,
                    "interceptStatusDescription": "string",
                    "totalBetCost": 0,
                    "maxAllowedTotalStake": 0,
                    "currentFixedPrice": 0,
                    "currentFixedPricePlace": 0,
                    "currentPoints": 0,
                    "calculatedTotalLines": 0,
                    "bet": {
                        "errorNo": 8008,
                        "errorText": "Bet rejected",
                        "betId": 0,
                        "transactionId": 0,
                        "totalBetCost": 0,
                        "maxAllowedTotalStake": 0,
                        "currentFixedPrice": 0,
                        "currentFixedPricePlace": 0,
                        "currentPoints": 0,
                        "currentBalance": 0,
                        "calculatedTotalLines": 0,
                        "ticketNumber": null
                    }
                }],
                "errors": [{
                    "interceptId": 0,
                    "status": "Rejected",
                    "description": "string"
                }]
            };
            hasDelay = true;
        }  
        else if (parseInt(interceptId) === 90400 || parseInt(interceptId) === 90401|| parseInt(interceptId) === 90402) {
            interceptData = {
                "intercepts":[{
                    "interceptId":interceptId,
                    "interceptStatusId":8,
                    "interceptStatusDescription":"Intercepted -> Rejected by Admin",
                    "totalBetCost":1.0000,
                    "maxAllowedTotalStake":1665.8100,
                    "currentFixedPrice":7.00000000,
                    "currentFixedPricePlace":0.00000000,
                    "currentPoints":0.00,
                    "calculatedTotalLines":1}],
                    "errors":[]
                };
        }
        else {
            interceptData = {
                "intercepts": [{
                    "interceptId": interceptId,
                    "interceptStatusId": 0,
                    "interceptStatusDescription": "string",
                    "totalBetCost": 0,
                    "maxAllowedTotalStake": 0,
                    "currentFixedPrice": 0,
                    "currentFixedPricePlace": 0,
                    "currentPoints": 0,
                    "calculatedTotalLines": 0,
                    "bet": {
                        "errorNo": 8008,
                        "errorText": "Bet rejected",
                        "betId": 0,
                        "transactionId": 0,
                        "totalBetCost": 0,
                        "maxAllowedTotalStake": 0,
                        "currentFixedPrice": 0,
                        "currentFixedPricePlace": 0,
                        "currentPoints": 0,
                        "currentBalance": 0,
                        "calculatedTotalLines": 0,
                        "ticketNumber": null
                    }
                }],
                "errors": [{
                    "interceptId": 0,
                    "status": "Rejected",
                    "description": "string"
                }]
            };
        }
        //res.requestGuid = req.body.requestGuid;
       if(has500){
        setTimeout((function() {
            res.status(500);
            res.send('string');
          }), 1000);
       }else if(hasDelay){
            setTimeout((function() {
              res.send(interceptData);
            }), 5000);
        }else{
          res.send(interceptData);
        }
    }
}

exports.InterceptRouter = InterceptRouter;

// Create the InterceptRouter, and export its configured Express.Router
const interceptRoutes = new InterceptRouter();
interceptRoutes.init();

exports.default = interceptRoutes.router;