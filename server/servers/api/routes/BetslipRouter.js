"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const express_1 = require("express");
const Betslip = require('../data/progress-betslip');
const Singlebet = require('../data/singlebetprogress');
const Multibet = require('../data/multibetprogress');

class Guid {
    static newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}

class BetslipRouter {
    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    //Pay out calc
    straightMultiPayoutRule(singleBets, stake) {
        var payout = 1;
        for (var i = 0; i < singleBets.length; i++) {
            payout *= Math.round(singleBets[i].FixedWin * 100) / 100;
        }
        return Math.round(payout * stake * 100) / 100; //(parseInt((payout * stake * 100).toString()) / 100);
    }
    getParlayPayouts(singleBets, type, totalStake, mumLegs, combos) {
        if (totalStake <= 0)
            return null;
        var tot_pot_rtn = 0.0;
        var leg_nums = mumLegs;
        if (combos > 0) {
            var stake = totalStake / combos;
        }
        //15legs   
        if (type == 'BXMUL-15L') {
            if (leg_nums >= 15) {
                tot_pot_rtn += this.payout15legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //14legs   
        if (type == 'BXMUL-14L') {
            if (leg_nums >= 14) {
                tot_pot_rtn += this.payout14legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //13legs   
        if (type == 'BXMUL-13L') {
            if (leg_nums >= 13) {
                tot_pot_rtn += this.payout13legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //12legs   
        if (type == 'BXMUL-12L') {
            if (leg_nums >= 12) {
                tot_pot_rtn += this.payout12legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //11legs   
        if (type == 'BXMUL-11L') {
            if (leg_nums >= 11) {
                tot_pot_rtn += this.payout11legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //10legs   
        if (type == 'BXMUL-10L') {
            if (leg_nums >= 10) {
                tot_pot_rtn += this.payout10legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //9legs   
        if (type == 'BXMUL-9L') {
            if (leg_nums >= 9) {
                tot_pot_rtn += this.payout9legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //8legs   
        if (type == 'GOLIATH' || type == 'BXMUL-8L') {
            if (leg_nums >= 8) {
                tot_pot_rtn += this.payout8legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //7legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'BXMUL-7L') {
            if (leg_nums >= 7) {
                tot_pot_rtn += this.payout7legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //6legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'BXMUL-6L') {
            if (leg_nums >= 6) {
                tot_pot_rtn += this.payout6legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //5legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'SUPYANKEE' || type == 'LUCKY31' || type == 'BXMUL-5L') {
            if (leg_nums >= 5) {
                tot_pot_rtn += this.payout5legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //4legs 
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'SUPYANKEE' || type == 'LUCKY31' || type == 'YANKEE' || type == 'LUCKY15' || type == 'BXMUL-4L') {
            if (leg_nums >= 4) {
                tot_pot_rtn += this.payout4legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //3legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'SUPYANKEE' || type == 'LUCKY31' || type == 'YANKEE' || type == 'LUCKY15' || type == 'TRIXIE' || type == 'PATENT' || type == 'BXMUL-3L') {
            if (leg_nums >= 3) {
                tot_pot_rtn += this.payout3legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        //2legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'SUPYANKEE' || type == 'LUCKY31' || type == 'YANKEE' || type == 'LUCKY15' || type == 'TRIXIE' || type == 'PATENT' || type == 'BXMUL-2L') {
            if (leg_nums >= 2) {
                tot_pot_rtn += this.payout2legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_rtn <= 0)
                return null;
        }
        if (leg_nums >= 1 && (type == 'LUCKY63' || type == 'LUCKY31' || type == 'LUCKY15' || type == 'PATENT')) {
            tot_pot_rtn += this.payout1leg(singleBets, stake, leg_nums);
        }
        return tot_pot_rtn;
    }
    payout15legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    for (var t = s + 1; t < leg_nums; t++) {
                                                        for (var u = t + 1; u < leg_nums; u++) {
                                                            for (var v = u + 1; v < leg_nums; v++) {
                                                                for (var w = v + 1; w < leg_nums; w++) {
                                                                    var newSinglebets = [];
                                                                    newSinglebets.push(singleBets[i]);
                                                                    newSinglebets.push(singleBets[j]);
                                                                    newSinglebets.push(singleBets[k]);
                                                                    newSinglebets.push(singleBets[l]);
                                                                    newSinglebets.push(singleBets[m]);
                                                                    newSinglebets.push(singleBets[n]);
                                                                    newSinglebets.push(singleBets[o]);
                                                                    newSinglebets.push(singleBets[p]);
                                                                    newSinglebets.push(singleBets[q]);
                                                                    newSinglebets.push(singleBets[r]);
                                                                    newSinglebets.push(singleBets[s]);
                                                                    newSinglebets.push(singleBets[t]);
                                                                    newSinglebets.push(singleBets[u]);
                                                                    newSinglebets.push(singleBets[v]);
                                                                    newSinglebets.push(singleBets[w]);
                                                                    var payout15legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                                    if (payout15legs <= 0) {
                                                                        return null;
                                                                    }
                                                                    tot_pot_rtn += payout15legs;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout14legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    for (var t = s + 1; t < leg_nums; t++) {
                                                        for (var u = t + 1; u < leg_nums; u++) {
                                                            for (var v = u + 1; v < leg_nums; v++) {
                                                                var newSinglebets = [];
                                                                newSinglebets.push(singleBets[i]);
                                                                newSinglebets.push(singleBets[j]);
                                                                newSinglebets.push(singleBets[k]);
                                                                newSinglebets.push(singleBets[l]);
                                                                newSinglebets.push(singleBets[m]);
                                                                newSinglebets.push(singleBets[n]);
                                                                newSinglebets.push(singleBets[o]);
                                                                newSinglebets.push(singleBets[p]);
                                                                newSinglebets.push(singleBets[q]);
                                                                newSinglebets.push(singleBets[r]);
                                                                newSinglebets.push(singleBets[s]);
                                                                newSinglebets.push(singleBets[t]);
                                                                newSinglebets.push(singleBets[u]);
                                                                newSinglebets.push(singleBets[v]);
                                                                var payout14legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                                if (payout14legs <= 0) {
                                                                    return null;
                                                                }
                                                                tot_pot_rtn += payout14legs;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout13legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    for (var t = s + 1; t < leg_nums; t++) {
                                                        for (var u = t + 1; u < leg_nums; u++) {
                                                            var newSinglebets = [];
                                                            newSinglebets.push(singleBets[i]);
                                                            newSinglebets.push(singleBets[j]);
                                                            newSinglebets.push(singleBets[k]);
                                                            newSinglebets.push(singleBets[l]);
                                                            newSinglebets.push(singleBets[m]);
                                                            newSinglebets.push(singleBets[n]);
                                                            newSinglebets.push(singleBets[o]);
                                                            newSinglebets.push(singleBets[p]);
                                                            newSinglebets.push(singleBets[q]);
                                                            newSinglebets.push(singleBets[r]);
                                                            newSinglebets.push(singleBets[s]);
                                                            newSinglebets.push(singleBets[t]);
                                                            newSinglebets.push(singleBets[u]);
                                                            var payout13legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                            if (payout13legs <= 0) {
                                                                return null;
                                                            }
                                                            tot_pot_rtn += payout13legs;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout12legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    for (var t = s + 1; t < leg_nums; t++) {
                                                        var newSinglebets = [];
                                                        newSinglebets.push(singleBets[i]);
                                                        newSinglebets.push(singleBets[j]);
                                                        newSinglebets.push(singleBets[k]);
                                                        newSinglebets.push(singleBets[l]);
                                                        newSinglebets.push(singleBets[m]);
                                                        newSinglebets.push(singleBets[n]);
                                                        newSinglebets.push(singleBets[o]);
                                                        newSinglebets.push(singleBets[p]);
                                                        newSinglebets.push(singleBets[q]);
                                                        newSinglebets.push(singleBets[r]);
                                                        newSinglebets.push(singleBets[s]);
                                                        newSinglebets.push(singleBets[t]);
                                                        var payout12legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                        if (payout12legs <= 0) {
                                                            return null;
                                                        }
                                                        tot_pot_rtn += payout12legs;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout11legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    var newSinglebets = [];
                                                    newSinglebets.push(singleBets[i]);
                                                    newSinglebets.push(singleBets[j]);
                                                    newSinglebets.push(singleBets[k]);
                                                    newSinglebets.push(singleBets[l]);
                                                    newSinglebets.push(singleBets[m]);
                                                    newSinglebets.push(singleBets[n]);
                                                    newSinglebets.push(singleBets[o]);
                                                    newSinglebets.push(singleBets[p]);
                                                    newSinglebets.push(singleBets[q]);
                                                    newSinglebets.push(singleBets[r]);
                                                    newSinglebets.push(singleBets[s]);
                                                    var payout11legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                    if (payout11legs <= 0) {
                                                        return null;
                                                    }
                                                    tot_pot_rtn += payout11legs;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout10legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                var newSinglebets = [];
                                                newSinglebets.push(singleBets[i]);
                                                newSinglebets.push(singleBets[j]);
                                                newSinglebets.push(singleBets[k]);
                                                newSinglebets.push(singleBets[l]);
                                                newSinglebets.push(singleBets[m]);
                                                newSinglebets.push(singleBets[n]);
                                                newSinglebets.push(singleBets[o]);
                                                newSinglebets.push(singleBets[p]);
                                                newSinglebets.push(singleBets[q]);
                                                newSinglebets.push(singleBets[r]);
                                                var payout10legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                if (payout10legs <= 0) {
                                                    return null;
                                                }
                                                tot_pot_rtn += payout10legs;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout9legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            var newSinglebets = [];
                                            newSinglebets.push(singleBets[i]);
                                            newSinglebets.push(singleBets[j]);
                                            newSinglebets.push(singleBets[k]);
                                            newSinglebets.push(singleBets[l]);
                                            newSinglebets.push(singleBets[m]);
                                            newSinglebets.push(singleBets[n]);
                                            newSinglebets.push(singleBets[o]);
                                            newSinglebets.push(singleBets[p]);
                                            newSinglebets.push(singleBets[q]);
                                            var payout9legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                            if (payout9legs <= 0) {
                                                return null;
                                            }
                                            tot_pot_rtn += payout9legs;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout8legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        var newSinglebets = [];
                                        newSinglebets.push(singleBets[i]);
                                        newSinglebets.push(singleBets[j]);
                                        newSinglebets.push(singleBets[k]);
                                        newSinglebets.push(singleBets[l]);
                                        newSinglebets.push(singleBets[m]);
                                        newSinglebets.push(singleBets[n]);
                                        newSinglebets.push(singleBets[o]);
                                        newSinglebets.push(singleBets[p]);
                                        var payout8legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                        if (payout8legs <= 0) {
                                            return null;
                                        }
                                        tot_pot_rtn += payout8legs;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout7legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    var newSinglebets = [];
                                    newSinglebets.push(singleBets[i]);
                                    newSinglebets.push(singleBets[j]);
                                    newSinglebets.push(singleBets[k]);
                                    newSinglebets.push(singleBets[l]);
                                    newSinglebets.push(singleBets[m]);
                                    newSinglebets.push(singleBets[n]);
                                    newSinglebets.push(singleBets[o]);
                                    var payout7legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                    if (payout7legs <= 0) {
                                        return null;
                                    }
                                    tot_pot_rtn += payout7legs;
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout6legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                var newSinglebets = [];
                                newSinglebets.push(singleBets[i]);
                                newSinglebets.push(singleBets[j]);
                                newSinglebets.push(singleBets[k]);
                                newSinglebets.push(singleBets[l]);
                                newSinglebets.push(singleBets[m]);
                                newSinglebets.push(singleBets[n]);
                                var payout6legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                if (payout6legs <= 0) {
                                    return null;
                                }
                                tot_pot_rtn += payout6legs;
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }
    payout5legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            var newSinglebets = [];
                            newSinglebets.push(singleBets[i]);
                            newSinglebets.push(singleBets[j]);
                            newSinglebets.push(singleBets[k]);
                            newSinglebets.push(singleBets[l]);
                            newSinglebets.push(singleBets[m]);
                            var payout5legs = this.straightMultiPayoutRule(newSinglebets, stake);
                            if (payout5legs <= 0) {
                                return null;
                            }
                            tot_pot_rtn += payout5legs;
                        }
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout4legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        var newSinglebets = [];
                        newSinglebets.push(singleBets[i]);
                        newSinglebets.push(singleBets[j]);
                        newSinglebets.push(singleBets[k]);
                        newSinglebets.push(singleBets[l]);
                        var payout4legs = this.straightMultiPayoutRule(newSinglebets, stake);
                        if (payout4legs <= 0) {
                            return null;
                        }
                        tot_pot_rtn += payout4legs;
                    }
                }
            }
        }
        return tot_pot_rtn;
    }

    payout3legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    var newSinglebets = [];
                    newSinglebets.push(singleBets[i]);
                    newSinglebets.push(singleBets[j]);
                    newSinglebets.push(singleBets[k]);
                    var payout3legs = this.straightMultiPayoutRule(newSinglebets, stake);
                    if (payout3legs <= 0) {
                        return null;
                    }
                    tot_pot_rtn += payout3legs;
                }
            }
        }
        return tot_pot_rtn;
    }

    payout2legs(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums - 1; i++) {
            for (var j = i + 1; j < leg_nums; j++) {
                var newSinglebets = [];
                newSinglebets.push(singleBets[i]);
                newSinglebets.push(singleBets[j]);
                var payout2legs = this.straightMultiPayoutRule(newSinglebets, stake);
                if (payout2legs <= 0) {
                    return null;
                }
                tot_pot_rtn += payout2legs;
            }
        }
        return tot_pot_rtn;
    }

    payout1leg(singleBets, stake, leg_nums) {
        var tot_pot_rtn = 0.0;
        for (var i = 0; i < leg_nums; i++) {
            var singlepayout = this.singlePayoutRule(singleBets[i], stake);
            if (singlepayout <= 0) {
                return null;
            }
            tot_pot_rtn += singlepayout;
        }
        return tot_pot_rtn;
    }

    singlePayoutRule(bet, stake) {
        var fixedWin = Math.round(bet.FixedWin * 100) / 100;
        var payout = (fixedWin || 0) * stake;
        return Math.round(payout * 100) / 100; //(parseInt((payout * 100).toString()) / 100);
    }

    parlayPayoutRule(singleBets, stake, name, numLegs, combos) {
        var tot_pot_rtn = this.getParlayPayouts(singleBets, name, stake, numLegs, combos);
        return Math.round(tot_pot_rtn * 100) / 100; //(parseInt((tot_pot_rtn * 100).toString()) / 100);
    }

    //////////////TAX/////////////////
    straightMultiTaxRule(singleBets, payout, stake) {
        var tax = 0;
        var columns = stake / 0.25;
        var profit = payout;
        var profitPerColumn = (profit - stake) / columns;
        var hasTax = profitPerColumn > 100;
        if (hasTax) {
            var taxedProfit = profitPerColumn - 100;
            var taxPerColumn = taxedProfit * (profitPerColumn > 500 ? 0.2 : 0.15);
            tax = taxPerColumn * columns;
        }
        return Math.round(tax * 100) / 100;
    }

    getParlayTax(singleBets, type, totalStake, mumLegs, combos) {
        if (totalStake <= 0)
            return null;
        var tot_pot_tax = 0.0;
        var leg_nums = mumLegs;
        var stake = 1;
        //15legs   
        if (type == 'BXMUL-15L') {
            if (leg_nums >= 15) {
                tot_pot_tax += this.tax15legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //14legs   
        if (type == 'BXMUL-14L') {
            if (leg_nums >= 14) {
                tot_pot_tax += this.tax14legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //13legs   
        if (type == 'BXMUL-13L') {
            if (leg_nums >= 13) {
                tot_pot_tax += this.tax13legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //12legs   
        if (type == 'BXMUL-12L') {
            if (leg_nums >= 12) {
                tot_pot_tax += this.tax12legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //11legs   
        if (type == 'BXMUL-11L') {
            if (leg_nums >= 11) {
                tot_pot_tax += this.tax11legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //10legs   
        if (type == 'BXMUL-10L') {
            if (leg_nums >= 10) {
                tot_pot_tax += this.tax10legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //9legs   
        if (type == 'BXMUL-9L') {
            if (leg_nums >= 9) {
                tot_pot_tax += this.tax9legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //8legs   
        if (type == 'GOLIATH' || type == 'BXMUL-8L') {
            if (leg_nums >= 8) {
                tot_pot_tax += this.tax8legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //7legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'BXMUL-7L') {
            if (leg_nums >= 7) {
                tot_pot_tax += this.tax7legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //6legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'BXMUL-6L') {
            if (leg_nums >= 6) {
                tot_pot_tax += this.tax6legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //5legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'SUPYANKEE' || type == 'LUCKY31' || type == 'BXMUL-5L') {
            if (leg_nums >= 5) {
                tot_pot_tax += this.tax5legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //4legs 
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'SUPYANKEE' || type == 'LUCKY31' || type == 'YANKEE' || type == 'LUCKY15' || type == 'BXMUL-4L') {
            if (leg_nums >= 4) {
                tot_pot_tax += this.tax4legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //3legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'SUPYANKEE' || type == 'LUCKY31' || type == 'YANKEE' || type == 'LUCKY15' || type == 'TRIXIE' || type == 'PATENT' || type == 'BXMUL-3L') {
            if (leg_nums >= 3) {
                tot_pot_tax += this.tax3legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        //2legs   
        if (type == 'SUPHEINZ' || type == 'GOLIATH' || type == 'HEINZ' || type == 'LUCKY63' || type == 'SUPYANKEE' || type == 'LUCKY31' || type == 'YANKEE' || type == 'LUCKY15' || type == 'TRIXIE' || type == 'PATENT' || type == 'BXMUL-2L') {
            if (leg_nums >= 2) {
                tot_pot_tax += this.tax2legs(singleBets, stake, leg_nums);
            }
            if (tot_pot_tax <= 0)
                return null;
        }
        if (leg_nums >= 1 && (type == 'LUCKY63' || type == 'LUCKY31' || type == 'LUCKY15' || type == 'PATENT')) {
            tot_pot_tax += this.tax1leg(singleBets, stake, leg_nums);
        }
        return tot_pot_tax;
    }

    tax15legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    for (var t = s + 1; t < leg_nums; t++) {
                                                        for (var u = t + 1; u < leg_nums; u++) {
                                                            for (var v = u + 1; v < leg_nums; v++) {
                                                                for (var w = v + 1; w < leg_nums; w++) {
                                                                    var newSinglebets = [];
                                                                    newSinglebets.push(singleBets[i]);
                                                                    newSinglebets.push(singleBets[j]);
                                                                    newSinglebets.push(singleBets[k]);
                                                                    newSinglebets.push(singleBets[l]);
                                                                    newSinglebets.push(singleBets[m]);
                                                                    newSinglebets.push(singleBets[n]);
                                                                    newSinglebets.push(singleBets[o]);
                                                                    newSinglebets.push(singleBets[p]);
                                                                    newSinglebets.push(singleBets[q]);
                                                                    newSinglebets.push(singleBets[r]);
                                                                    newSinglebets.push(singleBets[s]);
                                                                    newSinglebets.push(singleBets[t]);
                                                                    newSinglebets.push(singleBets[u]);
                                                                    newSinglebets.push(singleBets[v]);
                                                                    newSinglebets.push(singleBets[w]);
                                                                    var payout15legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                                    var tax15legs = this.straightMultiTaxRule(newSinglebets, payout15legs, stake);
                                                                    //if (tax15legs <= 0) { return null; }
                                                                    tot_pot_tax += tax15legs;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax14legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    for (var t = s + 1; t < leg_nums; t++) {
                                                        for (var u = t + 1; u < leg_nums; u++) {
                                                            for (var v = u + 1; v < leg_nums; v++) {
                                                                var newSinglebets = [];
                                                                newSinglebets.push(singleBets[i]);
                                                                newSinglebets.push(singleBets[j]);
                                                                newSinglebets.push(singleBets[k]);
                                                                newSinglebets.push(singleBets[l]);
                                                                newSinglebets.push(singleBets[m]);
                                                                newSinglebets.push(singleBets[n]);
                                                                newSinglebets.push(singleBets[o]);
                                                                newSinglebets.push(singleBets[p]);
                                                                newSinglebets.push(singleBets[q]);
                                                                newSinglebets.push(singleBets[r]);
                                                                newSinglebets.push(singleBets[s]);
                                                                newSinglebets.push(singleBets[t]);
                                                                newSinglebets.push(singleBets[u]);
                                                                newSinglebets.push(singleBets[v]);
                                                                var payout14legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                                var tax14legs = this.straightMultiTaxRule(newSinglebets, payout14legs, stake);
                                                                //if (tax14legs <= 0) { return null; }
                                                                tot_pot_tax += tax14legs;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax13legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    for (var t = s + 1; t < leg_nums; t++) {
                                                        for (var u = t + 1; u < leg_nums; u++) {
                                                            var newSinglebets = [];
                                                            newSinglebets.push(singleBets[i]);
                                                            newSinglebets.push(singleBets[j]);
                                                            newSinglebets.push(singleBets[k]);
                                                            newSinglebets.push(singleBets[l]);
                                                            newSinglebets.push(singleBets[m]);
                                                            newSinglebets.push(singleBets[n]);
                                                            newSinglebets.push(singleBets[o]);
                                                            newSinglebets.push(singleBets[p]);
                                                            newSinglebets.push(singleBets[q]);
                                                            newSinglebets.push(singleBets[r]);
                                                            newSinglebets.push(singleBets[s]);
                                                            newSinglebets.push(singleBets[t]);
                                                            newSinglebets.push(singleBets[u]);
                                                            var payout13legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                            var tax13legs = this.straightMultiTaxRule(newSinglebets, payout13legs, stake);
                                                            //if (tax13legs <= 0) { return null; }
                                                            tot_pot_tax += tax13legs;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax12legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    for (var t = s + 1; t < leg_nums; t++) {
                                                        var newSinglebets = [];
                                                        newSinglebets.push(singleBets[i]);
                                                        newSinglebets.push(singleBets[j]);
                                                        newSinglebets.push(singleBets[k]);
                                                        newSinglebets.push(singleBets[l]);
                                                        newSinglebets.push(singleBets[m]);
                                                        newSinglebets.push(singleBets[n]);
                                                        newSinglebets.push(singleBets[o]);
                                                        newSinglebets.push(singleBets[p]);
                                                        newSinglebets.push(singleBets[q]);
                                                        newSinglebets.push(singleBets[r]);
                                                        newSinglebets.push(singleBets[s]);
                                                        newSinglebets.push(singleBets[t]);
                                                        var payout12legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                        var tax12legs = this.straightMultiTaxRule(newSinglebets, payout12legs, stake);
                                                        //if (tax12legs <= 0) { return null; }
                                                        tot_pot_tax += tax12legs;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax11legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                for (var s = r + 1; s < leg_nums; s++) {
                                                    var newSinglebets = [];
                                                    newSinglebets.push(singleBets[i]);
                                                    newSinglebets.push(singleBets[j]);
                                                    newSinglebets.push(singleBets[k]);
                                                    newSinglebets.push(singleBets[l]);
                                                    newSinglebets.push(singleBets[m]);
                                                    newSinglebets.push(singleBets[n]);
                                                    newSinglebets.push(singleBets[o]);
                                                    newSinglebets.push(singleBets[p]);
                                                    newSinglebets.push(singleBets[q]);
                                                    newSinglebets.push(singleBets[r]);
                                                    newSinglebets.push(singleBets[s]);
                                                    var payout11legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                    var tax11legs = this.straightMultiTaxRule(newSinglebets, payout11legs, stake);
                                                    //if (tax11legs <= 0) { return null; }
                                                    tot_pot_tax += tax11legs;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax10legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            for (var r = q + 1; r < leg_nums; r++) {
                                                var newSinglebets = [];
                                                newSinglebets.push(singleBets[i]);
                                                newSinglebets.push(singleBets[j]);
                                                newSinglebets.push(singleBets[k]);
                                                newSinglebets.push(singleBets[l]);
                                                newSinglebets.push(singleBets[m]);
                                                newSinglebets.push(singleBets[n]);
                                                newSinglebets.push(singleBets[o]);
                                                newSinglebets.push(singleBets[p]);
                                                newSinglebets.push(singleBets[q]);
                                                newSinglebets.push(singleBets[r]);
                                                var payout10legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                                var tax10legs = this.straightMultiTaxRule(newSinglebets, payout10legs, stake);
                                                //if (tax10legs <= 0) { return null; }
                                                tot_pot_tax += tax10legs;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax9legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        for (var q = p + 1; q < leg_nums; q++) {
                                            var newSinglebets = [];
                                            newSinglebets.push(singleBets[i]);
                                            newSinglebets.push(singleBets[j]);
                                            newSinglebets.push(singleBets[k]);
                                            newSinglebets.push(singleBets[l]);
                                            newSinglebets.push(singleBets[m]);
                                            newSinglebets.push(singleBets[n]);
                                            newSinglebets.push(singleBets[o]);
                                            newSinglebets.push(singleBets[p]);
                                            newSinglebets.push(singleBets[q]);
                                            var payout9legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                            var tax9legs = this.straightMultiTaxRule(newSinglebets, payout9legs, stake);
                                            //if (tax9legs <= 0) { return null; }
                                            tot_pot_tax += tax9legs;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax8legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    for (var p = o + 1; p < leg_nums; p++) {
                                        var newSinglebets = [];
                                        newSinglebets.push(singleBets[i]);
                                        newSinglebets.push(singleBets[j]);
                                        newSinglebets.push(singleBets[k]);
                                        newSinglebets.push(singleBets[l]);
                                        newSinglebets.push(singleBets[m]);
                                        newSinglebets.push(singleBets[n]);
                                        newSinglebets.push(singleBets[o]);
                                        newSinglebets.push(singleBets[p]);
                                        var payout8legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                        var tax8legs = this.straightMultiTaxRule(newSinglebets, payout8legs, stake);
                                        //if (tax8legs <= 0) { return null; }
                                        tot_pot_tax += tax8legs;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax7legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                for (var o = n + 1; o < leg_nums; o++) {
                                    var newSinglebets = [];
                                    newSinglebets.push(singleBets[i]);
                                    newSinglebets.push(singleBets[j]);
                                    newSinglebets.push(singleBets[k]);
                                    newSinglebets.push(singleBets[l]);
                                    newSinglebets.push(singleBets[m]);
                                    newSinglebets.push(singleBets[n]);
                                    newSinglebets.push(singleBets[o]);
                                    var payout7legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                    var tax7legs = this.straightMultiTaxRule(newSinglebets, payout7legs, stake);
                                    //if (tax7legs <= 0) { return null; }
                                    tot_pot_tax += tax7legs;
                                }
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax6legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            for (var n = m + 1; n < leg_nums; n++) {
                                var newSinglebets = [];
                                newSinglebets.push(singleBets[i]);
                                newSinglebets.push(singleBets[j]);
                                newSinglebets.push(singleBets[k]);
                                newSinglebets.push(singleBets[l]);
                                newSinglebets.push(singleBets[m]);
                                newSinglebets.push(singleBets[n]);
                                var payout6legs = this.straightMultiPayoutRule(newSinglebets, stake);
                                var tax6legs = this.straightMultiTaxRule(newSinglebets, payout6legs, stake);
                                //if (tax6legs <= 0) { return null; }
                                tot_pot_tax += tax6legs;
                            }
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax5legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        for (var m = l + 1; m < leg_nums; m++) {
                            var newSinglebets = [];
                            newSinglebets.push(singleBets[i]);
                            newSinglebets.push(singleBets[j]);
                            newSinglebets.push(singleBets[k]);
                            newSinglebets.push(singleBets[l]);
                            newSinglebets.push(singleBets[m]);
                            var payout5legs = this.straightMultiPayoutRule(newSinglebets, stake);
                            var tax5legs = this.straightMultiTaxRule(newSinglebets, payout5legs, stake);
                            //if (tax5legs <= 0) { return null; }
                            tot_pot_tax += tax5legs;
                        }
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax4legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    for (var l = k + 1; l < leg_nums; l++) {
                        var newSinglebets = [];
                        newSinglebets.push(singleBets[i]);
                        newSinglebets.push(singleBets[j]);
                        newSinglebets.push(singleBets[k]);
                        newSinglebets.push(singleBets[l]);
                        var payout4legs = this.straightMultiPayoutRule(newSinglebets, stake);
                        var tax4legs = this.straightMultiTaxRule(newSinglebets, payout4legs, stake);
                        //if (tax4legs <= 0) { return null; }
                        tot_pot_tax += tax4legs;
                    }
                }
            }
        }
        return tot_pot_tax;
    }

    tax3legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 2; i++) {
            for (var j = i + 1; j < leg_nums - 1; j++) {
                for (var k = j + 1; k < leg_nums; k++) {
                    var newSinglebets = [];
                    newSinglebets.push(singleBets[i]);
                    newSinglebets.push(singleBets[j]);
                    newSinglebets.push(singleBets[k]);
                    var payout3legs = this.straightMultiPayoutRule(newSinglebets, stake);
                    var tax3legs = this.straightMultiTaxRule(newSinglebets, payout3legs, stake);
                    //if (tax3legs <= 0) { return null; }
                    tot_pot_tax += tax3legs;
                }
            }
        }
        return tot_pot_tax;
    }

    tax2legs(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums - 1; i++) {
            for (var j = i + 1; j < leg_nums; j++) {
                var newSinglebets = [];
                newSinglebets.push(singleBets[i]);
                newSinglebets.push(singleBets[j]);
                var payout2legs = this.straightMultiPayoutRule(newSinglebets, stake);
                var tax2legs = this.straightMultiTaxRule(newSinglebets, payout2legs, stake);
                tot_pot_tax += tax2legs;
            }
        }
        return tot_pot_tax;
    }

    tax1leg(singleBets, stake, leg_nums) {
        var tot_pot_tax = 0.0;
        for (var i = 0; i < leg_nums; i++) {
            var singlepayout = this.singlePayoutRule(singleBets[i], stake);
            var singletax = this.straightMultiTaxRule(singleBets[i], singlepayout, stake);
            //if (singletax <= 0) { return null; }
            tot_pot_tax += singletax;
        }
        return tot_pot_tax;
    }

    parlayTaxRule(singleBets, stake, name, numLegs, combos) {
        var tot_pot_tax = this.getParlayTax(singleBets, name, stake, numLegs, combos);
        return Math.round(tot_pot_tax * 100) / 100; //(parseInt((tot_pot_rtn * 100).toString()) / 100);
    }

    ///////////////////////////////
    getBetslipProgress(req, res, next) {
        var reqsinglebets = req.body.singleBets;
        var result = JSON.parse(JSON.stringify(Betslip));
        //push single bets
        for (var i = 0; i < reqsinglebets.length; i++) {
            result.Betslip.Bets.push(JSON.parse(JSON.stringify(Singlebet)));
        }
        //multi SubType and payout update
        var hasConflicts = false;
        var currentMultiRequestHash = "";
        for (var i = 0; i < reqsinglebets.length; i++) {
            currentMultiRequestHash += ("/" + reqsinglebets[i].leg.selection.eventId + "-" + reqsinglebets[i].leg.selection.outcomeId);
            result.Betslip.Bets[i].Stake = reqsinglebets[i].totalStake;
            result.Betslip.Bets[i].FixedWin = reqsinglebets[i].leg.selection.prices[0].value;
            result.Betslip.Bets[i].OutcomeId = reqsinglebets[i].leg.selection.outcomeId;
            result.Betslip.Bets[i].FixedMarketId = reqsinglebets[i].leg.selection.fixedMarketId;
            result.Betslip.Bets[i].MarketTypeCode = reqsinglebets[i].leg.selection.marketTypeCode;
            result.Betslip.Bets[i].EventId = reqsinglebets[i].leg.selection.eventId;
            result.Betslip.Bets[i].BetReferenceGuid = reqsinglebets[i].betReferenceGuid;
            //find out multi conflict 
            var hasEvent = false;
            for (var j = 0; j < reqsinglebets.length; j++) {
                if (reqsinglebets[i].leg.selection.eventId == reqsinglebets[j].leg.selection.eventId && i != j) {
                    hasEvent = true;
                    hasConflicts = true;
                }
            }
            if (hasEvent) {
                result.Betslip.Bets[i].Result = {
                    "ErrorText": "Multi Conflict",
                    "ErrorNumber": 1,
                    "TicketNumber": ""
                };
            }
        }
        //multiples
        if (!hasConflicts) {
            var singlebets = result.Betslip.Bets;
            if (singlebets.length == 2) {
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Multi";
                multibet.SubType = "Double";
                multibet.SubTypeCode = "MULT-2L";
                multibet.Combos = 1;
                multibet.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet);
            } else if (singlebets.length == 3) {
                //3 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Multi";
                multibet.SubType = "Treble";
                multibet.SubTypeCode = "MULT-3L";
                multibet.Combos = 1;
                multibet.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 3;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 3, 3);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 3, 3);
                result.Betslip.Bets.push(multibetDoubles);
                //Trixie
                var multibetTrixie = JSON.parse(JSON.stringify(Multibet));
                multibetTrixie.Type = "Parlay";
                multibetTrixie.SubType = "Trixie";
                multibetTrixie.SubTypeCode = "TRIXIE";
                multibetTrixie.Combos = 4;
                multibetTrixie.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'TRIXIE', 3, 4);
                multibetTrixie.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'TRIXIE', 3, 4);
                result.Betslip.Bets.push(multibetTrixie);
                //Patent 
                var multibetPatent = JSON.parse(JSON.stringify(Multibet));
                multibetPatent.Type = "Parlay";
                multibetPatent.SubType = "Patent";
                multibetPatent.SubTypeCode = "PATENT";
                multibetPatent.Combos = 7;
                multibetPatent.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'PATENT', 3, 7);
                multibetPatent.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'PATENT', 3, 7);
                result.Betslip.Bets.push(multibetPatent);
            } else if (singlebets.length == 4) {
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Multi";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "MULT-4L";
                multibet.Combos = 1;
                multibet.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 6;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 4, 6);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 4, 6);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 4;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 4, 4);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 4, 4);
                result.Betslip.Bets.push(multibetTrebles);
                //Yankee
                var multibetYankee = JSON.parse(JSON.stringify(Multibet));
                multibetYankee.Type = "Parlay";
                multibetYankee.SubType = "Yankee";
                multibetYankee.SubTypeCode = "YANKEE";
                multibetYankee.Combos = 11;
                multibetYankee.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'YANKEE', 4, 11);
                multibetYankee.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'YANKEE', 4, 11);
                result.Betslip.Bets.push(multibetYankee);
                //Lucky15
                var multibetLucky15 = JSON.parse(JSON.stringify(Multibet));
                multibetLucky15.Type = "Parlay";
                multibetLucky15.SubType = "Lucky 15";
                multibetLucky15.SubTypeCode = "LUCKY15";
                multibetLucky15.Combos = 15;
                multibetLucky15.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'LUCKY15', 4, 15);
                multibetLucky15.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'LUCKY15', 4, 15);
                result.Betslip.Bets.push(multibetLucky15);
            } else if (singlebets.length == 5) {
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Multi";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "MULT-5L";
                multibet5fold.Combos = 1;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet5fold.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet5fold);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 10;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 5, 10);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 5, 10);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 10;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 5, 10);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 5, 10);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 5;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 5, 5);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 5, 5);
                result.Betslip.Bets.push(multibet);
                //Super Yankee
                var multibetSuperYankee = JSON.parse(JSON.stringify(Multibet));
                multibetSuperYankee.Type = "Parlay";
                multibetSuperYankee.SubType = "Super Yankee";
                multibetSuperYankee.SubTypeCode = "SUPYANKEE";
                multibetSuperYankee.Combos = 26;
                multibetSuperYankee.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'SUPYANKEE', 5, 26);
                multibetSuperYankee.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'SUPYANKEE', 5, 26);
                result.Betslip.Bets.push(multibetSuperYankee);
                //Lucky 31
                var multibetLucky31 = JSON.parse(JSON.stringify(Multibet));
                multibetLucky31.Type = "Parlay";
                multibetLucky31.SubType = "Lucky 31";
                multibetLucky31.SubTypeCode = "LUCKY31";
                multibetLucky31.Combos = 31;
                multibetLucky31.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'LUCKY31', 5, 31);
                multibetLucky31.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'LUCKY31', 5, 31);
                result.Betslip.Bets.push(multibetLucky31);
            } else if (singlebets.length == 6) {
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Multi";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "MULT-6L";
                multibet6fold.Combos = 1;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet6fold.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet6fold);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 15;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 6, 15);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 6, 15);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 20;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 6, 20);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 6, 20);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 15;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 6, 15);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 6, 15);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 6;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 6, 6);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 6, 6);
                result.Betslip.Bets.push(multibet5fold);
                //HEINZ
                var multibetSuperYankee = JSON.parse(JSON.stringify(Multibet));
                multibetSuperYankee.Type = "Parlay";
                multibetSuperYankee.SubType = "Heinz";
                multibetSuperYankee.SubTypeCode = "HEINZ";
                multibetSuperYankee.Combos = 57;
                multibetSuperYankee.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'HEINZ', 6, 57);
                multibetSuperYankee.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'HEINZ', 6, 57);
                result.Betslip.Bets.push(multibetSuperYankee);
                //LUCKY63
                var multibetLucky63 = JSON.parse(JSON.stringify(Multibet));
                multibetLucky63.Type = "Parlay";
                multibetLucky63.SubType = "Lucky 63";
                multibetLucky63.SubTypeCode = "LUCKY63";
                multibetLucky63.Combos = 63;
                multibetLucky63.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'LUCKY63', 6, 63);
                multibetLucky63.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'LUCKY63', 6, 63);
                result.Betslip.Bets.push(multibetLucky63);
            } else if (singlebets.length == 7) {
                //7 leg multi
                var multibet7fold = JSON.parse(JSON.stringify(Multibet));
                multibet7fold.Type = "Multi";
                multibet7fold.SubType = "7 Fold";
                multibet7fold.SubTypeCode = "MULT-7L";
                multibet7fold.Combos = 1;
                multibet7fold.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet7fold.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet7fold.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet7fold);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 21;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 7, 21);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 7, 21);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 35;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 7, 35);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 7, 35);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 35;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 7, 35);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 7, 35);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 21;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 7, 21);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 7, 21);
                result.Betslip.Bets.push(multibet5fold);
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Parlay";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "BXMUL-6L";
                multibet6fold.Combos = 7;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-6L', 7, 7);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-6L', 7, 7);
                result.Betslip.Bets.push(multibet6fold);
                //SUPHEINZ
                var multibetSuperHeinz = JSON.parse(JSON.stringify(Multibet));
                multibetSuperHeinz.Type = "Parlay";
                multibetSuperHeinz.SubType = "Super Heinz";
                multibetSuperHeinz.SubTypeCode = "SUPHEINZ";
                multibetSuperHeinz.Combos = 120;
                multibetSuperHeinz.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'SUPHEINZ', 7, 120);
                multibetSuperHeinz.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'SUPHEINZ', 7, 120);
                result.Betslip.Bets.push(multibetSuperHeinz);
            } else if (singlebets.length == 8) {
                //8 leg multi
                var multibet8fold = JSON.parse(JSON.stringify(Multibet));
                multibet8fold.Type = "Multi";
                multibet8fold.SubType = "8 Fold";
                multibet8fold.SubTypeCode = "MULT-8L";
                multibet8fold.Combos = 1;
                multibet8fold.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet8fold.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet8fold.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet8fold);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 28;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 8, 28);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 8, 28);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 56;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 8, 56);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 8, 56);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 70;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 8, 70);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 8, 70);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 56;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 8, 56);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 8, 56);
                result.Betslip.Bets.push(multibet5fold);
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Parlay";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "BXMUL-6L";
                multibet6fold.Combos = 28;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-6L', 8, 28);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-6L', 8, 28);
                result.Betslip.Bets.push(multibet6fold);
                //7 leg multi
                var multibet7fold = JSON.parse(JSON.stringify(Multibet));
                multibet7fold.Type = "Parlay";
                multibet7fold.SubType = "7 Fold";
                multibet7fold.SubTypeCode = "BXMUL-7L";
                multibet7fold.Combos = 8;
                multibet7fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-7L', 8, 8);
                multibet7fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-7L', 8, 8);
                result.Betslip.Bets.push(multibet7fold);
                //GOLIATH
                var multibetGoliath = JSON.parse(JSON.stringify(Multibet));
                multibetGoliath.Type = "Parlay";
                multibetGoliath.SubType = "Goliath";
                multibetGoliath.SubTypeCode = "GOLIATH";
                multibetGoliath.Combos = 247;
                multibetGoliath.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'GOLIATH', 8, 247);
                multibetGoliath.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'GOLIATH', 8, 247);
                result.Betslip.Bets.push(multibetGoliath);
            } else if (singlebets.length == 9) {
                //9 leg multi
                var multibet9fold = JSON.parse(JSON.stringify(Multibet));
                multibet9fold.Type = "Multi";
                multibet9fold.SubType = "9 Fold";
                multibet9fold.SubTypeCode = "MULT-9L";
                multibet9fold.Combos = 1;
                multibet9fold.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet9fold.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet9fold.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet9fold);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 36;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 9, 36);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 9, 36);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 84;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 9, 84);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 9, 84);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 126;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 9, 126);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 9, 126);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 126;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 9, 126);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 9, 126);
                result.Betslip.Bets.push(multibet5fold);
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Parlay";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "BXMUL-6L";
                multibet6fold.Combos = 84;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-6L', 9, 84);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-6L', 9, 84);
                result.Betslip.Bets.push(multibet6fold);
                //7 leg multi
                var multibet7fold = JSON.parse(JSON.stringify(Multibet));
                multibet7fold.Type = "Parlay";
                multibet7fold.SubType = "7 Fold";
                multibet7fold.SubTypeCode = "BXMUL-7L";
                multibet7fold.Combos = 36;
                multibet7fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-7L', 9, 36);
                multibet7fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-7L', 9, 36);
                result.Betslip.Bets.push(multibet7fold);
                //8 leg multi
                var multibet8fold = JSON.parse(JSON.stringify(Multibet));
                multibet8fold.Type = "Parlay";
                multibet8fold.SubType = "8 Fold";
                multibet8fold.SubTypeCode = "BXMUL-8L";
                multibet8fold.Combos = 9;
                multibet8fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-8L', 9, 9);
                multibet8fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-8L', 9, 9);
                result.Betslip.Bets.push(multibet8fold);
            } else if (singlebets.length == 10) {
                //10 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Multi";
                multibet.SubType = "10 Fold";
                multibet.SubTypeCode = "MULT-10L";
                multibet.Combos = 1;
                multibet.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 45;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 10, 45);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 10, 45);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 120;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 10, 120);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 10, 120);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 210;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 10, 210);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 10, 210);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 252;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 10, 252);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 10, 252);
                result.Betslip.Bets.push(multibet5fold);
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Parlay";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "BXMUL-6L";
                multibet6fold.Combos = 210;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-6L', 10, 210);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-6L', 10, 210);
                result.Betslip.Bets.push(multibet6fold);
                //7 leg multi
                var multibet7fold = JSON.parse(JSON.stringify(Multibet));
                multibet7fold.Type = "Parlay";
                multibet7fold.SubType = "7 Fold";
                multibet7fold.SubTypeCode = "BXMUL-7L";
                multibet7fold.Combos = 120;
                multibet7fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-7L', 10, 120);
                multibet7fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-7L', 10, 120);
                result.Betslip.Bets.push(multibet7fold);
                //8 leg multi
                var multibet8fold = JSON.parse(JSON.stringify(Multibet));
                multibet8fold.Type = "Parlay";
                multibet8fold.SubType = "8 Fold";
                multibet8fold.SubTypeCode = "BXMUL-8L";
                multibet8fold.Combos = 45;
                multibet8fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-8L', 10, 45);
                multibet8fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-8L', 10, 45);
                result.Betslip.Bets.push(multibet8fold);
                //9 leg multi
                var multibet9fold = JSON.parse(JSON.stringify(Multibet));
                multibet9fold.Type = "Parlay";
                multibet9fold.SubType = "9 Fold";
                multibet9fold.SubTypeCode = "BXMUL-9L";
                multibet9fold.Combos = 10;
                multibet9fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-9L', 10, 10);
                multibet9fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-9L', 10, 10);
                result.Betslip.Bets.push(multibet9fold);
            } else if (singlebets.length == 11) {
                //11 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Multi";
                multibet.SubType = "11 Fold";
                multibet.SubTypeCode = "MULT-11L";
                multibet.Combos = 1;
                multibet.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 55;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 11, 55);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 11, 55);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 165;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 11, 165);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 11, 165);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 330;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 11, 330);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 11, 330);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 462;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 11, 462);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 11, 462);
                result.Betslip.Bets.push(multibet5fold);
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Parlay";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "BXMUL-6L";
                multibet6fold.Combos = 462;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-6L', 11, 462);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-6L', 11, 462);
                result.Betslip.Bets.push(multibet6fold);
                //7 leg multi
                var multibet7fold = JSON.parse(JSON.stringify(Multibet));
                multibet7fold.Type = "Parlay";
                multibet7fold.SubType = "7 Fold";
                multibet7fold.SubTypeCode = "BXMUL-7L";
                multibet7fold.Combos = 330;
                multibet7fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-7L', 11, 330);
                multibet7fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-7L', 11, 330);
                result.Betslip.Bets.push(multibet7fold);
                //8 leg multi
                var multibet8fold = JSON.parse(JSON.stringify(Multibet));
                multibet8fold.Type = "Parlay";
                multibet8fold.SubType = "8 Fold";
                multibet8fold.SubTypeCode = "BXMUL-8L";
                multibet8fold.Combos = 165;
                multibet8fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-8L', 11, 165);
                multibet8fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-8L', 11, 165);
                result.Betslip.Bets.push(multibet8fold);
                //9 leg multi
                var multibet9fold = JSON.parse(JSON.stringify(Multibet));
                multibet9fold.Type = "Parlay";
                multibet9fold.SubType = "9 Fold";
                multibet9fold.SubTypeCode = "BXMUL-9L";
                multibet9fold.Combos = 55;
                multibet9fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-9L', 11, 55);
                multibet9fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-9L', 11, 55);
                result.Betslip.Bets.push(multibet9fold);
                //10 leg multi
                var multibet10fold = JSON.parse(JSON.stringify(Multibet));
                multibet10fold.Type = "Parlay";
                multibet10fold.SubType = "10 Fold";
                multibet10fold.SubTypeCode = "BXMUL-10L";
                multibet10fold.Combos = 11;
                multibet10fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-10L', 11, 11);
                multibet10fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-10L', 11, 11);
                result.Betslip.Bets.push(multibet10fold);
            } else if (singlebets.length == 12) {
                //12 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Multi";
                multibet.SubType = "12 Fold";
                multibet.SubTypeCode = "MULT-12L";
                multibet.Combos = 1;
                multibet.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 66;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 12, 66);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 12, 66);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 220;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 12, 220);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 12, 220);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 495;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 12, 495);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 12, 495);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 792;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 12, 792);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 12, 792);
                result.Betslip.Bets.push(multibet5fold);
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Parlay";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "BXMUL-6L";
                multibet6fold.Combos = 924;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-6L', 12, 924);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-6L', 12, 924);
                result.Betslip.Bets.push(multibet6fold);
                //7 leg multi
                var multibet7fold = JSON.parse(JSON.stringify(Multibet));
                multibet7fold.Type = "Parlay";
                multibet7fold.SubType = "7 Fold";
                multibet7fold.SubTypeCode = "BXMUL-7L";
                multibet7fold.Combos = 792;
                multibet7fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-7L', 12, 792);
                multibet7fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-7L', 12, 792);
                result.Betslip.Bets.push(multibet7fold);
                //8 leg multi
                var multibet8fold = JSON.parse(JSON.stringify(Multibet));
                multibet8fold.Type = "Parlay";
                multibet8fold.SubType = "8 Fold";
                multibet8fold.SubTypeCode = "BXMUL-8L";
                multibet8fold.Combos = 495;
                multibet8fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-8L', 12, 495);
                result.Betslip.Bets.push(multibet8fold);
                //9 leg multi
                var multibet9fold = JSON.parse(JSON.stringify(Multibet));
                multibet9fold.Type = "Parlay";
                multibet9fold.SubType = "9 Fold";
                multibet9fold.SubTypeCode = "BXMUL-9L";
                multibet9fold.Combos = 220;
                multibet9fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-9L', 12, 220);
                multibet9fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-9L', 12, 220);
                result.Betslip.Bets.push(multibet9fold);
                //10 leg multi
                var multibet10fold = JSON.parse(JSON.stringify(Multibet));
                multibet10fold.Type = "Parlay";
                multibet10fold.SubType = "10 Fold";
                multibet10fold.SubTypeCode = "BXMUL-10L";
                multibet10fold.Combos = 66;
                multibet10fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-10L', 12, 66);
                multibet10fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-10L', 12, 66);
                result.Betslip.Bets.push(multibet10fold);
                //11 leg multi
                var multibet11fold = JSON.parse(JSON.stringify(Multibet));
                multibet11fold.Type = "Parlay";
                multibet11fold.SubType = "11 Fold";
                multibet11fold.SubTypeCode = "BXMUL-11L";
                multibet11fold.Combos = 12;
                multibet11fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-11L', 12, 12);
                multibet11fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-11L', 12, 12);
                result.Betslip.Bets.push(multibet11fold);
            } else if (singlebets.length == 13) {
                //13 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Multi";
                multibet.SubType = "13 Fold";
                multibet.SubTypeCode = "MULT-13L";
                multibet.Combos = 1;
                multibet.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 78;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 13, 78);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 13, 78);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 286;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 13, 286);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 13, 286);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 715;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 13, 715);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 13, 715);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 1287;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 13, 1287);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 13, 1287);
                result.Betslip.Bets.push(multibet5fold);
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Parlay";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "BXMUL-6L";
                multibet6fold.Combos = 1716;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-6L', 13, 1716);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-6L', 13, 1716);
                result.Betslip.Bets.push(multibet6fold);
                //7 leg multi
                var multibet7fold = JSON.parse(JSON.stringify(Multibet));
                multibet7fold.Type = "Parlay";
                multibet7fold.SubType = "7 Fold";
                multibet7fold.SubTypeCode = "BXMUL-7L";
                multibet7fold.Combos = 1716;
                multibet7fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-7L', 13, 1716);
                multibet7fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-7L', 13, 1716);
                result.Betslip.Bets.push(multibet7fold);
                //8 leg multi
                var multibet8fold = JSON.parse(JSON.stringify(Multibet));
                multibet8fold.Type = "Parlay";
                multibet8fold.SubType = "8 Fold";
                multibet8fold.SubTypeCode = "BXMUL-8L";
                multibet8fold.Combos = 1287;
                multibet8fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-8L', 13, 1287);
                multibet8fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-8L', 13, 1287);
                result.Betslip.Bets.push(multibet8fold);
                //9 leg multi
                var multibet9fold = JSON.parse(JSON.stringify(Multibet));
                multibet9fold.Type = "Parlay";
                multibet9fold.SubType = "9 Fold";
                multibet9fold.SubTypeCode = "BXMUL-9L";
                multibet9fold.Combos = 715;
                multibet9fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-9L', 13, 715);
                multibet9fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-9L', 13, 715);
                result.Betslip.Bets.push(multibet9fold);
                //10 leg multi
                var multibet10fold = JSON.parse(JSON.stringify(Multibet));
                multibet10fold.Type = "Parlay";
                multibet10fold.SubType = "10 Fold";
                multibet10fold.SubTypeCode = "BXMUL-10L";
                multibet10fold.Combos = 286;
                multibet10fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-10L', 13, 286);
                multibet10fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-10L', 13, 286);
                result.Betslip.Bets.push(multibet10fold);
                //11 leg multi
                var multibet11fold = JSON.parse(JSON.stringify(Multibet));
                multibet11fold.Type = "Parlay";
                multibet11fold.SubType = "11 Fold";
                multibet11fold.SubTypeCode = "BXMUL-11L";
                multibet11fold.Combos = 78;
                multibet11fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-11L', 13, 78);
                multibet11fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-11L', 13, 78);
                result.Betslip.Bets.push(multibet11fold);
                //12 leg multi
                var multibet12fold = JSON.parse(JSON.stringify(Multibet));
                multibet12fold.Type = "Parlay";
                multibet12fold.SubType = "12 Fold";
                multibet12fold.SubTypeCode = "BXMUL-12L";
                multibet12fold.Combos = 13;
                multibet12fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-12L', 13, 13);
                multibet12fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-12L', 13, 13);
                result.Betslip.Bets.push(multibet12fold);
            } else if (singlebets.length == 14) {
                //14 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Multi";
                multibet.SubType = "14 Fold";
                multibet.SubTypeCode = "MULT-14L";
                multibet.Combos = 1;
                multibet.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 91;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 14, 91);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 14, 91);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 364;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 14, 364);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 14, 364);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 1001;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 14, 1001);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 14, 1001);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 2002;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 14, 2002);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 14, 2002);
                result.Betslip.Bets.push(multibet5fold);
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Parlay";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "BXMUL-6L";
                multibet6fold.Combos = 3003;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-6L', 14, 3003);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-6L', 14, 3003);
                result.Betslip.Bets.push(multibet6fold);
                //7 leg multi
                var multibet7fold = JSON.parse(JSON.stringify(Multibet));
                multibet7fold.Type = "Parlay";
                multibet7fold.SubType = "7 Fold";
                multibet7fold.SubTypeCode = "BXMUL-7L";
                multibet7fold.Combos = 3432;
                multibet7fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-7L', 14, 3432);
                multibet7fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-7L', 14, 3432);
                result.Betslip.Bets.push(multibet7fold);
                //8 leg multi
                var multibet8fold = JSON.parse(JSON.stringify(Multibet));
                multibet8fold.Type = "Parlay";
                multibet8fold.SubType = "8 Fold";
                multibet8fold.SubTypeCode = "BXMUL-8L";
                multibet8fold.Combos = 3003;
                multibet8fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-8L', 14, 3003);
                multibet8fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-8L', 14, 3003);
                result.Betslip.Bets.push(multibet8fold);
                //9 leg multi
                var multibet9fold = JSON.parse(JSON.stringify(Multibet));
                multibet9fold.Type = "Parlay";
                multibet9fold.SubType = "9 Fold";
                multibet9fold.SubTypeCode = "BXMUL-9L";
                multibet9fold.Combos = 2002;
                multibet9fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-9L', 14, 2002);
                multibet9fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-9L', 14, 2002);
                result.Betslip.Bets.push(multibet9fold);
                //10 leg multi
                var multibet10fold = JSON.parse(JSON.stringify(Multibet));
                multibet10fold.Type = "Parlay";
                multibet10fold.SubType = "10 Fold";
                multibet10fold.SubTypeCode = "BXMUL-10L";
                multibet10fold.Combos = 1001;
                multibet10fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-10L', 14, 1001);
                multibet10fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-10L', 14, 1001);
                result.Betslip.Bets.push(multibet10fold);
                //11 leg multi
                var multibet11fold = JSON.parse(JSON.stringify(Multibet));
                multibet11fold.Type = "Parlay";
                multibet11fold.SubType = "11 Fold";
                multibet11fold.SubTypeCode = "BXMUL-11L";
                multibet11fold.Combos = 364;
                multibet11fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-11L', 14, 364);
                multibet11fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-11L', 14, 364);
                result.Betslip.Bets.push(multibet11fold);
                //12 leg multi
                var multibet12fold = JSON.parse(JSON.stringify(Multibet));
                multibet12fold.Type = "Parlay";
                multibet12fold.SubType = "12 Fold";
                multibet12fold.SubTypeCode = "BXMUL-12L";
                multibet12fold.Combos = 91;
                multibet12fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-12L', 14, 91);
                multibet12fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-12L', 14, 91);
                result.Betslip.Bets.push(multibet12fold);
                //13 leg multi
                var multibet13fold = JSON.parse(JSON.stringify(Multibet));
                multibet13fold.Type = "Parlay";
                multibet13fold.SubType = "13 Fold";
                multibet13fold.SubTypeCode = "BXMUL-13L";
                multibet13fold.Combos = 14;
                multibet13fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-13L', 14, 14);
                multibet13fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-13L', 14, 14);
                result.Betslip.Bets.push(multibet13fold);
            } else if (singlebets.length == 15) {
                //15 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Multi";
                multibet.SubType = "15 Fold";
                multibet.SubTypeCode = "MULT-15L";
                multibet.Combos = 1;
                multibet.PayoutPerUnit = BetslipRouter.prototype.straightMultiPayoutRule(singlebets, 1);
                multibet.TaxPerUnit = BetslipRouter.prototype.straightMultiTaxRule(singlebets, multibet.PayoutPerUnit, 1);
                result.Betslip.Bets.push(multibet);
                //Doubles (2-Leg Multis)
                var multibetDoubles = JSON.parse(JSON.stringify(Multibet));
                multibetDoubles.Type = "Parlay";
                multibetDoubles.SubType = "Double";
                multibetDoubles.SubTypeCode = "BXMUL-2L";
                multibetDoubles.Combos = 105;
                multibetDoubles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-2L', 15, 105);
                multibetDoubles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-2L', 15, 105);
                result.Betslip.Bets.push(multibetDoubles);
                //3 leg multi
                var multibetTrebles = JSON.parse(JSON.stringify(Multibet));
                multibetTrebles.Type = "Parlay";
                multibetTrebles.SubType = "Treble";
                multibetTrebles.SubTypeCode = "BXMUL-3L";
                multibetTrebles.Combos = 455;
                multibetTrebles.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-3L', 15, 455);
                multibetTrebles.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-3L', 15, 455);
                result.Betslip.Bets.push(multibetTrebles);
                //4 leg multi
                var multibet = JSON.parse(JSON.stringify(Multibet));
                multibet.Type = "Parlay";
                multibet.SubType = "4 Fold";
                multibet.SubTypeCode = "BXMUL-4L";
                multibet.Combos = 1365;
                multibet.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-4L', 15, 1365);
                multibet.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-4L', 15, 1365);
                result.Betslip.Bets.push(multibet);
                //5 leg multi
                var multibet5fold = JSON.parse(JSON.stringify(Multibet));
                multibet5fold.Type = "Parlay";
                multibet5fold.SubType = "5 Fold";
                multibet5fold.SubTypeCode = "BXMUL-5L";
                multibet5fold.Combos = 3003;
                multibet5fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-5L', 15, 3003);
                multibet5fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-5L', 15, 3003);
                result.Betslip.Bets.push(multibet5fold);
                //6 leg multi
                var multibet6fold = JSON.parse(JSON.stringify(Multibet));
                multibet6fold.Type = "Parlay";
                multibet6fold.SubType = "6 Fold";
                multibet6fold.SubTypeCode = "BXMUL-6L";
                multibet6fold.Combos = 5005;
                multibet6fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-6L', 15, 5005);
                multibet6fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-6L', 15, 5005);
                result.Betslip.Bets.push(multibet6fold);
                //7 leg multi
                var multibet7fold = JSON.parse(JSON.stringify(Multibet));
                multibet7fold.Type = "Parlay";
                multibet7fold.SubType = "7 Fold";
                multibet7fold.SubTypeCode = "BXMUL-7L";
                multibet7fold.Combos = 6435;
                multibet7fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-7L', 15, 6435);
                multibet7fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-7L', 15, 6435);
                result.Betslip.Bets.push(multibet7fold);
                //8 leg multi
                var multibet8fold = JSON.parse(JSON.stringify(Multibet));
                multibet8fold.Type = "Parlay";
                multibet8fold.SubType = "8 Fold";
                multibet8fold.SubTypeCode = "BXMUL-8L";
                multibet8fold.Combos = 6435;
                multibet8fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-8L', 15, 6435);
                multibet8fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-8L', 15, 6435);
                result.Betslip.Bets.push(multibet8fold);
                //9 leg multi
                var multibet9fold = JSON.parse(JSON.stringify(Multibet));
                multibet9fold.Type = "Parlay";
                multibet9fold.SubType = "9 Fold";
                multibet9fold.SubTypeCode = "BXMUL-9L";
                multibet9fold.Combos = 5005;
                multibet9fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-9L', 15, 5005);
                multibet9fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-9L', 15, 5005);
                result.Betslip.Bets.push(multibet9fold);
                //10 leg multi
                var multibet10fold = JSON.parse(JSON.stringify(Multibet));
                multibet10fold.Type = "Parlay";
                multibet10fold.SubType = "10 Fold";
                multibet10fold.SubTypeCode = "BXMUL-10L";
                multibet10fold.Combos = 3003;
                multibet10fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-10L', 15, 3003);
                multibet9fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-9L', 15, 5005);
                result.Betslip.Bets.push(multibet10fold);
                //11 leg multi
                var multibet11fold = JSON.parse(JSON.stringify(Multibet));
                multibet11fold.Type = "Parlay";
                multibet11fold.SubType = "11 Fold";
                multibet11fold.SubTypeCode = "BXMUL-11L";
                multibet11fold.Combos = 1365;
                multibet11fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-11L', 15, 1365);
                multibet11fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-11L', 15, 1365);
                result.Betslip.Bets.push(multibet11fold);
                //12 leg multi
                var multibet12fold = JSON.parse(JSON.stringify(Multibet));
                multibet12fold.Type = "Parlay";
                multibet12fold.SubType = "12 Fold";
                multibet12fold.SubTypeCode = "BXMUL-12L";
                multibet12fold.Combos = 455;
                multibet12fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-12L', 15, 455);
                multibet12fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-12L', 15, 455);
                result.Betslip.Bets.push(multibet12fold);
                //13 leg multi
                var multibet13fold = JSON.parse(JSON.stringify(Multibet));
                multibet13fold.Type = "Parlay";
                multibet13fold.SubType = "13 Fold";
                multibet13fold.SubTypeCode = "BXMUL-13L";
                multibet13fold.Combos = 105;
                multibet13fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-13L', 15, 105);
                multibet13fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-13L', 15, 105);
                result.Betslip.Bets.push(multibet13fold);
                //14 leg multi
                var multibet14fold = JSON.parse(JSON.stringify(Multibet));
                multibet14fold.Type = "Parlay";
                multibet14fold.SubType = "14 Fold";
                multibet14fold.SubTypeCode = "BXMUL-14L";
                multibet14fold.Combos = 15;
                multibet14fold.PayoutPerUnit = BetslipRouter.prototype.parlayPayoutRule(singlebets, 1, 'BXMUL-14L', 15, 15);
                multibet14fold.TaxPerUnit = BetslipRouter.prototype.parlayTaxRule(singlebets, 1, 'BXMUL-14L', 15, 15);
                result.Betslip.Bets.push(multibet14fold);
            }
        }
        for (var i = 0; i < result.Betslip.Bets.length; i++) {
            result.Betslip.Bets[i].BetId = i + 1;
        }
        //change to Troy's API structure
        var newresult = {
            "multiBets": [],
            "errors": [],
            "requestGuid": "",
            "id": ""
        };
        for (var i = 0; i < result.Betslip.Bets.length; i++) {
            if (result.Betslip.Bets[i].Type != "Single") {
                var bet = result.Betslip.Bets[i];
                bet.betReferenceGuid = Guid.newGuid();
                bet.betId = result.Betslip.Bets[i].BetId;
                bet.combos = result.Betslip.Bets[i].Combos;
                bet.payout = result.Betslip.Bets[i].Payout;
                bet.payoutPerUnit = result.Betslip.Bets[i].PayoutPerUnit;
                bet.taxPerUnit = result.Betslip.Bets[i].TaxPerUnit;
                bet.status = result.Betslip.Bets[i].Status;
                bet.subType = result.Betslip.Bets[i].SubType;
                bet.subTypeCode = result.Betslip.Bets[i].SubTypeCode;
                bet.type = result.Betslip.Bets[i].Type;
                bet.betHash = currentMultiRequestHash + "/#" + bet.subType;
                newresult.multiBets.push(bet);
            } else {
                //set errors              
                if (result.Betslip.Bets[i].Result != null && result.Betslip.Bets[i].Result.ErrorText != "") {
                    var error = {
                        "type": result.Betslip.Bets[i].Result.ErrorText,
                        "message": result.Betslip.Bets[i].Result.ErrorText,
                        "suggestbetGuid": result.Betslip.Bets[i].BetReferenceGuid
                    };
                    newresult.errors.push(error);
                };
            }
        }
        newresult.requestGuid = req.body.requestGuid;
        newresult.id = req.body.id;
        res.send(newresult);
    }

    getBetslipSubmit302(req, res, next) {
        res.writeHead(302, {'Location': 'http://127.0.0.1:3000/api/v1/sportsbook/clients/1-10/bets'});
        res.end();
    }

    getBetslipSubmit(req, res, next) {
        var singlebets = [];
        for (var i = 0; i < req.body.singleBets.length; i++) {
            if (req.body.singleBets[i].totalStake > 0) {
                singlebets.push(req.body.singleBets[i]);
            }
        }
        var multibets = req.body.multiBets;
        var result = JSON.parse(JSON.stringify(Betslip));
        //push single bets
        for (var i = 0; i < singlebets.length; i++) {
            result.Betslip.Bets.push(JSON.parse(JSON.stringify(Singlebet)));
        }
        //multi SubType and payout update
        var hasConflicts = false;
        for (var i = 0; i < singlebets.length; i++) {
            result.Betslip.Bets[i].Stake = singlebets[i].totalStake;
            result.Betslip.Bets[i].FixedWin = singlebets[i].leg.selection.prices[0].value;
            result.Betslip.Bets[i].OutcomeId = singlebets[i].leg.selection.outcomeId;
            result.Betslip.Bets[i].EventId = singlebets[i].leg.selection.eventId;
            result.Betslip.Bets[i].BetReferenceGuid = singlebets[i].betReferenceGuid;
            //find out multi conflict 
            var hasEvent = false;
            for (var j = 0; j < singlebets.length; j++) {
                if (singlebets[i].leg.selection.eventId == singlebets[j].leg.selection.eventId && i != j) {
                    hasConflicts = true;
                }
            }
        }
        result.Betslip.Status = "Finalised";
        //push single bets
        for (var i = 0; i < singlebets.length; i++) {
            if (singlebets[i] != undefined) {
                result.Betslip.Bets[i].ErrorText = "";
                result.Betslip.Bets[i].ErrorNumber = 0;
                result.Betslip.Bets[i].TotalBetCost = singlebets[i].Stake;
                result.Betslip.Bets[i].CalculatedTotalLines = 1;
                if (singlebets[i].leg.selection.prices[0].value != 1.80) {
                    result.Betslip.Bets[i].CurrentFixedPrice = 1.80;
                } else {
                    result.Betslip.Bets[i].CurrentFixedPrice = 500.00;
                }
                result.Betslip.Bets[i].CurrentFixedPricePlace = 0.0;
                result.Betslip.Bets[i].CurrentBalance = 6465.28;
                result.Betslip.Bets[i].CurrentPoints = 0.00;
                result.Betslip.Bets[i].InterceptId = 0;
                result.Betslip.Bets[i].MaxAllowedTotalStake = 0.0;
                result.Betslip.Bets[i].TicketNumber = "17W5EPF6";
            } else {
                result.Betslip.Bets[i].Stake = null;
            }
            result.Betslip.Bets[i].Status = "Finalised";
            result.Betslip.Bets[i].BetId = i + 1;
        }
        //push multi bets
        for (var i = 0; i < multibets.length; i++) {
            var multibet = {
                "Type": "",
                "SubType": "",
                "BetReferenceGuid": "176c2496-48ea-4505-af04-d7b775301dhf",
                "BetId": 503,
                "Stake": 1,
                "MaxAllowedTotalStake": 1,
                "CurrentFixedPrice": 0,
                "CurrentFixedPricePlace": 0,
                "CurrentPoints": 0,
                "CurrentBalance": 6465.28,
                "CalculatedTotalLines": 1,
                "TicketNumber": "17W5EPF6",
                "ErrorNumber": 3002,
                "ErrorText": "Invalid Bet Sub Type Code",
                "InterceptId": 0,
                "Status": "Finalised",
                "FixedWin": 2
            };
            if (multibets[i] != undefined) {
                multibet.Type = "Multi";
                multibet.SubType = multibets[i].betSubTypeCode;
                multibet.Stake = multibets[i].totalStake;
                multibet.BetReferenceGuid = multibets[i].betReferenceGuid;
            }
            multibet.BetId = i + singlebets.length + 1;
            result.Betslip.Bets.push(multibet);
        }
        var hasDelay = false;
        var has401 = false;
        var haspricechange = false;
        var priceChangeLegs = [];
        var hassuspended = false;
        var suspendedLegs = [];
        for (var i = 0; i < result.Betslip.Bets.length; i++) {
            if (result.Betslip.Bets[i].Stake <= 0 || result.Betslip.Bets[i].Stake == null) {
                result.Betslip.Bets[i].ErrorText = "";
                result.Betslip.Bets[i].ErrorNumber = 0;
                result.Betslip.Bets[i].TicketNumber = "";
            }
            //error messages  
            if (result.Betslip.Bets[i].Stake == 1009) {
                result.Betslip.Bets[i].ErrorText = "Insufficient funds";
                result.Betslip.Bets[i].ErrorNumber = 1009;
                result.Betslip.Bets[i].TicketNumber = "";
            } else if (result.Betslip.Bets[i].Stake == 1010) {
                result.Betslip.Bets[i].ErrorText = "Stake is greater than maximum allowed";
                result.Betslip.Bets[i].ErrorNumber = 1010;
                result.Betslip.Bets[i].TicketNumber = "";
            } else if (result.Betslip.Bets[i].Stake == 2103) {
                result.Betslip.Bets[i].ErrorText = "Price Update";
                result.Betslip.Bets[i].ErrorNumber = 2103;
                result.Betslip.Bets[i].TicketNumber = "";
                result.Betslip.Bets[i].FixedWin = result.Betslip.Bets[i].FixedWin + 0.10;
                //change multi to error text 
                haspricechange = true;
                if (result.Betslip.Bets[i].Type === 'Single') {
                    priceChangeLegs.push(result.Betslip.Bets[i].BetReferenceGuid);
                }
            } else if (result.Betslip.Bets[i].Stake == 2001) {
                result.Betslip.Bets[i].ErrorText = "Suspended";
                result.Betslip.Bets[i].ErrorNumber = 2001;
                result.Betslip.Bets[i].TicketNumber = "";
                //change multi to error text 
                hassuspended = true;
                if (result.Betslip.Bets[i].Type === 'Single') {
                    suspendedLegs.push(i + 1);
                }
            } else if (result.Betslip.Bets[i].Stake == 2014) {
                result.Betslip.Bets[i].ErrorText = "Bet Type is currently not available for betting";
                result.Betslip.Bets[i].ErrorNumber = 2014;
                result.Betslip.Bets[i].TicketNumber = "";
            } else if (result.Betslip.Bets[i].Stake == 1030) {
                result.Betslip.Bets[i].ErrorText = "Responsible gambling limit reached";
                result.Betslip.Bets[i].ErrorNumber = 1030;
                result.Betslip.Bets[i].TicketNumber = "";
            } else if (result.Betslip.Bets[i].Stake == 4000) {               
                hasDelay = true;
            } else if (result.Betslip.Bets[i].Stake == 4010) {
                has401 = true;              
            }else if (result.Betslip.Bets[i].Stake == 3020) {
                res.writeHead(302, { 'Location': 'http://127.0.0.1:3000/api/v1/sportsbook/clients/1-10/bets/302'});
                res.end();
            } else if (result.Betslip.Bets[i].Stake == 8000) {
                result.Betslip.Bets[i].ErrorText = "Bet Intercepted";
                result.Betslip.Bets[i].ErrorNumber = 8000;
                result.Betslip.Bets[i].TicketNumber = "";
                result.Betslip.Bets[i].InterceptId =
                    (result.Betslip.Bets[i].Type === "Single" ? 1000 + 
                        parseInt(result.Betslip.Bets[i].EventId) +  parseInt(result.Betslip.Bets[i].OutcomeId):
                        multibets[i].Legs.map(function (obj) {
                            return parseInt(obj.selection.eventId);
                        }).reduce(function sum(a, b) {
                            return a + b;
                        })
                        + multibets[i].Legs.map(function (obj) {
                            return parseInt(obj.selection.outcomeId);
                        }).reduce(function sum(a, b) {
                            return a + b;
                        })                    
                    );
                result.Betslip.Bets[i].ReasonId = -4;
                if (parseInt(result.Betslip.Bets[i].EventId) === 89403) {
                    result.Betslip.Bets[i].Delay = 5000;
                } else if (parseInt(result.Betslip.Bets[i].EventId) === 89165) {
                    result.Betslip.Bets[i].Delay = 8000;
                } else if (parseInt(result.Betslip.Bets[i].EventId) === 89153) {
                    result.Betslip.Bets[i].Delay = 12000;
                } else {
                    result.Betslip.Bets[i].Delay = 5000;
                }
            }
        }
        if (haspricechange) {
            //set price udpate for multi
            for (var j = 0; j < result.Betslip.Bets.length; j++) {
                if (result.Betslip.Bets[j].Type != "Single") {
                    result.Betslip.Bets[j].ErrorText = "Price Update";
                    result.Betslip.Bets[j].ErrorNumber = 2103;
                    result.Betslip.Bets[j].TicketNumber = "";
                }
            }
            //check if single dont have price change set 1st price change 
            var SinglePriceChange = result.Betslip.Bets.filter(b => b.Type === "Single" && b.ErrorNumber == 2103);
            var hasSinglePriceChange = SinglePriceChange.length > 0;
            if (!hasSinglePriceChange) {
                //change first bet to price update
                result.Betslip.Bets[0].ErrorText = "Price Update";
                result.Betslip.Bets[0].ErrorNumber = 2103;
                result.Betslip.Bets[0].TicketNumber = "";
                priceChangeLegs.push(result.Betslip.Bets[0].BetReferenceGuid);
            }
        }
        if (hassuspended) {
            //set Suspended for multi
            for (var j = 0; j < result.Betslip.Bets.length; j++) {
                if (result.Betslip.Bets[j].Type != "Single") {
                    result.Betslip.Bets[j].ErrorText = "Suspended";
                    result.Betslip.Bets[j].ErrorNumber = 2001;
                    result.Betslip.Bets[j].TicketNumber = "";
                }
            }
            //check if single dont have price change set 1st price change 
            var SingleSuspended = result.Betslip.Bets.filter(b => b.Type === "Single" && b.ErrorNumber == 2001);
            var hasSingleSuspended = SingleSuspended.length > 0;
            if (!hasSingleSuspended) {
                //change first bet to price update
                result.Betslip.Bets[0].ErrorText = "Suspended";
                result.Betslip.Bets[0].ErrorNumber = 2001;
                result.Betslip.Bets[0].TicketNumber = "";
                suspendedLegs.push(1);
            }
        }
        //change to Troy's API structure
        var newresult = {
            "bets": [],
            "errors": [],
            "requestGuid": "",
        };
        for (var i = 0; i < result.Betslip.Bets.length; i++) {
            if (result.Betslip.Bets[i].TicketNumber != "") {
                var bet = {
                    "betReferenceGuid": "ded21c22-85c2-4ff7-b94d-b98cc8d3a97e",
                    "betId": 485,
                    "totalBetCost": 1,
                    "maxAllowedTotalStake": 0,
                    "currentFixedPrice": 1.8,
                    "currentFixedPricePlace": 0,
                    "currentPoints": 0,
                    "currentBalance": 6465.28,
                    "calculatedTotalLines": 1,
                    "ticketNumber": "17SEDL87",
                    "interceptId": 0,
                    "delay": 0,
                    "reasonId": 0
                };
                bet.betReferenceGuid = result.Betslip.Bets[i].BetReferenceGuid;
                bet.betId = result.Betslip.Bets[i].BetId;
                bet.totalBetCost = result.Betslip.Bets[i].Stake;
                bet.currentFixedPrice = result.Betslip.Bets[i].FixedWin;
                bet.ticketNumber = result.Betslip.Bets[i].TicketNumber;
                bet.interceptId = result.Betslip.Bets[i].InterceptId;
                bet.delay = result.Betslip.Bets[i].Delay;
                bet.reasonId = result.Betslip.Bets[i].ReasonId;
                newresult.bets.push(bet);
            } else {
                if (result.Betslip.Bets[i].ErrorText == "Price Update" && result.Betslip.Bets[i].Type != "Single") {
                    //no price update for single bets add to first leg                     
                    if (priceChangeLegs.length == 0) {
                        priceChangeLegs.push(multibets[0].Legs[0].betReferenceGuid);
                    }
                    for (var p = 0; p < priceChangeLegs.length; p++) {
                        var error = {
                            "betReferenceGuid": "5036dbf6-e368-4dd0-9f75-98ff08cbebb1",
                            "legId": 0,
                            "errorNo": 2002,
                            "errorText": "Betting closed",
                            "totalBetCost": 0,
                            "maxAllowedTotalStake": 1,
                            "currentFixedPrice": 0,
                            "currentFixedPricePlace": 0,
                            "currentPoints": 0,
                            "calculatedTotalLines": 0,
                            "interceptId": 0,
                            "delay": 0,
                            "reasonId": 0
                        };
                        error.betReferenceGuid = result.Betslip.Bets[i].BetReferenceGuid;
                        error.errorNo = result.Betslip.Bets[i].ErrorNumber;
                        error.currentFixedPrice = result.Betslip.Bets[i].CurrentFixedPrice;
                        //find legId by Guid 
                        var legId = 0;
                        for (var g = 0; g < result.Betslip.Bets.length; g++) {
                            if (result.Betslip.Bets[g].BetReferenceGuid === priceChangeLegs[p]) {
                                legId = g + 1;
                            }
                        }
                        error.legId = legId;
                        error.currentFixedPrice = 1.80;
                        error.errorText = result.Betslip.Bets[i].ErrorText;
                        error.interceptId = result.Betslip.Bets[i].InterceptId;
                        error.delay = result.Betslip.Bets[i].Delay;
                        error.reasonId = result.Betslip.Bets[i].ReasonId;
                        error.currentFixedPricePlace = result.Betslip.Bets[i].CurrentFixedPricePlace;
                        newresult.errors.push(error);
                    }
                } else if (result.Betslip.Bets[i].ErrorText == "Suspended" && result.Betslip.Bets[i].Type != "Single") {
                    if (suspendedLegs.length == 0) {
                        suspendedLegs.push(1);
                    }
                    for (var p = 0; p < suspendedLegs.length; p++) {
                        var error = {
                            "betReferenceGuid": "5036dbf6-e368-4dd0-9f75-98ff08cbebb1",
                            "legId": 0,
                            "errorNo": 2002,
                            "errorText": "Betting closed",
                            "totalBetCost": 0,
                            "maxAllowedTotalStake": 1,
                            "currentFixedPrice": 0,
                            "currentFixedPricePlace": 0,
                            "currentPoints": 0,
                            "calculatedTotalLines": 0,
                            "interceptId": 0,
                            "delay": 0,
                            "reasonId": 0
                        };
                        error.betReferenceGuid = result.Betslip.Bets[i].BetReferenceGuid;
                        error.errorNo = result.Betslip.Bets[i].ErrorNumber;
                        error.currentFixedPrice = result.Betslip.Bets[i].CurrentFixedPrice;
                        error.legId = suspendedLegs[p];
                        error.errorText = result.Betslip.Bets[i].ErrorText;
                        error.interceptId = result.Betslip.Bets[i].InterceptId;
                        error.delay = result.Betslip.Bets[i].Delay;
                        error.reasonId = result.Betslip.Bets[i].ReasonId;
                        error.currentFixedPricePlace = result.Betslip.Bets[i].CurrentFixedPricePlace;
                        newresult.errors.push(error);
                    }
                } else {
                    var error = {
                        "betReferenceGuid": "5036dbf6-e368-4dd0-9f75-98ff08cbebb1",
                        "legId": 0,
                        "errorNo": 2002,
                        "errorText": "Betting closed",
                        "totalBetCost": 0,
                        "maxAllowedTotalStake": 1,
                        "currentFixedPrice": 0,
                        "currentFixedPricePlace": 0,
                        "currentPoints": 0,
                        "calculatedTotalLines": 0,
                        "interceptId": 0,
                        "delay": 0,
                        "reasonId": 0
                    };
                    error.betReferenceGuid = result.Betslip.Bets[i].BetReferenceGuid;
                    error.errorNo = result.Betslip.Bets[i].ErrorNumber;
                    error.currentFixedPrice = result.Betslip.Bets[i].CurrentFixedPrice;
                    error.legId = 0;
                    error.errorText = result.Betslip.Bets[i].ErrorText;
                    error.interceptId = result.Betslip.Bets[i].InterceptId;
                    error.delay = result.Betslip.Bets[i].Delay;
                    error.reasonId = result.Betslip.Bets[i].ReasonId;
                    error.currentFixedPricePlace = result.Betslip.Bets[i].CurrentFixedPricePlace;
                    newresult.errors.push(error);
                }
            }
        }
        newresult.requestGuid = req.body.requestGuid;
        if(has401){
            setTimeout((function() {
                res.status(401);
                res.send('string');
              }), 1000);
        }
        else if(hasDelay){
            setTimeout((function() {
              res.send(newresult);
            }), 5000);
        }else{
             res.send(newresult);
        }
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        //this.router.post('/progress', this.getBetslipProgress);
        //this.router.post('/confirm', this.getBetslipConfirm);
        this.router.post('/', this.getBetslipSubmit);
        this.router.get('/302', this.getBetslipSubmit302);
        
    }
}

exports.BetslipRouter = BetslipRouter;

// Create the HeroRouter, and export its configured Express.Router
const betslipRoutes = new BetslipRouter();
betslipRoutes.init();

exports.default = betslipRoutes.router;