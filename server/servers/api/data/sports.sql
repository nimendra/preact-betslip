select top 10 e.EventId, e.EventTypeID, et.EventTypeDesc,
EventName, AdvertisedStartTime, 1 TimeTillStartInSeconds, IsEventStarted,
IsMultiAllowed

from [event] e with (nolock)
inner join [LEventType] et ON e.EventTypeID = et.EventTypeID
where BettingCloseTime > GETUTCDATE()
--where e.EventTypeID =  

select top 100 E.eventId EventId, o.OutcomeID, max(o.OutcomeName) OutcomeName, max(f.FixedMarketID) FixedMarketID,
max(f.Price) Price
from [Outcome] o with (nolock) 
inner join [event] e with (nolock) on o.EventID = e.EventID
inner join [FixedMarket] f with (nolock) on e.EventID = f.EventID
inner join [LEventType] et ON e.EventTypeID = et.EventTypeID
where BettingCloseTime > GETUTCDATE()
group by e.EventID, o.OutcomeID