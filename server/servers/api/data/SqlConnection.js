"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

var sql = require("mssql");

class Sql {

    constructor() {}

    getBetInfo(sport, outcome) {
        var EventId = sport.EventId;
        var EventName = sport.EventName;
        var EventClassId = 0;
        var EventClass = "";
        var OutcomeId = outcome.OutcomeId;
        var OutcomeName = outcome.OutcomeName;
        var Price = outcome.Price;
        var MarketTypeCode = outcome.MarketTypeCode;
        var BetDetailTypeCode = outcome.MarketTypeCode;
        var Points = outcome.Points;
        var GroupingId = 0;
        var MarketDesc = sport.EventName;
        var FixedMarketId = outcome.FixedMarketId;
        var MasterEventClassId = 0;
        var CategoryClassId = 0;
        var IsCashoutAllowed = true;
        var GroupByHeader = false;
        var GroupByCode = false;
        var IsOpenForBetting = true;
        var IsEventStarted = false;
        var MasterEventId = 0;
        var EventTypeId = sport.EventTypeId;
        var MasterCategoryId = 0;
        var CategoryId = 0;
        var betInfo = EventId + "|" + EventName + "|" + EventClassId + "|" + OutcomeId + "|" + OutcomeName + "|" + Price + "|" +
            EventClass + "|" + MarketTypeCode + "|" + BetDetailTypeCode + "|" + Points + "|" + GroupingId +
            "|" + MarketDesc + "|" + FixedMarketId + "|" + MasterEventClassId + "|" + CategoryClassId + "|" +
            IsCashoutAllowed + "|" + GroupByHeader + "|" + GroupByCode + "|" + IsOpenForBetting + "|" +
            IsEventStarted + "|" + MasterEventId + "|" + EventTypeId + "|" + MasterCategoryId + "|" + CategoryId;
        return betInfo;
    }

    executeStatement(res) {
        // config for your database
        var config = {
            user: 'website',
            password: 'T8tTDqW2nDqZWJuxrpuz3Sak2RnE4G',
            server: '10.3.5.4',
            database: 'TBS'
        };
        // connect to your database
        sql.connect(config, function (err) {
            if (err)
                console.log(err);
            // create Request object
            var request = new sql.Request();
            // query to the database and get the records
            request.query(`declare @events table (eventId bigint, masterEventId bigint)
      
      insert into @events 
      select top 10 e.EventID, max(e.MasterEventID) MasterEventID
      from [Outcome] o with (nolock) 
      inner join [event] e with (nolock) on o.EventID = e.EventID
      inner join [MasterEvent] me with (nolock) on e.MasterEventID = me.MasterEventID
      inner join [FixedMarket] f with (nolock) on e.EventID = f.EventID
      inner join [LEventType] et with (nolock) ON e.EventTypeID = et.EventTypeID
      where BettingCloseTime > GETUTCDATE()
      and e.IsOpenForBetting = 1
      and e.ResultStatusID < 1
      and ISNULL(e.IsAbandoned,0)  = 0
      and e.IsVisibleOnAPI = 1
      and Price > 1
      group by e.EventId
      --where e.EventTypeID =  
      
      select e.EventID, e.EventTypeID, et.EventTypeDesc, (ME.MasterEventName + ' - ' + e.EventName + ' - '+ f.MarketTypeCode ) EventName, 
      e.AdvertisedStartTime, DATEDIFF(second, GETUTCDATE(), e.AdvertisedStartTime)TimeTillStartInSeconds, 
      e.IsEventStarted, e.IsMultiAllowed,
      o.OutcomeID, o.OutcomeName , FixedMarketID,
       cast(ROUND(Price, 2, 1) as decimal(18,2)) Price, Points, MarketTypeCode
      from [Outcome] o with (nolock) 
      inner join [event] e with (nolock) on o.EventID = e.EventID
      inner join [FixedMarket] f with (nolock) on e.EventID = f.EventID and o.OutcomeID = f.OutcomeID
      inner join [LEventType] et with (nolock) ON e.EventTypeID = et.EventTypeID
      inner join @events fe on e.EventID = fe.eventId
      inner join MasterEvent ME with (nolock) on fe.masterEventId = ME.MasterEventID
      where Price > 1
      and f.IsOpenForBetting = 1
      `, function (err, recordset) {
                if (err)
                    console.log(err);
                // send records as a response
                sql.close();
                var sportsArr = [];
                //map
                recordset.recordsets[0].forEach(rs => {
                    var sport = {
                        EventId: rs.EventID,
                        EventTypeId: rs.EventTypeID,
                        EventTypeDesc: rs.EventTypeDesc,
                        EventName: rs.EventName,
                        MarketName: rs.EventName,
                        AdvertisedStartTime: rs.AdvertisedStartTime,
                        TimeTillStartInSeconds: rs.TimeTillStartInSeconds,
                        IsEventStarted: rs.IsEventStarted,
                        IsMultiAllowed: rs.IsMultiAllowed,
                        Outcomes: []
                    };
                    if (!sportsArr.find(s => s.EventId === sport.EventId))
                        sportsArr.push(sport);
                });
                //Add outcomes
                sportsArr.forEach(s => {
                    var rs = recordset.recordsets[0].filter(rs => rs.EventID === s.EventId);
                    rs.forEach(o => {
                        var outcome = {
                            EventId: s.EventID,
                            OutcomeId: o.OutcomeID,
                            OutcomeName: o.OutcomeName,
                            MarketTypeCode: o.MarketTypeCode,
                            FixedMarketId: o.FixedMarketID,
                            Points: 0.00,
                            Price: o.Price
                        };
                        s.Outcomes.push(outcome);
                    });
                });
                sportsArr.map(function (sport) {
                    sport.Outcomes.map(function (outcome) {
                        outcome.BetInfo = new Sql().getBetInfo(sport, outcome);
                    });
                });
                res.send({
                    Sports: sportsArr
                });
            });
        });
    }
}

exports.default = Sql;