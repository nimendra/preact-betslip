"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const open = require("open");

var url = process.argv[2];
console.log("Opening browser on", url);
open(url);