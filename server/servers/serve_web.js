"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});

const detect = require("detect-port");
const WebServer_1 = require("./web/WebServer");

var webServerPort = parseInt(process.argv[2].trim() || "9876"),
    webServerRootPath = process.argv[3].trim(),
    webServer = new WebServer_1.default(webServerPort, webServerRootPath);

function startWebServer() {
    detect(webServerPort, (err, availablePort) => {
        if (err) {
            console.log(err);
            return;
        }
        if (webServerPort === availablePort) {
            webServer.start();
        } else {
            console.log("Web server already listening on port", webServerPort);
        }
    });
}

startWebServer();